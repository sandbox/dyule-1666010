/**
 * @file
 *  Handles saving and submitting submission forms.
 * 
 * @author 
 *  Daniel Yule <dyule@unbc.ca>
 * 
 * @copyright
 * 
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 * 
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in 
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

jQuery(document).ready(function($){
  var $save_button = $('#edit-save');
  var $forms = $('.edited_form,.approvable_form,.viewed_form,.created_form');
  $forms.find('input,select').delegate('change', function() {
    $('#jarrow_save_message_box').hide()
    $save_button.mousedown();
  });
  Drupal.behaviors.jarrowSubmissionSave = {
    attach: function (context) {
      $('#jarrow_save_message_box').hide();
      $save_button.mousedown();
    }
  }
  $('.submit_toggle').button().click(function(event) {
    var $dialog = $('<div />', {
      text: Drupal.t('Are you sure you want to submit this form?')
    })
    var $button = $(this);
    $('body').append($dialog);
    $dialog.dialog({
      title: Drupal.t('Submit this form?'),
      buttons: {
        'Submit': function (event) {
          var $submitID = $button.attr('id').replace('submit-toggle', 'submit'),
          $submitButton = $('#' + $submitID);
          
          $submitButton.click();
          $dialog.dialog('close');
        },
        'Cancel': function (event) {
          $dialog.dialog('close');
        }
      }
    });
    return false;
  });
  
  $('.approval_toggle').button().click(function(event) {
    var $dialog = $('<div />', {
      text: Drupal.t('Are you sure you want to approve this form?')
    })
    var $button = $(this);
    $('body').append($dialog);
    $dialog.dialog({
      title: Drupal.t('Approve this form?'),
      buttons: {
        'Approve': function (event) {
          var $submitID = $button.attr('id').replace('approval-toggle', 'approval-buttons-approve'),
          $submitButton = $('#' + $submitID);
          
          $submitButton.click();
          $dialog.dialog('close');
        },
        'Cancel': function (event) {
          $dialog.dialog('close');
        }
      }
    });
    return false;
  });
  
  $('.rejection_toggle').button().click(function(event) {
    var $dialog = $('<div />', {
      text: Drupal.t('Are you sure you want to reject this form?')
    })
    var $button = $(this);
    $('body').append($dialog);
    $dialog.dialog({
      title: Drupal.t('Reject this form?'),
      buttons: {
        'Reject': function (event) {
          var $submitID = $button.attr('id').replace('rejection-toggle', 'approval-buttons-reject'),
          $submitButton = $('#' + $submitID);
          
          $submitButton.click();
          $dialog.dialog('close');
        },
        'Cancel': function (event) {
          $dialog.dialog('close');
        }
      }
    });
    return false;
  });
})