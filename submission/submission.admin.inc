<?php

/**
 * @file
 *  Provide the administration screen for managing submissions.
 *
 * @author
 *  Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * Admin page callback file for the submission module.
 */

function jarrow_submission_admin($callback_arg = '') {

  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;

  switch ($op) {
    default:
      if (!empty($_POST['submissions']) && isset($_POST['operation'])) {
        switch ($_POST['operation']) {
          case 'delete':
            $build['jarrow_submission_delete_confirm'] = drupal_get_form('jarrow_submission_delete_confirm');
            break;
          case 'reset':
            $build['jarrow_submission_reset_confirm'] = drupal_get_form('jarrow_submission_reset_confirm');
            break;
          default:
            $build['jarrow_submission_admin_form'] = drupal_get_form('jarrow_submission_admin_form');
            break;
        }
      } else {
       // $build['jarrow_filter_form'] = drupal_get_form('jarrow_submission_filter_form');
        $build['jarrow_submission_admin_form'] = drupal_get_form('jarrow_submission_admin_form');
      }
  }
  return $build;
}

/**
 * Form builder; Submission administration page.
 *
 * @ingroup forms
 * @see user_admin_account_validate()
 * @see user_admin_account_submit()
 */
function jarrow_submission_admin_form() {

  $header = array(
    'submitter' => array('data' => t('Submitter'), 'field' => 'users.name'),
    'submission' => array('data' => t('Submission'), 'field' => 'proc.name'),
    'status' => array('data' => t('Status of Completion'), 'field' => 'sub.current_stage'),
    'operation' => array('data' => t('Operation')),
  );

  $query = db_select('etd_submission', 'sub');
  $query->innerJoin('etd_processes', 'proc', 'sub.process=proc.id');
  $query->innerJoin('users', 'users', 'sub.owner=users.uid');
  $query = $query
      ->fields('sub', array('id', 'owner', 'process', 'current_stage'))
      ->fields('proc', array('name'))
      ->fields('users', array('name'));

  //user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(sub.id)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->limit(50)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  // Create the update operations form
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $options = array();
  foreach (module_invoke_all('etd_submission_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'archive',
  );

  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $user_names = _jarrow_get_names_for_users();
  $options = array();
  foreach ($result as $submission) {
    $options[$submission->id] = array(
      'submitter' => array('data' => array('#type' => 'link', '#title' => $user_names[$submission->owner], '#href' => "user/$submission->owner")),
      'submission' => array('data' => array('#type' => 'link', '#title' => $submission->name, '#href' => "jarrow/submission/$submission->id/view")),
      'status' =>  jarrow_submission_admin_status($submission),
      'operation' => array('data' => array('#type' => 'link', '#title' => t('Audit'), '#href' => "jarrow/submission/$submission->id/audit")),
    );
  }

  $form['submissions'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No submissions available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Determine how far along a submission is to being completed
 */
function jarrow_submission_admin_status($submission) {

  $result = db_select('etd_submission', 'sub')
    ->fields('sub', array('completed'))
    ->condition('id', $submission->id)
    ->execute()
    ->fetch();
  if ($result->completed == 1) {
    return 'Completed';
  }

  $query = db_select('etd_process_stages', 'stage');
  $result = $query
      ->condition('stage.id', $submission->current_stage, '=')
      ->condition('stage.process', $submission->process, '=')
      ->fields('stage', array('name'))
      ->execute()
      ->fetch();
  return $result->name;
}



/**
 * Submit the submission administration update form.
 */
function jarrow_submission_admin_form_submit($form, &$form_state) {

  $operations = module_invoke_all('etd_submission_operations', $form, $form_state);
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked submissions.
  $submissions = array_filter($form_state['values']['submissions']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($submissions), $operation['callback arguments']);
    } else {
      $args = array($submissions);
    }
    call_user_func_array($function, $args);
  }
}

/**
 * Check that the user actually selected a submission to update.
 * If not, display an error message.
 */
function jarrow_submission_admin_form_validate($form, &$form_state) {
  $form_state['values']['submissions'] = array_filter($form_state['values']['submissions']);
  if (count($form_state['values']['submissions']) == 0) {
    form_set_error('', t('No submissions selected.'));
  }
}

/**
 * Confirm that the user would like to reset the selected submissions.
 */
function jarrow_submission_reset_confirm($form, &$form_state) {

  $form['confirm_text'] = array(
    '#type' => 'markup',
    '#markup' => '<p>'.t('Resetting these submissions means everything associated with these submissions will be deleted and they will be set to start the process over again.').
    '</p><p>'.t('Please confirm that you would like to reset the following submissions: ').'</p>',
  );

  $edit = $form_state['input'];

  $form['submissions'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);

  $submissions = jarrow_submission_load_multiple(array_keys(array_filter($edit['submissions'])));
  foreach ($submissions as $id=>$values) {

    $form['submissions'][$id] = array(
      '#type' => 'hidden',
      '#value' => $id,
      '#prefix' => '<li>',
      '#suffix' => $values['name'].' -- '.$values['submission'] . "</li>\n",
    );
  }

  $form['operation'] = array('#type' => 'hidden', '#value' => 'reset');

  return confirm_form($form,
                      t('Are you sure you want to reset these submissions?'),
                      JARROW_SUBMISSIONS_PATH, t('This action cannot be undone.'),
                      t('Reset'), t('Cancel'));
}

/**
 * Submit handler for mass submission reset form.
 *
 * @see user_multiple_cancel_confirm()
 * @see user_cancel_confirm_form_submit()
 */
function jarrow_submission_reset_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    $submissions = array();
    foreach ($form_state['values']['submissions'] as $id => $value) {
       $submissions[] = $id;
    }
    jarrow_submission_reset($submissions);
  }
  $form_state['redirect'] = JARROW_SUBMISSIONS_PATH;
}

/**
 * Reset the specified submission by deleting all associated files
 * and setting the submission back to the start of the process.
 *
 * @param $submission_ids
 */
function jarrow_submission_reset($submission_ids) {

  if ((user_access('administer etd')) && (count($submission_ids) > 0)) {
    $transaction = db_transaction();
    try{
      // Remove any uploaded files
      jarrow_submission_delete_uploaded_files($submission_ids);

      // Create the list of tables that we want to delete values from.
      $db_tables = array(
        'etd_submission_field' => 'submission',
        'etd_submission_digital_object_instances' => 'submission',
        'etd_role_associations' => 'submission',
        'etd_submission_stages' => 'submission',
        'etd_date_selector_entries' => 'submission',
        'etd_submission_form_approval' => 'submission',
      );
      foreach($db_tables as $db_table => $column) {
        db_delete($db_table)
          ->condition($column, $submission_ids, 'IN')
          ->execute();
      }

      foreach($submission_ids as $submission_id) {

        // Reset the submission back to the first stage
        $first_stage = jarrow_submission_get_first_stage($submission_id);
        db_update('etd_submission')
          ->fields(array('current_stage' => $first_stage))
          ->condition('id', $submission_id, '=')
          ->execute();

        // Reinsert the deleted submission stages with the updated time.
        db_insert('etd_submission_stages')
        ->fields(array(
          'submission' => $submission_id,
          'stage' => $first_stage,
          'creation_time' =>  time()))
        ->execute();
      }

      // Create the list of tables that we want to modify values.
      $db_tables = array(
        'etd_submission_form' => array('column' => 'submission', 'update_columns' => array('submitted' => 0, 'approved' => 0)),
      );
      foreach($db_tables as $db_table => $table_data) {
        db_update($db_table)
        ->fields($table_data['update_columns'])
        ->condition($table_data['column'], $submission_ids, 'IN')
        ->execute();
      }

      cache_clear_all('jarrow', 'cache', TRUE);
      drupal_set_message('Submission reset operation succeeded.');
    } catch (Exception $error) {
      $transaction->rollback();
      watchdog_exception('submission reset', $error);
      drupal_set_message('Submission reset operation failed.', 'error');
    }
  }
}
/**
 * Confirm that the user would like to delete the selected submissions.
 */
function jarrow_submission_delete_confirm($form, &$form_state) {

  $form['confirm_text'] = array(
    '#type' => 'markup',
    '#markup' => '<p>'.t('Please confirm that you would like to delete the following submissions: ').'</p>',
  );

  $edit = $form_state['input'];

  $form['submissions'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);

  $submissions = jarrow_submission_load_multiple(array_keys(array_filter($edit['submissions'])));
  foreach ($submissions as $id=>$values) {

    $form['submissions'][$id] = array(
      '#type' => 'hidden',
      '#value' => $id,
      '#prefix' => '<li>',
      '#suffix' => $values['name'].' -- '.$values['submission'] . "</li>\n",
    );
  }

  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
                      t('Are you sure you want to delete these submissions?'),
                      JARROW_SUBMISSIONS_PATH, t('This action cannot be undone.'),
                      t('Delete'), t('Cancel'));
}

/**
 * Submit handler for mass submission delete form.
 *
 * @see user_multiple_cancel_confirm()
 * @see user_cancel_confirm_form_submit()
 */
function jarrow_submission_delete_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    $submissions = array();
    foreach ($form_state['values']['submissions'] as $id => $value) {
       $submissions[] = $id;
    }
    jarrow_submission_delete($submissions);
  }
  $form_state['redirect'] = JARROW_SUBMISSIONS_PATH;
}

/**
 * Delete the selected submissions from the system.
 *
 * @param array $submission_ids
 */
function jarrow_submission_delete($submission_ids = array()) {

  if ((user_access('administer etd')) && (count($submission_ids) > 0)) {
    $db_tables = array(
      'etd_submission' => 'id',
      'etd_submission_form_approval' => 'submission',
      'etd_submission_form' => 'submission',
      'etd_submission_field' => 'submission',
      'etd_submission_digital_object_instances' => 'submission',
      'etd_date_selector_entries' => 'submission',
      'etd_role_associations' => 'submission');
    $rows_deleted = 0;
    $transaction = db_transaction();
    try{
      jarrow_submission_delete_uploaded_files($submission_ids);

      foreach($db_tables as $db_table => $column) {
        $rows_deleted += db_delete($db_table)
          ->condition($column, $submission_ids, 'IN')
          ->execute();
      }

      cache_clear_all('jarrow', 'cache', TRUE);
      drupal_set_message('Submissions deleted successfully.');
    } catch (Exception $error) {
      $transaction->rollback();
      watchdog_exception('submission delete', $error);
      drupal_set_message('Submission delete operation failed.', 'error');
    }
  }
}

/**
 * Delete any uploaded file for the given submissions
 *
 * @param $submission_ids
 */
function jarrow_submission_delete_uploaded_files($submission_ids) {

  $file_set = db_select('etd_submission', 'sub')
      ->fields('sub', array('file'))
      ->condition('id', $submission_ids, 'IN')
      ->isNotNull('file')
      ->execute()
      ->fetchCol();
   $files = file_load_multiple($file_set);
   foreach($files as $file) {
     /*$file_usage = file_usage_list($file);
     $keys = array_keys($file_usage['jarrow']['submission']);
     file_usage_delete($file, 'jarrow', 'submission', $keys[0], 0);*/
     file_delete($file, TRUE);
   }
   jarrow_submission_delete_digital_objects($submission_ids);
   //TODO need to add code to delete items from the repository
}

/**
 * Remove all references of the specified digital object from the database.
 *
 * @param int $submission_ids
 *
 * @return bool
 */
function jarrow_submission_delete_digital_objects($submission_ids) {

  $transaction = db_transaction();
  try{
    $file_set = db_select('etd_submission_digital_object_instances', 'doi')
        ->fields('doi', array('file_id'))
        ->condition('submission', $submission_ids, 'IN')
        ->isNotNUll('submission')
        ->execute()
        ->fetchCol();
    $files = file_load_multiple($file_set);
    foreach($files as $file) {
      file_delete($file, TRUE);
    }
    db_delete('etd_submission_digital_object_instances')
        ->condition('submission', $submission_ids, 'IN')
        ->execute();
    return TRUE;
  } catch (Exception $error) {
    $transaction->rollback();
    $filename = db_select('file_managed', 'fm')
        ->fields('fm', array('filename'))
        ->condition('fid', $digital_object_instance->file_id)
        ->execute()
        ->fetch();
    watchdog_exception('supplementary file deletion error. Could not remove file: '.$filename, $error);
    drupal_set_message('Failed to remove the file: '.$filename, 'error');
    return FALSE;
  }

}

function jarrow_submission_load_multiple($submission_ids = array()) {

  $query = db_select('etd_submission', 'sub');
  $query->innerJoin('etd_processes', 'proc', 'sub.process=proc.id');
  $query = $query
      ->fields('sub', array('id', 'owner'))
      ->fields('proc', array('name'))
      ->condition('sub.id', $submission_ids, 'IN');
  $result = $query->execute();

  $submissions = array();
  if ($result->rowCount() > 0) {
    $user_names = _jarrow_get_names_for_users();
    foreach($result as $submission) {
      $submissions[$submission->id] = array('name'=>$user_names[$submission->owner], 'submission'=>$submission->name);
    }
  }
  return $submissions;
}

/**
 * Return the first stage of the given submission
 */
function jarrow_submission_get_first_stage($submission_id) {

  $process_id = db_select('etd_submission')
          ->fields('etd_submission', array('process'))
          ->condition('id', $submission_id)
          ->execute()
          ->fetchField();

  return db_select('etd_process_stages')
          ->fields('etd_process_stages', array('id'))
          ->condition('process', $process_id)
          ->orderBy('weight')
          ->range(0, 1)
          ->execute()
          ->fetchField();
}
