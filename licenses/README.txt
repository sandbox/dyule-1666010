**Weller ETD Creative Commons License Templates**

This folder contains the templates used during installation for the terms field
of the Creative Commons licenses that come pre-installed with Weller ETD.  Modifying
these files after installation will have no effect.

The html code in these files is adapted from their Commons Deed license layer,
which can be found at http://creativecommons.org/licenses/