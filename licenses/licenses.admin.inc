<?php
/**
 * @file
 *  Handles the license selection and management component of Jarrow.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca> Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

function jarrow_license_admin($callback_arg = '') {

  $op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;

  switch ($op) {
    default:
      if (!empty($_POST['licenses']) && isset($_POST['operation'])) {
        switch ($_POST['operation']) {
          case 'delete':
            $build['jarrow_license_delete_confirm'] = drupal_get_form('jarrow_license_delete_confirm');
            break;
          default:
            $build['jarrow_license_admin_form'] = drupal_get_form('jarrow_license_admin_form');
            break;
        }
      } else {
        $build['jarrow_license_admin_form'] = drupal_get_form('jarrow_license_admin_form');
      }
  }
  return $build;
}

/**
 * Form builder; License administration page.
 *
 * @ingroup forms
 * @see user_admin_account_validate()
 * @see user_admin_account_submit()
 */
function jarrow_license_admin_form() {

  global $base_url;

  $header = array(
    'license' => array('data' => t('License'), 'field' => 'license.name'),
    'operation' => array('data' => t('Operations')),
    'operation2' => array(),
    'status' => array('data' => t('Status'), 'field' => 'license.active'),
  );

  $header['operation']['colspan'] = 2;
  $header['operation2']['style'] = 'display:none';

  $query = db_select('etd_licenses', 'license')
    ->fields('license', array('id', 'name', 'active'));


  //user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(license.id)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->limit(50)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  // Create the update operations form
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $options = array();
  foreach (module_invoke_all('etd_license_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'archive',
  );

  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $options = array();
  foreach ($result as $license) {
    $options[$license->id] = array(
      'license' => htmlspecialchars($license->name),
      'status' => ($license->active == 1) ? t('Enabled') : t('Disabled'),
      'operation' => array(
        'data' => array('#type' => 'link', '#title' => 'edit', '#href' => JARROW_LICENSES_PATH . "/$license->id/edit")
      ),
      'operation2' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => 'clone',
          '#href' => JARROW_LICENSES_PATH . '/' . $license->id . '/clone'
        )
      ),
    );
  }

  $form['licenses'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No licenses available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  $form['rescan_form'] = array(
    '#type' => 'submit',
    '#submit' => array('jarrow_license_import'),
    '#value' => t('Import from License Directory'),
  );

  $form['add_form'] = array(
    '#type' => 'submit',
    '#submit' => array('jarrow_license_add'),
    '#value' => t('Enter New License'),
  );
  return $form;
}

/**
 * Submit the submission administration update form.
 */
function jarrow_license_admin_form_submit($form, &$form_state) {

  $operations = module_invoke_all('etd_license_operations', $form, $form_state);
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked licenses.
  $licenses = array_filter($form_state['values']['licenses']);
  if (isset($operation['callback'])) {
    $function = $operation['callback'];
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($licenses), $operation['callback arguments']);
    } else {
      $args = array($licenses);
    }
    call_user_func_array($function, $args);
  }
}

/**
 * Implements hook_etd_license_operations().
 */
function jarrow_etd_license_operations($form = array(), $form_state = array()) {

  $operations = array(
    'enable' => array(
      'label' => t('Enable the selected licenses'),
      'callback' => 'jarrow_license_enable',
    ),
    'disable' => array(
      'label' => t('Disable the selected licenses'),
      'callback' => 'jarrow_license_disable',
    ),
    'delete' => array(
      'label' => t('Delete the selected licenses'),
    ),
  );

  return $operations;
}

/**
 * Enable the selected licenses.
 *
 * @param $licenses
 */
function jarrow_license_enable($licenses) {
  jarrow_license_update_enabled_setting($licenses, 1);
}

/**
 * Disable the selected licenses.
 *
 * @param $licenses
 */
function jarrow_license_disable($licenses) {
  jarrow_license_update_enabled_setting($licenses, 0);
}

/**
 * Update whether the specified licenses are enabled or not.
 *
 * @param $license_ids
 * @param $value
 */
function jarrow_license_update_enabled_setting($license_ids, $value) {

  if (db_update('etd_licenses')
      ->fields(array(
        'active' => $value,
      ))
      ->condition('id', $license_ids, 'IN')
      ->execute() === NULL
  ) {
    drupal_set_message(t('Could not update licenses'), 'error');
  } else {
    drupal_set_message(t('Licenses updated'));
  }
}

/**
 * Create a new license that the user can then fill out the details.
 *
 */
function jarrow_license_add() {

  $new_id = db_insert('etd_licenses')
    ->fields(array(
      'name' => t('New License'),
      'code' => '',
      'summary' => '',
      'terms' => '',
      'full_text_link' => '',
      'active' => 0,
    ))
    ->execute();
  if ($new_id === NULL) {
    drupal_set_message(t('Error creating license'), 'error');
    drupal_goto(JARROW_LICENSES_PATH);
  } else {
    drupal_set_message(t('License Created'));
    drupal_goto(JARROW_LICENSES_PATH . '/' . $new_id . '/edit');
  }
}

/**
 * Edit the license identified by license_id
 *
 * @param array $form
 * @param array $form_state
 * @param int $license_id
 *
 * @return array
 */
function jarrow_license_edit($form, &$form_state, $license_id) {

  $license = db_select('etd_licenses')
    ->fields('etd_licenses')
    ->condition('id', $license_id)
    ->execute()
    ->fetch();
  if ($license) {
    return array(
      'name' => array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#description' => t('The name of this license'),
        '#maxlength' => 256,
        '#default_value' => $license->name,
        '#required' => TRUE,
      ),
      'code' => array(
        '#type' => 'textfield',
        '#title' => t('Code'),
        '#description' => t('The code for this license.  This is typically a short unique string to identify this license'),
        '#maxlength' => 256,
        '#required' => TRUE,
        '#default_value' => $license->code,
      ),
      'summary' => array(
        '#type' => 'textarea',
        '#title' => t('Summary'),
        '#cols' => 60,
        '#rows' => 7,
        '#description' => t('A short description of the general form of this license'),
        '#maxlength' => 4096,
        '#default_value' => $license->summary,
      ),
      'terms' => array(
        '#type' => 'text_format',
        '#format' => $license->terms_format,
        '#title' => t('Terms'),
        '#description' => t('A longer, but non-legal description of the terms of this license'),
        '#maxlength' => 8192,
        '#cols' => 60,
        '#rows' => 20,
        '#default_value' => $license->terms,
      ),
      'full_text_link' => array(
        '#type' => 'textfield',
        '#title' => t('Link to Full Text'),
        '#description' => t('A link to the full legal terms of the license'),
        '#maxlength' => 512,
        '#default_value' => $license->full_text_link,
      ),
      'save' => array(
        '#type' => 'submit',
        '#validate' => array('jarrow_license_validate'),
        '#value' => t('Save Configuration'),
      ),
      'license_id' => array(
        '#type' => 'hidden',
        '#value' => $license_id,
      )
    );
  } else {
    drupal_set_message(t('Could not load license'), 'error');
    drupal_goto(JARROW_LICENSES_PATH);
  }
}

/**
 * Ensure the submitted license is valid.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_license_validate($form, $form_state) {

  $licenses = jarrow_license_load_licenses();
  // ensure the name and code of the license are unique
  $license_code = trim($form_state['values']['code']);
  $license_name = trim($form_state['values']['name']);
  if (strlen($license_code) == 0) {
    form_set_error('code', t("The license code cannot be blank."));
  }
  if (strlen($license_name) == 0) {
    form_set_error('code', t("The license name cannot be blank."));
  }
  foreach ($licenses as $license) {
    if ($license['code'] == $license_code) {
      form_set_error('code', t("A license code already exists with the name {$license_code}."));
    }
    if ($license['name'] == $license_name) {
      form_set_error('code', t("A license name already exists with the name {$license_name}."));
    }
  }
}

/**
 * Load the specified licenses form the database
 *
 * @return array
 */
function jarrow_license_load_licenses() {

  $result = db_select('etd_licenses', 'licenses')
    ->fields('licenses', array('id', 'name', 'code'))
    ->execute();

  $licenses = array();
  if ($result->rowCount() > 0) {
    foreach($result as $license) {
      $licenses[$license->id] = array('name'=>$license->name, 'code'=>$license->code);
    }
  }

  return $licenses;
}
 /**
 * Save the edited license.
 *
 * @param array $form
 * @param array $form_state
 */
function jarrow_license_edit_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (db_update('etd_licenses')
      ->fields(array(
        'name' => $values['name'],
        'code' => $values['code'],
        'summary' => $values['summary'],
        'terms' => $values['terms']['value'],
        'terms_format' => $values['terms']['format'],
        'full_text_link' => $values['full_text_link'],
      ))
      ->condition('id', $form_state['values']['license_id'])
      ->execute() === NULL
  ) {
    drupal_set_message(t('Could not save license'), 'error');
  } else {
    drupal_set_message(t('License saved'));
  }
}

/**
 * Confirm that the user would like to delete the selected licenses.
 */
function jarrow_license_delete_confirm($form, &$form_state) {

  $form['confirm_text'] = array(
    '#type' => 'markup',
    '#markup' => '<p>'.t('Please confirm that you would like to delete the following licenses: ').'</p>',
  );

  $edit = $form_state['input'];

  $form['licenses'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);

  $licenses = jarrow_license_load_multiple(array_keys(array_filter($edit['licenses'])));

  foreach ($licenses as $id=>$values) {

    $form['licenses'][$id] = array(
      '#type' => 'hidden',
      '#value' => $id,
      '#prefix' => '<li>',
      '#suffix' => $values['name'] . "</li>\n",
    );
  }

  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  return confirm_form($form,
    t('Are you sure you want to delete these licenses?'),
    JARROW_LICENSES_PATH, t('This action cannot be undone.'),
    t('Delete'), t('Cancel'));
}

/**
 * Submit handler for mass submission delete form.
 *
 * @see user_multiple_cancel_confirm()
 * @see user_cancel_confirm_form_submit()
 */
function jarrow_license_delete_confirm_submit($form, &$form_state) {

  if ($form_state['values']['confirm']) {
    $licenses = array();
    foreach ($form_state['values']['licenses'] as $id => $value) {
      $licenses[] = $id;
    }
    jarrow_license_delete($licenses);
  }
  $form_state['redirect'] = JARROW_LICENSES_PATH;
}

/**
 * Load the specified licenses form the database
 *
 * @param array $license_ids
 *
 * @return array
 */
function jarrow_license_load_multiple($license_ids = array()) {

  $result = db_select('etd_licenses', 'licenses')
    ->fields('licenses', array('id', 'name'))
    ->condition('id', $license_ids, 'IN')
    ->execute();

  $licenses = array();
  if ($result->rowCount() > 0) {
    foreach($result as $license) {
      $licenses[$license->id] = array('name'=>$license->name);
    }
  }

  return $licenses;
}

/**
 * Delete the specified licenses if they are not currently being used
 * by any submission.
 *
 * @param $license_ids
 */
function jarrow_license_delete($license_ids = array()) {

  // Need to check if any of licenses are currently be used by a submission.


  // If the license is not being used by a submission then it is okay to delete.
  if (db_delete('etd_licenses')
      ->condition('id', $license_ids, 'IN')
      ->execute() === NULL
  ) {
    drupal_set_message(t('Could not delete licenses'), 'error');
  } else {
    drupal_set_message(t('Licenses Deleted'));
  }
}

/**
 * Clone the specified license.
 *
 * @param int $license_id
 */
function jarrow_license_clone($license_id) {
  $old_license = db_select('etd_licenses')
    ->fields('etd_licenses')
    ->condition('id', $license_id)
    ->execute()
    ->fetch();
  if ($old_license) {
    $new_id = db_insert('etd_licenses')
      ->fields(array(
        'name' => $old_license->name . ' - ' . t('copy'),
        'code' => $old_license->code . ' - ' . t('copy'),
        'summary' => $old_license->summary,
        'terms' => $old_license->terms,
        'full_text_link' => $old_license->full_text_link,
      ))
      ->execute();
    if ($new_id !== NULL) {
      drupal_set_message(t('License cloned'));
      drupal_goto(JARROW_LICENSES_PATH . '/' . $new_id . '/edit');
    }
  }
  drupal_set_message(t('Could not clone the license'), 'error');
  drupal_goto(JARROW_LICENSES_PATH);
}

/**
 * Scan the default licenses directory and add any new licenses not already
 * in the system.
 *
 */
function jarrow_license_import() {

  $path = drupal_get_path('module', 'jarrow') . '/licenses/default_licenses';
  $imported_files = 0;
  if ($handle = opendir($path)) {
    while (FALSE !== ($entry = readdir($handle))) {
      $ext = pathinfo($entry, PATHINFO_EXTENSION);
      if ($ext === 'license') {
        if (jarrow_license_import_license_file($path, $entry)) {
          $imported_files++;
        }
      }
    }
    closedir($handle);
  }
  $message = t('License directory rescanned. ' . $imported_files);
  $message .= ($imported_files == 1) ? t(' license were added.'): t(' licenses were added.');
  drupal_set_message($message);
  drupal_goto(JARROW_LICENSES_PATH);
}

/**
 * Import the specified license file identified by filepath and filename.
 * The license contained in the file is expected to be simple xml object
 * that can be loaded.
 */
function jarrow_license_import_license_file($filepath, $filename) {

  $file_contents = file_get_contents($filepath .'/'. $filename);
  if ($file_contents === FALSE) {
    return FALSE;
  }
  if (substr($file_contents, 0, 5) != '<?xml') {
    return FALSE;
  }
  $xml = new DOMDocument();
  $xml->loadXML($file_contents);

  //$xml = simplexml_load_string($file_contents, 'SimpleXMLElement', LIBXML_NOCDATA);
  $tags = array('name', 'code', 'summary', 'terms', 'link');
  $license_elements = array();
  foreach ($tags as $tag) {
    $elements = $xml->getElementsByTagName($tag);
    foreach ($elements as $element) {
      // the assumption here is that there should only be one element contained in elements
      $license_elements[$tag] = $element->nodeValue;
    }
  }

  $result = db_select('etd_licenses')
    ->fields('etd_licenses', array('id', 'name', 'code'))
    ->execute()
    ->fetchAllAssoc('id');

  foreach ($result as $record) {
    if ((strtolower(trim($record->name)) == strtolower(trim($license_elements[$tags[0]]))) ||
      (strtolower(trim($record->code)) == strtolower(trim($license_elements[$tags[1]])))
    ) {
      return FALSE;
    }
  }

  $i = 0;
  $new_id = db_insert('etd_licenses')
    ->fields(array(
      'name' => $license_elements[$tags[$i++]],
      'code' => $license_elements[$tags[$i++]],
      'summary' => $license_elements[$tags[$i++]],
      'terms' => $license_elements[$tags[$i++]],
      'full_text_link' => $license_elements[$tags[$i++]],
    ))
    ->execute();
  return TRUE;
}
  