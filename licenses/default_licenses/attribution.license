    <div id="deed-rights"
       dir="ltr" style="text-align: left">  
<h3 resource="http://creativecommons.org/ns#Reproduction"
        rel="cc:permits">You are free:</h3>
    <ul class="license-properties">
      <li class="license share"
          rel="cc:permits"
          resource="http://creativecommons.org/ns#Distribution">
        <strong>to Share</strong> — to copy, distribute and transmit the work
      </li>

      
        <li class="license remix"
            rel="cc:permits"
            resource="http://creativecommons.org/ns#DerivativeWorks">
          <strong>to Remix</strong> — to adapt the work
        </li>
      

      
        <li class="license commercial">
          to make commercial use of the work
        </li>
    </ul>

  </div>
  <div id="deed-conditions">
    <h3>Under the following conditions:</h3>

    <ul dir="ltr" style="text-align: left"
        class="license-properties">
      
      <li class="license by"
          rel="cc:requires" resource="http://creativecommons.org/ns#Attribution">
          <p>
            <strong>Attribution</strong> &mdash;
            
              <span id="attribution-container">
                You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
              </span>
            
            <span id="by-more-container" />
          </p>

        </li>
      
    </ul>
  </div>

  <div id="deed-understanding">
    <h3>
      With the understanding that:
    </h3>

    <ul class="understanding license-properties">
      <li class="license">
        <strong>Waiver</strong>
        &mdash;
        Any of the above conditions can be waived if you get permission from the copyright holder.
      </li>

      <li class="license">
        <strong>Public Domain</strong>
        &mdash;
        Where the work or any of its elements is in the <a href="http://wiki.creativecommons.org/Public_domain" id="public_domain" class="helpLink">public domain</a> under applicable law, that status is in no way affected by the license.
      </li>

      <li class="license">
        <strong>Other Rights</strong>
        &mdash;
        In no way are any of the following rights affected by the license:

        <ul>
          <li>
            Your fair dealing or <a href="http://wiki.creativecommons.org/Frequently_Asked_Questions#Do_Creative_Commons_licenses_affect_fair_use.2C_fair_dealing_or_other_exceptions_to_copyright.3F" id="fair_use" class="helpLink">fair use</a> rights, or other applicable copyright exceptions and limitations;
          </li>

          
            <li>
              The author's <a href="http://wiki.creativecommons.org/Frequently_Asked_Questions#I_don.E2.80.99t_like_the_way_a_person_has_used_my_work_in_a_derivative_work_or_included_it_in_a_collective_work.3B_what_can_I_do.3F" id="moral_rights" class="helpLink">moral</a> rights;
            </li>
          
          

          <li>
            Rights other persons may have either in the work itself or in how the work is used, such as <a href="http://wiki.creativecommons.org/Frequently_Asked_Questions#When_are_publicity_rights_relevant.3F" class="helpLink" id="publicity_rights">publicity</a> or privacy rights.
          </li>
        </ul>
      </li>

      <li rel="cc:requires"
          resource="http://creativecommons.org/ns#Notice">
        <strong>Notice</strong>
        &mdash;
        For any reuse or distribution, you must make clear to others the license terms of this work. The best way to do this is with a link to this web page.
      </li>
    </ul>
  </div>