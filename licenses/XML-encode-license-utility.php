#!/usr/bin/php
<?php

$name = 'test license';
$code = 'test';
$summary = 'Some rambling text';
$full_text_link = '';
$path = './';
$filename = 'attribution.license';

create_XML_license();

function getInput() {
echo "Please enter the name of license: ";
$handle = fopen ("php://stdin","r");
$name = trim(fgets($handle));

echo "Please enter the code for the license: ";
//$handle = fopen ("php://stdin","r");
$code = trim(fgets($handle));

echo "Please enter the summary for the license: ";
//$handle = fopen ("php://stdin","r");
$summary = trim(fgets($handle));

echo "Please enter the URL link to the full text of the license: ";
//$handle = fopen ("php://stdin","r");
$full_text_link = trim(fgets($handle));

echo "Please enter the file path and filename of the HTML encoded description of the license: ";
//$handle = fopen ("php://stdin","r");
$filepath = trim(fgets($handle));

fclose($handle);
}

function check_inputs() {
  global $name, $code, $summary, $path, $filename;
  
  return ((strlen($name) > 0) && (strlen($code) > 0) && (strlen($summary) > 0) && (strlen($path) > 0) && (strlen($filename) > 0));
} 
function create_XML_license() {
   global $name, $code, $summary, $path, $filename, $full_text_link;

  if (! check_inputs()) {
    echo "Not all the required inputs where entered. \nExiting program. \nPlease try again.\n";
    return;
  }
  
  $html_text = file_get_contents($path . $filename);

  $doc = new DOMDocument();
  $license = $doc->createElement("license");
  $doc->appendChild($license);
  $node = $doc->createElement("name", $name); 
  $license->appendChild($node); 
  $node = $doc->createElement("code", $code); 
  $license->appendChild($node); 
  $node = $doc->createElement("summary", $summary); 
  $license->appendChild($node); 
  $node = $doc->createElement("link", $full_text_link); 
  $license->appendChild($node); 
  $terms = $doc->createElement("terms");
  $license->appendChild($terms);
  $node = $doc->createCDATASection($html_text); 
  $terms->appendChild($node);
  
  $doc->preserveWhiteSpace = false;
  $doc->formatOutput = true;
  $doc->substituteEntities = false;
  

  /*
  $xml = new SimpleXMLElement('<license></license>');
  $xml->addChild('name', $name);
  if (isset($code)) {
    $xml->addChild('code', $code);
  }
  $xml->addChild('summary', $summary);
  if (isset($full_link_text)) {
    $xml->addChild('link', $full_text_link);
  }
  
  $xml->addChild('terms', "<![CDATA[$html_text]]>");
  */
  
  //Create the new filename
  $new_filename = str_replace(' ', '-', $name).'.license';
  
  
  //Write the xml to file
  //file_put_contents($path . $new_filename, $xml->asXML());
    // $doc->loadXML($xml->asXML());
    $doc->save($path . $new_filename);  
}

?>