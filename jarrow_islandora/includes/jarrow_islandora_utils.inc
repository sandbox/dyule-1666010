<?php
/**
 * @file
 *  A collection of utility functions to facilitate communication between
 *  Jarrow and the Islandora Repository
 *
 * @author
 *  Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British
 * Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Make a direct connection to the Fedora Repository
 *
 * @return IslandoraTuque|null
 */
function jarrow_islandora_utils_connect() {

  module_load_include('inc', 'islandora', 'includes/IslandoraTuque');

  $tuque = & drupal_static('islandora_get_tuque_connection');
  if (!$tuque) {
    if (IslandoraTuque::exists()) {
      try {
        $tuque = new IslandoraTuque();
      } catch (Exception $e) {
        drupal_set_message(t('Unable to connect to the repository %e', array('%e' => $e)), 'error');
        return NULL;
      }
    } else {
      return NULL;
    }
  }
  return $tuque->repository;
}

/**
 * Ingest the given object. Taken from islandora.module
 *
 * @param NewFedoraObject $object
 *   An ingestable FedoraObject.
 *
 * @return FedoraObject
 *   The ingested FedoraObject.
 */
function jarrow_islandora_utils_ingest_object(NewFedoraObject &$object) {

  return $object->repository->ingestObject($object);
}

/**
 * Delete's or purges the given object. Taken from islandora.module
 *
 * @param FedoraObject $object
 *   An object to delete.
 *
 * @return bool
 *   TRUE if successful, FALSE otherwise.
 */
function jarrow_islandora_utils_delete_object(FedoraObject &$object) {

  try {
    $object->repository->purgeObject($object->id);
    $object = NULL;
    return TRUE;
  } catch (Exception $e) {
    // Exception message gets logged in Tuque Wrapper.
    return FALSE;
  }
}

/**
 * Create the full text description for the PDF object. Copied from the
 * Islandora PDF solution pack and modified.
 *
 * @param string $model
 * @param FedoraObject $fedora_object
 * @param string $file_uri
 * @param string $dsid
 *
 * @return bool|string
 */
function jarrow_islandora_utils_add_fulltext_description($model, $fedora_object, $file_uri, $dsid) {

  $source = jarrow_islandora_utils_get_realpath($file_uri);
  $temp = drupal_tempnam("temporary://", "fulltext") . '.txt';
  $text_file_uri = jarrow_islandora_utils_get_realpath($temp);

  switch ($model) {
    case JARROW_ISLANDORA_PDF_MODEL:
      $executable = variable_get('islandora_pdf_path_to_pdftotext', '/usr/bin/pdftotext');
      $command = "$executable -layout $source $text_file_uri";
      exec($command, $output);
      break;
  }

  if ($text_file_uri !== FALSE) {
    $document_info = array(
      'uri' => $text_file_uri,
      'filename' => 'Full text output of document',
      'filemime' => 'text/plain',
    );
    $success = jarrow_islandora_utils_ingest_datastream($fedora_object, $dsid, 'M', $document_info);
  }
  file_unmanaged_delete($temp);

  if ($success == FALSE) {
    drupal_set_message(t('Failed to create @dsid derivative.', array('@dsid' => $dsid)), 'error');
  }
  return $success;
}

/**
 * Create a jpeg image of the first page of the pdf
 *
 * @param $model
 * @param $fedora_object
 * @param $file_uri
 * @param $dsid
 * @param $width
 * @param $height
 *
 * @return bool
 */
function jarrow_islandora_utils_create_thumbnail($model, &$fedora_object, $file_uri, $dsid, $width, $height) {

  $success = FALSE;
  $image_file_uri = jarrow_islandora_utils_create_jpeg_image($model, $file_uri, $dsid, $width, $height);
  if ($image_file_uri !== FALSE) {
    $document_info = array(
      'uri' => $image_file_uri,
      'filename' => 'Thumbnail',
      'filemime' => 'image/jpeg',
    );
    $success = jarrow_islandora_utils_ingest_datastream($fedora_object, $dsid, 'M', $document_info);
  }
  if ($success == FALSE) {
    drupal_set_message(t('Failed to create thumbnail for ' . $fedora_object['name']), 'error');
  }
  return $success;
}

/**
 * Create a jpeg image of the specified pdf file identified by its file uri.
 * This code is copied from the islandora pdf solution pack.
 *
 * @param string $source
 *    what type of file is the image being generated from (ie pdf)
 * @param string $file_uri
 * @param string $dsid
 * @param int $width in pixels
 * @param int $height in pixels
 *
 * @return bool
 */
function jarrow_islandora_utils_create_jpeg_image($source, $file_uri, $dsid, $width, $height) {

  $matches = array();
  preg_match("/\/([^.]*).*$/", $file_uri, $matches); // Get the base name of the source file.
  $temp = drupal_tempnam("temporary://", "{$matches[1]}.$dsid.jpg");
  //$temp = drupal_tempnam("temporary://", basename($file_uri, '.php').'.'.$dsid.'.jpg');
  $dest = jarrow_islandora_utils_get_realpath($temp);
  $args['quality'] = '-quality ' . escapeshellarg(variable_get('imagemagick_quality', 75));
  $args['previewsize'] = '-resize ' . escapeshellarg("{$width}x{$height}");
  $args['colors'] = '-colorspace RGB';
  $context = array(
    'source' => $file_uri,
    'destination' => $dest,
  );
  drupal_alter('imagemagick_arguments', $args, $context);
  // To make use of ImageMagick 6's parenthetical command grouping we need to make
  // the $source image the first parameter and $dest the last.
  // See http://www.imagemagick.org/Usage/basics/#cmdline
  switch ($source) {
    case JARROW_ISLANDORA_PDF_MODEL:
      // we only want to convert the first page
      $filepath = $file_uri . '[0]';
      break;
    default:
      $filepath = $file_uri;
  }
  $command = escapeshellarg($filepath) . ' ' . implode(' ', $args) . ' ' . escapeshellarg("jpg:$dest");
  $output = '';
  $ret = -1;
  if (_imagemagick_convert_exec($command, $output, $ret) !== TRUE) {
    $message = 'imagemagick convert failed to create derivative<br/>Error: @ret<br/>Command: @command<br/>Output: @output';
    $variables = array('@ret' => $ret, '@command' => $command, '@output' => $output);
    watchdog('jarrow_islandora_utils', $message, $variables, WATCHDOG_ERROR);
    return FALSE;
  }
  return $dest;
}

/**
 * Return the absolute path given by the uri. We use our own method as
 * the realpath in Drupal has been marked as deprecated.
 *
 * @param $uri
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_realpath($uri) {

  module_load_include('inc', 'jarrow', 'workflow/digital_objects/digital_objects');
  return jarrow_digital_object_realpath($uri);
}

/**
 * Return the number of pages in a pdf file.
 *
 * @param string $pdf_uri absolute filepath to pdf file
 *
 * @return int
 */
function jarrow_islandora_utils_count_pages($pdf_uri) {

  $pdftext = file_get_contents($pdf_uri);
  $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
  return $num;
}

/**
 * @param $fedora_object
 * @param string $stream_label
 * @param string $stream_id
 * @param string $stream_type
 * @param string $xml_stream
 */
function jarrow_islandora_utils_ingest_xml_stream(&$fedora_object, $stream_id, $stream_label, $stream_type, $xml_stream) {

  $ds = $fedora_object->constructDatastream($stream_id, $stream_type);
  $ds->label = $stream_label;
  $ds->mimetype = 'text/xml';
  $ds->setContentFromString($xml_stream);
  $fedora_object->ingestDatastream($ds);
}

/**
 * Perform the crosswalk for the specified metadata and then update the appropriate data stream.
 *
 * @param $fedora_object
 * @param DOMDocument $metadata
 * @param string $crosswalk_id
 * @param string $stream_id
 * @param string $stream_label
 * @param string $stream_type
 */
function jarrow_islandora_utils_perform_crosswalk(&$fedora_object, $metadata, $crosswalk_id, $stream_id, $stream_label, $stream_type) {

  // this will be called many times so we don't want to create new
  // XSLT processors every time so we will hold references to them in here
  $xsltProcessors = & drupal_static(__FUNCTION__);

  if (!isset($xsltProcessors[$crosswalk_id])) {
    $xsl = new DOMDocument;
    $xsl->load(drupal_get_path('module', 'jarrow_islandora') . '/xml/' . $crosswalk_id . '.xsl');
    // Configure the transformer
    $xsltProcessors[$crosswalk_id] = new XSLTProcessor;
    $xsltProcessors[$crosswalk_id]->importStyleSheet($xsl); // attach the xsl rules
  }
  $transformed_xml = $xsltProcessors[$crosswalk_id]->transformToXML($metadata);

  if (isset($fedora_object[$stream_id])) {
    $fedora_object[$stream_id]->content = $transformed_xml;
  } else {
    jarrow_islandora_utils_ingest_xml_stream($fedora_object, $stream_id, $stream_label, $stream_type, $transformed_xml);
  }
}

/**
 * Load the fedora object identified by object id
 *
 * @param $object_id
 *
 * @return bool|null
 */
function jarrow_islandora_utils_load_fedora_object($object_id) {

  $repository = jarrow_islandora_utils_connect();
  if ($repository) {
    try {
      return $repository->getObject(urldecode($object_id));
    } catch (Exception $e) {
      if ($e->getCode() == '404') {
        return FALSE;
      } else {
        return NULL;
      }
    }
  } else {
    IslandoraTuque::getError();
  }
  // Assuming access denied in all other cases for now.
  return NULL;
}

/**
 * Add the specified days, months and years to the given date.
 *
 * @param int $givendate
 *    in unix time
 * @param int $day
 * @param int $mth
 * @param int $yr
 *
 * @return string
 */
function jarrow_islandora_utils_add_date($givendate, $day = 0, $mth = 0, $yr = 0) {

  // dsm('date = ' . $givendate . ' days = ' . $day . ' months = ' . $mth . ' years = ' . $yr);

  $newdate = date('Y-m-d h:i:s', mktime(date('h', $givendate),
    date('i', $givendate), date('s', $givendate), date('m', $givendate) + $mth,
    date('d', $givendate) + $day, date('Y', $givendate) + $yr));
  return $newdate;
}

/**
 * Collect all the dublin core fields relevant to the digital
 * object being ingested as well as its file location and mime type.
 * Return this information in an indexed array.
 *
 * @param object $submission
 * @param int $file_id
 * @param int $digital_object_instance_id
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_digital_object_info($submission, $file_id, $digital_object_instance_id) {

  $results = db_select('file_managed', 'fm')
    ->fields('fm', array('filename', 'uri', 'filemime'))
    ->condition('fid', $file_id)
    ->condition('status', 1)
    ->execute()
    ->fetch();

  $object_info = array();
  foreach ($results as $key => $result) {
    $object_info[$key] = $result;
  }
  // make sure the uri filepath is absolute
  $object_info['uri'] = jarrow_islandora_utils_get_realpath($object_info['uri']);

  $dc_fields = jarrow_islandora_load_dublin_core_fields();
  // Collection the data element ids we are interested in
  $data_element_ids = array();
  $data_element_map = array();
  foreach ($submission->data_model->elements as $data_element) {
    if (isset($data_element->dc_mapping) && isset($dc_fields[$data_element->dc_mapping['name']])) {
      $data_element_ids[] = $data_element->id;
      $data_element_map[$data_element->id] = $data_element->dc_mapping;
    }
  }
  $results = db_select('etd_submission_field', 'sub')
    ->fields('sub', array('data_element', 'data'))
    ->condition('submission', $submission->id)
    ->condition('data_element', $data_element_ids, 'IN')
    ->condition('digital_object_instance', $digital_object_instance_id)
    ->execute();

  foreach ($results as $result) {
    //dsm($result);
    $value = unserialize($result->data);
    $dc_field_id = $data_element_map[$result->data_element];
    if (!isset($object_info[$dc_fields[$dc_field_id]['name']])) {
      $object_info[$dc_fields[$dc_field_id]['name']] = array();
    }
    $object_info[$dc_fields[$dc_field_id]['name']][] = array(
      'dc_id' => $dc_field_id,
      'element_id' => $result->data_element,
      'value' => $value
    );
  }
  return $object_info;
}

/**
 * Create the audio thumbnail and ingest it.
 *
 * @param object $fedora_object
 */
function jarrow_islandora_create_audio_thumbnail(&$fedora_object) {

  $file_uri = drupal_get_path('module', 'islandora_audio') . '/images/audio-TN.jpg';
  jarrow_islandora_utils_add_datastream($fedora_object, $file_uri, 'TN', 'image/jpeg', 'M');
}

/**
 * Create the video thumbnail and ingest it.
 *
 * @param object $fedora_object
 * @param string $file_uri
 */
function jarrow_islandora_create_video_thumbnail(&$fedora_object, $file_uri) {

  $thumbnail = jarrow_islandora_create_video_snapshot($file_uri);
  if ($thumbnail) {
    jarrow_islandora_utils_add_datastream($fedora_object, $thumbnail, 'TN', 'image/jpeg', 'M');
    file_unmanaged_delete($thumbnail);
  } else {
    $file_uri = drupal_get_path('module', 'islandora_video') . '/images/crystal_clear_app_camera.png';
    jarrow_islandora_utils_add_datastream($fedora_object, $file_uri, 'TN', 'image/jpeg', 'M');
  }
}

/**
 * Create the video thumbnail from the video if possible. Got this code from
 * the islandora video solution pack.
 *
 * @param $file_uri
 *
 * @return bool|string
 */
function jarrow_islandora_create_video_snapshot2($file_uri) {

  $out_file = $file_uri . '-TN.jpg';
  $ffmpeg_executable = variable_get('islandora_video_ffmpeg_path', 'ffmpeg');

  $tn_creation = "$ffmpeg_executable -i $file_uri -r 1 -f image2 -vframes 1 -ss 4 $out_file";

  $return_value = FALSE;
  exec($tn_creation, $output, $return_value);

  if ($return_value == '0') {
    return $out_file;
  } else {
    return FALSE;
  }
  return FALSE;

}

/**
 * Create the video thumbnail from the video if possible. Got this code from
 * the islandora video solution pack.
 *
 * @param $file_uri
 *
 * @return bool|string
 */
function jarrow_islandora_create_video_snapshot($file_uri) {

  $out_file = $file_uri . '-TN.jpg';
  $ffmpeg_executable = variable_get('islandora_video_ffmpeg_path', 'ffmpeg');

  $ret_value = FALSE;
  $vid_length = "$ffmpeg_executable -i $file_uri 2>&1";
  exec($vid_length, $time_output, $ret_value);

  $dur_match = FALSE;
  $duration = '';
  foreach ($time_output as $key => $value) {
    preg_match('/Duration: (.*), start/', $value, $time_match);
    if (count($time_match)) {
      $dur_match = TRUE;
      $duration = $time_match[1];
      break;
    }
  }

  if ($dur_match) {
    // Snip off the ms because we don't care about them.
    $time_val = preg_replace('/\.(.*)/', '', $duration);
    $time_array = explode(':', $time_val);
    $output_time = floor((($time_array[0] * 360) + ($time_array[1] * 60) + $time_array[2]) / 2);

    $tn_creation = "$ffmpeg_executable -itsoffset -4 -ss $output_time -i $file_uri -vcodec mjpeg -vframes 1 -an -f rawvideo $out_file";

    $return_value = FALSE;
    exec($tn_creation, $output, $return_value);

    if ($return_value == '0') {
      return $out_file;
    } else {
      return FALSE;
    }
  }
  return FALSE;
}

/**
 * Determine which object model should be applied based on mime type.
 *
 * @param array $mime_type
 *    the portion of the mime type after the '/' character
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_application_model($mime_type) {

  $model = NULL;
  switch ($mime_type) {
    case 'pdf':
    case 'doc':
    case 'csv':
    case 'xls':
    case 'xlsx':
    case 'ppt':
    case 'pps':
    case 'docx':
    case 'odt':
  }
  return $model;
}

/**
 * Determine which object model should be applied based on mime type.
 *
 * @param array $mime_type
 *    the portion of the mime type after the '/' character
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_audio_model($mime_type) {

  $model = NULL;
  switch ($mime_type) {
    case 'wma':
    case 'wav':
    case 'x-wav':
    case 'aif':
    case 'ogg':
    case 'mp3':
    case 'mpeg':
      $model = JARROW_ISLANDORA_AUDIO_MODEL;
  }
  return $model;
}

/**
 * Determine which object model should be applied based on mime type.
 *
 * @param array $mime_type
 *    the portion of the mime type after the '/' character
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_image_model($mime_type) {

  $model = NULL;
  switch ($mime_type) {
    case 'gif':
    case 'jpeg':
    case 'jpg':
    case 'png':
      $model = JARROW_ISLANDORA_IMAGE_MODEL;
      break;
    case 'tiff':
    case 'tif':
      $model = JARROW_ISLANDORA_LARGE_IMAGE_MODEL;
      break;
  }
  return $model;
}

/**
 * Determine which object model should be applied based on mime type.
 *
 * @param array $mime_type
 *   the portion of the mime type after the '/' character
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_text_model($mime_type) {

  $model = NULL;
  switch ($mime_type) {
    case 'css':
    case 'html':
    case 'txt':
      $model = JARROW_ISLANDORA_TEXT_MODEL;
      break;
  }
  return $model;
}

/**
 * Determine which object model should be applied based on mime type.
 *
 * @param array $mime_type
 *    the portion of the mime type after the '/' character
 *
 * @return mixed
 */
function jarrow_islandora_utils_get_video_model($mime_type) {

  $model = NULL;
  switch ($mime_type) {
    case 'mov':
    case 'mpeg':
    case 'mpeg4':
    case 'mp4':
    case 'swf':
    case 'avi':
    case 'flv':
    case 'ogg':
    case 'x-ms-wmv':
    case 'x-msvideo':
    case 'x-flv':
    case 'mkv':
    case 'x-matroska':
    case 'quicktime':
      $model = JARROW_ISLANDORA_VIDEO_MODEL;
      break;
  }
  return $model;
}

/**
 * Create a jpeg image and ingest it.
 *
 * @param object $fedora_object
 * @param string $source
 * @param array $object_info
 * @param string $dsid
 * @param int $width
 * @param int $height
 *
 * @return bool
 */
function jarrow_islandora_utils_create_image(&$fedora_object, $source, $object_info, $dsid, $width, $height) {

  $success = FALSE;
  $image_file_uri = jarrow_islandora_utils_create_jpeg_image($source, $object_info['uri'], $dsid, $width, $height);
  if ($image_file_uri !== FALSE) {
    $success = jarrow_islandora_utils_add_datastream($fedora_object, $image_file_uri, $dsid, 'image/jpeg', 'M');
  }
  file_unmanaged_delete($image_file_uri);
  return $success;
}

/**
 * If the source audio file isn't an mp3 already, then create a mp3 version of that
 * file and ingest it.
 *
 * @param object $fedora_object
 * @param array $object_info
 * @param string $dsid
 *
 * @return bool
 */
function jarrow_islandora_utils_create_mp3(&$fedora_object, $object_info, $dsid) {

  $success = FALSE;
  $file_extension = substr(strrchr($object_info['filename'], '.'), 1);
  if ($file_extension == 'mp3') {
    if (($object_info['filemime'] == 'audio/mpeg') || ($object_info['filemime'] == 'audio/mp3')) {
      return;
    }
  }
  $audio_file_uri = jarrow_islandora_utils_create_mp3_file($object_info['uri']);
  if ($audio_file_uri !== FALSE) {
    $success = jarrow_islandora_utils_add_datastream($fedora_object, $audio_file_uri, $dsid, 'audio/mpeg', 'M');
  }
  file_unmanaged_delete($audio_file_uri);
  return $success;
}

/**
 * Creates the mp3 file from the given audio file.
 *
 * @param string $file_uri
 *   The URI to the audio file the derivative will be generated from.
 *
 * @return string
 *   A URI to the generated derivative if successful, FALSE otherwise.
 */
function jarrow_islandora_utils_create_mp3_file($file_uri) {

  $output = array();
  $file = jarrow_islandora_utils_get_realpath($file_uri);
  $outfile = $file . '.mp3';
  $lame_url = variable_get('islandora_lame_url', '/usr/bin/lame');
  $command = "$lame_url -V5 --vbr-new '${file}' '${outfile}'";
  $ret = FALSE;
  exec($command, $output, $ret);
  if ($ret == '0') {
    return $outfile;
  }
  return FALSE;
}

/**
 * Create the video formats that we want.
 *
 * @param object $fedora_object
 * @param array $object_info
 *
 * @return mixed
 */
function jarrow_islandora_utils_create_video(&$fedora_object, $object_info) {

  $file_extension = substr(strrchr($object_info['filename'], '.'), 1);
  if (($file_extension == 'mp4') && ($object_info['filemime'] == 'video/mp4')) {
    jarrow_islandora_utils_ingest_datastream($fedora_object, 'MP4', 'M', $object_info);
  } else {
    // Make and ingest MP4 file.
    $video_file_uri = jarrow_islandora_video_create_mp4($object_info['uri']);
    if ($video_file_uri !== FALSE) {
      jarrow_islandora_utils_add_datastream($fedora_object, $video_file_uri, 'MP4', 'video/mp4', 'M');
      file_unmanaged_delete($video_file_uri);
    }
  }
  // Make and ingest OGG file. This file needs to be generated last so that we can create
  // a thumbnail from it.
  $video_file_uri = jarrow_islandora_video_create_ogg($object_info['uri']);
  if ($video_file_uri !== FALSE) {
    jarrow_islandora_utils_add_datastream($fedora_object, $video_file_uri, 'OGG', 'video/ogg', 'M');
  }

  return $video_file_uri;
}

/**
 * Create the mpeg4 version of the video file.
 *
 * @param string $file_uri
 *
 * @return bool|string
 */
function jarrow_islandora_video_create_mp4($file_uri) {

  $file = jarrow_islandora_utils_get_realpath($file_uri);
  $output_file = $file . '.mp4';

  $ffmpeg_executable = variable_get('islandora_video_ffmpeg_path', 'ffmpeg');
  $command = "$ffmpeg_executable -i $file -f mp4 -vcodec copy -acodec copy $output_file";
  $return_value = FALSE;
  exec($command, $output, $return_value);

  if ($return_value == '0') {
    drupal_set_message('MP4 created successfully.');
    return $output_file;
  } else {
    drupal_set_message('MP4 creation failed.');
  }
  return FALSE;
}

/**
 * Create the ogg vorbis version of the video file.
 *
 * @param string $file_uri
 *
 * @return bool|string
 */
function jarrow_islandora_video_create_ogg($file_uri) {

  $file = drupal_realpath($file_uri);
  $output_file = $file . '.ogg';

  $theora_executable = variable_get('islandora_video_ffmpeg2theora_path', 'ffmpeg2theora');
  $command = "$theora_executable $file -o $output_file";
  $return_value = FALSE;
  exec($command, $output, $return_value);

  if ($return_value == '0') {
    drupal_set_message('OGG created successfully.');
    return $output_file;
  } else {
    drupal_set_message('OGG creation failed.');
  }
  return FALSE;
}

/**
 * Add the file based data stream to the specified fedora object.
 *
 * @param object $fedora_object
 * @param string $file_uri
 * @param string $dsid
 * @param string $mimetype
 * @param string $ds_reference
 *
 * @return bool
 */
function jarrow_islandora_utils_add_datastream(&$fedora_object, $file_uri, $dsid, $mimetype, $ds_reference) {

  $document_info = array(
    'uri' => $file_uri,
    'filename' => $dsid,
    'filemime' => $mimetype,
  );
  $success = jarrow_islandora_utils_ingest_datastream($fedora_object, $dsid, $ds_reference, $document_info);
  return $success;
}

/**
 * Ingest the document into the repository
 *
 * @param object $fedora_object
 * @param string $ds_id
 *     which is the datastream id
 * @param string $ds_reference
 *     which can only be M, or X. E or R is not permissable
 * @param array $document_info
 *
 * @return boolean
 */
function jarrow_islandora_utils_ingest_datastream(&$fedora_object, $ds_id, $ds_reference, $document_info) {

  try {
    if (empty($fedora_object[$ds_id])) {
      $ds = $fedora_object->constructDatastream($ds_id, $ds_reference);
      $fedora_object->ingestDatastream($ds);
    } else {
      $ds = $fedora_object[$ds_id];
    }
    $ds->setContentFromFile($document_info['uri']);
    $ds->label = $document_info['filename'];
    $ds->mimetype = $document_info['filemime'];
    return TRUE;
  } catch (exception $error) {
    return FALSE;
  }
}

/**
 * We want to move the collections identified by pid to be located in the
 * theses supplemental collection.
 *
 * @param IslandoraTuque $connection
 * @param array $collection_pids
 * @param string $destination_pid
 *
 */
function jarrow_islandora_utils_move_collection_models($connection, $collection_pids, $destination_pid) {

  $repository = $connection->repository;
  $destination_object = $repository->getObject($destination_pid);
  if ($destination_object) {
    foreach ($collection_pids as $collection_pid) {
      $fedora_object = $repository->getObject($collection_pid);
      if ($fedora_object !== FALSE) {
        $fedora_object->relationships->remove(FEDORA_RELS_EXT_URI, 'isMemberOfCollection', 'islandora:root');
        $fedora_object->relationships->add(FEDORA_RELS_EXT_URI, 'isMemberOfCollection', $destination_pid);
      }
    }
  } else {
    drupal_set_message("Fedora Object with PID: $destination_pid isn't in the repository!", 'error');
  }
}

/**
 * Apply the collection policy as stored in the file designated by $cp_filename
 *
 * @param $jarrow_collection
 * @param string $module_path
 * @param string $datastreams_path
 * @param string $cp_filename
 */
function jarrow_islandora_utils_apply_collection_policy(&$jarrow_collection, $module_path, $datastreams_path, $cp_filename) {

  // Collection Policy Datastream.
  $datastream = $jarrow_collection->constructDatastream('COLLECTION_POLICY', 'X');
  $datastream->label = 'COLLECTION_POLICY';
  $datastream->mimetype = 'text/xml';
  $datastream->setContentFromFile($datastreams_path . '/' . $cp_filename, FALSE);
  $jarrow_collection->ingestDatastream($datastream);
  // TN Datastream.
  $datastream = $jarrow_collection->constructDatastream('TN', 'M');
  $datastream->label = 'Thumbnail';
  $datastream->mimetype = 'image/png';
  $datastream->setContentFromFile($module_path . '/folder.png', FALSE);
  $jarrow_collection->ingestDatastream($datastream);
}

/**
 * Create two lookup arrays so that we can quickly determine which data elements
 * are connected to which form elements.
 *
 * @return array
 */
function jarrow_islandora_utils_load_form_elements() {
  $result = db_select('etd_form_elements', 'fe')
    ->fields('fe', array('id', 'form', 'type_config'))
    ->execute();
  $by_form_id = array();
  $by_element_id = array();
  foreach ($result as $element) {
    //As the type config field is module provided, it is simply stored as a serialized
    //string, which must be de-serialized before being passed to the user.
    $type_config = unserialize($element->type_config);
    $by_form_id[$element->form.'_'.$element->id] = $type_config;
    if (isset($type_config->linked_form_elements)) {
      $by_element_id[$type_config->associated_element] = $type_config->linked_form_elements;
    }
  }
  return array('by_form_id' => $by_form_id, 'by_element_id' => $by_element_id);
}