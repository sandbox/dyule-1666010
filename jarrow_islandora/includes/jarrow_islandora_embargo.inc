<?php

/**
 * Determine if the submission has an embargo applied to it.
 *
 * @param $submission
 *
 * @return bool
 */
function jarrow_islandora_submission_embargoed($submission) {

  $embargo_date = jarrow_islandora_get_embargo_date($submission);
  if (isset($embargo_date)) {
    //Add the submission to the embargoed list;
    return jarrow_islandora_enable_embargo($submission->id, $embargo_date);
  }
  return FALSE;
}

/**
 * Return the date when the embargo ends. Return null if there is no embargo date.
 *
 * @param $submission
 *
 * @return date | null
 */
function jarrow_islandora_get_embargo_date($submission) {

  foreach ($submission->data_model->elements as $data_element) {
    if (stristr($data_element->name, 'publishing embargo') !== FALSE) {
      $result = db_select('etd_submission_field', 'sub')
        ->fields('sub', array('data'))
        ->condition('submission', $submission->id)
        ->condition('data_element', $data_element->id)
        ->execute()
        ->fetchAll();
      if (count($result) > 1) {
        drupal_set_message(t('More than one value was found for the publishing embargo.'), 'error');
      } else {
        if (count($result) > 0) {
          $value = unserialize($result[0]->data);
          if ($data_element->type == 'select') {
            $options = json_decode($data_element->type_config->list_values);
            return jarrow_islandora_determine_embargo_expiry_date($options->$value);
          } else {
            return jarrow_islandora_determine_embargo_expiry_date($value);
          }
        }
      }
    }
  }
  return NULL;
}

/**
 * Determine the date when the embargo expires. If there is no expiry date then
 * no publishing empbargo will be applied.
 *
 * @param string $publishing_delay
 *   The amount of time to delay publishing by
 *
 * @return date
 */
function jarrow_islandora_determine_embargo_expiry_date($publishing_delay) {

  $determine_date = FALSE;
  $month = 0;
  $year = 0;
  $day = 0;
  if (stristr($publishing_delay, t('month')) !== FALSE) {
    $substring = stristr($publishing_delay, t('month'), TRUE);
    $delay = end(explode(' ', trim($substring)));
    if (is_numeric($delay)) {
      if (preg_match('/^\d+$/', $delay)) {
        $month = $delay;
      } else {
        $day += $delay * 30;
      }
      $determine_date = TRUE;
    }
  }
  if (stristr($publishing_delay, 'year') !== FALSE) {
    $substring = stristr($publishing_delay, t('year'), TRUE);
    $delay = end(explode(' ', trim($substring)));
    if (is_numeric($delay)) {
      if (preg_match('/^\d+$/', $delay)) {
        $year = $delay;
      } else {
        $day += $delay * 365;
      }
      $determine_date = TRUE;
    }
  }
  if (stristr($publishing_delay, 'day') !== FALSE) {
    $substring = stristr($publishing_delay, t('day'), TRUE);
    $delay = end(explode(' ', trim($substring)));
    if (is_numeric($delay)) {
      $day += (int) $delay;
      $determine_date = TRUE;
    }
  }
  if ($determine_date) {
    $now = time();
    $date = mktime(0, 0, 0, date("m", $now), date("d", $now), date("Y", $now));
    return jarrow_islandora_utils_add_date($date, $day, $month, $year);
  }
  return NULL;
}

/**
 * Add specified submission to the embargoed table so that it can be easily
 * checked by the cron job to see when it expires.
 *
 * @param $submission_id
 * @param $embargo_expiry_date
 *
 * @return bool
 */
function jarrow_islandora_enable_embargo($submission_id, $embargo_expiry_date) {

  if (strtotime($embargo_expiry_date) <= time()) {
    // Return since the embargo date has already expired
    return FALSE;
  };
  // Log the expiry date for the submission
  db_merge('etd_embargos')
    ->key(array('submission_id' => $submission_id))
    ->fields(array(
      'submission_id' => $submission_id,
      'expiry_date' => $embargo_expiry_date
    ))
    ->execute();
  //jarrow_islandora_check_embargo_expiry_date();
  return TRUE;
}

/**
 * Remove the publishing embargo from the specified fedora object.
 *
 * @param $submission_ids
 *    an array of submission_ids to delete
 */
function jarrow_islandora_remove_embargo($submission_ids) {

  db_delete('etd_embargos')
    ->condition('submission_id', $submission_ids, 'IN')
    ->execute();
}

/**
 * Detect if a publishing embargo period has expired and if so remove the embargo.
 */
function jarrow_islandora_check_embargo_expiry_date() {

  watchdog('jarrow_islandora_embargo', 'Cron function triggered to check embargo expiry dates');
  $now = time();
  $records = db_select('etd_embargos', 'em')
    ->fields('em')
    ->execute();
  $submission_ids = array();
  foreach ($records as $record) {
    if (strtotime($record->expiry_date) <= $now) {
      $submission_ids[] = $record->submission_id;
    }
  }
  if (count($submission_ids) > 0) {
    // Remove embargos
    jarrow_islandora_remove_embargo($submission_ids);
    foreach ($submission_ids as $submission_id) {
      // Publish submission
      $submission = _jarrow_load_submission($submission_id);
      jarrow_islandora_ingest_submission($submission);
    }
  }
}
