<?xml version="1.0" encoding="UTF-8"?>

<!--
    Created by Peter Hvezda
    University of Northern BC - Geoffrey R. Weller Library

    This XSLT transforms the Library's generic XML to Simple Dublin Core XML.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc.xsd">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:copy-of select="node()|@*"/>
        </xsl:copy>

    </xsl:template>

    <xsl:template match="generic_etd">

        <oai_dc:dc xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                   xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                   xsi:noNamespaceSchemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc.xsd">

            <xsl:apply-templates/>

        </oai_dc:dc>

    </xsl:template>

    <xsl:template match="*">
        <xsl:choose>
            <!-- Ignore the tags we aren't interested in -->
            <xsl:when test="contains(name(),'.contributor.role')"/>
            <!-- Process the tags we are interested in -->
            <xsl:when test="contains(name(),'dc.')">
                <xsl:variable name="newtag" select="concat('dc:',substring-after(name(),'.'))"/>
                <xsl:choose>
                    <xsl:when test="contains($newtag, '.')">
                        <xsl:element name="{substring-before($newtag,'.')}">
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="{$newtag}">
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
