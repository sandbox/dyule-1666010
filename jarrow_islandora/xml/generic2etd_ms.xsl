<?xml version="1.0" encoding="UTF-8"?>

<!--
    Created by Peter Hvezda
    University of Northern BC - Geoffrey R. Weller Library

    This XSLT transforms generic XML to ETD-MS XML.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:etd_ms="http://www.ndltd.org/standards/metadata/etdms/1.0/"
                xsi:schemaLocation="http://www.ndltd.org/standards/metadata/etdms/1.0/ http://www.ndltd.org/standards/metadata/etdms/1.0/etdms.xsd">

    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:copy-of select="node()|@*"/>
        </xsl:copy>

    </xsl:template>

    <xsl:template match="*">
        <xsl:choose>
            <xsl:when test="name()='generic_etd'">
                <etd_ms:thesis>
                    <xsl:apply-templates/>
                </etd_ms:thesis>
            </xsl:when>
            <!-- Ignore the tags we are not interested in at this moment. We will process them later. -->
            <xsl:when test="contains(name(),'thesis.degree.')"/>
            <xsl:when test="contains(name(),'master')"/>
            <xsl:when test="contains(name(),'related')"/>
            <!-- Process the associated_tags. -->
            <xsl:when test="contains(name(),'associated_tags')">
                <xsl:variable name="related" select="related/*"/>
                <xsl:for-each select="master/*">
                    <xsl:variable name="currenttag" select="name()"/>
                    <xsl:variable name="node" select="current()"/>
                    <xsl:variable name="newtag" select="concat('etd_ms:',substring-after($currenttag,'.'))"/>
                    <xsl:for-each select="$related">
                        <xsl:call-template name="associated">
                            <xsl:with-param name="element-tag" select="$newtag"/>
                            <xsl:with-param name="element-value" select="$node"/>
                            <xsl:with-param name="related" select="current()"/>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:when>
            <!-- Process the tags we are currently interested in. -->
            <xsl:otherwise>
                <xsl:variable name="newtag" select="concat('etd_ms:',substring-after(name(),'.'))"/>
                <xsl:choose>
                    <xsl:when test="contains($newtag, '.')">
                        <xsl:element name="{substring-before($newtag,'.')}">
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="{$newtag}">
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="associated">
        <xsl:param name="element-tag"/>
        <xsl:param name="element-value"/>
        <xsl:param name="related"/>
        <xsl:variable name="tagname" select="name($related)"/>
        <xsl:choose>
            <!-- if this is a contributor.role then process it accordingly. -->
            <xsl:when test="contains($tagname,'.contributor.role')">
                <xsl:if test="contains($element-tag,'contributor')">
                    <xsl:element name="{$element-tag}">
                        <xsl:attribute name="role">
                            <xsl:value-of select="current()"/>
                        </xsl:attribute>
                        <xsl:value-of select="$element-value"/>
                    </xsl:element>
                </xsl:if>
            </xsl:when>
            <!--
            <xsl:otherwise>
                <xsl:element name="{$element-tag}">
                </xsl:element>
                <xsl:variable name="newtag" select="concat('etd_ms:',substring-after($tagname,'.'))"/>
                <xsl:element name="{$newtag}">
                </xsl:element>
            </xsl:otherwise>
            -->
        </xsl:choose>
    </xsl:template>

    <xsl:template name="degree" match="*[contains(name(),'thesis.degree.')][1]">
        <etd_ms:degree>
            <xsl:for-each select="//*[contains(name(),'thesis.degree.')]">
                <xsl:element name="{concat('etd_ms:',substring-after(name(),'thesis.degree.'))}">
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:for-each>
        </etd_ms:degree>
    </xsl:template>
</xsl:stylesheet>
