<?php

/**
 * Implements hook_etd_perform_crosswalks().
 *
 * @param string $fedora_object_pid
 *    The pid of the fedora object you will need to load and perform any
 *    necessary crosswalks with.
 */

function jarrow_islandora_etd_perform_crosswalks($fedora_object_pid) {
}

/**
 * Implements hook_etd_create_generic_metadata_xml.
 *
 * @param object $submission
 * @param array $md_fields
 *    an array of metadata fields
 * @param string $fedora_object_id
 *    The pid of the fedora object you will need to load and perform any
 *    necessary crosswalks with.
 */

function jarrow_islandora_etd_create_generic_metadata_xml($submission, $md_fields, $fedora_object_id) {
}
