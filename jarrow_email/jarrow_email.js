(function($){
  Drupal.behaviors.jarrow_email = {
    attach: function(context, settings) {
      $('.data_model_tree').once('data-tree').tree({
        data: $.parseJSON(Drupal.settings.jarrow.data_source_tree), 
        autoOpen: false,
        onCreateLi: function (node, $li) {
          $li.attr('id', 'data_element-' + node.id);
        }
        
      }).bind('tree.click', function(e) {
        var node = e.node;
        if(node.children.length == 0) {
          var imgSrc = 'admin/jarrow/workflow/datamodels/' + 
          Drupal.settings.jarrow.data_model_id + 
          '/templateimages/' + node.id,
          editor = Drupal.wysiwyg.instances[Drupal.wysiwyg.activeId];
          if(editor.editor === 'none') {
            if($('.wysiwyg-toggle-wrapper a:visible').length === 0) {
              $('#edit-no-editor-dialog').dialog('open');
            } else {
              $('#edit-plain-text-dialog').dialog('open');
            }
          } else {
            Drupal.wysiwyg.instances[Drupal.wysiwyg.activeId].insert('<img src="' 
              + imgSrc + '" alt="[' + node.name + ']" />');
          }
        } else {
            $('#data_element-' + node.id + ' .toggler').trigger('click');
        }
      }); 
      $('.jarrow_save_template').once('editor-toggle').mousedown(function(e) {
          //PH Not sure what the purpose of these two lines are for.
          //PH I commented them out and it fixed a display bug
      //  $('.wysiwyg-toggle-wrapper a').click();
     //   $('.wysiwyg-toggle-wrapper a').click();
      })
      $('#edit-new-template').once('new-email').mousedown(function(e) {
        var newName = prompt(Drupal.t('Please enter a name for the new template'));
        $('#edit-name-field').val(newName);
        this.click();
      });
      $('.jarrow_delete_button').once('delete-template').mousedown(function(e) {
        var response = confirm(Drupal.t('Are you sure you want to delete this template?'));
        if(response) {
          this.click();
        }
      });
      
      $('.jarrow_rename_button').once('rename-template').mousedown(function(e) {
        var newName = prompt(Drupal.t('Please enter a new name for the template'), $('#edit-left-panel .vertical-tab-button.selected a *:first').text());
        $('#edit-name-field').val(newName);
        this.click();
      });
      $('#edit-no-editor-dialog').dialog({
        autoOpen: false,
        buttons: [
        {
          text: Drupal.t('OK'),
          click: function() {
            $(this).dialog('close');
          }
        }
        ],
        modal: true,
        title: Drupal.t('Enable Rich Text'),
        width: 330
      });
      $('#edit-plain-text-dialog').dialog({
        autoOpen: false,
        buttons: [
        {
          text: Drupal.t('OK'),
          click: function() {
            $(this).dialog('close');
          }
        }
        ],
        modal: true,
        title: Drupal.t('Enable Rich Text'),
        width: 330
      });
    }
  };
}(jQuery));