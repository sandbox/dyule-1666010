<?php

/**
 * Implements hook_schema()
 * @return array
 *  An array defining the etd_email_template database table.
 */
function jarrow_email_schema() {
  $schema = array();

  $schema['etd_email_templates'] = array(
    'description' => 'Stores the templates used to format emails sent to various members of the submissions',
    'fields' => array(
      'id' => array(
        'description' => 'A unique identifier for this email template',
        'type' => 'serial',
      ),
      'process' => array(
        'description' => 'The process this email template is associated with',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The name of this email template as displayed to the user',
        'type' => 'varchar',
        'length' => 256,
        'not null' => TRUE,
      ),
      'from_user' => array(
        'description' => 'The uid of the Drupal user this email will be sent from',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'subject' => array(
        'description' => 'The subject of this email',
        'type' => 'varchar',
        'length' => 1024,
        'not null' => FALSE,
      ),
      'template' => array(
        'description' => 'The body of the email template',
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id', 'process'),
    'foreign keys' => array(
      'process' => array(
        'table' => 'etd_processes',
        'columns' => array('process' => 'id'),
      ),
      'foreign keys' => array(
        'from_user' => array(
          'table' => 'users',
          'column' => 'uid',
        )
      )
    ),
  );

  $schema['etd_email_addresses'] = array(
    'description' => 'Used for saving email addresses of people and organizations external to the system',
    'fields' => array(
       'id' => array(
         'description' => 'The id of the email address',
         'type' => 'serial',
         'not null' => TRUE,
         'unsigned' => TRUE,
       ),
       'name' => array(
         'description' => 'The name of the person or organization',
         'type' => 'varchar',
         'length' => 256,
         'not null' => TRUE,
       ),
       'email' => array(
         'description' => 'The email address',
         'type' => 'varchar',
         'length' => 256,
         'not null' => TRUE,
       ),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}

/**
 * Implements hook_install() 
 */
function jarrow_email_install() {
  //Create a new filter which will be based on the filtered html format that
  //comes with the standard Drupal install (see standard_install() in the
  //standard profile).
  $jarrow_email_format = (object) array(
        'format' => 'jarrow_email',
        'name' => 'Jarrow Email Template',
        'weight' => 11,
        'filters' => array(
          'filter_url' => array(
            'weight' => 0,
            'status' => 1,
          ),
          'filter_html' => array(
            'weight' => 1,
            'status' => 1,
          ),
          'filter_autop' => array(
            'weight' => 2,
            'status' => 1,
          ),
          'filter_html_corrector' => array(
            'weight' => 10,
            'status' => 1,
          ),
          'filter_html_escape' => array(
            'weight' => 10,
            'status' => 1,
          ),
        )
  );
  filter_format_save($jarrow_email_format);
}

/**
 * Implements hook_uninstall() 
 */
function jarrow_email_uninstall() {
  //Delete the only variable we're storing
  variable_del('jarrow_email_user');
  //Drupal doesn't allow us to delete input formats see filter_format_disable() 
  //in filter.module, so we'll leave it alone.
}

/**
 *
 * Add new database table - email addresses.
 */
function jarrow_email_update_7001() {

 db_create_table('etd_email_addresses', array(
     'description' => 'Used for saving email addresses of people and organizations external to the system',
     'fields' => array(
       'id' => array(
         'description' => 'The id of the email address',
         'type' => 'serial',
         'not null' => TRUE,
         'unsigned' => TRUE,
       ),
       'name' => array(
         'description' => 'The name of the person or organization',
         'type' => 'varchar',
         'length' => 256,
         'not null' => TRUE,
       ),
       'email' => array(
         'description' => 'The email address',
         'type' => 'varchar',
         'length' => 256,
         'not null' => TRUE,
       ),
     ),
     'primary key' => array('id'),
   ));

}

