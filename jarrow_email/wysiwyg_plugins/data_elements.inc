<?php

/**
 * @file
 * Wysiwyg API integration on behalf of Node module.
 */

/**
 * Implementation of hook_wysiwyg_plugin().
 */
function jarrow_email_data_elements_plugin() {
  $plugins['jarrow_data_elements'] = array(
    'title' => t('Jarrow Data Elements'),
    'vendor url' => 'http://code.library.unbc.ca',
    'icon file' => 'break.gif',
    'icon title' => t('Insert a data element into the content'),
    'settings' => array(),
  );
  return $plugins;
}

