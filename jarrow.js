/**
 * @file
 *  Provides some functionality required for all Jarrow related pages.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

var jarrow = new function () {

    /*
     * Create a private function for splitting the form name
     *
     */

    var splitFormName = function (formName) {

        var nameregex = /\[([a-zA-Z0-9_]+)\]((\[[a-zA-Z0-9_][a-zA-Z0-9_]*\])+)?/,
            matches = nameregex.exec(formName),
            result;
        if (matches === null) {
            return null;
        }
        if (matches[1] === undefined) {
            result = [];
        } else if (matches[2] === undefined) {
            result = [matches[1]];
        } else {
            result = [matches[1]].concat(splitFormName(matches[2]));
        }
        return result;
    };

    /**
     * Create the array form elements.
     *
     * @param context
     * @returns object
     */
    this.createArrayFromFormElements = function (context) {

        var formArray = {};
        if (context === undefined) {
            context = document.body;
        }

        jQuery(context).find('input,select,textarea').each(function () {
            var element_name = jQuery(this).attr('name'),
                isCheckbox = jQuery(this).attr('type') === 'checkbox',
                isRadioButton = jQuery(this).attr('type') === 'radio',
                matches,
                indexes,
                parentArray,
                descriptor,
                names,
                i;

            if (element_name.length > 0) {
               // var names = element_name.split( /(?=\[)/ );
                // matches = /([a-zA-Z_][a-zA-Z0-9_]*)((\[[a-zA-Z0-9_][a-zA-Z0-9_]*\])+)?/.exec(element_name);
               // matches = /([a-zA-Z0-9_]*)((\[[a-zA-Z0-9_\.\s]*\])+)*/.exec(element_name);
                matches = /([\w]*)((\[[\w\.\s]*\])+)*/.exec(element_name);
                descriptor = element_name.substring(matches[1].length, element_name.length);
                names = [matches[1], descriptor.replace( /[\[|\]]/g,'')];
                /*
                console.log('****** next loop ******');
                console.log(element_name, 'element name');
                console.log(isCheckbox, 'is checkbox');
                console.log(descriptor, 'descriptor');
                console.log(formArray, 'formArray');
                console.log(matches, 'matches');
                console.log(names, 'names');
                */
                if (matches[2] === undefined) {
                    indexes = [matches[1]];
                } else {
                    // Need to set up indexes this way for the triggered/action elements
                    indexes = [matches[1]].concat(splitFormName(matches[2]));
                    //indexes = names;
                }
                //console.log(indexes, 'indexes');
                if (formArray[indexes[0]] === undefined) {
                    //console.log('formArray[indexes[0]] is undefinded');
                    if (indexes[1] === undefined) {
                        //console.log('[indexes[1] is undefinded');
                        //if(!isCheckbox || jQuery(this).attr('checked')) {
                        if (isCheckbox) {
                            formArray[indexes[0]] = jQuery(this).attr('checked') ? '1' : '0';
                            /* } else if (isRadioButton) {
                             formArray[indexes[0]] = jQuery(this).attr('checked') ? 0: 1;*/
                        } else {
                            formArray[indexes[0]] = jQuery(this).val();
                        }
                    } else {
                        formArray[indexes[0]] = {};
                    }
                }
                parentArray = formArray[indexes[0]];
                //console.log(parentArray, 'parentArray');
                for (i = 1; i < indexes.length; i++) {
                    if (indexes[i] === undefined) {
                        break;
                    }
                    //console.log(indexes[i], " = indexes[" + i + "]");
                    if (!parentArray[indexes[i]]) {
                        if (indexes[i + 1] === undefined) {
                            //console.log('[indexes[' + (i + 1) + '] is undefined');
                            //if (!isCheckbox || jQuery(this).attr('checked')) {
                            if (isCheckbox) {
                                if (matches[1] === 'type_config') {
                                    //parentArray[matches[2]] = jQuery(this).attr('checked') ? 1 : 0;
                                    parentArray[descriptor] = jQuery(this).attr('checked') ? '1' : '0';
                                } else {
                                    parentArray[indexes[i]] = jQuery(this).attr('checked') ? '1' : '0';
                                }
                            } else {
                                parentArray[indexes[i]] = jQuery(this).val();
                            }
                        } else {
                            parentArray[indexes[i]] = {};
                        }
                    }
                    parentArray = parentArray[indexes[i]];
                }
            }

        });
        return formArray;
    };
};
