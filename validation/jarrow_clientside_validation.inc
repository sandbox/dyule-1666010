<?php


/**
 * Implements hook_fapi_validation_rules().
 */

function jarrow_fapi_validation_rules() {

  return array(
    'word_count' => array(
      'callback' => 'jarrow_wordcount_validation',
      'error_msg' => 'Too many words in %field',
    ),
    'type_validation' => array(
      'callback' => 'jarrow_data_type_validator',
      'error_msg' => t('%field is not a valid value for the selected text type.'),
    ),
  );
}

/**
 * @param $value
 * @param $params
 *
 * @return bool
 */
function jarrow_wordcount_validation($value, $params) {

  //if params is not set then assume it mean unlimited words can be entered.
  if (strlen($params[0]) == 0) {
    return TRUE;
  }
  $word_count = count(preg_split("/\\S+/", trim($value)));
  return $word_count <= intval($params[0]);
}

/**
 * Validation callback function.
 */

function jarrow_data_type_validator($value, $params, &$element, &$form_state) {

  if (isset($form_state['values']['type_config']['format'])) {
    $rule = $form_state['values']['type_config']['format'];
    if (!jarrow_validation_apply_fapi_validation($rule, $value)) {
     // dsm('answer is wrong');
      form_set_error($element['#name'], jarrow_validation_get_textfield_error_message($rule));
      //return false;
    }
    if ($params[0] != $rule) {
//      $form_state['rebuild'] = TRUE;
    }
  }
  return true;
}

/**
 * Ensure the value conforms to the rule specified.
 *
 * @param $rule
 * @param $value
 *
 * @return mixed
 */
function jarrow_validation_apply_fapi_validation($rule, $value) {

  module_load_include('module', 'fapi_validation');

  switch ($rule) {
    case 'numeric':
      return fapi_validation_rule_numeric($value);

    case 'alpha':
      return fapi_validation_rule_alpha($value);

    case 'alpha_numeric':
      return fapi_validation_rule_alpha_numeric($value);

    case 'email':
      return fapi_validation_rule_email($value);

    case 'length':
      return fapi_validation_rule_length($value);

    case 'chars':
      return fapi_validation_rule_chars($value);

    case 'url':
      return fapi_validation_rule_url($value);

    case 'ipv4':
      return fapi_validation_rule_ipv4($value);

    case 'alpha_dash':
      return fapi_validation_rule_alpha_dash($value);

    case 'digit':
      return fapi_validation_rule_digit($value);

    case 'decimal':
      return fapi_validation_rule_decimal($value);

    case 'regexp':
      return fapi_validation_rule_regexp($value);
  }
  return TRUE;
}

/**
 * Add the correct javascript handling for all validation added by jarrow.
 *
 * @param array $js_rules
 *  The rules to add to the array for.
 * @param array $element
 *  The element to add the rules to
 * @param array $context
 *  Additional context information.  Includes the following elements: 'type' => 'fapi', 'rule' => $rule, 'message' => $message, 'params' => $params
 *    - type: The type of validation happening (eg: fapi)
 *    - rule: The name of the rule to process
 *    - message: The message to display when the rule fails
 *    - params: Any additional parameters.
 */

function jarrow_clientside_validation_rule_alter(&$js_rules, $element, $context) {

  switch ($context['type']) {
    case 'fapi':
      switch ($context['rule']['callback']) {
        case 'jarrow_wordcount_validation':
          _clientside_validation_set_minmax_words($element['#name'], $element['#title'], NULL, $context['params'][0], $js_rules);
          break;
        case 'jarrow_data_type_validator':
          _jarrow_validation_set_type_validator($element['#name'], $element['#title'], $context['params'][0], $js_rules);
          break;
      }
      break;
  }
}

/**
 * Set up the code necessary to perform client side validation
 *
 * @param string $name
 * @param title $title
 * @param $rule
 * @param array $js_rules
 */

function _jarrow_validation_set_type_validator($name, $title, $rule, &$js_rules) {


  drupal_add_js(drupal_get_path('module', 'jarrow') . '/validation/js/dynamic_textfield_type_validator.js');
  $title = variable_get('clientside_validation_prefix', '') . $title . variable_get('clientside_validation_suffix', '');
  $js_rules[$name]['typeValidator'] = array('type' => $rule);
  $js_rules[$name]['messages']['typeValidator'] = jarrow_validation_get_textfield_error_message($rule, $title);
}

/**
 * Return the appropriate error message based upon which validation rule is supplied.
 *
 * @param string $validation_rule
 * The validation rule that is currently being used.
 *
 * @param string $field
 * The name of the input field to assign the error to.
 *
 * @return string
 */
function jarrow_validation_get_textfield_error_message($validation_rule, $field = NULL) {

  $text = '';
  if ($validation_rule != 'any') {
    switch ($validation_rule) {
      case 'numeric':
        $message = 'numeric values';
        break;
      case 'email':
        $message = 'properly formatted email addresses';
        break;
      case 'url':
        $message = 'properly formatted url addresses';
        break;
      case 'ipv4':
        $message = 'properly formatted IP addresses';
        break;
      case 'alpha_numeric':
        $message = 'alpha numeric characters';
        break;
      case 'alpha_dash':
        $message = 'alphabetical characters and the dash character';
        break;
      case 'decimal':
        $message = 'decimal values';
        break;
      case 'digit':
        $message = 'integer values';
        break;
    }
    if (isset($message)) {
      $text = ($field == NULL) ? t('Please, only use ' . $message) :
        t('Please, only use ' . $message . ' for %field.', array('%field' => $field));
    }
  }
  return $text;
}
