///jQuery wrapper
(function ($) {

    // Create a function that we can call to perform the validation whenever we would like.
    var jarrowTextfieldValidator = function (event) {
        //Add your custom method with the 'addMethod' function of jQuery.validator
        //http://docs.jquery.com/Plugins/Validation/Validator/addMethod#namemethodmessage
        jQuery.validator.addMethod("typeValidator", function (value, element, param) {

            // Allow for an empty string.
            if ($.trim(value) === "") {
                return true;
            }
            // Else check to see that what is input is what is expected
            switch (param['type']) {
                case 'numeric':
                    return !isNaN(parseFloat(value)) && isFinite(value);
                case 'email':
                    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return pattern.test(value);
                case 'url':
                    var pattern = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/;
                    return pattern.test(value);
                case 'ipv4':
                    var pattern = /^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])'. '(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/;
                    return pattern.test(value);
                case 'alpha_numeric':
                    var pattern = /^[a-z0-9]+$/i;
                    return pattern.test(value);
                case 'alpha_dash':
                    var pattern = /^[-\sa-zA-Z]+$/;
                    return pattern.test(value);
                case 'decimal':
                    var pattern = /\d+\.\d+/;
                    return pattern.test(value);
                case 'digit':
                    var number = parseInt(value);
                    return (number === value) && (value % 1 == 0);
            }
            // If it gets to here, then assume it is okay.
            return true;
        }, jQuery.format('Field can not be empty'));
    };

    //Define a Drupal behaviour with a custom name
    Drupal.behaviors.jarrowDynamicTextfieldValidator = {
        attach: function (context) {
            //Add an eventlistener to the document reacting on the 'clientsideValidationAddCustomRules' event.
            $(document).bind('clientsideValidationAddCustomRules', jarrowTextfieldValidator);
        }
    };

    // Create our own event for the type_config[format] select because the change event is being eaten somewhere


    // Add a change listener to the specified select box.
    $(document).delegate("select[name='type_config[format]']", 'mouseup', function() {
        var selectedValue = $("select[name='type_config[format]']").find(":selected").val();
        console.log("the value you selected: " + selectedValue);
        $("input[name='type_config[type_config_container][default_answer]']").valid();
    });


})(jQuery);
