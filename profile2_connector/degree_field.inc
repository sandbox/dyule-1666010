<?php

/**
 * @file
 *  Provides a school/program/degree selection field, for use with Profiles2
 * 
 *  The provided field can be used with any entity, but is designed for
 *  integration with Profiles2.
 * 
 * @author 
 *  Daniel Yule <dyule@unbc.ca>
 * 
 * @copyright
 * 
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 * 
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in 
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */



function jarrow_get_degree_form_element($school_id = NULL, $program_id = NULL, $degree_id = NULL, $editable = TRUE) {
  include_once 'school_database.inc';
  $school_id = ($school_id == '0' || is_null($school_id)) ? NULL : intval($school_id);
  $program_id = $program_id == '0' ||is_null($program_id) || $school_id == NULL ? NULL : intval($program_id);
  $degree_id = $degree_id == '0'  ||is_null($degree_id)|| $program_id == NULL ? NULL : intval($degree_id);


  $schools = jarrow_database_etd_list_schools();
  $school_list = array(0 => '----');
  foreach ($schools as $s_id => $school) {
    $school_list[$s_id] = $school->name;
  }

  if ($school_id == NULL) {
    $programs = jarrow_database_etd_list_programs();
  }
  else {
    $programs = jarrow_database_etd_list_programs($school_id);
  }

  $program_list = array(0 => '----');
  foreach ($programs as $p_id => $program) {
    $program_list[$p_id] = $program->name;
  }

  if ($program_id == NULL) {
    $degrees = jarrow_database_etd_list_degrees();
  }
  else {
    $degrees = jarrow_database_etd_list_degrees($program_id);
  }

  $degree_list = array(0 => '----');
  foreach ($degrees as $d_id => $degree) {
    $degree_list[$d_id] = $degree->name;
  }

  $program_display = ($school_id == NULL) ? 'none' : 'inline';

  $degree_display = ($program_id == NULL || $school_id == NULL) ? 'none' : 'inline';

  if ($editable) {

    $degree_set = array(
      'school' => array(
        '#type' => 'select',
        '#title' => t('School'),
        //'#tree' => TRUE,
   //     '#parents' => array('etd', 'school'),
        '#options' => $school_list,
        '#default_value' => $school_id == NULL ? 0 : $school_id,
        '#required' => TRUE,
        '#ajax' => array(
          'callback' => 'jarrow_degree_info_output',
          'wrapper' => 'degree_info',
          'method' => 'replace',
          'effect' => 'none'
        )
      ),
      'program' => array(
        '#type' => 'select',
        '#title' => t('Program'),
   //     '#tree' => TRUE,
 //       '#parents' => array('etd', 'program'),
        '#prefix' => "<span style='display: $program_display'>",
        '#suffix' => '</span>',
        '#required' => TRUE,
        '#options' => $program_list,
        '#default_value' => $program_id == NULL ? 0 : $program_id,
        '#ajax' => array(
          'callback' => 'jarrow_degree_info_output',
          'wrapper' => 'degree_info',
          'method' => 'replace',
          'effect' => 'none'
        )
      ),
      'degree' => array(
        '#type' => 'select',
        '#title' => t('Degree'),
//        '#tree' => TRUE,
 //       '#parents' => array('etd', 'degree'),
        '#prefix' => "<span style='display: $degree_display'>",
        '#suffix' => '</span>',
        '#required' => TRUE,
        '#options' => $degree_list,
        '#default_value' => $degree_id == NULL ? 0 : $degree_id
      )
    );
  }
  else {
    $degree_set = array(
      'school' => array(
        '#type' => 'item',
        '#title' => t('School'),
        '#value' => $school_id == NULL ? t('Unknown') : $school_list[$school_id],
      ),
      'program' => array(
        '#type' => 'item',
        '#title' => t('Program'),
        '#prefix' => "<span style='display: $program_display'>",
        '#suffix' => '</span>',
        '#value' => $program_id == NULL ? t('Unknown') : $program_list[$program_id],
      ),
      'degree' => array(
        '#type' => 'item',
        '#title' => t('Degree'),
        '#prefix' => "<span style='display: $degree_display'>",
        '#suffix' => '</span>',
        '#value' => $degree_id == NULL ? t('Unknown') : $degree_list[$degree_id],
      )
    );
  }

  return $degree_set;
}


function jarrow_degree_info_output($form, $form_state) {

  
  //Look for the element on an entity configuration page.
  foreach(element_children($form_state['field']) as $field_name) {
    $field_info = $form_state['field'][$field_name]['und'];
    if($field_info['field']['type'] == 'degree_program') {
      return $form['instance']['default_value_widget'][$field_name]['und'][0];
    }
  }
  
  //Look for the element on an entity display page.
  foreach(element_children($form_state['field']['#parents']) as $entity_name) {
    $child = $form_state['field']['#parents'][$entity_name]['#fields'];
    foreach(element_children($child) as $field_name) {
      $field_info = $child[$field_name]['und'];
      if($field_info['field']['type'] == 'degree_program') {
        return $form[$entity_name][$field_name]['und'][0];
      }
    }
  }
}

//function jarrow_degree_info_output() {
//
//  $form_element = jarrow_get_degree_form_element($_POST['school'], $_POST['program'], $_POST['degree']);
//
//  //Use the information saved on the form to help us pulling the correct form out of the database
//  $form_state = array('submitted' => FALSE);
//  $form_build_id = $_POST['form_build_id'];
//  //Retrieve the form and state from the cache based on the above information
//  if (!$form = form_get_cache($form_build_id, $form_state)) {
//    exit();
//  }
//
//  //Add out newly generated panel to the form
//  $form['etd']['degree_info'] = $form_element;
//  $form['etd']['degree_info']['#prefix'] = '<div id="degree_info">';
//  $form['etd']['degree_info']['#suffix'] = '</div>';
//
//  //Cache the form again.
//  form_set_cache($form_build_id, $form, $form_state);
//  $form += array(
//    '#post' => $_POST,
//    '#programmed' => FALSE,
//  );
//
//  // Rebuild the form.
//  $form = form_builder('poll_node_form', $form, $form_state);
//
//  //Retrieve the part of our form we want to send back.
//  $degree_form = $form['etd']['degree_info'];
//
//  //Get rid of the wrapper code so we don't fill the wrapper with a wrapper.
//  unset($degree_form['#prefix'], $degree_form['#suffix']);
//
//  //Render the degree panel
//  $output = drupal_render($degree_form);
//
//  //Send the resulting HTML to the client
//  drupal_json(array('status' => TRUE, 'data' => $output));
//}
