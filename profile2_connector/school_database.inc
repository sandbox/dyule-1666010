<?php

/**
 * @file
 *  Gives the backend used for configuring schools in the Jarrow System.
 * 
 * @author 
 *  Daniel Yule <dyule@unbc.ca>
 * 
 * @copyright
 * 
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 * 
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in 
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */



/**
 * Since the fallback for user information is to request it on the user form, this function supplies nothing, and in
 * doing so will cause the user to be prompted to enter all the information.
 * @param stdObj $user_account The user account associated with the user.
 * @param array $input_data Data that has been supplied by the user.
 * @return array An array containing one refence to the user id.
 */
function jarrow_database_etd_user_info($user_account, $input_data) {
  if(isset($input_data['etd']) && count($input_data['etd']) > 0) {
    $user_array = $input_data['etd'];
  } else {
    if(isset($user_account->data['jarrow_etd'])) {
    $user_array = ((array) $user_account->data['jarrow_etd']);
    } else {
      $user_array = array();
    }
  }
  
  
  return $user_array + array('user_id' => $user_account->name);
}

/**
 * Returns a list of roles the system defines.  Must include 'student'
 * 
 * Skeleton Implementation.
 * 
 * @todo Build in a role editing component.
 * @return type 
 */
function jarrow_database_etd_roles() {
  return array('student' => 'Student') +
  (array)json_decode(variable_get('jarrow_role_list'));
//  return array(
//    'student' => t('Student'),
//    'supervisor' => t('Supervisor'),
//    'committee' => t('Committee Member'),
//    'chair' => t('Defense Chair'),
//    'overseer' => t('Overseer'),
//  );
}

/**
 *Informs the module that the user's core attributes can be edited through the user screen
 * @return boolean TRUE
 */
function jarrow_database_etd_edit_user_permitted() {
  return TRUE;
}

/**
 * Implementation of hook_etd_list_schools.
 * 
 * Lists the schools currently stored in the database.
 * @return array An array with keys corresponding to school ids and values corresponding to school objects.
 */
function jarrow_database_etd_list_schools() {
  $query = db_select('etd_schools');
  $query->fields('etd_schools', array('id', 'name', 'short_name'));
  $query->orderBy('name');
  $result = $query->execute();
  $schools = array();
  foreach ($result as $school) {
  #while ($school = db_fetch_object($result)) {
    $schools[$school->id] = $school;
  }
  return $schools;
}

/**
 * Implementation of hook_etd_add_school.
 * 
 * Adds the given school to the database
 * @param string $school_name The name of the school to insert
 * @return int The id of the inserted school, or -1 if the insertion failed.
 */
function jarrow_database_etd_add_school($school_name, $short_name) {
  //$result = db_query('INSERT INTO {etd_schools} (name, short_name) VALUES (\'%s\', \'%s\');', $school_name, $short_name);
  $sid = db_insert('etd_schools')->fields(array('name' => $school_name, 'short_name' => $short_name))->execute();
 
  return $sid;
}

/**
 * Gets the name of the school associated with the given id.
 * @param int $school_id An integer representing the schools id.
 * @return stdObj An object representing the school or false if there was no school associated with the given id.
 */
function jarrow_database_etd_view_school($school_id) {
  $school_id = intval($school_id);
  $result = db_select('etd_schools')->fields('etd_schools', array('name', 'short_name'))->condition('id', $school_id, '=')->execute();
  
  return $result->fetch();
}

/**
 *Updates school information with the supplied data
 * @param int $school_id The if of the school to update
 * @param string $school_name The name of the school
 * @param string $school_short_name The abbreviated name of the school.
 */
function jarrow_database_etd_edit_school($school_id, $school_name, $school_short_name) {
  $school_id = intval($school_id);
  #$result = db_query('UPDATE {etd_schools} SET name=\'%s\', short_name=\'%s\' WHERE id=%d', $school_name, $school_short_name, $school_id);
  $result = db_update('etd_schools')->fields(array('name'=> $school_name, 'short_name' => $school_short_name))->condition('id', $school_id)->execute();
  return $result !== NULL;
}

/**
 * Deletes an entire school, along with all programs offered by that school
 * @param int $school_id The id of the school to delete
 */
function jarrow_database_etd_delete_school($school_id) {
  $programs = jarrow_database_etd_list_programs($school_id);
  foreach ($programs as $program_id => $program) {
    jarrow_database_etd_delete_program($program_id);
  }
  #return db_query('DELETE FROM {etd_schools} WHERE id=%d', $school_id);
  return db_delete('etd_schools')->condition('id', $school_id)->execute();
}

/**
 * Gets a list of all the programs associated with a given school and optionally with a given degree.
 * @param int $school_id The id of the school we want a list of.  If NULL, list all programs
 * @param int $degree_id The id of the degree we want a listing of programs associated with.  If NULL, list all programs at the given school.
 * @return An array with keys representing to program ids and values consisting of the program name.
 */
function jarrow_database_etd_list_programs($school_id = NULL, $degree_id = NULL) {
  $query = db_select('etd_programs', 'progs')->fields('progs', array('id', 'name', 'short_name'));
  
  if($school_id == NULL){
    if ($degree_id != NULL) {
      $alias = $query->innerJoin('etd_degree_associations', 'eda', 'progs.id = eda.program_id');
      $query->condition("$alias.degree_id", $degree_id, '=');
    }
  } else {
    if ($degree_id == NULL) {
      $query = $query->condition('progs.school_id', $school_id, '=');
    }
    else {
      $alias = $query->innerJoin('etd_degree_associations', 'eda', 'progs.id = eda.program_id');
      $query->condition(db_and()->condition("$alias.degree_id", $degree_id, '=')->condition('progs.school_id', $school_id, '='));
    }
  }
  $result = $query->execute();
  $programs = array();
  #while ($row = db_fetch_object($result)) {
  foreach($result as $program) {
    $programs[$program->id] = $program;
  }

  return $programs;
}

function jarrow_database_etd_add_program($program_name, $short_name, $school_id) {
  $school_id = intval($school_id);
  #if (jarrow_database_etd_view_school($school_id)) {
  #  $result = db_query('INSERT INTO {etd_programs} (name, short_name, school_id) VALUES (\'%s\', \'%s\', %d)', $program_name, $short_name, $school_id);
  #  if ($result)
  #  //This function is deprecated in D7
  #    return db_last_insert_id('etd_programs', 'id');
  #}
  #return -1;
  
  $pid = db_insert('etd_programs')->fields(array('name' => $program_name, 'short_name' => $short_name, 'school_id'=>$school_id))->execute();
  
  return $pid;
}

/**
 * Updates the information associated with a given program
 * @param int $program_id The ID of the program to update
 * @param string $program_name The updated name of the program
 * @param string $program_short_name The abbreviated name of the program
 * @param int $school_id The id of the school this program should be associated with.
 * @param array $degree_list The listing of degrees associated with this program, represented by a boolean array.
 * @return boolean Whether or not the update was successful 
 */
function jarrow_database_etd_edit_program($program_id, $program_name, $program_short_name, $school_id, $degree_list) {
  $program_id = intval($program_id);
  $result = db_update('etd_programs')->fields(array('name' => $program_name, 'short_name' => $program_short_name, 'school_id' => $school_id))->condition('id', $program_id)->execute();
  if ($result !== NULL && $degree_list) {
    $result = db_delete('etd_degree_associations')->condition('program_id', $program_id)->execute();
    foreach ($degree_list as $degree_id => $enabled) {
      if ($enabled)
        $result = db_insert('etd_degree_associations')->fields (array('degree_id'=> $degree_id, 'program_id'=> $program_id))->execute ();
    }
  }
  return $result !== NULL;
}

/**
 * Gets the name of the program associated with the given id.
 * @param int $program_id An integer representing the program's id.
 * @return string A string representing the name of the program or false if there was no program associated with the given id.
 */
function jarrow_database_etd_view_program($program_id) {
  $program_id = intval($program_id);
#  $result = db_query_range('SELECT name, short_name, school_id FROM {etd_programs} WHERE id=%d', $program_id, 0, 1);

  $result = db_select('etd_programs')->fields('etd_programs', array('name', 'short_name', 'school_id'))->condition('id', $program_id)->execute();
  
  return $result->fetch();
  
}

/**
 * Deletes a program and all degree associations
 * @param int $program_id The id of the program we are removing
 * @return boolean True if the removal was successful, false otherwise. 
 */
function jarrow_database_etd_delete_program($program_id) {
  $program_id = intval($program_id);
  #$result = db_query('DELETE FROM {etd_programs} WHERE id=%d', $program_id);
  $result = db_delete('etd_programs')->condition('id', $program_id)->execute();
  if ($result) {
    #$result = db_query('DELETE FROM {etd_degree_associations} WHERE program_id=%d', $program_id);
    $result = db_delete('etd_degree_associations')->condition('program_id', $program_id);
    return $result != FALSE;
  }
}

function jarrow_database_etd_list_degrees($program_id = NULL) {
  $query = db_select('etd_degrees', 'deg')->fields('deg', array('id', 'name', 'short_name'));
  if ($program_id != NULL) {
    $alias = $query->innerJoin('etd_degree_associations', 'eda', 'deg.id=eda.degree_id');
    $query = $query->condition("$alias.program_id", $program_id);
  }
  $query = $query->orderBy('deg.name');
  $result = $query->execute();
  $degrees = array();
  foreach ($result as $degree) {
    $degrees[$degree->id] = $degree;
  }
  return $degrees;
}

function jarrow_database_etd_add_degree($degree_name, $degree_short_name) {
    $did = db_insert('etd_degrees')->fields(array('name' => $degree_name, 'short_name' => $degree_short_name))->execute();
  
  return $did;
}

function jarrow_database_etd_add_degree_association($degree_id, $program_id) {
  $did = db_insert('etd_degree_associations')->fields(array('program_id' => $program_id, 'degree_id' => $degree_id))->execute();
  return $did;
}

function jarrow_database_etd_view_degree($degree_id) {
  $degree_id = intval($degree_id);
  #$result = db_query_range('SELECT name, short_name FROM {etd_degrees} WHERE id=%d', $degree_id, 0, 1);

  #return db_fetch_object($result);
  
  $result = db_select('etd_degrees')->fields('etd_degrees', array('name', 'short_name'))->condition('id', $degree_id)->execute();
  
  return $result->fetch();
}

function jarrow_database_etd_edit_degree($degree_id, $degree_name, $degree_short_name, $program_list) {
  $degree_id = intval($degree_id);
  #$result = db_query('UPDATE {etd_degrees} SET name=\'%s\', short_name=\'%s\' WHERE id=%d', $degree_name, $degree_short_name, $degree_id);

  $result = db_update('etd_degrees')->fields(array('name' => $degree_name, 'short_name' =>$degree_short_name))->condition('id', $degree_id)->execute();
  if ($result !== NULL && $program_list) {
    #$result = db_query('DELETE FROM {etd_degree_associations} WHERE degree_id=%d', $degree_id);
    $result = db_delete('etd_degree_associations')->condition('degree_id', $degree_id)->execute();
    foreach ($program_list as $program_id => $enabled) {
      if ($enabled)
        #$result = db_query('INSERT INTO {etd_degree_associations} (degree_id, program_id) VALUES (%d, %d)', $degree_id, $program_id);
        $result = db_insert('etd_degree_associations')->fields (array('degree_id'=> $degree_id, 'program_id'=> $program_id))->execute ();
        
    }
  }
  return $result !== NULL;
}

function jarrow_database_etd_delete_degree($degree_id) {
  $degree_id = intval($degree_id);
//  $result = db_query('DELETE FROM {etd_degrees} WHERE id=%d', $degree_id);
//  if ($result) {
//    $result = db_query('DELETE FROM {etd_degree_associations} WHERE degree_id=%d', $degree_id);
//    return $result != FALSE;
//  }
  
  $result = db_delete('etd_degrees')->condition('id', $degree_id)->execute();
  if ($result) {
    $result = db_delete('etd_degree_associations')->condition('degree_id', $degree_id);
    return $result != FALSE;
  }
}