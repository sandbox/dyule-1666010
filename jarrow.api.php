<?php
/**
 * @defgroup jarrow_etd Jarrow ETD Module Integration
 * @{
 * Documentation for building modules to integrate with the Jarrow ETD Module
 * 
 * 
 * The Jarrow ETD Module has been designed to make integration as intuitive as 
 * possible for Drupal developers, following standard conventions and ideas.  
 * Jarrow defines many integraton points, most of which are implemented as
 * hook_TYPE_info() style module hooks.
 * 
 * A strong understanding of the underlying architecture of Jarrow is highly
 * recommended prior to building a new module to integrate with it.  This can be
 * found at /jarrow/docs/architecture/
 * @}
 */