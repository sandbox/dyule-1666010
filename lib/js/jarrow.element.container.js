/**
 * @file
 *  Defines the controller for an Element Container.
 *
 *  An Element Container is a possibly hierarchical collection of elements, each
 *  of which has attributes which can be edited.  This file provides a widget
 *  which can be attached to a page to provide element creation, manipulation and
 *  deletion.
 *
 *  This element requires a div which will hold the generated elements and a form
 *  which will be displayed to configure each of the elements.  The data used to
 *  generate the elements and the form used to configure them are expected to
 *  have a certain structure.
 *
 *  In particular, each element of the form that the user can configure
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca> Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

(function ($) {
    $.widget('ui.elementContainer', {
        //function(containerElementSelector, configPanelSelector, $messageField, dataSource , $)
        default_text: '(' + Drupal.t('blank') + ')',

        /**
         * Load the form elements that are the children of other elements.
         *
         * @param element
         * @param prefix
         * @param suffix
         * @param context
         * @private
         */
        _loadFormElementChildren: function (element, prefix, suffix, context) {
            if (!$.isPlainObject(element)) {
                return;
            }
            var child,
                attributeSearch,
                container = this,
                checkPattern = new RegExp("^" + prefix.replace("[", "\\[") + "[^\\]]*\\]\\[[^\\]]*\\]$");

            function setCheckboxes(key, value) {
                var i,
                    attribute,
                    val,
                    name = $(value).attr('name'),
                    prefix_name = prefix.substring(0, (prefix.indexOf('[') > 0) ? prefix.indexOf('[') : prefix.length),
                    name_array = name.substring(prefix.length, name.length - 1).split('][', 2);
                /*
                 console.log('****start of function****');
                 console.log(element, 'element');
                 console.log(key, 'loadFormElementChildren: key');
                 console.log(value, 'loadFormElementChildren: value');
                 console.log(child, 'loadFormElementChildren: child');
                 console.log(prefix_name, 'loadFormElementChildren: prefix');
                 console.log('loadFormElementChildren: name: ' + name);
                 console.log('loadFormElementChildren: reg expre is: ' + "^" + prefix.replace("[", "\\[") + "[^\\]]*\\]\\[[^\\]]*\\]$");
                */
                $(value).unbind('change');

                if (checkPattern.test(name) && (name === (prefix_name + child))) {
                    if ($.isArray(element[child])) {
                        /*
                        attribute = name_array[0];
                        val = name_array[1];

                         console.log('element[child] is an array');
                         console.log('element[child] is '+element[child]);
                         console.log('attr = '+attribute);
                         console.log('val = '+val);

                        $(value).removeAttr('checked');
                        for (i = 0; i < element[child].length; i++) {
                            if (element[child][i] == val) {
                                $(value).attr('checked', 'checked');
                            }
                        }
                        */
                    } else {
                        // Take care of the case where the selected value is in the
                        // type or source config portion of the form.
                        if (element.hasOwnProperty(child)) {
                            if (element[child] == 1) {
                                $(value).attr('checked', 'checked');
                            } else {
                                $(value).removeAttr('checked');
                            }
                        }
                    }
                }
            }

            var checkboxes = context.find('input[type="checkbox"]');
            for (child in element) {
                checkboxes.each(setCheckboxes);
                attributeSearch = '[name="' + prefix + child + suffix + '"]';
                context.find('input' + attributeSearch + ',textarea' + attributeSearch + ',select' + attributeSearch)
                    .val(element[child])
                    .change();
                container._loadFormElementChildren(element[child], prefix + child + '][', suffix, context);
            }
        },

        /**
         *
         * @param id
         * @private
         */
        _removeElementByID: function (id) {
            var c_element,
                container = this;
            this.options.dataSource.deleteElement(id);
            if (this.cur_element && parseInt($(this.cur_element).attr('id').substring(3), 10) === id) {
                this.cur_element = null;
            }
            for (c_element in this.options.dataSource.getElements()) {
                if (this.options.dataSource.getElement(c_element).parent == id) {
                    container._removeElementByID(this.options.dataSource.getElement(c_element).id);
                }
            }
        },

        /**
         *
         * @param parent
         * @param $qElement
         * @param index
         * @private
         */
        _assignParentsFrom: function (parent, $qElement, index) {
            var container = this,
                id = $qElement.attr('id').substring(3);
            this.options.dataSource.getElement(id).parent = parent.id;
            this.options.dataSource.getElement(id).weight = index + 1;
            $qElement.children('div ul').first().children('.element_config_form').each(function (index, element) {
                container._assignParentsFrom(container.options.dataSource.getElement(id), $(element), index);
            });
        },

        /**
         *
         * @param parent
         * @returns {string}
         * @private
         */
        _generateHTMLForElements: function (parent) {

            var innerHTML = "",
                container = this,
                HTML,
                elements = this.options.dataSource.getElements(),
                sorted_elements = [];
            for (element in elements) {
                sorted_elements.push(elements[element]);
            }
            sorted_elements.sort(function (a, b) {
                return a.weight - b.weight;
            });
            $.each(sorted_elements, function (key, value) {
                var htmlElements;
                if (value.parent == parent) {
                    innerHTML += function (element) {
                        var insideHTML = '',
                            liClass = 'element_config_form';
                        if (Drupal.settings.jarrow.filter_field !== undefined) {
                            if (element[Drupal.settings.jarrow.filter_field] != undefined && element[Drupal.settings.jarrow.filter_field] != Drupal.settings.jarrow.filter_id) {
                                return '';
                            }
                        }
                        if (container.options.dataSource.isGroup(element)) {
                            htmlElements = container._generateHTMLForElements(element.id);
                            insideHTML += '<ul class="element_group">';
                            insideHTML += htmlElements;//.length > 0 ? htmlElements : '<li></li>';
                            insideHTML += '</ul>';
                            liClass += ' element_group_parent';
                        } else {
                            liClass += ' element_group_leaf';
                        }
                        HTML = '<li id="de_' + element.id + '" class="'
                            + liClass
                            + '"><div><span>'
                            + element.name + '</span><span class="element_type"> - '
                            + element.type_label
                            + '</span><span class="remove_element">'
                            + Drupal.t('remove') + '</span>'
                            + '<div style="clear:both"></div></div>'
                            + insideHTML
                            + '</li>';
                        return HTML;
                    }(value);
                }
            });
            return innerHTML;
        },

        /**
         * Expand or collapse an element group depending on its previous state.
         *
         * @param $groupElement
         * @private
         */
        _toggleElementGroup: function ($groupElement) {
            if ($groupElement.hasClass('eg-collapsed')) {
                $groupElement.animate({
                    'height': $groupElement.data('oldHeight')
                }, 'fast', undefined, function () {
                    $groupElement.removeClass('eg-collapsed');
                    //  $groupElement.removeClass('element_group_leaf'); //PH modification
                    $groupElement.addClass('eg-expanded');
                    $groupElement.height('auto');
                });
            } else {
                $groupElement.data('oldHeight', $groupElement.height());
                $groupElement.animate({
                    'height': '1em'
                }, 'fast');
                $groupElement.removeClass('eg-expanded');
                $groupElement.addClass('eg-collapsed');
                // $groupElement.addClass('element_group_leaf'); //PH modification
            }
        },

        /**
         * Create the dialog that permits the user to edit the settings
         * for a data model, form editor or process manager element. Also
         * set any bindings for any specific form elements.
         *
         *
         * @param element
         * @param context
         * @param suppressConfig
         */
        triggerEditPane: function (element, context, suppressConfig) {

            var container = this,
                configForm = this.options.configForm,
                id,
                attributeSearch,
                defaultAnswer,
                dataElementLinks,
                child;

            configForm.find('select').unbind('change', this.saveConfigForm);
            configForm.find('input,textarea').unbind('change', this.saveConfigForm);
            if (element !== undefined) {
                this.cur_element = element;
            }
            // Get the id of the element that was clicked
            id = $(this.cur_element).attr('id').substring(3);

            element = this.options.dataSource.getElement(id);
            // console.log(element, 'element');
            if (context === undefined) {
                context = configForm.children('form').children('div');
            }
            context.find('input[type="text"],textarea').val('');
            this.element.find('li').removeClass('selected_data_element');
            $(this.cur_element).addClass('selected_data_element');

            // Find all checkbox input elements and set whether they are checked or not
            //context.find('input[type="checkbox"]').each(function (key, value) {
            function setCheckboxes(key, value) {
                var val,
                    name = $(value).attr('name'),
                    name_prefix = name.substring(0, (name.indexOf('[') > 0) ? name.indexOf('[') : name.length),
                    name_array,
                    i;
                /*
                 console.log(element, 'element');
                 console.log(key, 'key');
                 console.log(value, 'value');
                 console.log(name, 'name');
                 console.log(name_prefix, 'name prefix');
                 console.log(child, 'child');
                */
                if ((child === name) || (child === name_prefix)) {
                    /*
                     console.log(element, 'element');
                     console.log(key, 'key');
                     console.log(value, 'value');
                     console.log(name, 'name');
                     console.log(name_prefix, 'name prefix');
                     console.log(child, 'child');
                     //$(value).removeAttr('checked');
                    */
                    if ($.isArray(element[child])) {
                        //PH I don't this section is ever called.
                        /*
                        name_array = name.substring(0, name.length).split('][', 2);
                        val = name_array[1];

                         console.log('name array: ' + name_array);
                         console.log(val, 'val');
                         console.log('element[child] is an array');
                         console.log(element[child]);
                        $(value).removeAttr('checked');
                        for (i = 0; i < element[child].length; i++) {
                            if (element[child][i] == val) {
                                $(value).attr('checked', 'checked');
                            }
                        }
                        */
                    } else {
                        for (var index in element[child]) {
                            if (element[child].hasOwnProperty(index)) {
                                if (name === (child + index)) {
                                    // Take care of the case where the selected value is in the
                                    // type or source config portion of the form.
                                    if (element[child][index] == 1) {
                                        $(value).attr('checked', 'checked');
                                    } else {
                                        $(value).removeAttr('checked');
                                    }
                                } else if (name === child) {
                                    // Take care of the case where the selected value is NOT in the
                                    // type or source config portion of the form.
                                    if (element[child] == 1) {
                                        $(value).attr('checked', 'checked');
                                    } else {
                                        $(value).removeAttr('checked');
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (element.type !== 'group') {
                var checkboxes = context.find('input[type="checkbox"]');
                for (child in element) {
                    // Go through and set the checkbox states.
                    checkboxes.each(setCheckboxes);
                    if (element.hasOwnProperty(child)) {
                        // if the element has children then load its children elements
                        attributeSearch = '[name="' + child + '"]';
                        context.find('input' + attributeSearch + ',select' + attributeSearch).val(element[child]);
                        container._loadFormElementChildren(element[child], child + '[', ']', context);
                    }
                }

                context.find('*').change();

                configForm.find(' select,input,textarea').change({
                        container: container
                    },
                    container.saveConfigForm
                );

                if (suppressConfig === undefined || !suppressConfig) {
                    configForm.dialog('open');
                }

                // Add a keyup listener to the default answer textbox so that if it isn't empty
                // it displays the hidden selector.
                $(':input[type="text"][name="type_config[type_config_container][default_answer]"]').live('keyup', function () {
                        var temp = $.trim($(this).val());
                        if (temp.length > 0) {
                            $('.default_answer_editable').show();
                        } else {
                            $('.default_answer_editable').hide();
                        }
                    }
                );

                // Ensure that the default answer is editable selector is displayed or
                // hidden appropriately when the form is first displayed.
                if ((element.hasOwnProperty('type_config')) && (element.type_config.hasOwnProperty('type_config_container'))) {
                    if (element.type_config.type_config_container.hasOwnProperty('default_answer')) {
                        defaultAnswer = $.trim(element.type_config.type_config_container.default_answer);
                        if (defaultAnswer.length > 0) {
                            $('.default_answer_editable').show();
                        } else {
                            $('.default_answer_editable').hide();
                        }
                    }
                }


                // Add a change listener to associated element link for dc mappings
                $(':input[type="text"][name="type_config[associated_element]"]').live('change', function () {
                    // Get the data element id
                    var dataElementID = $.trim($(this).val()),
                        dataElementLinks = Drupal.settings.jarrow.item.data_element_links;
                    if (dataElementLinks.hasOwnProperty(dataElementID)) {
                        $('#linked_form_elements').show();
                    } else {
                        $('#linked_form_elements').hide();
                    }
                });

                // Ensure that the default answer is editable selector is displayed or
                // hidden appropriately when the form is first displayed.
                if ((element.hasOwnProperty('type_config')) && (element.type_config.hasOwnProperty('associated_element'))) {
                    dataElementLinks = Drupal.settings.jarrow.item.data_element_links;
                    if (dataElementLinks.hasOwnProperty(element.type_config.associated_element)) {
                        $('#linked_form_elements').show();
                    } else {
                        $('#linked_form_elements').hide();
                    }
                }

            } else {
                configForm.dialog('close');
            }
        },

        /**
         *
         */
        assignParents: function () {
            var container = this,
                $qElement,
                id;
            this.unsaved_state = true;
            this.element.children('li').each(function (index, element) {
                $qElement = $(element);
                id = $qElement.attr('id').substring(3);
                container.options.dataSource.getElement(id).parent = null;
                container.options.dataSource.getElement(id).weight = index + 1;
                $qElement.children('div ul').first().children('.element_config_form').each(function (index, element) {
                    container._assignParentsFrom(container.options.dataSource.getElement(id), $(element), index);
                });
            });
        },

        /**
         * Set the flag that changes have been made and need to be saved.
         */
        requestSave: function () {
            this.unsaved_state = true;
        },

        /**
         * Create the data element list.
         */
        createDataElementList: function () {
            var innerHTML = this._generateHTMLForElements(null);

            this.element.html(innerHTML);

            $('.element_group_parent').each(function (key, value) {
                var $element = $(value);
                $element.data('oldHeight', $element.height());
                $element.height('1em');
                $element.addClass('eg-collapsed');
                // $element.addClass('element_group_leaf'); //PH modification
            });
        },

        /**
         * The user has dragged and dropped an element into the configuration pane.
         * Add the element to the list and open the editor.
         *
         * @param element
         * @param replacementElement
         * @param suppressConfig
         */
        addElement: function (element, replacementElement, suppressConfig) {
            var container = this,
                $newElement,
                $innerDiv,
                $spanText,
                $spanType,
                $newGroup,
                $removeElement;
            $newElement = jQuery('<li/>', {
                id: "de_" + element.id,
                "class": "element_config_form"
            });
            replacementElement.after($newElement);
            $innerDiv = $('<div/>');
            $newElement.append($innerDiv);
            $spanText = jQuery('<span/>', {
                text: element.name
            });
            $innerDiv.append($spanText);
            $spanType = jQuery('<span/>', {
                text: ' - ' + element.type_label,
                className: 'element_type'
            });
            $innerDiv.append($spanType);
            $removeElement = jQuery('<span/>', {
                "class": "remove_element"

            }).text(Drupal.t('remove'));
            $innerDiv.append($removeElement);
            $innerDiv.append($('<div/>', {
                style: 'clear: both'
            }));

            $spanText.editInPlace({
                    bg_over: 'transparent',
                    save_if_nothing_changed: true,
                    default_text: container.default_text,
                    callback: function (original_element, html) {
                        if (html.length === 0) {
                            return container.default_text;
                        }
                        var id = parseInt($(this.parentNode.parentNode).attr('id').substring(3), 10);
                        container.changeElementName(id, html);
                        container.last_moved = undefined;
                        return html;
                    }
                }
            );

            if (this.options.dataSource.isGroup(element)) {
                $spanText.click();
                $newGroup = $('<ul/>', {
                    'class': 'element_group'
                });
                $newElement.append($newGroup);
                $newGroup.mousemove(function (event) {
                    event.stopPropagation();
                });

                $newElement.addClass('element_group_parent');
            } else {
                $newElement.addClass('element_group_leaf');
                this.triggerEditPane($newElement, undefined, suppressConfig);
            }

        },

        /**
         * Remove the selected element from the configuration form.
         *
         * @param element
         */
        removeDataElement: function (element) {

            this.unsaved_state = true;
            var id = $(element.parentNode.parentNode).attr('id').substring(3);
            this._removeElementByID(id);
            $(element.parentNode.parentNode).remove();
        },

        /**
         * Change the name of the element identified by id to its new name.
         *
         * @param id
         * @param newName
         */
        changeElementName: function (id, newName) {
            this.unsaved_state = true;
            this.options.dataSource.getElement(id).name = newName;
        },

        /**
         * Save the changes made to the configuration form.
         *
         * @param event
         */
        saveConfigForm: function (event) {

            function objects_equal(obj1, obj2) {
                var prop;
                if ($.isPlainObject(obj1) && $.isPlainObject(obj2)) {
                    console.log('is plain object');
                    for (prop in obj1) {
                        if (obj1.hasOwnProperty(prop)) {
                            console.log(obj1.hasOwnProperty(prop), 'obj 1 has property '+prop);
                            console.log(obj2[prop], 'obj 2 property');
                            if (obj2[prop] === undefined) {
                                console.log('object 2 property undefined returning false');
                                return false;
                            } else if (!objects_equal(obj1[prop], obj2[prop])) {
                                console.log('objects do not equal  returning false');
                                return false;
                            }
                        }
                    }
                    for (prop in obj2) {
                        if (obj2.hasOwnProperty(prop)) {
                            console.log(obj2.hasOwnProperty(prop), 'obj 2 has property '+prop);
                            console.log(obj1[prop], 'obj 1 property');
                            if (obj1[prop] === undefined) {
                                console.log('object 1 property undefined returning false');
                                return false;
                            }
                        }
                    }
                    console.log('objects equal returning true');
                    return true;
                } else if (obj1 === obj2) {
                    console.log('objects do equal -> returning true');
                    return true;
                }
                console.log('just returning false');
                return false;
            }

            var container = event.data.container,
                id,
                element,
                child,
                form_array;

            if (container.cur_element) {
                id = $(container.cur_element).attr('id').substring(3);
                element = container.options.dataSource.getElement(id);
                form_array = jarrow.createArrayFromFormElements();
                console.log('form_array ', form_array);
                console.log('element ', element);
                // Compare each element int the original form_array with the currently displayed form
                // (ie the element variable) and check if there are any differences between the two.
                // If so then set the unsaved state to true.
                for (child in element) {
                    if (form_array[child] !== undefined) {
                        console.log('form_array['+child+'] = ', form_array[child]);
                        console.log('element['+child+'] = ', element[child]);
                        if (!objects_equal(element[child], form_array[child])) {
                            console.log('form_array['+child+'] != element['+child+']');
                            element[child] = form_array[child];
                            container.unsaved_state = true;
                        }
                    }
                }
                element.type_label = $('select[name="type"] option:selected').text();
                $(container.cur_element).find('span:first-child').text(element.name);
                $(container.cur_element).find('span.element_type').text(' - ' + element.type_label);
            }
        },

        /**
         * Save the element collection as a serialized array and post this information back
         * to the server.
         *
         * @param saveAsDraft
         */
        saveCollection: function (saveAsDraft) {
            var container = this,
                serialized_collection;
            if (undefined === saveAsDraft) {
                saveAsDraft = false;
            }
            if (saveAsDraft) {
                setTimeout(function () {
                    container.saveCollection(true);
                }, 2000);
                if (!container.unsaved_state) {
                    return;
                }
            }
            this.element.trigger('savebegin');
            this.unsaved_state = false;
            serialized_collection = this.options.dataSource.serialize();
            $.post(this.options.saveURL, {
                draft: saveAsDraft,
                collection: serialized_collection

            }, function (data) {
                if (data.success) {
                    container.element.trigger('savecomplete', [true, saveAsDraft]);
                } else {
                    container.element.trigger('savecomplete', [false, saveAsDraft]);
                }
            });
        },

        /**
         * Initialize the configuration form. This is drop zone in which users drag and drop an element
         * into and then configure that element. This is used by the Data Model screen, Form Editor screen
         * and the Process Manager screen.
         *
         * @private
         */
        _init: function () {
            var container = this;
            this.unsaved_state = false;

            this.createDataElementList();

            this.element.addClass('element_group');
            $('#element_container>.element_group')
                .nestedSortable({
                    //connectWith: '.element_group_parent:not(.eg-collapsed)>.element_group, #element_container>.element_group',
                    placeholder: 'ui-sortable-placeholder', // the type of placeholder to use
                    forcePlaceholderSize: true,
                    tolerance: 'intersect', // what type of tolerance to use: intersect, pointer
                    handle: 'div', //PH added
                    //helper: 'clone', // not sure what this does
                    maxLevels: 0, // unlimited levels
                    listType: 'ul', // The list type used
                    disableNesting: 'element_group_leaf', //The class name of the items that will not accept nested lists
                    tabSize: 20, //The number of pixels left or right of the target in order to be nested or outside the current list.
                    items: 'li',
                    distance: 10,
                    toleranceElement: '> div', // The tolerance element to use when determining intersections
                    errorClass: 'ui-nestedSortable-error',
                    cursorAt: {
                        left: 0
                    },
                    stop: function (event, ui) {
                        var id_attr = ui.item.attr('id'),
                            parts;


                        if (id_attr.substring(0, 7) === 'itemadd') {
                            parts = id_attr.split(/-/, 3);
                            if (parts.length === 2) {
                                container.options.dataSource.createElement(parts[1], undefined, ui.item);
                            } else {
                                container.options.dataSource.createElement(parts[2], parts[1], ui.item);
                            }
                        } else if (id_attr.substring(0, 12) === 'data_element') {
                            parts = id_attr.split(/-/, 2);
                            container.options.dataSource.createDataFormElement(ui.item, parts[1]);
                        }

                        container.assignParents();
                    }
                });
            $('.element_config_form')
                .mousemove(function (event) {
                    //event.stopPropagation();
                });

            this.element.find('li span:first-child').editInPlace({
                    bg_over: 'transparent',
                    save_if_nothing_changed: true,
                    default_text: container.default_text,
                    callback: function (original_element, html) {
                        if (html.length === 0) {
                            return container.default_text;
                        }
                        var id = parseInt($(this.parentNode.parentNode).attr('id').substring(3), 10);
                        container.changeElementName(id, html);
                        return html;
                    }
                }
            );

            $('.element_config_form').live('click', function (event, ui) {
                if (event.target === this) {

                    if ($(this).hasClass('element_group_parent')) {

                        container._toggleElementGroup($(this));
                    }
                }
            });

            // An element has been double clicked so open up the appropriate editor pane
            $('.element_config_form').live('dblclick', function (event, ui) {
                if (event.target === this) {
                    if (!$(this).hasClass('element_group_parent')) {

                        container.triggerEditPane(this);
                    }
                    event.stopPropagation();
                }
            });

            $('.element_config_form>div').live('click', function (event) {
                $(this.parentNode).click();
            });
            $('.element_config_form>div').live('dblclick', function (event) {
                $(this.parentNode).dblclick();
            });
            $('.element_config_form').live('mousemove', function (event) {
                if (event.target !== container.last_moved) {
                    container.last_moved = event.target;
                    $('.element_config_form').removeClass('ui-hover');
                    $(this).addClass('ui-hover');
                }
            });
            $('.element_config_form').live('mouseout', function (event) {
                if (container.last_moved && !$.contains(container.last_moved, event.relatedTarget)) {
                    container.last_moved = null;
                    $(this).removeClass('ui-hover');
                    event.stopPropagation();
                }
            });
            $('.element_config_form .remove_element').live('click', function (event, ui) {
                container.removeDataElement(this);
                event.stopPropagation();
            });


            $('#config_form').dialog({
                autoOpen: false,
                modal: true,
                position: 'top',
                resizable: false,
                width: 600,
                buttons: [
                    {
                        text: Drupal.t('Close'),
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ],
                close: function () {
                    container.saveConfigForm({
                        data: {
                            container: container
                        }
                    });
                }
            }).keyup(function (e) {
                if (e.keyCode === $.ui.keyCode.ENTER) {
                    $(this).dialog('close');
                }

            });

            setTimeout(function () {
                container.saveCollection(true);
            }, 2000);
            Drupal.behaviors.jarrow = {
                attach: function (context, settings) {
                    if (context.is('div') && context.attr('id') !== undefined && context.attr('id') !== false) {
                        container.triggerEditPane(undefined, context, true);
                    }
                }
            };
        }



    });

})(jQuery);

