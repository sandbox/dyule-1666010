/**
 * jQuery UI Editable List widget
 * 
 * Copyright 2012 Daniel Yule 
 * 
 * Provides a widget for creating and editing lists. The value of the text field
 * used to create this widget can be a serialzed form of the list to be 
 * displayed.  The value will be updated with every change to the list.
 * 
 * Dependent on s stringify function being provided by either the browser
 * or by json2.
 * 
 * This widget provides no methods, and the only event it triggers is change()
 * 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

(function ($){ 
  //Construct a new widget with mouse interactions provided by the ui.mouse
  //prototype.
  $.widget('ui.editablelist', $.ui.mouse, {
    options: {
      
    },
    
    _init: function () {
      //Save a jQuery object version of the original element
      var $dom = $(this.element),
      //Read in the current value of the original element
      data,

      //Create a new <ul> element to hold the list
      $list = $('<ul/>', {
        className: 'editablelist',
        style: 'list-style: none outside none'
      }),/*.css('width', $dom.css('width')),*/
      $buttonContainer = $('<div/>', {
        className: 'editable-list-buttons'
      }),
      //Create the buttons for adding and removing items.
      $plusButton = $('<button type="button">+</button>'),
      //An index for iterating through items in the data list
      itemIdx,
      //Used to hold each list element as it's created
      $item;
      
      //Remove any escaped quote elements in the data.
      $dom.val($dom.val().replace(/&quot;/g, '"'))
      
      //Read in the data from the the current value of the original element
      try{
        data = JSON.parse($dom.val());
      } catch (err) {
        data = [];
      }
      //Used to create a new list element for every entry in the data array.
      //The entry is passed in as the variable text
      //activate is used to determine if the item should start as being editable
      //index indicates the id of the item to create.  If it is undefined, then
      //a new unique id is assigned
      function _createElement(text, activate, index) {

        //The list element that represents this entry
        var $item = $('<li/>', {
          className: 'editablelist-item'
        }),
        
        //The span that will hold the actual text of the entry.  The text must
        //held in a span for the sake of the inline editing that will happen
        $span = $('<span/>', {
          text:text,
          className: 'editable-list-label'
        }),
        
        //The remove button that can be used to delete an element of the list
        $remove = $('<span/>', {
          text: '-',
          className: 'editable-list-remove'
        }),
        // The editable span that is being added to the list.
        $editable,
        //A counter to go through all of the indices in the data array
        dataIdx,
        //A temporary variabes used to convert the string based keys to integers.
        tempIdx;
        if(index === undefined) {
          index = 1;
          for(dataIdx in data) {
            tempIdx = parseInt(dataIdx, 10);
            if (tempIdx >= index) {
              index  = tempIdx + 1;
            }
          }
        }
        
        //Add the constructed items to the list
        $list.append($item);
        $item.append($span);
        $item.append($remove);
        //Add a div to the list item in order to ensure the list item's height
        //is large enough to hold the text inside of it.
        $item.append($('<div/>', {
          style: "clear: both"
        }));
        
        $item.data('index', index);
        //Add functionality to the remove button.
        $remove.click(function() {
          $(this.parentNode).remove();
          _updateValue();
        })
        //Set the span holding the text as editable
        //$editable = $dom.siblings().find('li span.editable-list-label').editInPlace({
        $span.editInPlace({
          bg_over: 'transparent',
          callback: function(original_element, html){
            _updateValue(original_element, html);
            return(html);
          }
        });
        if(activate) {
          $span.click();
        }
        return $item;
      }//_createElement
      
      //Update the value of the original input element with a serialized version
      //of the list
      function _updateValue(e, newVal) {
        data = {};
        //Iterate over each item in the list and extract its text value
        //then insert it into the data variable
        $dom.siblings().find('li.editablelist-item').each(function(key, value) {
          var id = $(value).data('index');
          if(e !== undefined && $.contains(value, e.currentTarget)) {
            data[id] = newVal.value;
            
          } else {
            
            data[id] = $(value).children('span.editable-list-label').text();
          }
        });
        
        //Serialze the data variable
        $dom.val(JSON.stringify(data));
        
        //Trigger a change event on the original data element (since val()
        //doesn't do it automatically.
        $dom.change();
        
      }//updateValue
      
      //Hide the original element and replace it with the list element
      $dom.hide();
      $dom.after($list);
      
      //Make the list sortable so elements can be rearranged.  We update the
      //list after the re-arrangement happens.
      $list.sortable({
        stop: function(event, ui) {
          _updateValue();
        }
      });
      
      //Create a list item for every item in the list
      for(itemIdx in data) {
        if(data.hasOwnProperty(itemIdx)) {
          _createElement(data[itemIdx], false, itemIdx);
          
        }
      }
      //Add The plus sign for creating new elements.
      
      $plusButton.click(function(){
        _createElement('New Item', true);
        _updateValue();
      });
      $list.after($buttonContainer);
      $buttonContainer.append($plusButton);
    }//_init
  });//$.ui.widget
}(jQuery))