/**
 * jQuery UI Tree Dropdown widget
 *
 * Copyright 2012 Daniel Yule
 *
 *
 * Provides a dropdown that displays hierarchical information in a tree view.
 *
 * Dependent on Marco Braak's jqTree Plugin, which is found at
 * http://mbraak.github.com/jqTree/.  The jqTree source file must be included
 * prior to including this file.
 *
 * This widget is designed to build off of a text input element.  When creating
 * a new dropdowntree, the tree data to display in the drop down must be
 * specified.
 *
 * The tree data is defined using a javascript array.  Each element of the array
 * should be a data object with the attributes 'label' and 'id'.  The label will
 * be what is displayed on the tree.  The id will be the value taken by the
 * control when selected.  Optionally the object can have another attribute
 * 'children,' which is an array of data objects.
 *
 * For example (taken and modified from the jqTree documentation):
 *
 * <code>
 *  var data = [
 {
     label: 'node1',
     children: [
         { label: 'child1' },
         { label: 'child2' }
     ]
 },
 {
     label: 'node2',
     children: [
         { label: 'child3' }
     ]
 }
 ];
 $('#tree1').dropdowntree({
    data: data,
    autoOpen: true
});
 * </code>
 *
 * The data structure for the tree is created in the function jarrow_data_element_tree_display()
 * in the file form_element_types.inc
 *
 *
 * For more information on how to control the tree dropdown, please see the
 * jqTree documentation.
 *
 * Note: setting the value of the source control using .val() will not work
 * unless you also trigger change().  Getting the value works as expected.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//******************************************************************************
//* Note: the formatting around declarations and some other areas of this file *
//* is not as readable as could be desired.  This is a product of NetBean's    *
//* inability to configure more than the very basic parameters of javascript   *
//* formatting.                                                                *
//******************************************************************************


//The following is standard javascript module namespacing convention.
//The opening parenthesis (and closing parenthesis at the end of the file)
//make the delcaration a valid expression and therefore able to be interpreted.
//By convention they also mark an autoexecuting function.
//The function declaration creates an anonymous function which takes one
//argument, named $.  This will be passed in the jQuery object, which allows
//for the conventional (and space saving) usage of $ as a shortcut for jQuery
(function ($) {

    //Declare a new widget.  The $.ui.mouse argument indicates means to use the
    //mouse object as a prototype, which immediately gives a bunch of mouse
    //interaction to the widget
    $.widget('ui.dropdowntree', $.ui.mouse, {

        //Any events this widget creates will be prefixed by 'treedropdown'
        widgetEventPrefix: "treedropdown",

        //We don't have any built in options.  We do, however, inherit the
        //built in stuff from jqTree
        options: {

        },

        //Called when the object is first created, but not when it is reinitialized.
        _create: function () {
            //Create the dropdown button
            var $button = $('<button type="button">\u25bc</button>'),
            //Create the div that will hold the tree when it is dropped down.
                $div = $('<div class="dropdowndiv"></div>'),
            // Create a copy of the original element.  This will be used
            //for displaying the label that has been selected.  The
            //original element will hold the actual value.
                $displayElement = $(this.element).clone(),
            //Needed to be able to refer to the source element within
            //different scopes
                element = this,
            //The DOM element corresponding to the original input
                $dom = $(this.element),
            //The dimensions of the original input
                width,
                height,
                left,
                top;

            //Get rid of any leftover tree dropdowns that weren't cleaned up
            //previously
            //@todo This means that multiple tree dropdowns will destroy each
            //other
            $('.dropdowntree_aux').remove();


            //Add functionality to the dropdown button.
            $button.click(function () {
                element.toggle();
            });

            //Add the three newly created elements to the document.  These
            //elements will subsequently be physically positioned closer to the
            //source input
            $dom.after($button);
            $dom.after($div);
            $dom.after($displayElement);

            //Differentiate the source input from the display input in the
            //document
            $displayElement
                .attr('id', $dom.attr('id') + '_display')
                .removeAttr('name')
                //Can't edit the label text or bad stuff will happen
                .attr('readonly', true);

            //Reset the positioning of the elements that have been created so
            //that they are sitting on top of or close to the original input.
            function resetPositioning() {
                //We need to get padding from the parent element as that throws off the
                //calculation of offset from parent element.
                var $parent = $dom.offsetParent();
                //Just to be sure we hide the underlying source element.
                $dom.css('visibility', 'visible');
                //Sort out the dimensions of the original input
                width = $dom.outerWidth() - 2;
                height = $dom.outerHeight();
                //jQuery returns padding sizes measured in px(always) so we need only
                //strip off the 'px' on the end, which parseFloat does nicely.
                //The float offset was needed at one point, but I'm not convinced it
                //still is.
                left = $dom.position().left /*- parseFloat($parent.css('padding-left'))*/;
                top = $dom.position().top /*- parseFloat($parent.css('padding-top'))*/;

                $dom.css('visibility', 'hidden');
                //All elements have the class dropdowntree_aux added to them
                //to make them easier to find and remove later.

                //Move the display element directly on top of the original
                //output, so it appears to BE the input.
                $displayElement
                    .css('top', top)
                    .css('left', left)
                    .css('position', 'absolute')
                    .width($dom.width())
                    .height($dom.height())
                    .addClass('dropdowntree_aux dropdowntree_input');

                //Place the drop down div directly beneath the original input
                //so it looks like it is coming out of the bottom.
                $div
                    .css('top', top + height)
                    .css('left', left)
                    .css('position', 'absolute')
                    .width(width)
                    .addClass('dropdowntree_aux dropdowntree_selector');

                //Place the button on top of the original input (and the display input)
                //on the right side.  The button will be square and the same height as
                //the input, so if the input is too tall with respect to its width, the
                //button will take up a lot of space.
                $button
                    .css('top', top)
                    .css('left', left + width - height + 2)
                    .css('position', 'absolute')
                    .css('padding', '0')
                    .css('margin', '0')
                    .width(height)
                    .height(height)
                    .addClass('dropdowntree_aux');
            } //resetPositioning

            //Reset the positioning so everything is placed properly.
            resetPositioning();

            //Also move the auxiliary components when the window is resized.
            //Usually this doesn't matter, but when if the user makes the interface
            //elements bigger or smaller (typically with ctrl +/-) then the
            //positioning has to be reset.
            $(window).resize(resetPositioning);

            //Save the auxiliary components so they can be accessed later.
            this.$div = $div;
            this.$button = $button;
            this.$displayElement = $displayElement;

            //Load the display value for this component based on the value of the
            //original component.  It is assumed that the original input element
            //has a value which corresponds to an id in the data tree.
            //We also want to do this if the data is changed.
            this._loadFromID();
            $dom.change(function () {
                element._loadFromID();
            });
        },//_create

        //Called each time the dropdowntree widget is created on a particular
        //element.  This function creates the tree widget within the dropdown
        //div and binds an event to trigger when the user selects an options
        _init: function () {
            //Store the original element for access later.
            var element = this,
            //Makes referring to the options easier.
                options = this.options;


            //Hide the tree view initially
            this.$div.hide();
            //Remember that the tree view is hidden
            options.showing = false;
            //Allow the tree to be selected (or the drop down is pretty useless)
            options.selectable = true;

            //Create the tree
            this.$div.tree(options);

            //Register a listener so that when the user selects an element of the
            //tree, the value gets stored and the display gets updated.
            this.$div.bind('tree.click', function (event) {
                var node = event.node;
                //Set the display element to the label of the selected node.
                element.$displayElement.val(node.name);

                //Set the original element to the id of the selected node.
                $(element.element).val(node.id);

                //Make sure that any listeners on the change event get fired.
                $(element.element).change();

                //Hide the dropdown, as something has been selected.
                element.collapse();
            });

        },//_init

        //Load the display value associated with a particular id into the display
        //element.
        _loadFromID: function () {
            //Save a reference to the data that the tree is generated from.
            var data = this.options.data,
            //Get the id stored in the original element.
                id = $(this.element).val(),
            //An index for iterating over the nodes in the data tree.
                nodeIdx,
            //The value that the if corresponds to.  If none is found, it will
            //simply be the empty string
                value = "";

            //Recursively iterate through the children of a data node and look for
            //an ID that matches the one in the original input element.
            function _findID(node) {
                //The index of the child we are currently looking at
                var index,
                //The possible value of the recursive search through a node's children
                    result;

                //Loop through all the children of the given node.
                for (index = 0; index < node.children.length; index += 1) {
                    //If the id matches the one we are looking for, then we're set
                    if (node.children[index].id === id) {
                        return node.children[index].label;
                        //If this node has children, recursively search through them for
                        //possible matches
                    }
                    if (node.children[index].children) {
                        result = _findID(node.children[index]);
                        //This function returns the empty string if it is unsuccessful, so
                        //if the string isn't empty, we've found the value.  Return it.
                        if (result !== "") {
                            return result;
                        }
                    }
                }
                //When we're unsucessful, return the empty string
                return "";
            } //_findID

            //Iterate over all the top level data objects in the data tree.
            for (nodeIdx = 0; nodeIdx < data.length; nodeIdx += 1) {
                //If the ID matches, then we're done looking. (We assume IDs are unique)
                if (data[nodeIdx].id === id) {
                    value = data[nodeIdx].label;
                    break;
                    //If there is no ID match, but there are children, then look for the
                    //ID among them.
                } else if (data[nodeIdx].children) {
                    value = _findID(data[nodeIdx]);
                    if (value !== "") {
                        break;
                    }
                }
            }
            //Set the value of the display element with the value that we found.
            this.$displayElement.val(value);


        },//_loadFromID


        //Open or close the tree.
        toggle: function () {
            //If the tree is currently showing, hide it.
            if (this.options.showing) {
                this.collapse();
                //Otherwise show it.
            } else {
                this.options.showing = true;
                this.$div.show();
                //Up arrow
                this.$button.text('\u25b2');
                //Set focus on the text field so we can tell when the user clicks away.
                $(this.element).focus();
            }
        },//toggle

        //Hide the dropdown tree.
        collapse: function () {
            this.options.showing = false;
            this.$div.hide();
            //Down arrow
            this.$button.text('\u25bc');
        }

    });//$.ui.widget
}(jQuery)); //Invoke the function containing this file.
