<?php

/**
 * @file Provides information about built in data element types
 *
 * The Jarrow ETD Module provides several built in datatypes that can be used
 * in an ETD workflow.  This file defines their configuration, display and
 * storage.
 *
 * @author
 *       Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */
function jarrow_etd_data_element_types($source) {

  switch ($source) {
    case 'form':
      return array(
        'text' => array(
          'name' => t('Text'),
          'edit' => 'jarrow_text_field',
          'display' => 'jarrow_text_field_display',
          'config' => 'jarrow_text_config'
        ),
        'select' => array(
          'name' => t('Drop Down List'),
          'edit' => 'jarrow_select_field',
          'display' => 'jarrow_select_field_display',
          'config' => 'jarrow_select_config',
        ),
        'file' => array(
          'name' => t('Single File Submission'),
          'edit' => 'jarrow_file_field',
          'display' => 'jarrow_file_field_display',
          'config' => 'jarrow_file_config',
          'update' => 'jarrow_file_field_update',
        ),
        'date' => array(
          'name' => t('Date Picker'),
          'edit' => 'jarrow_date_field',
          'display' => 'jarrow_date_field_display',
          'config' => 'jarrow_date_config',
        ),
        'radioselect' => array(
          'name' => t('Radio-Select'),
          'edit' => 'jarrow_radioselect_field',
          'display' => 'jarrow_radioselect_field_display',
          'config' => 'jarrow_select_config',
        ),
        'multiselect' => array(
          'name' => t('Multi-Select'),
          'edit' => 'jarrow_multiselect_field',
          'display' => 'jarrow_multiselect_field_display',
          'config' => 'jarrow_select_config',
        ),
        'longtext' => array(
          'name' => t('Long Text'),
          'edit' => 'jarrow_longtext_field',
          'display' => 'jarrow_longtext_field_display',
          'config' => 'jarrow_longtext_config',
        ),
        'license' => array(
          'name' => t('License'),
          'edit' => 'jarrow_license_field',
          'display' => 'jarrow_license_field_display',
          'config' => 'jarrow_license_config',
        ),
      );
    case 'role':
      return array(
        'role' => array(
          'name' => t('Role'),
          'edit' => 'jarrow_role_field',
          'display' => 'jarrow_role_field_display',
        ),
      );
  }
  return array();
}

/**
 * Define the text format help descriptions for the various text input types a user can select.
 *
 * @param $type_config
 *
 * @return array
 */
function jarrow_text_type_descriptions($type_config) {

  $format = isset($type_config['format']) ? $type_config['format'] : 'any';

  // Set the default option_elements then override as necessary.
  $option_elements = jarrow_get_default_answer_form($type_config);

  switch ($format) {
    case 'any':
      $description = t('Allows any values to be entered');
      break;
    case 'numeric':
      $description = t('Allows only numbers (in any format)');
      break;
    case 'email':
      $description = t('Allows any email address (does not check to ensure the email address exists)');
      break;
    case 'url':
      $description = t('Accepts any URL (does not check to ensure the url is active)');
      break;
    case 'ipv4':
      $description = t('Allows any IPv4 address (typically looks like xx.xx.xx.xx)');
      break;
    case 'alpha_numeric':
      $description = t('Allows alphabetical letters and numbers');
      break;
    case 'alpha_dash':
      $description = t('Allows alphabetical letters and the dash character ( - )');
      break;
    case 'digit':
      $description = t('Allows only digits (no decimals or dashes)');
      break;
    case 'decimal':
      $description = t('Allows any decimal number (assumes a dot decimal, eg 4.567)');
      break;
    case 'length':
      $description = t('Limits the number of characters to the specified value');
      $option_elements = array(
        'max_length' => array(
          '#type' => 'textfield',
          '#title' => t('Maximum Length'),
          '#description' => t('The maximum length of this field'),
          '#name' => 'type_config[max_length]',
          '#default_value' => 60,
        ),
      );
      break;
    case 'regexp':
      $description = t('Matches the arbitrary regular expression included below.');
      $option_elements = array(
        'regexp' => array(
          '#type' => 'textfield',
          '#title' => t('Regular Expression'),
          '#description' => t('The regular expression used to validate the code'),
          '#name' => 'type_config[regexp]',
          '#default_value' => 60,
        ),
      );
      break;
    default:
      $description = t('Unknown option');
      $option_elements = array();
      break;
  }

  return array(
    'description' => $description,
    'options' => $option_elements
  );
}

/**
 * Create the default answer form element
 *
 * @param $config_object
 *
 * @return array
 */
function jarrow_get_default_answer_form($config_object) {

  if ((count($config_object) > 0) && (isset($config_object['format']))) {
    $textfield = jarrow_config_text_field($config_object['type_config_container']['default_answer'], $config_object['format']);
    $textfield['#title'] = t('Default Value');
    $textfield['#description'] = t('Optional: Enter a default value to be entered into this field.');
    $default_answer = array(
      'default_answer' => $textfield,
      'default_answer_editable' => jarrow_get_default_answer_editable_form(),
    );
  } else {
    $default_answer = array(
      'default_answer' => array(
        '#type' => 'textfield',
        '#title' => t('Default Value'),
        '#description' => t('Optional: Enter a default value to be entered into this field.'),
        '#default_value' => '',
      ),
      'default_answer_editable' => jarrow_get_default_answer_editable_form(),
    );
  }
  return $default_answer;
}

/**
 * Configure the text field to use the appropriate validation rule
 *
 * @param $default_value
 * @param null $validation_rule
 *
 * @return array
 */
function jarrow_config_text_field($default_value, $validation_rule = NULL) {

  if (!isset($validation_rule)) {
    $validation_rule = 'any';
  }

  $length = 1024;
  if ($validation_rule === 'any') {
    $textfield = array(
      '#type' => 'textfield',
      '#default_value' => $default_value,
    );
  } else {
    $textfield = array(
      '#type' => 'textfield',
      '#default_value' => $default_value,
      '#maxlength' => $length,
      '#rules' => array(
        array(
          'rule' => "type_validation[$validation_rule]",
          'error' => jarrow_validation_get_textfield_error_message($validation_rule),
        ),
      ),
    );
  }
  return $textfield;
}

/**
 * Create the form that lets the user select if the value in the default
 * answer field is editable or not.
 *
 * @return array
 */
function jarrow_get_default_answer_editable_form() {

  return array(
    '#type' => 'select',
    '#title' => t('Is default value editable'),
    '#default_value' => 0,
    '#options' => array(
      0 => t('No'),
      1 => t('Yes')
    ),
    '#prefix' => '<div class="default_answer_editable">',
    '#suffix' => '</div>',
  );
}

/**
 * Create the type configuration panel for a text element.
 *
 * @param $type_config
 *
 * @return array
 */
function jarrow_text_config($type_config) {

  $type_description = jarrow_text_type_descriptions($type_config);

  return array(
    'format' => array(
      '#type' => 'select',
      '#title' => t('Format'),
      '#description' => t('The format of the data allowed in this field'),
      '#options' => array(
        'any' => t('Any'),
        'numeric' => t('Numeric'),
        'email' => t('Email'),
        'url' => t('URL'),
        'ipv4' => t('IPv4 Address'),
        'alpha_numeric' => t('Alpha-Numeric'),
        'alpha_dash' => t('Alpha and Dash'),
        'digit' => t('Digits'),
        'decimal' => t('Decimal Number'),
        'length' => t('Length'),
        'regexp' => t('Regular Expression'),
      ),
      '#default_value' => 'any',
      '#ajax' => array(
        'wrapper' => 'type_config_format_container',
        'callback' => 'jarrow_text_field_config_ajax',
      )
    ),
    'type_config_container' => array(
        '#type' => 'container',
        '#id' => 'type_config_format_container',
        'description' => array(
          '#type' => 'item',
          '#title' => t('Format Description'),
          '#markup' => $type_description['description'],
        ),
      ) + $type_description['options'],
  );
}

/**
 * Searches for an element of the form that has an id equal to type_config_format_container.
 *
 * Designed to be used as a callback for an ajax method replacing the format configuration component
 *
 * @param array $form
 *  The form to look through.
 * @param type $form_state
 *  The form state (ignored)
 *
 * @return array
 *  The element that has the appropriate id or an empty array
 */
function jarrow_text_field_config_ajax($form, &$form_state) {

  //Iterate through each of the children of the current form
  foreach (element_children($form) as $key) {
    //Check if they have the desired id
    if (isset($form[$key]['#id']) && $form[$key]['#id'] === 'type_config_format_container') {
      return $form[$key];
    }
    //Recursively search through any children of the given element
    $result = jarrow_text_field_config_ajax($form[$key], $form_state);

    //If a proper array is returned, then we've found the element we're looking
    //for.  If it's empty then keep looking.
    if (count($result) > 0) {
      return $result;
    }
  }
  //If we can't find the element, return an empty array
  return array();
}

function jarrow_select_config() {

  $path = drupal_get_path('module', 'jarrow');
  drupal_add_js($path . '/providers/support/js/data_model_editablelist.js');
  drupal_add_js($path . '/lib/js/jquery.ui.editablelist.js');
  return array(
    'list_values' => array(
      '#type' => 'textfield',
      '#title' => t('Value List'),
      '#maxlength' => 4098,
      '#description' => t('The list of values the user can choose between'),
    )
  );
}

/**
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_text_field($current_value, $config_object) {

  if (!isset($config_object->format)) {
    $config_object->format = 'any';
  }
  $rule = $config_object->format; // PH I think this broken it should be $rule = array('rule' => $config_object->format);
  //$rules = array('rule' => $config_object->format);
  $length = 1024;
  switch ($config_object->format) {
    case 'length':
      $rule .= "[$config_object->max_length]";
      $length = $config_object->max_length;
      break;
    case 'regexp':
      $rule .= "[/^$config_object->regexp\$/]";
      break;
  }
  if ($rule === 'any') {
    $textfield = array(
      '#type' => 'textfield',
      '#default_value' => $current_value,
    );
  } else {
    $textfield = array(
      '#type' => 'textfield',
      '#default_value' => $current_value,
      '#maxlength' => $length,
      '#rules' => array(
        $rule,
      ),
    );
  }

  return $textfield;
}

/**
 * Display the value in the text field properly formatted according to its type.
 *
 * @param string $current_value
 * @param object $config_object
 *
 * @return array
 */
function jarrow_text_field_display($current_value, $config_object) {

  if (isset($config_object->format)) {
    if ($config_object->format == 'email') {
      return array(
        '#type' => 'item',
        '#markup' => l($current_value, 'mailto:' . $current_value),
      );
    } elseif ($config_object->format == 'url') {
      return array(
        '#type' => 'item',
        '#markup' => l($current_value, $current_value),
      );
    } elseif ($config_object->format == 'ipv4') {
      return array(
        '#type' => 'item',
        '#markup' => l($current_value, 'http://' . $current_value),
      );
    }
  }
  return array(
    '#type' => 'item',
    '#markup' => $current_value,
  );
}

/**
 * Create a select list
 *
 * @param string $current_value
 * @param object $config_object
 *
 * @return array
 */
function jarrow_select_field($current_value, $config_object) {

  $value_list = json_decode($config_object->list_values);
  $options = (is_object($value_list)) ? get_object_vars($value_list) : $value_list;
  return array(
    '#type' => 'select',
    '#default_value' => $current_value,
    '#options' => $options,
  );
}

function jarrow_select_field_display($current_value, $config_object) {

  $value_list = get_object_vars(json_decode($config_object->list_values));
  if (!isset($value_list[$current_value])) {
    $current_value = 1; //PH changed index from 0 to 1 as the array starts at index 1
  }
  return array(
    '#type' => 'item',
    '#markup' => $value_list[$current_value],
  );
}

function jarrow_file_field($current_value, $config_object) {

  if (is_array($current_value) && isset($current_value['fid'])) {
    $current_value = $current_value['fid'];
  }
  return array(
    '#type' => 'managed_file',
    '#default_value' => $current_value,
    '#upload_location' => 'private://etd_temp',
  );
}

function jarrow_file_field_display($current_value, $config_object) {

  if ($current_value) {
    if (is_array($current_value) && isset($current_value['fid'])) {
      $current_value = $current_value['fid'];
    }
    $file = file_load($current_value);
    if ($file) {
      return array(
        '#type' => 'item',
        '#markup' => theme('file_link', array('file' => $file)) . ' ',
      );
    }
  }
  return array(
    '#type' => 'item',
    '#markup' => ' ',
  );
}

function jarrow_file_field_update($current_value, $submission, $config_object, $old_value = NULL) {

  if (is_array($current_value) && isset($current_value['fid'])) {
    $current_value = $current_value['fid'];
  }
  if (is_array($old_value) && isset($old_value['fid'])) {
    $old_value = $old_value['fid'];
  }
  if ($old_value != $current_value) {
    if ($old_value) {
      $old_file = file_load($old_value);
      if ($old_file) {
        file_usage_delete($old_file, 'jarrow');
        file_delete($old_file);
      }
    }
    if ($current_value) {
      // Load the file via file.fid.
      $file = file_load($current_value);
      // Change status to permanent.
      $file->status = FILE_STATUS_PERMANENT;
      // Save.
      file_save($file);
      // Record that the module (in this example, user module) is using the file.
      file_usage_add($file, 'jarrow', 'submission', $submission->id);
    }
  }
}

/**
 * Define the role field. It uses autocomplete to help the user
 * select the desired user with a given role.
 *
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_role_field($current_value, $config_object) {

  return array(
    '#type' => 'textfield',
    '#default_value' => $current_value,
    '#autocomplete_path' => "jarrow/js/role_list/{$config_object->role}/{$config_object->maximum}",
  );
}

/**
 * Return a list users who have the specificed role. This is used to
 * provide autocomplete services for textfields that are looking for
 * users.
 *
 * @param int $drupal_role_id
 * @param int $max_entries
 * @param string $username
 */
function jarrow_role_listing($drupal_role_id, $max_entries, $username) {

  $strings = explode(',', $username);
  $list_len = count($strings);
  $username = trim($strings[$list_len - 1]);
  if ($max_entries > 0 && $list_len > $max_entries) {
    drupal_json_output(array(implode(',', array_slice($strings, 0, $max_entries)) => t('Maximum positions assigned')));
    drupal_exit();
  } else {
    if ($list_len > 1) {
      $prefix = implode(', ', array_slice($strings, 0, $list_len - 1)) . ', ';
    } else {
      $prefix = '';
    }
  }
  $user_list = db_select('users', 'u')
    ->fields('u', array('uid'))
    ->condition('name', '%' . db_like($username) . '%', 'LIKE')
    ->execute()
    ->fetchCol();

  $role_array = array();
  $info_sources = module_invoke_all('etd_user_info');

  foreach ($info_sources as $info) {
    $new_list = call_user_func($info['search_function'], $username);
    if (is_array($new_list)) {
      $user_list = array_merge($user_list, $new_list);
    }
  }
  $user_list = array_unique($user_list);
  $user_lookup = user_load_multiple($user_list);
  $user_lookup = jarrow_filter_users_by_role($user_lookup, $drupal_role_id);
  $user_names = _jarrow_get_names_for_users(array_keys($user_lookup));

  foreach ($user_lookup as $uid => $user) {
    $user_name = $user->name;
    $array_key = $prefix . $user_names[$uid] . ' <' . $user_name . '>';
    $role_array[$array_key] = check_plain($user_names[$uid] . ' <' . $user_name . '>');
  }
  drupal_json_output($role_array);
  drupal_exit();
}

/**
 * Filter the given user list by the desired role id
 *
 * @param array $user_list an array of user objects
 * @param int $drupal_role_id
 *
 * @return array
 */
function jarrow_filter_users_by_role($user_list, $drupal_role_id) {

  $filtered_list = array();
  if ($drupal_role_id > 0) {
    foreach ($user_list as $user) {
      if (isset($user->roles[$drupal_role_id])) {
        $filtered_list[$user->uid] = $user;
      }
    }
  }
  return $filtered_list;
}

/**
 * Display the role field.
 *
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_role_field_display($current_value, $config_object) {

  return array(
    '#type' => 'item',
    '#markup' => check_plain($current_value),
  );
}

/*
function jarrow_role_field_config() {

  dsm('jarrow_role_field_config is being called and there is a method call error on the line following this dsm.');
  $roles = jarrow_get_roles();
  $result = db_select('role', 'ur')
    ->fields('ur', array('rid', 'name'))
    ->execute();
  $drupal_roles = array('0' => t('all'));
  foreach ($result as $role) {
    $drupal_roles[$role->rid] = $role->name;
  }
  return array(
    'position' => array(
      '#type' => 'select',
      '#title' => 'Position',
      '#description' => 'The position to set users as',
      '#options' => $roles,
    ),
    'role' => array(
      '#type' => 'select',
      '#title' => 'Associated Drupal Role',
      '#description' => 'The Drupal Role to choose members from',
      '#options' => $drupal_roles,
    ),
    'maximum' => array(
      '#type' => 'select',
      '#title' => 'Maximum',
      '#description' => 'The maximum number of members this role can have',
      '#options' => array(
        0 => t('No Maximum'),
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
      ),
    ),
  );
}
*/
function jarrow_date_config() {

  // Get list of all available date formats.
  $date_options = array();
  $date_types = system_get_date_types(); // Call this to rebuild the list, and to have default list.

  foreach ($date_types as $type => $type_info) {
    $date_options[$type] = check_plain($type_info['title']);
  }
  return array(
    'date_format' => array(
      '#type' => 'select',
      '#title' => 'Date Format',
      '#description' => 'The date format to use for displaying this date, as configured from the drupal date/time settings.',
      '#options' => $date_options,
    ),
  );
}

function jarrow_date_field($current_value, $config_object) {

  $path = drupal_get_path('module', 'jarrow');
  $format_string = variable_get('date_format_' . $config_object->date_format, 'DD d/m/Y');
  if ($format_string === "") {
    $format_string = variable_get('date_format_medium', 'DD d/m/Y');
  }

  $allowed_chars = 'dDjlzFMnmYy';
  $missing_chars = 'NSwWtLoaABgGhHisueIOPTZcrU';
  $date_picker_format = preg_replace(
    array(
      '/d/',
      '/D/',
      '/j/',
      '/l/',
      '/z/',
      '/F/',
      '/M/',
      '/n/',
      '/m/',
      '/Y/',
      '/y/'
    ), array(
      'dd',
      'D',
      'd',
      'DD',
      'o',
      'MM',
      'M',
      'm',
      'mm',
      'yy',
      'y',
    ), preg_replace(
      array(
        "/[$missing_chars]+([^$missing_chars$allowed_chars]+[$missing_chars]+)?/",
        "/([$allowed_chars])+[^$allowed_chars]+$/",
        "/^[^$allowed_chars]+([$allowed_chars]+)/",
      ), array('', '$1', '$1'), $format_string
    )
  );


  drupal_add_library('system', 'ui.datepicker');
  drupal_add_js($path . '/providers/support/js/datepicker.js');
  drupal_add_js(array('jarrow' => array('date_format' => $date_picker_format)), 'setting');
  return array(
    '#type' => 'textfield',
    '#default_value' => $current_value,
    '#attributes' => array('class' => array('jarrow_datefield')),
  );
}

function jarrow_date_field_display($current_value, $config_object) {

  return array(
    '#type' => 'item',
    '#markup' => $current_value,
  );
}

function jarrow_radioselect_field($current_value, $config_object) {

  $value_list = get_object_vars(json_decode($config_object->list_values));
  return array(
    '#type' => 'radios',
    '#options' => $value_list,
    '#default_value' => (array) ($current_value ? $current_value : array()),
  );
}

function jarrow_radioselect_field_display($current_value, $config_object) {

  $value_list = get_object_vars(json_decode($config_object->list_values));
  return array(
    '#type' => 'radios',
    '#options' => $value_list,
    '#default_value' => $current_value,
    '#disabled' => TRUE,
  );
}

function jarrow_multiselect_field($current_value, $config_object) {

  $value_list = get_object_vars(json_decode($config_object->list_values));
  return array(
    '#type' => 'checkboxes',
    '#options' => $value_list,
    '#default_value' => (array) ($current_value ? $current_value : array()),
  );
}

function jarrow_multiselect_field_display($current_value, $config_object) {

  $value_list = get_object_vars(json_decode($config_object->list_values));
  return array(
    '#type' => 'checkboxes',
    '#options' => $value_list,
    '#default_value' => (array) ($current_value ? $current_value : array()),
    '#disabled' => TRUE,
  );
}

function jarrow_longtext_config() {

  return array(
    'max_words' => array(
      '#type' => 'textfield',
      '#title' => t('Maximum Words'),
      '#default_value' => '150',
    ),
  );
}

function jarrow_longtext_field($current_value, $config_object) {

  if ($config_object->max_words <= 0) {
    $length_regex = '.*';
  } else {
    $length_regex = '\\S+' . str_repeat('\\s+\\S+', $config_object->max_words - 1);
  }
  return array(
    '#type' => 'textarea',
    '#default_value' => $current_value,
    '#rules' => array(
      "word_count[$config_object->max_words]",
    ),
  );
}

function jarrow_longtext_field_display($current_value, $config_object) {

  return array(
    '#type' => 'item',
    '#markup' => $current_value,
  );
}

/**
 * Create the form that allows the user to view and edit a web page.
 *
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_web_page($current_value, $config_object) {

  $form = array(
    '#type' => 'text_format',
    '#rows' => 8,
    '#default_value' => $current_value,

  );

  return $form;
}

function jarrow_web_page_display($current_value, $config_object) {

  return array(
    '#type' => 'item',
    '#markup' => $current_value,
  );
}

/**
 * Display the active licenses that the administrator can choose from.
 *
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_license_config($current_value, $config_object) {

  // Get the enabled licenses from the database.
  $license_result = db_select('etd_licenses')
    ->fields('etd_licenses', array('code', 'name'))
    ->condition('active', 1)
    ->execute();

  // Create a list of available selections.
  $licenses = array();
  foreach ($license_result as $license) {
    $licenses[$license->code] = htmlspecialchars($license->name);
  }

  // Get the currently selected licenses
  $selected_licenses = array();
  if (isset($current_value['selected_licenses'])) {
    foreach ($current_value['selected_licenses'] as $selected) {
      if ($selected !== NULL) {
        $selected_licenses = $selected;
      }
    }
  }

  // Return the config form.
  $config_form['selected_licenses'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Licenses to Display'),
    '#description' => t('Select which licenses to display to the user'),
    '#options' => $licenses,
    '#default_value' => $selected_licenses,
  );

  $config_form['license_agreement'] = array(
    '#type' => 'select',
    '#title' => t('Type of license agreement'),
    '#default_value' => 0,
    '#options' => array(
      0 => t('Published under selected license(s)'),
      1 => t('Must agree to selected license(s)')
    ),
    '#prefix' => '<div class="license-agreement">',
    '#suffix' => '</div>',
  );
  return $config_form;

}

/**
 * Create the form that allows the user to view license information and
 * select which ones they want.
 *
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_license_field($current_value, $config_object) {

  $license_code = (isset($current_value['license'])) ? $current_value['license'] : NULL;

  $license_result = jarrow_license_get_licenses($config_object);

  // Create the appropriate form
  if ($config_object->license_agreement) {
    $form = jarrow_license_create_license_agreement_form($license_result);
  } else {
    $form = jarrow_license_create_publishing_license_form($license_result, $license_code);
  }

  return $form;
}

/**
 * Return the desired set of licenses.
 *
 * @param $config_object
 *
 * @return DatabaseStatementInterface|null
 */
function jarrow_license_get_licenses($config_object) {

  $license_query = db_select('etd_licenses')
    ->fields('etd_licenses', array('id', 'code', 'name', 'summary', 'terms', 'terms_format', 'full_text_link'));

  $selected_licenses = jarrow_license_get_selected_licenses($config_object);

  if (!empty($selected_licenses)) {
    $license_query = $license_query->condition('code', $selected_licenses, 'in');
  }
  return $license_query->execute();
}

/**
 * Create the license form that the user must agree with.
 *
 * @param array $licenses
 * @param bool read_only
 * @param array $current_value
 *
 * @return mixed
 */
function jarrow_license_create_license_agreement_form($licenses, $read_only = FALSE, $current_value = NULL) {

  $form = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="jarrow_license_agreement">',
    '#suffix' => '</div>',
  );

  if (!$read_only) {
    $license_count = $licenses->rowCount();
    if ($license_count > 0) {
      $text = ($license_count > 1) ? 'licenses:' : 'license:';
      $form['text'] = array(
        '#type' => 'markup',
        '#markup' => '<p>' . t('You must agree to the following ' . $text) . '</p>',
      );
    }
  }

  foreach ($licenses as $license) {
    $id = 'license_id_' . $license->id;
    $form[$id] = array(
      '#type' => 'fieldset',
      '#title' => t($license->name),
      '#collapsible' => FALSE,
    );

    $form[$id]['summary'] = array(
      '#type' => 'item',
      '#title' => t('Summary'),
      '#markup' => $license->summary,
    );

    $form[$id]['terms'] = array(
      '#type' => 'fieldset',
      '#title' => t('License Terms'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'terms' => array(
        '#type' => 'markup',
        '#markup' => check_markup($license->terms, $license->terms_format),
      ),
    );
    $form[$id]['full_text_link'] = array(
      '#type' => 'item',
      '#title' => t('Link to Full Legal Text'),
      '#markup' => l($license->full_text_link, $license->full_text_link),
    );

    $form[$id]['agreement'] = array(
      '#type' => 'item',
      '#title' => t('Agreement'),
    );

    if ($read_only) {
      $form[$id]['agree'] = array(
        '#type' => 'checkbox',
        '#title' => t('I agree'),
        '#disabled' => TRUE,
      );
      if (($current_value != NULL) && isset($current_value[$id])) {
        $form[$id]['agree']['#default_value'] = $current_value[$id]['agree'];
      }
    } else {
      $form[$id]['agree'] = array(
        '#type' => 'checkbox',
        '#title' => t('I agree'),
        '#required' => TRUE,
      );
    }

  }
  return $form;
}

/**
 * Create the form in which the user can select which license they wish
 * to publish under.
 *
 * @param array $licenses
 * @param array $license_code
 *
 * @return array
 */
function jarrow_license_create_publishing_license_form($licenses, $license_code = NULL) {

  $license_options = array();
  $license_details = array();
  $found_code = FALSE;
  foreach ($licenses as $license) {
    if (!$found_code && (!isset($license_code) || $license->code == $license_code)) {
      $license_details['code'] = $license->code;
      $license_details['summary'] = $license->summary;
      $license_details['terms'] = $license->terms;
      $license_details['link'] = $license->full_text_link;
      $license_details['terms_format'] = $license->terms_format;
      $found_code = TRUE;
    }
    $license_options[$license->code] = $license->name;
  }

  $form = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="jarrow_license_wrapper">',
    '#suffix' => '</div>',
  );

  $form['text'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Please select the license that you which to publish under.') . '</p>',
  );
  $form['license'] = array(
    '#type' => 'radios',
    '#options' => $license_options,
    '#name' => 'type_config[license]',
    '#default_value' => $license_details['code'],
    '#ajax' => array(
      'callback' => 'jarrow_license_field_ajax',
      'wrapper' => 'jarrow_license_wrapper',
    ),
  );

  $form['summary'] = array(
    '#type' => 'item',
    '#title' => t('Summary'),
    '#markup' => $license_details['summary'],
  );

  $form['terms'] = array(
    '#type' => 'fieldset',
    '#title' => t('License Terms'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'terms' => array(
      '#type' => 'markup',
      '#markup' => check_markup($license_details['terms'], $license_details['terms_format']),
    ),
  );
  $form['full_text_link'] = array(
    '#type' => 'item',
    '#title' => t('Link to Full Legal Text'),
    '#markup' => l($license_details['link'], $license_details['link']),
  );

  return $form;
}

/**
 * Process the license config object to return only the selected licenses.
 * (ie return the license code).
 *
 * @param $config_object
 *
 * @return array
 */
function jarrow_license_get_selected_licenses($config_object) {

  $selected_licenses = array();
  if (isset($config_object)) {
    $start = strlen('[selected_licenses]') + 1;
    foreach ($config_object as $key => $value) {
      if (strpos($key, '[selected_licenses]') === 0) {
        if ($value) {
          $selected_licenses[] = substr($key, $start, (strlen($key) - $start - 1));
        }
      }
    }
  }
  return $selected_licenses;
}

/**
 * Display the license field.
 *
 * @param $current_value
 * @param $config_object
 *
 * @return array
 */
function jarrow_license_field_display($current_value, $config_object) {

  if ($config_object->license_agreement) {
    $form = jarrow_license_display_required_licenses($current_value, $config_object);
  } else {
    $form = jarrow_license_display_publishing_license($current_value);
  }
  return $form;
}

/**
 * Display the publishing license selected.
 *
 * @param array $current_value
 * @param object $config_object
 *
 * @return array
 */
function jarrow_license_display_required_licenses($current_value, $config_object) {

  $licenses = jarrow_license_get_licenses($config_object);
  $form = jarrow_license_create_license_agreement_form($licenses, TRUE, $current_value);
  return $form;
}

/**
 * Display the publishing license selected.
 *
 * @param $current_value
 *
 * @return array
 */
function jarrow_license_display_publishing_license($current_value) {

  if (isset($current_value['license'])) {
    $license_code = $current_value['license'];
    $license = db_select('etd_licenses')
      ->fields('etd_licenses', array('name', 'summary', 'terms', 'terms_format', 'full_text_link'))
      ->condition('code', $license_code)
      ->execute()
      ->fetch();
  } else {
    $license = db_select('etd_licenses')
      ->fields('etd_licenses', array('name', 'summary', 'terms', 'terms_format', 'full_text_link'))
      ->execute()
      ->fetch();
  }

  return array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="jarrow_license_wrapper">',
    '#suffix' => '</div>',
    'license' => array(
      '#type' => 'item',
      '#title' => t('License'),
      '#markup' => $license->name,
    ),
    'summary' => array(
      '#type' => 'item',
      '#title' => t('Summary'),
      '#markup' => $license->summary,
    ),
    'terms' => array(
      '#type' => 'fieldset',
      '#title' => 'License Terms',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'terms' => array(
        '#type' => 'markup',
        '#markup' => check_markup($license->terms, $license->terms_format),
      ),
    ),
    'full_text_link' => array(
      '#type' => 'item',
      '#title' => t('Link to Full Legal Text'),
      '#markup' => l($license->full_text_link, $license->full_text_link),
    ),
  );
}

/**
 * Ajax callback to update the currently selected license information.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function jarrow_license_field_ajax($form, &$form_state) {

  return jarrow_find_prefix($form, '<div id="jarrow_license_wrapper">');
}

/**
 * Find the element that has the same prefix value as the one specified
 * and return it.
 *
 * @param $elements
 * @param $prefix
 *
 * @return array
 */
function jarrow_find_prefix($elements, $prefix) {

  if (isset($elements['#prefix']) && $elements['#prefix'] == $prefix) {
    return $elements;
  }
  foreach (element_children($elements) as $value) {
    $child_value = jarrow_find_prefix($elements[$value], $prefix);
    if (count($child_value) > 0) {
      return $child_value;
    }
  }
  return array();
}

