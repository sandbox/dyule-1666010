/**
 * @file
 *  Turns the associated_element field into a dropdown tree.
 * 
 * @author 
 *  Daniel Yule <dyule@unbc.ca>
 * 
 * @copyright
 * 
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 * 
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in 
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

//It is important that this javascript gets included on the type config form 
//for form elements.  The only way to force it to execute using drupal's ajax 
//is to include it as a file

Drupal.behaviors.jarrow_dropdowntree = {
  attach: function(context, settings) {
    if(jQuery(context).attr('id') === 'config_form_type_wrapper'){
      jQuery('[name="type_config[associated_element]"]').dropdowntree({
        data: jQuery.parseJSON(settings.jarrow.data_source_tree), 
        autoOpen: false,
        onCanSelectNode: function (node) {
          // Nodes without children can be selected, nodes with children cannot.
          return (node.children.length == 0);
        }
      });
    }
  }
};