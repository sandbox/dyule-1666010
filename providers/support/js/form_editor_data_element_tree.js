/**
 * @file
 *  Turns the data model display into a tree view.
 * 
 * @author 
 *  Daniel Yule <dyule@unbc.ca>
 * 
 * @copyright
 * 
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 * 
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in 
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */
//It is important that this javascript gets included on the type config form 
//for form elements.  The only way to force it to execute using drupal's ajax 
//is to include it as a file
 

jQuery(document).ready(function ($) {
      $('.data_model_tree').tree({
        data: $.parseJSON(Drupal.settings.jarrow.data_source_tree), 
        autoOpen: false,
        onCreateLi: function (node, $li) {
          $li.attr('id', 'data_element-' + node.id);
        }
        
      }); 
      $('.data_model_tree li').draggable({
        helper: 'clone',
          connectToSortable: "#element_container>ul"
      });
});