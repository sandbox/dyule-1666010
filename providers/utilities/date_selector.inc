<?php
/**
 * @file
 *  Functions relating for date selector management and configuration.
 *
 *  The purpose of the date selector is to provide a mechanism in which
 *  different individuals can select the dates and times in which they
 *  are available (for a particular event - ie meeting).
 *
 *  All the individuals who appear in the date selector can only edit
 *  their particular line but they can see all the other individuals
 *  selections.
 *
 *  Once all the selection have been made then a common date is picked
 *  by someone and set (this is done outside of this module). The purpose
 *  of this module is just allow people to pick which dates they are
 *  available.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This creates a table in which all the created date selectors are listed.
 * Here the user can be edit, delete or create new date selectors.
 *
 * This table appears on the Date Selector settings screen.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function jarrow_date_selectors_list($form, &$form_state) {

  $date_selector_result = db_select('etd_date_selector', 'ds')
    ->fields('ds', array('id', 'name'))
    ->execute();

  $rows = array();
  foreach ($date_selector_result as $date_selector) {
    $rows[] = array(
      'data' => array(
        check_plain($date_selector->name),
        l('edit', JARROW_DATE_PATH . '/' . $date_selector->id),
        l('delete', JARROW_DATE_PATH . '/' . $date_selector->id . '/delete'),
      )
    );
  }

  return array(
    'selector_list' => array(
      '#theme' => 'table',
      '#header' => array(
        array('data' => t('Date Selector')),
        array('data' => t('Operations'), 'colspan' => 2),
      ),
      '#rows' => $rows,
    ),
    'submit_button' => array(
      '#type' => 'submit',
      '#value' => 'Add New',
    ),
  );
}

/**
 * Create a new date selector entry in the database table when
 * the user clicks the Add New button.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_date_selectors_list_submit($form, &$form_state) {

  $result = db_insert('etd_date_selector')
    ->fields(array(
      'name' => '',
      'offset' => 21,
      'before_after' => 0,
      'duration' => 14,
      'enabled_days' => 127,
      'time_slots' => '["All Day"]',
    ))
    ->execute();
  drupal_set_message('Date Selector Created');
  drupal_goto(JARROW_DATE_PATH . '/' . $result);
  drupal_exit();
}

/**
 * Create the form that permits the user to set the values for
 * a date selector.
 *
 * @param array $form
 * @param array $form_state
 * @param int $date_selector_id
 *
 * @return array
 */
function jarrow_date_selector_settings($form, &$form_state, $date_selector_id) {

  $path = drupal_get_path('module', 'jarrow');
  drupal_add_library('jarrow', 'ui.editable-list');
  drupal_add_js($path . '/providers/utilities/date_selector.js');
  libraries_load('jquery_editinplace');
  $date_selector = db_select('etd_date_selector', 'ds')
    ->fields('ds')
    ->condition('id', $date_selector_id)
    ->execute()
    ->fetchObject();

  $enabled_days = array();
  if (($date_selector->enabled_days & 0x1) == 0x1) {
    $enabled_days[] = 'monday';
  }
  if (($date_selector->enabled_days & 0x2) == 0x2) {
    $enabled_days[] = 'tuesday';
  }
  if (($date_selector->enabled_days & 0x4) == 0x4) {
    $enabled_days[] = 'wednesday';
  }
  if (($date_selector->enabled_days & 0x8) == 0x8) {
    $enabled_days[] = 'thursday';
  }
  if (($date_selector->enabled_days & 0x16) == 0x16) {
    $enabled_days[] = 'friday';
  }
  if (($date_selector->enabled_days & 0x32) == 0x32) {
    $enabled_days[] = 'saturday';
  }
  if (($date_selector->enabled_days & 0x64) == 0x64) {
    $enabled_days[] = 'sunday';
  }
  $form = array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => 'Name',
      '#description' => 'The name of this date selector',
      '#default_value' => htmlspecialchars($date_selector->name),
      '#max_length' => 256,
    ),
    'offset' => array(
      '#type' => 'select',
      '#title' => 'Interval',
      '#description' => 'Select the number of weeks after the reference date to default the selection to.',
      '#default_value' => $date_selector->offset,
      '#options' => array(
        0 => t('Right Away'),
        1 => t('1 Day'),
        2 => t('2 Days'),
        3 => t('3 Days'),
        5 => t('5 Days'),
        7 => t('1 week'),
        14 => t('2 weeks'),
        21 => t('3 weeks'),
        28 => t('4 weeks'),
        35 => t('5 weeks'),
        42 => t('6 weeks'),
        49 => t('7 weeks'),
        56 => t('8 weeks'),
        63 => t('9 weeks'),
        70 => t('10 weeks'),
        77 => t('11 weeks'),
        84 => t('12 weeks'),
      ),
    ),
    'before_after' => array(
      '#type' => 'select',
      '#title' => 'Before/After',
      '#description' => 'Whether to start before or after the reference date',
      '#default_value' => $date_selector->before_after == 1 ? 'after' : 'before',
      '#options' => array(
        'before' => t('Before'),
        'after' => t('After'),
      ),
    ),
    'duration' => array(
      '#type' => 'select',
      '#title' => 'Duration',
      '#description' => 'The length of the window to allow users to select their time within',
      '#default_value' => $date_selector->duration,
      '#options' => array(
        0 => t('Right Away'),
        1 => t('1 Day'),
        2 => t('2 Days'),
        3 => t('3 Days'),
        5 => t('5 Days'),
        7 => t('1 week'),
        14 => t('2 weeks'),
        21 => t('3 weeks'),
        28 => t('4 weeks'),
        35 => t('5 weeks'),
        42 => t('6 weeks'),
        49 => t('7 weeks'),
        56 => t('8 weeks'),
        63 => t('9 weeks'),
        70 => t('10 weeks'),
        77 => t('11 weeks'),
        84 => t('12 weeks'),
      ),
    ),
    'enabled_days' => array(
      '#type' => 'checkboxes',
      '#title' => 'Enabled Days',
      '#description' => 'The days of the week that are available to be booked',
      '#options' => array(
        'monday' => t('Monday'),
        'tuesday' => t('Tuesday'),
        'wednesday' => t('Wednesday'),
        'thursday' => t('Thursday'),
        'friday' => t('Friday'),
        'saturday' => t('Saturday'),
        'sunday' => t('Sunday'),
      ),
      '#default_value' => $enabled_days,
    ),
    'time_slots' => array(
      '#type' => 'textfield',
      '#title' => 'Time Slots',
      '#description' => 'A list of time slots that may be available for booking a room',
      '#default_value' => check_plain($date_selector->time_slots),
      '#attributes' => array('class' => array('editable-list-element')),
    ),
    'date_selector_id' => array(
      '#type' => 'hidden',
      '#value' => $date_selector_id,
    ),
    'save' => array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
    ),
  );
  return $form;
}

/**
 * Commit the changes the user has made to the settings of a date selector.
 *
 * @param array $form
 * @param array $form_state
 */
function jarrow_date_selector_settings_submit($form, &$form_state) {

  $values = $form_state['values'];
  $date_selector_id = $values['date_selector_id'];
  $enabled_days = 0;
  $enabled_days |= ($values['enabled_days']['monday'] ? 0x1 : 0x0);
  $enabled_days |= ($values['enabled_days']['tuesday'] ? 0x2 : 0x0);
  $enabled_days |= ($values['enabled_days']['wednesday'] ? 0x4 : 0x0);
  $enabled_days |= ($values['enabled_days']['thursday'] ? 0x8 : 0x0);
  $enabled_days |= ($values['enabled_days']['friday'] ? 0x16 : 0x0);
  $enabled_days |= ($values['enabled_days']['saturday'] ? 0x32 : 0x0);
  $enabled_days |= ($values['enabled_days']['sunday'] ? 0x64 : 0x0);

  if (db_update('etd_date_selector')
      ->fields(array(
        'name' => $values['name'],
        'offset' => $values['offset'],
        'before_after' => $values['before_after'] == 'after' ? 1 : 0,
        'duration' => $values['duration'],
        'enabled_days' => $enabled_days,
        'time_slots' => $values['time_slots'],
      ))
      ->condition('id', $date_selector_id)
      ->execute() === NULL
  ) {
    drupal_set_message('Problem saving settings', 'error');
    drupal_exit();
  }
  drupal_set_message('Settings Updated');
}

/**
 * Create the date selector for editing.
 *
 * @param object configuration
 * @param array $form
 * @param int $form_user
 * @param object $submission
 * @param int $form_element_id
 *
 * @return array
 */
function jarrow_date_selector_edit($configuration, $form, $form_user = NULL, $submission = NULL, $form_element_id) {

  list($timeslots, $enabled_days, $start_time, $end_time, $entries) =
    jarrow_date_selector_init($configuration, $form_user, $submission, $form, $form_element_id);

  $form = array(
    '#type' => 'dateselector',
    '#enabled_days' => $enabled_days,
    '#timeslots' => $timeslots,
    '#default_value' => array('entries' => $entries),
    '#display_date' => $start_time,
    '#users' => array($form_user => $form_user),
    '#active_user' => $form_user,
    '#start_date' => $start_time,
    '#end_date' => $end_time,
    '#element_validate' => array('_jarrow_date_selector_validate'),
  );
  return $form;
}

/**
 * Initialize the date selector view
 *
 * @param object configuration
 * @param array $form
 * @param int $form_user
 * @param object $submission
 * @param int $form_element_id
 *
 * @return array
 */
function jarrow_date_selector_view($configuration, $form, $form_user = NULL, $submission = NULL, $form_element_id) {

  global $user;

  $path = drupal_get_path('module', 'jarrow');
  drupal_add_css($path . '/providers/utilities/date_selector.css');
  list($timeslots, $enabled_days, $start_time, $end_time, $entries) =
    jarrow_date_selector_init($configuration, NULL, $submission, $form, $form_element_id);

  $user_ids = jarrow_get_role_user_list($submission, TRUE);
  $users = _jarrow_get_names_for_users($user_ids);

  $form = array(
    '#type' => 'dateselector',
    '#enabled_days' => $enabled_days,
    '#timeslots' => $timeslots,
    '#default_value' => array('entries' => $entries), // contains which dates are selected
    '#display_date' => $start_time,
    '#users' => $users,
    '#start_date' => $start_time,
    '#end_date' => $end_time,
    '#active_user' => $user->uid,
    '#attributes' => array('class' => array('date-selector')),
  );
  return $form;
}

/**
 * Save the date selections. If the value cannot be saved into the database,
 * then rollback the transaction
 *
 * @param object $configuration
 *    The configuration information of the date selector
 * @param object $submission
 *    The submission these date selections refer to
 * @param int $user_id
 *    The user id of user submitting the dates
 * @param array $value
 *    An array of date selections by user id
 * @param int $form_id
 * @param int $form_element_id
 */
function jarrow_date_selector_save($configuration, $submission, $user_id, $value, $form_id, $form_element_id) {

  $txn = db_transaction();
  if (isset($value['entries'])) {
    foreach ($value['entries'] as $user_id => $entries) {
      if (jarrow_date_selector_delete_entry($configuration->selector, $submission->id, $user_id, $form_id, $form_element_id) === NULL) {
        $txn->rollback();
        return;
      }
    }
    foreach ($value['entries'] as $user_id => $entries) {
      foreach ($entries as $t_id => $dates) {
        foreach ($dates as $date => $enabled) {
          if ($enabled) {
            $result = db_insert('etd_date_selector_entries')
              ->fields(array(
                'date_selector' => $configuration->selector,
                'submission' => $submission->id,
                'user' => $user_id,
                'date' => $date,
                'timeslot' => $t_id,
                'form_id' => $form_id,
                'form_element_id' => $form_element_id,
              ))
              ->execute();
            if ($result === NULL) {
              $txn->rollback();
              return;
            }
          }
        }
      }
    }
  } else {
    if (jarrow_date_selector_delete_entry($configuration->selector, $submission->id, $user_id, $form_id, $form_element_id) === NULL) {
      $txn->rollback();
      return;
    }
  }


  /*
 foreach ($value['entries'][$user_id] as $t_id => $dates) {
   foreach ($dates as $date => $enabled) {
     if ($enabled) {
       if (db_insert('etd_date_selector_entries')
               ->fields(array(
                 'date_selector' => $configuration->selector,
                 'submission' => $submission->id,
                 'user' => $user_id,
                 'date' => $date,
                 'timeslot' => $t_id,
               ))
               ->execute() === NULL) {
         $txn->rollback();
         return;
       }
               dsm('dates saved!!');
     }
   }
 } */
}

/**
 * Remove from the database all date selections by user_id the specified
 * date selector and submission id
 */
function jarrow_date_selector_delete_entry($date_selector_id, $submission_id, $user_id, $form_id, $form_element_id) {

  return db_delete('etd_date_selector_entries')
    ->condition('date_selector', $date_selector_id)
    ->condition('submission', $submission_id)
    ->condition('user', $user_id)
    ->condition('form_id', $form_id)
    ->condition('form_element_id', $form_element_id)
    ->execute();
}

/**
 * Make sure the date starts at midnight.
 *
 * NOTE: 86400 is the number of seconds in a day.
 *
 * @param int $unixtime
 *
 * @return int
 */
function jarrow_date_start_at_midnight($unixtime) {

  return $unixtime - ($unixtime % 86400);
}

/**
 * Create the Date Selector.
 *
 * Note: I have renamed the $element variable to be $date_selector
 * for easier readability.
 *
 * @param array $date_selector
 * @param array $form_state
 *
 * @return array
 */
function jarrow_date_selector_process($date_selector, &$form_state) {

  global $user;
  $path = drupal_get_path('module', 'jarrow');
  drupal_add_css($path . '/providers/utilities/date_selector.css');

  $id = $date_selector['#id'];
  $start_date = isset($date_selector['#start_date']) ? $date_selector['#start_date'] : 0;
  $end_date = isset($date_selector['#end_date']) ? $date_selector['#end_date'] : PHP_INT_MAX;
  $display_date = $date_selector['#display_date'];

  $start_date = is_int($start_date) ? $start_date : jarrow_process_date($start_date);
  $end_date = is_int($end_date) ? $end_date : jarrow_process_date($end_date);
  $display_date = is_int($display_date) ? $display_date : jarrow_process_date($display_date);
  $display_date = $display_date < $start_date ? $start_date :
    ($display_date > $end_date ? $end_date : $display_date);

  $users = (isset($date_selector['#users']) && (count($date_selector['#users']) > 0)) ? $date_selector['#users'] : array($user->uid => $user->name);

  //Create the hidden values for the days which have been selected but are not
  //viewable.  We add every date here, and then remove any which lie in the
  //visible range later
  $hidden_values = _jarrow_date_selector_get_hidden_values($date_selector, $users);

  // Check to see if the user clicked one of the navigation buttons.
  // If so, then change the date forward or backward by a week.
  if (FALSE) {
    // Running this hack because $form_state['triggering_element'] is not
    // being set and that causes the date selector to fail. Until the problem
    // can be resolved I need to use this hack to make it work
    $refresh_date_selector = jarrow_date_selector_button_hack($date_selector);
  } else {
    // This should be the proper function call if the ajax was working properly
    $refresh_date_selector = jarrow_date_selector_adjust_dates($date_selector, $form_state);

  }

  if (count($refresh_date_selector) > 0) {
    $display_date = $refresh_date_selector['new_date'];
  }

  $old_timezone = date_default_timezone_get();
  date_default_timezone_set('UTC');

  // Determine what dates to show
  list($header, $rows, $prev_button, $next_button) = _jarrow_date_selector_calculate_dates($date_selector, $start_date, $end_date, $display_date, $users, $hidden_values);

  $date_selector['#rows'] = $rows;
  $date_selector['#header'] = $header;
  $date_selector['#tree'] = TRUE;
  $date_selector['#prefix'] = (isset($date_selector['#prefix']) ? $date_selector['#prefix'] : '') . '<div id="' . $id . '-date-selector-wrapper" class="date-selector-wrapper">';
  $date_selector['#suffix'] = '</div>' . (isset($date_selector['#suffix']) ? $date_selector['#suffix'] : '');
  $date_selector['#display_date'] = $display_date;
  $date_selector['next'] = $next_button;
  $date_selector['prev'] = $prev_button;
  $date_selector['#hidden_values'] = $hidden_values;
  //if ($refresh_date_selector) {
  if (count($refresh_date_selector) > 0) {
    $form_state['ajax_date_selector_container'] = $date_selector;
  }
  date_default_timezone_set($old_timezone);
  return $date_selector;
}

/**
 * Create the hidden values for the days which have been selected but are not
 * viewable.  We add every date here, and then remove any which lie in the
 * visible range later.
 *
 * @param array $date_selector
 * @param array $users
 *
 * @return array
 */
function _jarrow_date_selector_get_hidden_values($date_selector, $users) {

  $hidden_values = array();
  foreach ($users as $uid => $user_name) {
    if (isset($date_selector['#value']['entries'][$uid])) {
      foreach ($date_selector['#value']['entries'][$uid] as $t_id => $dates) {
        if (is_array($dates)) {
          foreach ($dates as $date => $enabled) {
            if ($enabled) {
              $hidden_values[$uid][$t_id][$date] = $date_selector['entries'][$uid][$t_id][$date] = array(
                '#type' => 'hidden',
                '#theme' => 'hidden',
                '#name' => $date_selector['#name'] . '[entries][' . $uid . '][' . $t_id . '][' . $date . ']',
                '#value' => 1,
              );
            }
          }
        }
      }
    }
  }
  return $hidden_values;
}

/**
 * Determine what dates the date selector should be displaying.
 *
 * @param array $date_selector
 * @param int $start_date
 *    The date that defines the beginning of the time interval.
 * @param int $end_date
 *    The last date defines the end of the time interval.
 * @param int $display_date
 *    The date the date selector should begin the display with.
 *    This will change if the time interval spans several weeks.
 * @param array $users
 *    The list of users that need to select their available dates.
 *    This contains their user ids.
 * @param array $hidden_values
 *    Values that we need to keep track of. In this case the selected dates by each individual.
 *
 * @return array
 */
function _jarrow_date_selector_calculate_dates($date_selector, $start_date, $end_date, &$display_date, $users, &$hidden_values) {

  static $days = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',);

  $enabled_days = array_intersect($date_selector['#enabled_days'], $days);
  $id = $date_selector['#id'];
  $timeslots = (array) $date_selector['#timeslots'];
  $active_user = isset($date_selector['#active_user']) ? $date_selector['#active_user'] : NULL;
  $multi_user = count($users) > 1;

  // Create the next/previous buttons
  $prev_button = jarrow_date_selector_create_button($id, '<<<', $date_selector['#name'], 'prev');
  $next_button = jarrow_date_selector_create_button($id, '>>>', $date_selector['#name'], 'next');

  // Since Drupal uses Sunday as 0, but php assumes the week starts on Monday,
  // we have to move one index backwards to compensate.
  $first_day = variable_get('date_first_day', 0);
  $adjusted_first_day = ($first_day == 0) ? 6 : $first_day - 1;

  do {
    if ($display_date < $start_date) {
      $display_date = $start_date;
    } else {
      if ($display_date > $end_date) {
        $display_date = $end_date;
      }
    }
    $past_first_day = TRUE;
    $past_last_day = TRUE;

    // Figure out the dates that each day of the week will display.
    // We use monday as a reference (because php likes that) and increment by
    // the number of seconds in a day (86400) each day.  However, after the first
    // day we want to move back a week, since when we display the days, we'll be
    // starting on the first day and incrementing from there.  Thus, the lowest
    // date should be the first date, so we subtract a week from it and any
    // following date.
    $monday = strtotime('midnight', strtotime('monday this week', $display_date));
    $week_offset = (($monday + $adjusted_first_day * 86400) < $display_date) ? 604800 : 0;

    $header = array();
    $dates = array();
    $epoch_dates = array();
    for ($day = 0; $day < 7; $day++) {
      $epoch_dates[$day] = $monday + ($day * 86400) - ($day >= $adjusted_first_day ? 604800 : 0) + $week_offset;
      $dates[$day] = date('Ymd', $epoch_dates[$day]);
      if ($epoch_dates[$day] + 604800 <= $end_date && isset($enabled_days[$day])) {
        $past_last_day = FALSE;
      }
      if ($epoch_dates[$day] - 604800 >= $start_date && isset($enabled_days[$day])) {
        $past_first_day = FALSE;
      }
    }

    if ($past_first_day) {
      $header[] = '';
    } else {
      $header[] = array(
        'data' => ajax_pre_render_element($prev_button)
      );
    }
    //Loop through the days of the week, starting with the first day.  Only add
    //entries for enabled days
    $day = $adjusted_first_day;
    $displayable_date = FALSE;
    do {
      if (isset($enabled_days[$day])) {
        if ($multi_user) {
          $colskip = count($timeslots);
        } else {
          $colskip = 1;
        }
        if ($epoch_dates[$day] >= $start_date && $epoch_dates[$day] <= $end_date) {
          $header [] = array('data' => date('D, M d Y', $epoch_dates[$day]), 'colspan' => $colskip);
          $displayable_date = TRUE;
        }
      }
      $day++;
      $day %= 7;
    } while ($day != $adjusted_first_day);
    if (!$displayable_date) {
      $display_date = $monday + 604800; // advance to the next week
    }
  } while (!$displayable_date);

  if ($past_last_day) {
    $header[] = '';
  } else {
    $header [] = array(
      'data' => ajax_pre_render_element($next_button)
    );
  }

  //Go through each time slot and add a row for selecting each day.
  $rows = array();
  if ($multi_user) {
    $row = array('');
    for ($day = 0; $day < 7; $day++) {
      if (isset($enabled_days[$day]) && $epoch_dates[$day] >= $start_date && $epoch_dates[$day] <= $end_date) {
        foreach ($timeslots as $timeslot) {
          $row[] = $timeslot;
        }
      }
    }
    $row [] = '';
    $rows[] = $row;
    $row_array = $users;
  } else {
    $row_array = $timeslots;
  }

  $day = 0;
  foreach ($row_array as $r_id => $row_label) {

    $row = array();

    // Determine the user and row that we are currently processing
    if ($multi_user) {
      $timeslot_array = $timeslots;
      $current_user = $r_id;
    } else {
      $timeslot_array = array($r_id => $row_label);
      $user_ids = array_keys($users);
      $current_user = $user_ids[0]; // there should only be one
    }

    $row_class = ($current_user != $active_user) ? 'date-selector-row-disabled' : $row_class = 'date-selector-row-enabled';

    $row[] = array('data' => check_plain($row_label), 'class' => $row_class);
    do {
      if (isset($enabled_days[$day]) && $epoch_dates[$day] >= $start_date && $epoch_dates[$day] <= $end_date) {

        foreach ($timeslot_array as $t_id => $timeslot) {

          //Construct the checkbox to represent this time slot on the given day.
          //The important part is to correctly set the name so that the value will
          //be passed to the server correctly.
          $checkbox = array(
            '#type' => 'checkbox',
            '#name' => $date_selector['#name'] . '[entries][' . $current_user . '][' . $t_id . '][' . $dates[$day] . ']',
          );

          if ($current_user != $active_user) {
            $checkbox['#attributes'] = array('disabled' => TRUE);
          }

          // If the default_values parameter has been set then ensure the selected dates
          //are checked.

          if (isset($date_selector['#timeslots'])) {
            foreach ($date_selector['#timeslots'] as $selected_timeslot) {
              if ($selected_timeslot == $timeslot) {
                if (isset($date_selector['#default_value']['entries'][1])) {
                  foreach ($date_selector['#default_value']['entries'][1] as $date_selection_array) {
                    foreach ($date_selection_array as $dateSelected => $value) {
                      if (strpos($checkbox['#name'], '[' . $dateSelected . ']') !== FALSE) {
                        $checkbox['#checked'] = TRUE;
                      }
                    }
                  }
                }
              }
            }
          }

          if (isset($hidden_values[$current_user][$t_id][$dates[$day]])) {
            if ($current_user == $active_user) {
              unset($hidden_values[$current_user][$t_id][$dates[$day]]);
            }
            $checkbox['#checked'] = TRUE;
          }
          $date_selector['entries'][$current_user][$t_id][$dates[$day]] = $checkbox;
          $row[] = array('data' => drupal_render($checkbox), 'class' => $row_class);
        }
      }
      $day++;
      $day %= 7;
    } while ($day != $adjusted_first_day);

    $row [] = array('data' => check_plain($row_label), 'class' => $row_class);
    $rows[] = array('data' => $row);
  }

  return array($header, $rows, $prev_button, $next_button);
}

/**
 * Helper function to create the next or previous buttons for the date selector
 *
 * @param int $id
 * @param string $button_text
 * @param string $name
 * @param string $direction
 *
 * @return array
 */
function jarrow_date_selector_create_button($id, $button_text, $name, $direction) {


  $button = array(
    '#type' => 'button',
    '#value' => $button_text,
    '#id' => $id . '-' . $direction,
    '#name' => $name . '[' . $direction . ']',
    '#date_selector_direction' => $direction,
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'event' => 'click',
      'callback' => 'jarrow_date_picker_ajax',
      'wrapper' => $id . '-date-selector-wrapper',
      //  'progress' => array(
      //    'message' => NULL,
      //    'type' => 'throbber',
      // ),
    ),
  );
  return $button;
}

/**
 * This hack has been implemented because $form_state['triggering_element'] is not
 * being set and causes the date selector to operate incorrectly. Until the problem
 * can be resolved I need to use this hack to make it work. Once it is fixed
 * delete this function.
 *
 * NOTE: 604800 is the number of seconds in a week.
 *
 * @param array $date_selector
 *
 * @return array
 */
function jarrow_date_selector_button_hack($date_selector) {

  $result = array();
  if (isset($_POST['_triggering_element_name'])) {
    $element = $_POST['_triggering_element_name'];
    if (stristr($element, 'frm')) {
      $pieces = explode("[", $element);
      $processed = array();
      foreach ($pieces as $piece) {
        $position = strpos($piece, ']');
        if ($position !== FALSE) {
          $processed[] = substr($piece, 0, $position);
        } else {
          $processed[] = $piece;
        }
      }
      $form_id = str_replace('_', '-', $processed[0]) . '-' . $processed[1];
      if (stristr($date_selector['#id'], $form_id)) {
        $result = array('form_id' => $form_id, 'direction' => $processed[2]);
        if ($result['direction'] == 'prev') {
          $result['new_date'] = $date_selector['#value']['display_date'] - 604800;
        } else {
          $result['new_date'] = $date_selector['#value']['display_date'] + 604800;
        }
      }
    }
  }
  return $result;
}

/**
 * This is the proper function to be using once jarrow_date_selector_button_hack
 * is no longer needed.
 *
 * NOTE: 604800 is the number of seconds in a week.
 *
 * @param array $date_selector
 * @param $form_state
 *
 * @return boolean
 */
function jarrow_date_selector_adjust_dates($date_selector, &$form_state) {

  $result = array();
  if (isset($form_state['triggering_element']) &&
    isset($form_state['triggering_element']['#date_selector_direction'])
  ) {
    $element = $form_state['triggering_element']['#name'];
    if (stristr($element, 'frm')) {
      $pieces = explode("[", $element);
      $processed = array();
      foreach ($pieces as $piece) {
        $position = strpos($piece, ']');
        if ($position !== FALSE) {
          $processed[] = substr($piece, 0, $position);
        } else {
          $processed[] = $piece;
        }
      }
      $form_id = str_replace('_', '-', $processed[0]) . '-' . $processed[1];
      if (stristr($date_selector['#id'], $form_id)) {
        $result = array(
          'form_id' => $form_id,
          'direction' => $form_state['triggering_element']['#date_selector_direction']
        );
        if ($result['direction'] == 'prev') {
          $result['new_date'] = $date_selector['#value']['display_date'] - 604800;
        } else {
          $result['new_date'] = $date_selector['#value']['display_date'] + 604800;
        }
      }
    }
  }
  return $result;
}

/**
 * Callback element needs only select the portion of the form to be updated.
 * Since #ajax['callback'] return can be HTML or a renderable array (or an
 * array of commands), we can just return a piece of the form.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function jarrow_date_picker_ajax($form, &$form_state) {
  dd(print_r($form_state, TRUE));
  return $form_state['ajax_date_selector_container'];
}

/**
 * Validate that the date selector has selections if this widget
 * is marked as required.
 *
 * @param array $element
 * @param array $form
 * @param array $form_state
 *
 * @return bool|null
 */
function _jarrow_date_selector_validate($element, $form, &$form_state) {

  global $user;

  // If this is an ajax call then return as we don't want
  // do any error checking unitl the submit button is pressed.
  if ($_GET['q'] == 'system/ajax') {
    return;
  }

  //If limit validation errors contains no elements then do not perform
  // any error checking.
  if (count($form['triggering_element']['#limit_validation_errors']) == 0) {
    return;
  }
  $error = FALSE;
  if (in_array($user->uid, $element['#users'])) {
    if ($element['#required'] == 1) {
      if (isset($element['#value']['entries'][$user->uid])) {
        $error = (count($element['#value']['entries'][$user->uid]) == 0);
      } else {
        $error = TRUE;
        $element_id = $element['#parents'][0];
        $form_state[$element_id]['error_state'] = TRUE;
      }
      if ($error) {
        form_set_error(implode('][', $element['#parents']), 'You must select at least one date.');
        drupal_set_message('You must select at least one date for ' . $element['#title'], 'error');
      }
    }
  }
  return $error;
}

/**
 * Prevent the submission handler from continuing and redisplay the current page.
 *
 */
function jarrow_date_selector_display_error() {

  $current_url = explode('/', request_uri());
  $new_url = '';
  $size = count($current_url);
  $build_url = FALSE;
  for ($i = 1; $i < $size; $i++) {
    if ($build_url) {
      $new_url .= '/' . $current_url[$i];
    } else {
      if (($current_url[$i] == 'jarrow') && !$build_url) {
        $new_url .= $current_url[$i];
        $build_url = TRUE;
      }
    }
  }
  drupal_goto($new_url);
}

/**
 * Check to see if all the users who needed to submit date selections have
 * done so.
 *
 * @param $date_selector
 *
 * @return bool
 */
function jarrow_date_selector_have_all_users_submitted($date_selector) {

  $users = $date_selector['#users'];
  $entries = $date_selector['#default_value']['entries'];
  foreach ($users as $key => $user) {
    if (!isset($entries[$key])) {
      return FALSE;
    }
  }
  return TRUE;
}
