<?php

$path = drupal_get_path('module', 'jarrow');
include_once $path . '/workflow/data_model/data_model.inc';

/**
 * @file
 *  Provides the built in form elements for Jarrow.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca> Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define the jarrow specific element types that a form can have.
 *
 * @return array
 */
function jarrow_etd_form_element_types() {

  return array(
    'data' => array(
      'name' => t('Data Source'),
      'display' => 'jarrow_view_object',
      'edit' => 'jarrow_data_object',
      'config' => 'jarrow_data_config',
      'save' => 'jarrow_data_object_save',
    ),
    'view' => array(
      'name' => t('Data Source Read Only'),
      'display' => 'jarrow_view_object',
      'edit' => 'jarrow_view_object',
      'config' => 'jarrow_data_view_config',
    ),
    'date_selector' => array(
      'name' => t('Day Selector'),
      'display' => 'jarrow_date_selector_view',
      'edit' => 'jarrow_date_selector_edit',
      'config' => 'jarrow_date_selector_config',
      'save' => 'jarrow_date_selector_save',
    ),
    'long_text' => array(
      'name' => t('Long Text Description'),
      'display' => 'jarrow_long_text_view',
      'config' => 'jarrow_long_text_config',
    ),
  );
}

/**
 * Return the form object.
 *
 * @param object $configuration
 * @param object $form
 * @param object $submission
 * @param int $form_user
 * @param boolean $readonly
 *
 * @return mixed
 */
function jarrow_get_form_object($configuration, $form, $submission, $form_user, $readonly) {

  $associated_element = $configuration->associated_element;
  if (isset($associated_element)) {
    $element = $submission->data_model->elements[$associated_element];

    if ($element) {
      $types = module_invoke_all('etd_data_element_types', $element->source);
      if (isset($types[$element->type])) {
        $sources = module_invoke_all('etd_data_element_sources');
        if (isset($sources[$element->source])) {
          $value = call_user_func($sources[$element->source]['get'], $element->source_config, $associated_element, $form_user, $submission);
          $display_function = NULL;
          $disable_edit = FALSE;
          if (strtolower($types[$element->type]['name']) == 'profile item') {
            // Since profiles don't have edit functions associated with them - just display them
            $display_function = $types[$element->type]['display'];
          } else {
            // Check if the default answer variable has been set.
            if (isset($element->type_config->type_config_container->default_answer)) {
              $default_answer = $element->type_config->type_config_container->default_answer;
              if ((strlen(trim($default_answer)) > 0) && ($element->type_config->type_config_container->default_answer_editable == 0)) {
                // If the default answer is not editable, then just display this field.
                if ($readonly) {
                  // display in an already submitted form
                  $display_function = $types[$element->type]['display'];
                } else {
                  // display in an editable form
                  $display_function = $types[$element->type]['edit'];
                  $disable_edit = TRUE;
                }
              } else {
                $display_function = $readonly ? $types[$element->type]['display'] : $types[$element->type]['edit'];
              }
            } else {
              $display_function = $readonly ? $types[$element->type]['display'] : $types[$element->type]['edit'];
            }
          }
          //$value = isset($value[$form_user]) ? $value[$form_user] : NULL;
          $ret_array = call_user_func($display_function, $value, $element->type_config);
          if ($disable_edit) {
            // We need to set these elements to readonly as we want the values returned
            // to us so that we can save them. If you change this to use disabled then
            // you will break the system.
            $ret_array['#attributes']['readonly'] = 'readonly';
            $ret_array['#attributes']['class'][] = 'read-only';
          }
          return $ret_array;
        }
      }
    }
  }
}

/**
 * @param $configuration
 * @param $form
 * @param null $form_user
 * @param null $submission
 *
 * @return mixed
 */
function jarrow_data_object($configuration, $form, $form_user = NULL, $submission = NULL) {

  return jarrow_get_form_object($configuration, $form, $submission, $form_user, FALSE);
}

/**
 * @param $configuration
 * @param $form
 * @param null $form_user
 * @param null $submission
 *
 * @return mixed
 */
function jarrow_view_object($configuration, $form, $form_user = NULL, $submission = NULL) {

  return jarrow_get_form_object($configuration, $form, $submission, $form_user, TRUE);
}

/**
 * Save the given submitted value into the database.
 *
 * @param object $configuration
 * @param object $submission
 * @param int $user_id
 * @param string $value
 *
 * @return bool
 */
function jarrow_data_object_save($configuration, $submission, $user_id, $value) {

  $associated_element = $configuration->associated_element;
  if (isset($associated_element)) {
    $element = $submission->data_model->elements[$associated_element];
    if ($element) {
      //As both of the config fields are module provided, they are simply stored as a serialized
      //string, which must be de-serialized before being passed to the user.
      $sources = module_invoke_all('etd_data_element_sources');
      if (isset($sources[$element->source])) {
        $types = module_invoke_all('etd_data_element_types', $element->source);
        if (isset($types[$element->type])) {
          if (isset($types[$element->type]['update'])) {
            $update_function = $types[$element->type]['update'];
            $old_val = jarrow_get_value_form($element->source_config, $associated_element, $user_id, $submission);
            $old_val = $old_val[$user_id];
            call_user_func($update_function, $value, $submission, $element->type_config, $old_val);
          }
          if (call_user_func($sources[$element->source]['set'], $element->source_config, $associated_element, $submission->id, $user_id, $value)) {
            return TRUE;
          }
        }
      }
    }
  }
  return FALSE;
}

/**
 * Create the tree display for form elements.
 *
 * @param int $parent_id
 * @param object $elements
 * @param $type
 *
 * @return array
 */
function jarrow_data_element_tree_display($parent_id, $elements, $type) {

  static $sources;
  if (!$sources) {
    $sources = module_invoke_all('etd_element_sources');
  }
  $data = array();

  foreach ($elements as $element) {
    if ($element->parent == $parent_id && (!isset($element->digital_object) || $element->digital_object == 0)) {
      $this_data = array('label' => $element->name, 'id' => $element->id);

      //The data element is read only if the source element doesn't define a
      //'set' function.
      $this_data['read_only'] = !isset($sources[$element->source]) || !isset($sources[$element->source]['set']);

      if ($element->type == 'group') {
        $this_data['children'] = jarrow_data_element_tree_display($element->id, $elements, $type);
        if (count($this_data['children']) > 0) {
          $data[] = $this_data;
        }
      } else {
        $data[] = $this_data;
      }
    }
  }
  return $data;
}

/**
 * Create the config panel for use with form elements and set its
 * state to read only.
 *
 * @param $data_model_id
 *
 * @return array
 */
function jarrow_data_view_config($data_model_id) {

  return jarrow_data_config($data_model_id, TRUE);
}

/**
 * Create the data config panel for use with form elements. In
 * this case it is a select box with a tree style selector.
 *
 * @param $data_model_id
 * @param bool $readonly
 *
 * @return array
 */
function jarrow_data_config($data_model_id, $readonly = FALSE) {

  $path = drupal_get_path('module', 'jarrow');
  //include_once $path . '/workflow/data_model/data_model.inc';
  module_load_include('inc', 'jarrow', 'data_model');
  module_load_include('inc', 'jarrow', 'form_editor');
  libraries_load('jqtree');
  drupal_add_js($path . '/lib/js/jquery.tree.dropdown.js');
  drupal_add_css($path . '/lib/css/jquery.tree.dropdown.css');

  $elements = jarrow_load_data_elements($data_model_id);
  $tree = jarrow_data_element_tree_display(NULL, $elements, $readonly ? 'readonly' : NULL);

  drupal_add_js(array('jarrow' => array('data_source_tree' => json_encode($tree))), 'setting');
  drupal_add_js($path . '/providers/support/js/form_editor_dropdown_tree.js');

  return array(
   'associated_element' => array(
      '#type' => 'textfield',
      '#title' => t('Destination Data Element'),
      '#description' => t('The data element this form will save its value to. Use this field with care as you may overwrite an existing value.'),
      '#ajax' => array(
        'event'=>'change',
        'wrapper' => 'linked_form_elements',
        'callback' => 'jarrow_form_editor_linked_form_ajax',
      ),
    ),
  );
}

/**
 * Create the date selector config panel.
 *
 * @param $data_model_id
 *
 * @return array
 */
function jarrow_date_selector_config($data_model_id) {

  $path = drupal_get_path('module', 'jarrow');

  include_once $path . '/workflow/data_model/data_model.inc';
  $elements = jarrow_load_data_elements($data_model_id);
  $dates = array(0 => 'Form Creation Date');
  foreach ($elements as $element_id => $element) {
    if ($element->type == 'date') {
      $dates[$element_id] = $element->name;
    }
  }

  $date_selector_result = db_select('etd_date_selector', 'ds')
    ->fields('ds', array('id', 'name'))
    ->execute();

  $date_selectors = array();
  foreach ($date_selector_result as $date_selector) {
    $date_selectors[$date_selector->id] = $date_selector->name;
  }
  return array(
    'selector' => array(
      '#type' => 'select',
      '#title' => t('Date Selector'),
      '#description' => 'The date selector to use as a template',
      '#options' => $date_selectors,
    ),
    'reference_date' => array(
      '#type' => 'select',
      '#title' => t('Reference Date'),
      '#description' => t('The date to use as a reference for this selector'),
      '#options' => $dates,
    ),
  );
}

/**
 * Initialize the date selector by loading up the values stored in the
 * database.
 */
function jarrow_date_selector_init($configuration, $form_user, $submission, $form, $form_element_id) {

  $date_selector = db_select('etd_date_selector')
    ->fields('etd_date_selector')
    ->condition('id', $configuration->selector)
    ->execute()
    ->fetch();

  $enabled_days = array();
  if (($date_selector->enabled_days & 0x1) == 0x1) {
    $enabled_days[] = 'monday';
  }
  if (($date_selector->enabled_days & 0x2) == 0x2) {
    $enabled_days[] = 'tuesday';
  }
  if (($date_selector->enabled_days & 0x4) == 0x4) {
    $enabled_days[] = 'wednesday';
  }
  if (($date_selector->enabled_days & 0x8) == 0x8) {
    $enabled_days[] = 'thursday';
  }
  if (($date_selector->enabled_days & 0x16) == 0x16) {
    $enabled_days[] = 'friday';
  }
  if (($date_selector->enabled_days & 0x32) == 0x32) {
    $enabled_days[] = 'saturday';
  }
  if (($date_selector->enabled_days & 0x64) == 0x64) {
    $enabled_days[] = 'sunday';
  }
  $timeslots = json_decode($date_selector->time_slots);


  $creation_time = db_select('etd_submission_stages')
    ->fields('etd_submission_stages', array('creation_time'))
    ->condition('submission', $submission->id)
    ->condition('stage', $submission->current_stage)
    ->execute()
    ->fetchField();

  $creation_time = intval($creation_time);

  $start_time = jarrow_date_start_at_midnight($creation_time + 86400 * intval($date_selector->offset));

  // add 86399 seconds to ensure the end date ends just before midnight
  $end_time = $start_time + 86400 * intval($date_selector->duration) + 86399;

  $entry_query = db_select('etd_date_selector_entries')
    ->fields('etd_date_selector_entries')
    ->condition('date_selector', $configuration->selector)
    ->condition('submission', $submission->id)
    ->condition('form_id', $form->id)
    ->condition('form_element_id', $form_element_id);
  if (!is_null($form_user)) {
    $entry_query = $entry_query->condition('user', $form_user);
  }
  $entry_result = $entry_query->execute();
  $entries = array($form_user => array());
  foreach ($entry_result as $entry) {
    $entries[$entry->user][$entry->timeslot][$entry->date] = 1;
  }

  return array($timeslots, $enabled_days, $start_time, $end_time, $entries);
}


function jarrow_long_text_config() {

  return array(
    'display_text' => array(
      '#type' => 'textarea',
      '#title' => t('text to display'),
    ),
  );
}

function jarrow_long_text_view($configuration) {

  return array(
    '#type' => 'item',
    '#markup' => $configuration->display_text,
  );
}
