<?php

/**
 * @file
 *  Handles the 'form' element source for Jarrow
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implements hook etd_data_element_sources().
 *
 * @return array
 */
function jarrow_etd_data_element_sources() {
  $sources = array(
    'form' => array(
      'label' => 'Form Input',
      'config' => 'jarrow_get_source_form',
      'get' => 'jarrow_get_value_form',
      'set' => 'jarrow_set_value_form',
      'addable' => TRUE,
    ),
    'role' => array(
      'label' => 'Role',
      'get' => 'jarrow_get_value_role',
      'set' => 'jarrow_set_value_role',
      'addable' => FALSE,
    ),
  );
  return $sources;
}

function jarrow_get_value_role($data_object, $element_id, $user_id, $submission) {
  $user_ids = db_select('etd_role_associations')
      ->fields('etd_role_associations', array('user'))
      ->condition('role', $data_object['role_id'])
      ->condition('submission', $submission->id)
      ->execute()
      ->fetchCol();


  $names = _jarrow_get_names_for_users($user_ids);
  $user_names = array();
  foreach ($user_ids as $this_user_id) {
    if ($this_user_id > 0) {

      $user_obj = user_load($this_user_id);
      if (isset($names[$this_user_id]) && strlen($names[$this_user_id]) > 0) {
        $output = $names[$this_user_id];
        $output .= '<' . $user_obj->name . '>';
      }
      else {

        $output = $user_obj->name;
      }
      $user_names[] = $output;
    }
  }
  return implode(',', $user_names);
}

function jarrow_set_value_role($data_object, $element_id, $submission_id, $user_id, $data) {
  $user_strings = explode(',', $data);
  $matches = array();
  $txn = db_transaction();
  db_delete('etd_role_associations')
      ->condition('role', $data_object['role_id'])
      ->condition('submission', $submission_id)
      ->execute();
  foreach ($user_strings as $user_string) {
    preg_match('/([^<]*<([^>]+)>)|([^<]+)/', $user_string, $matches);
    if (count($matches) > 1) {
      $user_name = $matches[2];
      if (strlen($user_name) == 0) {
        $user_name = $matches[3];
      }
      $loaded_user = jarrow_user_for_username($user_name);
      if ($loaded_user) {
        if (db_insert('etd_role_associations')
                ->fields(array(
                  'role' => $data_object['role_id'],
                  'submission' => $submission_id,
                  'user' => $loaded_user->uid,
                ))
                ->execute() === NULL) {
          $txn->rollback();
          return FALSE;
        }
      }
    }
    else {
      return FALSE;
    }
  }
}

/**
 * @param $data_object
 *
 * @return array
 */
function jarrow_get_source_form($data_object) {
  return array(
    'access' => array(
      '#type' => 'select',
      '#description' => 'Selects whether this data will be have one instance per user or for the entire submission',
      '#options' => array('single' => 'Single', 'multiple' => 'Multiple'),
      '#title' => t('Access'),
      '#default_value' => isset($data_object) ? $data_object->access : 'single'
    ),
  );
}

/**
 * @param $data_object
 * @param $element_id
 * @param $user_id
 * @param null $submission
 * @param int $digital_object_instance
 *
 * @return mixed|null|string
 */
function jarrow_get_value_form($data_object, $element_id, $user_id, $submission = NULL, $digital_object_instance = 0) {

  $is_multiple = isset($data_object) && isset($data_object->access) && $data_object->access !== 'single';
  $digital_object_id = (is_object($digital_object_instance)) ? $digital_object_instance->id: $digital_object_instance;
  if ($submission !== NULL) {
    $result = db_select('etd_submission_field', 'sub')
        ->fields('sub', array('data', 'user'))
        ->condition('submission', $submission->id)
        ->condition('data_element', $element_id)
        ->condition('digital_object_instance', $digital_object_id)
        ->orderBy('user')
        ->execute();

    if ($result) {
      if ($is_multiple) {
        foreach ($result as $datum) {
          if ($datum->user == $user_id) {
            return unserialize($datum->data);
          }
        }
      } else {
        $data = $result->fetch();
        return $data ? unserialize($data->data) : jarrow_get_default_answer($element_id);
      }
    } else {
      return NULL;
    }
  } else {
    return base64_encode(drupal_random_bytes(6));
  }
}

/**
 * Return the default answer for a form (if supplied).
 *
 * @param $element_id
 *
 * @return null
 */
function jarrow_get_default_answer($element_id) {

  $result = db_select('etd_data_elements', 'de')
    ->fields('de', array('type_config'))
    ->condition('id', $element_id)
    ->execute()
    ->fetch();
  $type_config = unserialize($result->type_config);
  if (isset($type_config->type_config_container) && isset($type_config->type_config_container->default_answer)) {
    $answer = $type_config->type_config_container->default_answer;
    if (count($answer) > 0) {
      return $answer;
    }
  }
  return NULL;
}

/**
 * Save the submission value into etd_submission_field database table. This
 * function gets called once for every submitted value.
 *
 * @param object $data_object
 * @param int $element_id
 * @param int $submission_id
 * @param int $user_id
 * @param string $data
 * @param int $digital_object_instance
 *
 * @return bool
 */
function jarrow_set_value_form($data_object, $element_id, $submission_id, $user_id, $data, $digital_object_instance = 0) {

  if (isset($data_object) && isset($data_object->access) && $data_object->access !== 'single') {
    $user_match = $user_id;
  } else {
    $user_match = 0;
  }

  db_delete('etd_submission_field')
      ->condition('submission', $submission_id)
      ->condition('data_element', $element_id)
      ->condition('user', $user_match)
      ->condition('digital_object_instance', $digital_object_instance)
      ->execute();

  $data = serialize($data);
  $query = db_insert('etd_submission_field')
      ->fields(array(
    'submission' => $submission_id,
    'data_element' => $element_id,
    'user' => $user_match,
    'data' => $data,
    'digital_object_instance' => $digital_object_instance,
      )
  );
  return $query->execute() !== NULL;
}

