<?php

/**
 * @file
 *  Handles the creation and manipulation of data models and data elements.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

module_load_include('inc', 'jarrow', 'providers/data_element_types');
module_load_include('inc', 'jarrow', 'providers/element_sources');
module_load_include('inc', 'jarrow', 'workflow/metatags/metatags');

/**
 * Loads a data model's definition from the database.
 *
 * @param int $data_model_id
 *    The id of the data model to load from the database
 * @param boolean $load_draft
 *    If true, and there is a draft version of the data model in the database,
 *    then the function will load that instead
 *
 * @return stdObj
 *    An object representing the data model, or null if the data model could not be loaded.
 */
function jarrow_load_data_model($data_model_id, $load_draft) {

  $data_model = FALSE;
  if ($load_draft) {
    //Check if there is a draft version of this data model
    $result = db_select('etd_data_model_drafts', 'dmd')
      ->fields('dmd', array('id', 'data'))
      ->condition('id', $data_model_id)
      ->execute();
    $draft = TRUE;
    if ($result) {
      $data_model = $result->fetch();
    }
  }

  //If the data model was not able to be loaded from the drafts table, then try to load it from the saved table.
  if (!$data_model) {
    $data_models = & drupal_static(__FUNCTION__);
    if (!isset($data_models[$data_model_id])) {
      $cache = cache_get('jarrow_data_models');
      if ($cache) {
        $data_models = $cache->data;
        if (isset($data_models[$data_model_id])) {
          $data_model = $data_models[$data_model_id];
          $draft = FALSE;
        }
      }
    } else {
      $data_model = $data_models[$data_model_id];
      $draft = FALSE;
    }
  }
  if (!$data_model) {
    $result = db_select('etd_data_models', 'dm')
      ->fields('dm', array('id', 'name'))
      ->condition('id', $data_model_id)
      ->execute();
    $draft = FALSE;
    if ($result) {
      $data_model = $result->fetch();
    }
  }

  if ($data_model) {
    //Generate a javascript object to embed in the page to hold the data model we'll be working with.
    if ($draft) {
      //If we've loaded a draft page, then we must simply decode the saved data.
      $data_model = json_decode($data_model->data);
      $data_model->isDraft = TRUE;
    } elseif (!isset($data_models[$data_model_id])) {
      //We'll be loading the roles so we need the role include file
      $path = drupal_get_path('module', 'jarrow');
      include_once $path . '/workflow/roles/roles.inc';
      //Load the roles associated with this data model
      $roles = jarrow_load_roles($data_model_id);
      $data_model->roles = $roles;

      //Now we load the digital objects
      include_once $path . '/workflow/digital_objects/digital_objects.inc';
      //Load the roles associated with this data model
      $digital_objects = jarrow_digital_object_load_definitions($data_model_id);
      $data_model->digital_objects = $digital_objects;

      //If it's not a draft, we load the associated data elements from the data elements page.
      $elements = jarrow_load_data_elements($data_model_id);
      //Decorate each of the form elements with the label for their type
      foreach ($elements as $element) {
        if ($element->type != 'group') {
          $types = module_invoke_all('etd_data_element_types', $element->source);
          $element->type_label = $types[$element->type]['name'];
        } else {
          $element->type_label = t('Group');
        }
      }
      $data_model->elements = $elements;
      $data_models[$data_model_id] = $data_model;
      cache_set('jarrow_data_models', $data_models, 'cache');
    }
  }


  if (!$load_draft) {
    drupal_alter('data_model', $data_model);
  }

  return $data_model;
}

/**
 * Load all of the data model elements associated with this data model.
 *
 * @param type $data_model_id
 *    The data model that to load elements for
 *
 * @return array
 *    An array of elements that have been loaded.  The
 *    elements are loaded as a flat array, regardless of their hierarchical
 *    structure.  The javascript handles displaying them in a hierarchy.
 */
function jarrow_load_data_elements($data_model_id) {

  $result = db_select('etd_data_elements', 'de')
    ->fields('de')
    ->condition('data_model', $data_model_id)
    ->orderBy('weight')
    ->execute();
  $elements = array();
  foreach ($result as $element) {
    //As both of the config fields are module provided, they are simply stored as a serialized
    //string, which must be de-serialized before being passed to the user.
    $element->type_config = unserialize($element->type_config);
    $element->source_config = unserialize($element->source_config);

    //In case nothing has been saved, create at least an array so that there
    //is an element there when serialized.
    if (!$element->type_config) {
      $element->type_config = array(
        'max_length' => 60,
      );
    }

    if (!$element->source_config) {
      $element->source_config = array(
        'access' => 'single',
      );
    }

    $elements[$element->id] = $element;
  }
  return $elements;
}

/**
 * Generate the form used to configure each data element.
 *
 * @param array $form
 *    The currently generated form should be empty, and is ignored.
 * @param array $form_state
 *    The current state of the form, in case this is being submitted with ajax.
 * @param $data_model
 *
 * @return array
 *       The configuration form represented as an array.
 * @todo Consider making types and config options more human readable.
 */
function jarrow_element_config_pane($form, &$form_state, $data_model) {


  //Load all of the sources for the data elements
  $source_array = module_invoke_all('etd_data_element_sources');

  //Build an array that is suitable for use in a select
  $sources = array();
  foreach ($source_array as $source_id => $source) {
    if ($source['addable']) {
      $sources[$source_id] = $source['label'];
    }
  }

  // Assemble the pieces needed to create the form.
  $source_config_array = _jarrow_data_model_get_source_configuration($form_state, $source_array, $data_model->id);
  $type_config_array = _jarrow_data_model_get_type_configuration($form_state, $source_config_array['type']);
  $default_metatag = (isset($form_state['values'])) ? $form_state['values']['dc_mapping']: 0;
  $dc_fields = _jarrow_data_model_get_dc_fields_form($default_metatag);
  //$linked_fields = _jarrow_data_model_get_linked_fields_form($data_model->id, $form_state);

  //Build the form to display.  This form is built as a tree to allow modules supplying types or sources to
  //easily separate their configuration data from the rest.
  //Each of the two config panels (type and source) are placed in a wrapper.  Each wrapper is updated if any
  //change is made to the select elements associated with them.  This is handled with Drupal's standard ajax
  //library.

  // IMPORTANT NOTE: The name of the element must be the same as the field it represents in the database, otherwise
  // the check to see if the field has changed will not work.
  $form = array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => 'The name of this element',
      '#default_value' => '',
      '#required' => TRUE,
    ),
    'description' => array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#description' => t('Optional: The description for this element, to be displayed as a tip for the user'),
      '#default_value' => '',
    ),
    'source' => array(
      '#type' => 'select',
      '#title' => t("Source"),
      '#description' => t('The type of input required to read in this data element.  Corresponds to a type of form interface element'),
      '#default_value' => 'form',
      '#options' => $sources,
      '#ajax' => array(
        'callback' => 'jarrow_source_config_ajax',
        'wrapper' => 'config_form_source_wrapper',
        'method' => 'replace'
      )
    ),
    'source_config' => $source_config_array['source'],
    'type' => $source_config_array['element'],
    'type_config' => $type_config_array,
    'dc_mapping' => $dc_fields,
   // 'linked_metatags' => $linked_fields['linked_metatags'],
    '#tree' => TRUE,
  );
  return $form;
}

/**
 * Return the appropriate source configuration panel
 */

function _jarrow_data_model_get_source_configuration($form_state, $source_array, $data_model_id) {

  //If this form is being requested from ajax with values already loaded,
  //then load the appropriate source config panel.
  $type_array = array();
  $source_config_array = array();
  if (isset($form_state['input']['source'])) {
    $source_config = isset($form_state['input']['source_config']) ?
      $form_state['input']['source_config'] : array();
    $source_config_function = $source_array[$form_state['input']['source']]['config'];
    $source_config_array = call_user_func($source_config_function, $source_config, $data_model_id);
    $source_config_array['#type'] = 'fieldset';
    $source_config_array['#title'] = 'Source Configuration';
    $source_config_array['#collapsible'] = TRUE;
    $source_config_array['#collapsed'] = TRUE;
    //Load all of the types for the data elements
    $type_array = module_invoke_all('etd_data_element_types', $form_state['input']['source']);
  }

  //Put the source config panel in a wrapper.  Since the config function might
  //have added its own prefix or suffix, we must include those if they exist.
  $source_config_array['#prefix'] = '<div id="config_form_source_wrapper">' . (isset($source_config_array['#prefix']) ? $source_config_array['#prefix'] : "");

  //Build an array that is suitable for use in a select
  $types = array();
  foreach ($type_array as $type_id => $type) {
    $types[$type_id] = $type['name'];
  }

  $type_element = array();
  if (count($types) > 0) {
    $type_element = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#description' => 'The type of input required to read in this data element.  Corresponds to a type of form interface element',
      '#default_value' => 'text',
      '#options' => $types,
      '#suffix' => '</div>',
      '#ajax' => array(
        'callback' => 'jarrow_type_config_ajax',
        'wrapper' => 'config_form_type_wrapper',
        'method' => 'replace'
      )
    );
  } else {
    $source_config_array['#suffix'] = (isset($source_config_array['#suffix']) ? $source_config_array['#suffix'] : "") . '</div>';
  }
  return array('source' => $source_config_array, 'type' => $type_array, 'element' => $type_element);
}


/**
 * Return the appropriate type configuration panel
 *
 */

function _jarrow_data_model_get_type_configuration($form_state, $type_array) {

  //If this form is being requested from ajax with values already loaded,
  //then load the appropriate type config panel.
  $type_config_array = array();
  //dsm($form_state);
  if (isset($form_state['input']['type'])) {
    $type_config = isset($form_state['input']['type_config']) ?
      $form_state['input']['type_config'] : array();

    $type_config_function = $type_array[$form_state['input']['type']]['config'];
    $type_config_array = call_user_func($type_config_function, $type_config);
    $type_config_array['#type'] = 'fieldset';
    $type_config_array['#title'] = 'Type Configuration';
    $type_config_array['#collapsible'] = TRUE;
    $type_config_array['#collapsed'] = TRUE;
  }

  //Put the type config panel in a wrapper.  Since the config function might
  //have added its own prefix or suffix, we must include those if they exist.
  $type_config_array['#prefix'] = '<div id="config_form_type_wrapper">' . (isset($type_config_array['#prefix']) ? $type_config_array['#prefix'] : "");
  $type_config_array['#suffix'] = (isset($type_config_array['#suffix']) ? $type_config_array['#suffix'] : "") . '</div>';
  return $type_config_array;
}

/**
 * Create the form array for displaying Dublin Core fields.
 *
 * @param int $default_value
 *
 * @return array The part of the form generated for displaying Dublin Core fields
 */
function _jarrow_data_model_get_dc_fields_form($default_value = 0) {

  $dc_fields = jarrow_data_model_get_dc_fields();
  $form = array(
    '#type' => 'select',
    '#title' => t('Dublin Core Mapping'),
    '#description' => 'The Dublin Core field that the data model element will be mapped to',
    '#default_value' => $default_value,
    '#options' => $dc_fields,
    '#ajax' => array(
      'wrapper' => 'linked_metatag_elements',
      'callback' => 'jarrow_data_model_ajax_callback',
      'method' => 'replace',
    ),
  );
  return $form;
}

/**
 * Return a list of the Dublin Core fields.
 *
 */
function jarrow_data_model_get_dc_fields() {

  //Look up all the dublin core field names.
  $result = jarrow_metatags_get_tags();
  $dc_fields = array(0 => t('None'));
  //Put the loaded dc fields into an array suitable for a select.
  foreach ($result as $field) {
    $dc_fields[$field->id] = $field->name;
  }
  return $dc_fields;
}

/**
 * Create the form to allow the user to select which data element is related to the one
 * they are currently editing. This is needed so that we can relate answers on the form
 * when they are provided.
 *
 * @param $form_state
 * @param $data_model_id
 *
 * @return array|null
 */
function _jarrow_data_model_get_linked_fields_form($data_model_id, &$form_state) {

  $metatag_id = $form_state['values']['dc_mapping'];
  $linked_tags = jarrow_metatags_get_linked_tags(jarrow_metatags_get_tag_with_id($metatag_id));

  $form['linked_metatags'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#attributes' => array(
      'id' => drupal_html_id('linked_metatag_section')
    )
  );
  if (count($linked_tags) > 0) {
    $form['linked_metatags']['linked_metatags_options'] = array(
      '#type' => 'select',
      '#title' => t('Select associated metatag'),
      '#options' => $linked_tags,
      '#prefix' => '<div id="linked_metatag_elements">',
      '#suffix' => '</div>',
    );
  } else {
    $form['linked_metatags']['linked_metatags_options'] = array(
      '#type' => 'markup',
      '#markup' => '',
      '#prefix' => '<div id="linked_metatag_elements">',
      '#suffix' => '</div>',
    );
  }
  return $form;
}

/**
 * Return the list of linked metatags for an ajax call.
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function jarrow_data_model_ajax_callback($form, &$form_state) {

  return $form['linked_metatags'];
}

/**
 * Filter the form for an ajax call concerning the type config box.
 *
 * @param array $form
 *    The form as it has been generated.
 * @param array $form_state
 *    The current state of the form.  Ignored.
 *
 * @return array
 *    The part of the form generated for configuring types
 */
function jarrow_type_config_ajax($form, &$form_state) {

  $form['type_config']['#suffix'] = str_replace($form['type_config']['#suffix'], '</div>', '</div></div>');
  return $form['type_config'];
}

/**
 * Filter the form for an ajax call concerning the source config box.
 *
 * @param array $form
 *    The form as it has been generated.
 * @param array $form_state
 *    The current state of the form.  Ignored.
 *
 * @return array
 *    The part of the form generated for configuring sources
 */
function jarrow_source_config_ajax($form, &$form_state) {

  return array(
    'source_config' => $form['source_config'],
    'type' => $form['type'],
  );
}

/**
 * Save all the data elements associated with the specified data model.
 *
 * Any element not included will be removed from the etd_data_elements_table.
 *
 * This function iterates through every submitted data element and either updates
 * the elements that exist or creates new elements if they don't.  It then deletes
 * any elements which are in the database but not the data model.
 *
 * This function also saves all the roles associated with this data model,
 * following the same approach outlined for elements.
 *
 * @param std_class $data_model
 *    The data model we will be saving.  In particular, the id and elements
 *    attributes of the data_model must be set.  The id is an integer
 *    specifying the same of this data model, and the elements is an array of data elements,
 *    whose members represent fields in the etd_data_elements table.
 *
 * @return null|boolean
 *    True if every element was saved, or NULL if any transaction failed.
 */
function jarrow_save_data_elements($data_model) {

  //Remove all roles from the database that are associated with this data model
  //(these will be replaced by the new roles stored in the passed in data model
  //object)
  if (db_delete('etd_roles')
      ->condition('data_model', $data_model->id)
      ->execute() === NULL
  ) {
    return NULL;
  }

  //We use 'type' to handle 'source' on the javascript side of things
  //because item_edit.js assumes it for creation-on-drop.
  foreach ($data_model->roles as $role) {
    if (db_insert('etd_roles')
        ->fields(array(
          'id' => $role->id,
          'data_model' => $data_model->id,
          'name' => $role->name,
          'source' => $role->type,
          'source_config' => serialize($role->type_config),
          'weight' => $role->weight,
        ))->execute() === NULL
    ) {
      return NULL;
    }
  }

  //Now we apply the same process to the digital objects associated with this
  //data model
  if (db_delete('etd_digital_objects')
      ->condition('data_model', $data_model->id)
      ->execute() === NULL
  ) {
    return NULL;
  }

  $valid_object_ids = array();
  foreach ($data_model->digital_objects as $digital_object) {
    $valid_object_ids [$digital_object->id] = TRUE;
    $result = db_insert('etd_digital_objects')
      ->fields(array(
        'id' => $digital_object->id,
        'data_model' => $data_model->id,
        'name' => $digital_object->name,
        'description' => $digital_object->description,
        'type' => $digital_object->type,
        'minimum' => $digital_object->minimum,
        'maximum' => $digital_object->maximum,
        'access' => $digital_object->access,
        'config' => serialize($digital_object->config),
        'weight' => $digital_object->weight,
      ))
      ->execute();
    if ($result === NULL) {
      return NULL;
    }
  }

  //Create a list of ids for elements that are in the data model.
  $valid_elements = array();
  foreach ($data_model->elements as $element) {

    //Verify that the data model for this element matches the current
    //data model.
    if ($element->data_model != $data_model->id) {
      return NULL;
    }

    //Since the digital object these elements are associated with might have
    //been deleted, we only save them if they refer to a valid digital object
    if (!isset($element->digital_object) ||
      $element->digital_object == 0 ||
      isset($valid_object_ids[$element->digital_object])
    ) {

      //Add the current id to the list of elements that we are attempting to save.
      $valid_elements[] = intval($element->id);
    }
    //Save all of the elements which are in the first level of the hierarchy.  This relies on
    //the data model being constructed correctly prior to being passed in to this function.

    if ($element->parent == NULL) {

      $result = db_merge('etd_data_elements')
        ->key(array('id' => $element->id, 'data_model' => $element->data_model))
        ->fields(array(
          'digital_object' => $element->digital_object,
          'name' => $element->name,
          'description' => $element->description,
          'parent' => $element->parent,
          'type' => $element->type,
          'type_config' => serialize($element->type_config),
          'source' => $element->source,
          'source_config' => serialize($element->source_config),
          'dc_mapping' => $element->dc_mapping,
          'weight' => $element->weight,
        ))
        ->execute();
      if ($result === NULL) {
        return NULL;
      }
      //If the element is a group, then it may have children, so we scan the list of
      //data elements to add them all.
      if ($element->type == 'group') {
        if (jarrow_save_data_element_children($data_model, $element) === NULL) {
          return NULL;
        }
      }
    }
  }

  //Delete any data elements previously associated with this data model, which have not been included
  //in the current version.
  $query = db_delete('etd_data_elements')->condition('data_model', $data_model->id);
  //The database doesn't like an empty set of ids for the not-in condition, so we only add it if we aren't deleting
  //every possible data element (such as if there is only one left, and we are erasing it).
  if (count($valid_elements) > 0) {
    $query = $query->condition('id', $valid_elements, 'not in');
  }
  return $query->execute() !== NULL;
}

/**
 * Saves any data elements which are children of the given parent and associated with the data model.
 *
 * @param stdClass $data_model The data model to save all children for.
 * @param stdClass $parent     The parent to save all the children for
 *
 * @return null|boolean True if the children were saved, NULL otherwise.
 */
function jarrow_save_data_element_children($data_model, $parent) {

  foreach ($data_model->elements as $element) {

    if ($element->parent == $parent->id) {

      //Save all of the elements which are children of the specified parent.  This relies on
      //the data model being constructed correctly prior to being passed in to this function.
      $result = db_merge('etd_data_elements')
        ->key(array('id' => $element->id, 'data_model' => $element->data_model))
        ->fields(array(
          'name' => $element->name,
          'description' => $element->description,
          'parent' => $element->parent,
          'type' => $element->type,
          'type_config' => serialize($element->type_config),
          'source' => $element->source,
          'source_config' => serialize($element->source_config),
          'dc_mapping' => $element->dc_mapping,
          'digital_object' => $element->digital_object,
          'weight' => $element->weight,
        ))
        ->execute();
      if ($result === NULL) {
        return NULL;
      }
      //If the element is a group, then it may have children, so we scan the list of
      //data elements to add them all.
      if ($element->type == 'group') {
        if (jarrow_save_data_element_children($data_model, $element) === NULL) {
          return NULL;
        }
      }
    }
  }
  return TRUE;
}

/**
 *
 *
 * @return array
 */
function jarrow_data_model_add_list() {

  $sources = module_invoke_all('etd_data_element_sources');
  $elements = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(
      array(
        'data' => t('Organization'),
        'class' => array('jarrow_add_item_header'),
      ),
      array(
        'data' => '<div>' . t('Group') . '</div>',
        'class' => array('jarrow_add_item'),
        'id' => 'itemadd-organization-group',
      ),
    ),
  );

  foreach ($sources as $source => $source_info) {
    if ($source_info['addable']) {
      $elements['#items'][] = array(
        'data' => $source_info['label'],
        'class' => array('jarrow_add_item_header'),
      );
      $source_types = module_invoke_all('etd_data_element_types', $source);
      foreach ($source_types as $type => $type_info) {
        $elements['#items'][] = array(
          'data' => '<div>' . $type_info['name'] . '</div>',
          'class' => array('jarrow_add_item'),
          'id' => 'itemadd-' . $source . '-' . $type,
        );
      }
    }
  }
  return $elements;
}

/**
 *
 *
 * @param $data_model
 *
 * @return array
 */
function jarrow_data_model_get_related($data_model) {

  $form_result = db_select('etd_forms')
    ->fields('etd_forms', array('id', 'name'))
    ->condition('data_model', $data_model->id)
    ->execute();
  $related_forms = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => t('Forms'),
    '#items' => array(),
  );
  foreach ($form_result as $related_form) {
    $related_forms['#items'] [] = array(
      'data' => l(check_plain($related_form->name), 'admin/config/jarrow/workflow/forms/' . $related_form->id . '/edit'),
    );
  }

  $process_result = db_select('etd_processes', 'ps')
    ->fields('ps', array('name', 'id'))
    ->condition('data_model', $data_model->id)
    ->execute();
  $related_processes = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => t('Processes'),
    '#items' => array(),
  );
  foreach ($process_result as $process) {
    $related_processes['#items'] [] = array(
      'data' => l(check_plain($process->name), JARROW_PROCESS_PATH . '/' . $process->id . '/edit'),
    );
  }


  return array(
    'related_pane' => array(
      '#type' => 'fieldset',
      '#title' => t('Related Items'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible', 'collapsed'),
      ),
      'related_forms' => jarrow_check_item_list($related_forms),
      'related_processes' => jarrow_check_item_list($related_processes),
    ),
  );
}

