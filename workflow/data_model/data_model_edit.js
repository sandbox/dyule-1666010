/**
 * @file
 *  Provides code used by item_edit.js for creating data elements
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

jQuery.extend(true, jarrow, {
    item_edit: {
        defaultElement: function (id) {
            return {
                id: id,
                data_model: Drupal.settings.jarrow.item.id,
                digital_object: Drupal.settings.jarrow.filter_id,
                parent: null,
                description: "",
                default_answer: "",
                editable_field: 1,
                type: 'text',
                type_config: {
                    max_length: 60
                },
                dc_mapping: 0,
                name: "",
                source: 'form',
                source_config: {
                    access: 'single'
                },
                weight: 0
            };
        },
        initializeData: function () {
            var $container = jQuery('#element_container'),
                $title_bar = jQuery('#item_title_bar'),
                $object_select = jQuery('<select/>', {
                    id: 'digital_object_select',
                    name: 'digital_object_select',
                    'class': 'form-select'
                }),
                item = Drupal.settings.jarrow.item,
                object_id;
            $container.append(jQuery('<ul/>', {
                className: 'element_list'
            }));
            $object_select.append(jQuery('<option/>', {
                value: 0,
                text: Drupal.t('Main Object')
            }));
            for (object_id in item.digital_objects) {
                if (object_id === Drupal.settings.jarrow.filter_id) {
                    $object_select.append(jQuery('<option/>', {
                        selected: 'selected',
                        value: object_id,
                        text: item.digital_objects[object_id].name
                    }));
                } else {
                    $object_select.append(jQuery('<option/>', {
                        value: object_id,
                        text: item.digital_objects[object_id].name
                    }));
                }
            }

            $title_bar.append(jQuery('<div/>', {
                id: 'digital_object_container'
            }).append(jQuery('<label/>', {
                    'for': 'digital_object_select',
                    text: Drupal.t('Digital Object')
                }))
                .append($object_select));
            $object_select.change(function (e) {
                var new_filter_id = jQuery(this).val();
                $container.elementContainer('saveCollection', true);
                if (new_filter_id === 0) {
                    window.location = Drupal.settings.jarrow.edit_url;
                } else {
                    window.location = Drupal.settings.jarrow.edit_url + '/' + jQuery(this).val();
                }
            });

        }
    }
});
