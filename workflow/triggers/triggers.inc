<?php
/**
 * @file
 *  Handles the creation and management of trigger conditions and actions
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Load the triggers and actions from the database.
 *
 * @param $process_id
 * @param $stage_id
 * @param $form_instance_id
 * @return array
 */
function jarrow_load_triggers($process_id, $stage_id, $form_instance_id) {

  $triggers_query = db_select('etd_conditions', 'cond')
    ->fields('cond', array('id', 'type', 'config'))
    ->condition('cond.process', $process_id)
    ->condition('cond.stage', $stage_id)
    ->condition('cond.form_instance', $form_instance_id);
  $triggers_query->leftJoin('etd_actions', 'act', 'act.condition_id=cond.id');
  $triggers_query = $triggers_query->fields('act', array('id', 'type', 'config'))
    ->condition('act.process', $process_id)
    ->condition('act.stage', $stage_id)
    ->condition('act.form_instance', $form_instance_id)
    ->orderBy('cond.id');
  $triggers = $triggers_query->execute();

  $is_first_condition = TRUE;
  $is_first_action = TRUE;
  $conditions = array();
  foreach ($triggers as $trigger) {
    if (!isset($conditions[$trigger->id])) {
      $conditions[$trigger->id] = (object) (array(
        'type' => $trigger->type,
        'config' => unserialize($trigger->config),
        'actions' => array(),
        'collapsed' => !$is_first_condition,
      ));
      if ($is_first_condition) {
        $is_first_condition = FALSE;
        $is_first_action = TRUE;
      }
    }
    if (isset($trigger->act_id)) {
      $conditions[$trigger->id]->actions[$trigger->act_id] = (object) (array(
        'type' => $trigger->act_type,
        'config' => unserialize($trigger->act_config),
        'collapsed' => !$is_first_action,
      ));
      $is_first_action = FALSE;
    }
  }

  return $conditions;
}

/**
 * Save the triggers and actions into the database.
 *
 * @param $conditions
 * @param $process_id
 * @param $stage_id
 * @param $form_instance_id
 */
function jarrow_trigger_save($conditions, $process_id, $stage_id, $form_instance_id) {
  db_delete('etd_conditions')
    ->condition('process', $process_id)
    ->condition('stage', $stage_id)
    ->condition('form_instance', $form_instance_id)
    ->execute();
  db_delete('etd_actions')
    ->condition('process', $process_id)
    ->condition('stage', $stage_id)
    ->condition('form_instance', $form_instance_id)
    ->execute();
  foreach ($conditions as $condition_id => $condition) {
    if (!is_null($condition)) {
      db_insert('etd_conditions')
        ->fields(array(
          'id' => $condition_id,
          'process' => $process_id,
          'stage' => $stage_id,
          'form_instance' => $form_instance_id,
          'type' => $condition->type,
          'config' => serialize($condition->config),
        ))
        ->execute();
      foreach ($condition->actions as $action_id => $action) {
        debug(print_r($action,true));
        if (!is_null($action)) {
          db_insert('etd_actions')
            ->fields(array(
              'id' => $action_id,
              'condition_id' => $condition_id,
              'process' => $process_id,
              'stage' => $stage_id,
              'form_instance' => $form_instance_id,
              'type' => $action->type,
              'config' => serialize($action->config),
            ))
            ->execute();
        }
      }
    }
  }
}

/**
 * Create the trigger configuration form.
 *
 * @param array $form
 * @param array $form_state
 * @param object $process
 *
 * @return array
 */
function jarrow_trigger_config($form, &$form_state, $process) {

  $trigger_element = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Triggers'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'triggers' => array(
      '#type' => 'container',
      '#id' => 'etd_trigger_container',
    ),
    'add_trigger' => array(
      '#type' => 'button',
      '#value' => t('Add Trigger'),
      '#name' => 'add_trigger',
    ),
    'form_instance_id' => array(
      '#type' => 'hidden',
      '#value' => '0',
      '#name' => 'form_instance_id',
      '#id' => 'form_instance_id',
    ),
    'stage_id' => array(
      '#type' => 'hidden',
      '#value' => '0',
      '#name' => 'stage_id',
      '#id' => 'stage_id',
    ),
    'reload_process' => array(
      '#type' => 'hidden',
      '#value' => TRUE,
      '#name' => 'reload_process',
    ),
    // Not sure if this load config button is use. It is currently not displayed
    'load_config_button' => array(
      '#type' => 'button',
      '#value' => t('Load Config'),
      '#name' => 'load_trigger_config',
      '#id' => 'load_trigger_config',
      '#prefix' => '<div style="display: none">',
      '#suffix' => '</div>',
      '#ajax' => array(
        'callback' => 'jarrow_trigger_ajax_callback',
        'wrapper' => 'etd_trigger_container',
      ),
    )
  );

  if (isset($form_state['input']['form_instance_id'])) {
    if (isset($form_state['input']['reload_process'])) {
      $process = jarrow_load_process($process->id, TRUE);
    }
    $form_id = $form_state['input']['form_instance_id'];
    $stage_id = $form_state['input']['stage_id'];


    $condition_types = module_invoke_all('etd_conditions');
    $condition_type_options = array();
    foreach ($condition_types as $condition_type_id => $condition_type) {
      $condition_type_options[$condition_type_id] = $condition_type['name'];
    }
    $action_types = module_invoke_all('etd_actions');

    $action_type_options = array();
    foreach ($action_types as $action_type_id => $action_type) {
      $action_type_options[$action_type_id] = $action_type['name'];
    }
    if (!is_array($process->stages)) {
      $stages = get_object_vars($process->stages);
    } else {
      $stages = $process->stages;
    }
    if (!is_array($stages[$stage_id]->forms)) {
      $forms = get_object_vars($stages[$stage_id]->forms);
    } else {
      $forms = $stages[$stage_id]->forms;
    }

    // Create the condition portion of the form.
    $conditions = $forms[$form_id]->conditions;
    foreach ($conditions as $cond_id => $condition) {
      if ($condition) {
        $condition_config_elements = call_user_func($condition_types[$condition->type]['config'], $condition->config);
        $trigger_element['triggers'][$cond_id] = array(
          '#type' => 'fieldset',
          '#title' => call_user_func($condition_types[$condition->type]['title_function'], $condition->config),
          '#collapsible' => TRUE,
          '#collapsed' => $condition->collapsed,
          '#attributes' => array('class' => array('condition')),
          'remove' => array(
            '#type' => 'markup',
            '#markup' => '<span id="remove-condition_' . $cond_id . '" class="remove_condition">' . t('remove') . '</span>',
          ),
          'type' => array(
            '#type' => 'select',
            '#title' => t('Type'),
            '#options' => $condition_type_options,
            '#default_value' => $condition->type,
          ),
          'config' => $condition_config_elements,
          'actions' => array(),
          'add_action_button' => array(
            '#type' => 'button',
            '#value' => t('Add Action'),
            '#name' => 'add-action_' . $cond_id,
            '#id' => 'add-action_' . $cond_id,
            '#attributes' => array('class' => array('add_action_button')),
          )
        );
        // Create the action portion of the interface
        foreach ($condition->actions as $act_id => $action) {
          $stage_id = $form_state['input']['stage_id'];
          $action_config_elements = call_user_func($action_types[$action->type]['config'], $action->config, $process, $stage_id);
          $trigger_element['triggers'][$cond_id]['actions'][$act_id] = array(
            '#type' => 'fieldset',
            '#title' => call_user_func($action_types[$action->type]['title_function'], $action->config, $process),
            '#collapsible' => TRUE,
            '#collapsed' => $action->collapsed,
            '#attributes' => array('class' => array('action')),
            'remove' => array(
              '#type' => 'markup',
              '#markup' => '<span id="remove-action_' . $cond_id . '_' . $act_id . '" class="remove_action">' . t('remove') . '</span>',
            ),
            'type' => array(
              '#type' => 'select',
              '#title' => t('Type'),
              '#options' => $action_type_options,
              '#default_value' => $action->type,
            ),
            'config' => $action_config_elements,
          );
        }
      }
    }
  }
  return $trigger_element;
}

function jarrow_trigger_ajax_callback($form, &$form_state) {
  return $form['trigger_config']['triggers'];
}

/**
 * Implements hook_etd_conditions().
 *
 * @return array
 */
function jarrow_etd_conditions() {
  return array(
    'submission' => array(
      'name' => t('Form Submitted'),
      'config' => 'jarrow_condition_submitted_config',
      'title_function' => 'jarrow_condition_submitted_title',
      'condition_function' => 'jarrow_condition_submitted_check',
    ),
    'approval' => array(
      'name' => t('Form Approved'),
      'config' => 'jarrow_condition_approved_config',
      'title_function' => 'jarrow_condition_approved_title',
      'condition_function' => 'jarrow_condition_approved_check',
    ),
    'rejection' => array(
      'name' => t('Form Rejected'),
      'config' => 'jarrow_condition_rejected_config',
      'title_function' => 'jarrow_condition_rejected_title',
      'condition_function' => 'jarrow_condition_rejected_check',
    ),
  );
}

function jarrow_condition_submitted_config($current_config) {
  return array();
}

function jarrow_condition_submitted_title($current_config) {
  return t('On Form Submission');
}

function jarrow_condition_submitted_check($config_object, $submission, $current_form) {
  return $current_form->submitted != '0';
}

function jarrow_condition_approved_config($current_config) {
  return array();
}

function jarrow_condition_approved_title($current_config) {
  return t('On Form Approval');
}

function jarrow_condition_approved_check($config_object, $submission, $current_form) {
  return $current_form->approved != '0';
}

function jarrow_condition_rejected_config($current_config) {
  return array();
}

function jarrow_condition_rejected_title($current_config) {
  return t('On Form Rejection');
}

function jarrow_condition_rejected_check($config_object, $submission, $current_form) {
  return $current_form->approved == '0' && $current_form->submitted == '0';
}

/**
 * Define the options for the move stage actions.
 * Implements hook_etd_actions().
 *
 * @return array
 */
function jarrow_etd_actions() {
  return array(
    'move_stage' => array(
      'name' => t('Move To Stage'),
      'config' => 'jarrow_action_move_stage_config',
      'title_function' => 'jarrow_action_move_stage_title',
      'action_function' => 'jarrow_action_move_stage_action',
      'condition_function' => 'jarrow_action_condition_check',
    ),
  );
}

/**
 * Create a list of the available stages.
 *
 * @param object $process
 *
 * @return array
 */
function jarrow_get_stage_list($process) {
  $stages = array();
  foreach ($process->stages as $stage) {
    if (!is_null($stage)) {
      $stages[$stage->id] = $stage->name;
    }
  }
  return $stages;
}

/**
 * Present a select list of the stages the user can select to move to.
 *
 * @param object $current_config
 * @param object $process
 * @param int $stage_id
 *
 * @return array
 */
function jarrow_action_move_stage_config($current_config, $process, $stage_id) {

  $stages = jarrow_get_stage_list($process);
  $form = array();
  $form['stage'] = array(
      '#type' => 'select',
      '#title' => t('Stage'),
      '#options' => $stages,
      '#value' => $current_config->stage
    );
  $form['conditions'] = jarrow_action_condition_form($stage_id, $current_config, $process);

  return $form;
}

/**
 * Create the form that allow you to attach a condition to the move to stage
 * action. This is only applicable if the stage in question has a select list.
 *
 * @param $stage_id
 * @param $current_config
 * @param $process
 *
 * @return array
 */
function jarrow_action_condition_form($stage_id, $current_config, $process) {

  $form_elements = jarrow_action_get_form_elements_for_stage($stage_id, $process);
  if (empty($form_elements)) {
    return $form_elements;
  }

  $element_ids = array_keys($form_elements);
  $data_elements = jarrow_action_get_form_data_elements_for_stage($element_ids);
  if (empty($data_elements)) {
    return $data_elements;
  }

  $form = array('conditions' => array());
  foreach ($data_elements as $id => $data_element) {
    $title = $form_elements[$id]->name;
    $value_options = $data_element->list_values;
    $options = t('Not Applicable') + $value_options;
    $selection = (isset($current_config->conditions->$id)) ? $current_config->conditions->$id: NULL;
    $form['conditions'][$id] = array(
      '#type' => 'select',
      '#title' => t('When '.$title.' has value:'),
      '#options' => $options,
      '#default_value' => $selection,
    );
  }
  return $form['conditions'];
}

/**
 * Return a list of data elements with the specified ids and which are of type
 * select.
 *
 * @param $element_ids
 *
 * @return array
 */
function jarrow_action_get_form_data_elements_for_stage($element_ids) {

  $results = db_select('etd_data_elements', 'de')
    ->fields('de', array('type_config', 'id'))
    ->condition('id', $element_ids, 'IN')
    ->condition('type', 'select')
    ->execute();
  $data_elements = array();
  foreach($results as $result) {
    // As the type config field is stored as a serialized string,
    // it must be de-serialized before it can be used.
    $result->type_config = unserialize($result->type_config);
    $result->type_config->list_values = json_decode($result->type_config->list_values, true);
    $data_elements[$result->id] = $result->type_config;
  }
  return $data_elements;
}

/**
 * Return the list of form elements for the specified stage and process.
 *
 * @param $stage_id
 * @param $process
 *
 * @return array
 */
function jarrow_action_get_form_elements_for_stage($stage_id, $process) {

  $query = db_select('etd_process_form_instances', 'pfi');
  $query->join('etd_form_elements', 'fe', 'pfi.form=fe.form');
  $results = $query
    ->fields('fe', array('type_config', 'name', 'form', 'id'))
    ->condition('pfi.stage', $stage_id)
    ->condition('pfi.process', $process->id)
    ->execute();
  $elements = array();
  foreach ($results as $element) {
    // As the type config field is stored as a serialized string,
    // it must be de-serialized before it can be used.
    $element->type_config = unserialize($element->type_config);
    $elements[$element->type_config->associated_element] = $element;
  }
   return $elements;
}

/**
 * Create the move to stage title.
 *
 * @param $current_config
 * @param $process
 * @return mixed
 */
function jarrow_action_move_stage_title($current_config, $process) {
  $stages = jarrow_get_stage_list($process);
  return t('Move to Stage ' . $stages[$current_config->stage]);
}

/**
 * Advance the submission to the next stage
 *
 * @param stdObj$config_object
 * @param stdObj $submission
 *  The submission object as loaded from _jarrow_load_submission()
 * @param stdObj $current_form
 *  The form this email action is configured on.  Currently ignored.
 */
function jarrow_action_move_stage_action($config_object, &$submission, $current_form) {
  if (isset($config_object->stage)) {
    $submission->current_stage = $config_object->stage;
    db_update('etd_submission')
      ->fields(array('current_stage' => $config_object->stage))
      ->condition('id', $submission->id)
      ->execute();
  }
}
