<?php

/**
 * @file
 *  Functions for handling creating and manipulation of roles.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

$path = drupal_get_path('module', 'jarrow');
include_once $path . '/workflow/data_model/data_model.inc';

/**
 * Return the names of the users listed by user_ids. If include_full_name is
 * true return their full real name otherwise return their user name
 */
function _jarrow_get_names_for_users($user_ids = NULL, $include_full_name = TRUE) {
  $users_info = array();
  $user_names = &drupal_static(__FUNCTION__, array());

  $filtered_ids = array();
  if (is_array($user_ids)) {
    foreach ($user_ids as $user_id) {
      if (!isset($found_users[$user_id])) {
        $filtered_ids[] = $user_id;
      }
    }
  } elseif (!$user_ids) {
    $user_ids = $filtered_ids = db_select('users')
        ->fields('users', array('uid'))
        ->execute()
        ->fetchCol();
  }

  if (count($filtered_ids) > 0) {
    $info_sources = module_invoke_all('etd_user_info');

    $info_weights = variable_get('jarrow_info_weights', NULL);
    if (isset($info_weights)) {
      $new_info = array();
      foreach ($info_sources as $source => $info) {
        if (isset($info_weights[$source])) {
          $new_info[$info_weights[$source]] = $info;
        } else {
          $new_info[] = $info;
        }
      }
      $info_sources = $new_info;
      ksort($info_sources);
    }

    foreach ($info_sources as $source) {
      $source_result = call_user_func($source['info_function'], $filtered_ids);
      $users_info = $users_info + $source_result;
    }

    foreach ($filtered_ids as $this_user_id) {
      if (!isset($users_info[$this_user_id])) {
        $users_info[$this_user_id] = array();
      }
    }
    $users = user_load_multiple(array_keys($users_info));
    foreach ($users_info as $this_user_id => $user_info) {
      if (!(is_numeric($this_user_id) && $this_user_id == 0)) {
        $name = ($include_full_name && isset($user_info['title']) ? $user_info['title'] . ' ' : '') .
            (isset($user_info['first_name']) ? $user_info['first_name'] . ' ' : '') .
            ($include_full_name && isset($user_info['middle_name']) ? $user_info['middle_name'] . ' ' : '') .
            (isset($user_info['last_name']) ? $user_info['last_name'] . ' ' : '');
        if (strlen($name) === 0) {
          if (isset($users[$this_user_id])) {
            $user_names[$this_user_id] = $users[$this_user_id]->name;
          } else {
            $user_names[$this_user_id] = $users[$this_user_id];
          }
        } else {
          $user_names[$this_user_id] = $name;
        }
      }
    }
  }
  return array_intersect_key($user_names, array_flip($user_ids));
}

/**
 * Return the user object given the username
 */
function jarrow_user_for_username($username) {
  // Create the user if they don't already exist in the system
  module_invoke_all('etd_create_user', $username);
  // return the user
  return user_load_by_name($username);
}

function jarrow_etd_get_grad_students($user_id, $return_as_ids = FALSE) {

  $student_users = array();
  //Ensure that the user_id supplied is not the anonymous user or the super user id
  if (($user_id == 0) || ($user_id == 1)) {
    return $student_users;
  }
  $query = db_select('etd_submission', 'sub')
      ->fields('sub', array('owner'))
      ->condition('rs.user', $user_id);
  $query->leftJoin('etd_role_associations', 'rs', 'rs.submission=sub.id');

   $submission_owners = $query->execute();
   $result = array();
   foreach ($submission_owners as $owner) {
      $result[] = $owner->owner;
   }
   $student_users = array_unique($result);
   return $student_users;
}

/**
 * Return the roles a given user id has for a particular submission id.
 *
 * Currently this method is not being used anywhere
 */
function jarrow_etd_user_roles($user_id, $submission_ids) {
  $roles = array();
  $role_result = db_select('etd_role_associations')
      ->fields('etd_role_associations', array('submission', 'role'))
      ->condition('user', $user_id)
      ->condition('submission', $submission_ids)
      ->execute();
  foreach ($role_result as $role) {
    $roles[$role->submission][] = $role->role;
  }
  return $roles;
}


/**
 * Return a list of all the Drupal user's ids associated with the
 * specified submission
 */

function jarrow_get_role_user_list($submission, $flatten_result = FALSE) {

  $role_list = &drupal_static(__FUNCTION__);
  if (!isset($role_list[$submission->id])) {
    //$role_sources = module_invoke_all('etd_role_sources');
    $committee_user_ids = module_invoke_all('etd_get_committee_user_ids', $submission);
    $role_list[$submission->id]['student'] = array($submission->owner);
    $roles = $submission->data_model->roles;
    $user_ids = array();
    foreach ($roles as $role) {
      $config_object = $role->type_config;
      if ($role->source === 'form') {
        if (isset($user_ids[$role->id])) {
          array_merge($user_ids[$role->id], jarrow_role_get_user_id_from_form_inputs($submission, $role->id, $role->source));
        } else {
          $user_ids[$role->id] = jarrow_role_get_user_id_from_form_inputs($submission, $role->id, $role->source);
        }
      } else if (isset($config_object -> role_type) && isset($committee_user_ids[$config_object -> role_type])) {
        if (isset($user_ids[$role->id])) {
          array_merge($user_ids[$role->id], $committee_user_ids[$config_object -> role_type]);
        } else {
          $user_ids[$role->id] = $committee_user_ids[$config_object -> role_type];
        }
      }
    }
    if (count($user_ids) == 0 ) {
      $user = user_load($submission->owner);
      $profile = profile2_load_by_user($user, 'student');
      if ($profile) {
        $name = _jarrow_get_profile_field_value($profile, 'field_first_name');
        $name .= ' '._jarrow_get_profile_field_value($profile, 'field_middle_name');
        $name .= ' '._jarrow_get_profile_field_value($profile, 'field_last_name');
        if (strlen(trim($name)) == 0) {
           $name = $user->name;
        }
      } else {
         $name = $user->name;
      }
      drupal_set_message('No committee has been assigned to student: '.$name, 'error');
    }
    $role_list[$submission->id]['committee'] = $user_ids;
  }

  if ($flatten_result) {
    // We are only interested in returning the user ids and don't care what
    // there role is so return just an array of ids
    $user_ids = jarrow_flatten_array($role_list[$submission->id]);
    return array_unique($user_ids);
  }
  return $role_list[$submission->id];
}

/**
 * Flatten a multidimensional array to one dimension, optionally preserving keys.
 *
 * @param $array the array to flatten
 * @param $preserve_keys  0 (default) to not preserve keys, 1 to preserve string keys only, 2 to preserve all keys
 * @param $result internal use argument for recursion
 */
function jarrow_flatten_array($array, $preserve_keys = 0, &$result = array()) {

  foreach($array as $key => $child) {
    if (is_array($child)) {
      $result = jarrow_flatten_array($child, $preserve_keys, $result);
    } elseif ($preserve_keys + is_string($key) > 1) {
      $result[$key] = $child;
    } else {
      $result[] = $child;
    }
  }
  return $result;
}

/**
 * Return a list of user ids of all users who are affiliated with the specifed
 * submission, have the specified role id and who's source is from a form
 */
function jarrow_role_get_user_id_from_form_inputs($submission, $role_id, $role_source) {

  $user_ids = array();
  if ($role_source === 'form') {
    $role_results = db_select('etd_role_associations')
        ->fields('etd_role_associations', array('role', 'user'))
        ->condition('submission', $submission->id)
        ->condition('role', $role_id)
        ->execute()
        ->fetchCol(1);
    foreach ($role_results as $result) {
      $user_ids[] = $result;
    }
  }
  return $user_ids;
}

function jarrow_role_edit($data_model_id) {

  global $base_url;
  $path = drupal_get_path('module', 'jarrow');
  $data_model = jarrow_load_data_model($data_model_id, TRUE);

  if (!$data_model) {
    drupal_set_message(t('Could not load data model'), 'error');
    drupal_goto(JARROW_DATAMODEL_PATH);
    drupal_exit();
  }



  //Add all necessary javascript and css to the page.
  libraries_load('jquery_editinplace');
  libraries_load('json_js');
  drupal_add_css($path . '/workflow/common/css/item_edit.css');
  drupal_add_js($path . '/workflow/common/js/item_edit.js');
  drupal_add_js($path . '/lib/js/jarrow.element.container.js');
  drupal_add_css($path . '/lib/css/jarrow.element.container.css');


  drupal_add_library('jarrow', 'ui.editable-list');
  drupal_add_library('system', 'ui.sortable');
  drupal_add_library('system', 'ui.button');
  drupal_add_library('system', 'ui.dialog');
  drupal_add_library('system', 'ui.droppable');
  drupal_add_library('system', 'ui.draggable');
  libraries_load('nestedsortable');
  drupal_add_js($path . '/lib/js/jquery.tree.dropdown.js', 'file');
  drupal_add_js($path . '/workflow/roles/roles.js', 'file');
  drupal_add_js(array(
    'jarrow' => array(
      'save_url' => "$base_url/".JARROW_DATAMODEL_PATH."/save",
      'discard_url' => "$base_url/".JARROW_DATAMODEL_PATH."/$data_model_id/discard",
      'tree_url' => "$base_url/".JARROW_DATAMODEL_PATH."/$data_model_id/tree",
      'element_index' => 'roles',
      'item' => $data_model,
    )
      ), 'setting');


//Create the buttons for interacting with the item.
  $control_array = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attached' => array(
      'js' => array(
        'misc/form.js',
        'misc/collapse.js',
      ),
    ),
    '#attributes' => array(
      'class' => array('collapsible', 'control_box'),
    ),
    'commit_message' => array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'commit_message',
      ),
    ),
    //The discard draft button will be hidden using css and javascript unless
    //a draft is currently being edited.
    'discard_draft' => array(
      '#type' => 'button',
      '#value' => t("Discard Changes"),
      '#id' => 'discard_draft',
    ),
    'save_item' => array(
      '#type' => 'button',
      '#value' => t("Commit Changes"),
      '#id' => 'save_item',
    ),
  );

  $control_array['message_field'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'message_field',
    ),
  );
  $control_html = drupal_render($control_array);

  $related_pane_array = jarrow_data_model_get_related((object) array('id' => $data_model_id));

  $related_pane = drupal_render($related_pane_array);

  $add_pane_array = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attached' => array(
      'js' => array(
        'misc/form.js',
        'misc/collapse.js',
      ),
    ),
    '#attributes' => array(
      'class' => array('collapsible'),
    ),
    '#title' => t('Role Sources'),
    '#weight' => 0,
    'items' => jarrow_role_add_list(),
  );
  $add_pane = drupal_render($add_pane_array);
  //Load the form used for editing each data element.  This is handled by a different function
  //both for modularity and also so that drupal's ajax facililty will be able to easily generate
  //replacement html for components of this form.
  $config_form_array = drupal_get_form('jarrow_role_element_config');
  $config_form = drupal_render($config_form_array);
  $output = jarrow_display_secondary_tabs_for_data_model();
  $output .= '<div id="item_title_bar"><span id="itm_meta_data"><h2 id="itm_name">' . check_plain($data_model->name) . '</h2>';

  $output .= "</span></div><div id='collection_form'><div id='element_container'></div></div><div id='option_panel'>$control_html$related_pane$add_pane</div><div id='config_form'>$config_form</div>";

  $discard_confirm_text = t('Are you sure you want to discard your changes?');
  $output .= '<div id="item_discard"><p>' . $discard_confirm_text . '</p></div>';

  return $output;
}

function jarrow_load_roles($data_model_id) {
  $sources = module_invoke_all('etd_role_sources');
  $roles = array();
  $role_result = db_select('etd_roles')
      ->fields('etd_roles')
      ->condition('data_model', $data_model_id)
      ->condition('source', array_keys($sources), 'in')
      ->execute();
  foreach ($role_result as $role) {
    $role->type_config = unserialize($role->source_config);
    $role->type = $role->source;
    $role->type_label = $sources[$role->source]['name'];
    $roles[$role->id] = $role;
  }

  return $roles;
}

function jarrow_role_element_config($form, &$form_state) {
  $sources = module_invoke_all('etd_role_sources');

  $source_options = array();

  foreach ($sources as $source => $source_info) {
    $source_options[$source] = $source_info['name'];
  }

  //If this form is being requested from ajax with values already loaded,
  //then load the appropriate type config panel.
  if (isset($form_state['values']['type'])) {
    $source_config_function = $sources[$form_state['values']['type']]['config'];
    $type_config_array = call_user_func($source_config_function);
    $type_config_array['#type'] = 'fieldset';
    $type_config_array['#collapsible'] = TRUE;
    $type_config_array['#title'] = t('Source Configuration');
  }
  else {
    $type_config_array = array();
  }
  //Put the type config panel in a wrapper.  Since the config function might
  //have added its own prefix or suffix, we must include those if they exist.
  $type_config_array['#prefix'] = '<div id="config_form_type_wrapper">' . (isset($type_config_array['#prefix']) ? $type_config_array['#prefix'] : "");
  $type_config_array['#suffix'] = (isset($type_config_array['#suffix']) ? $type_config_array['#suffix'] : "") . '</div>';
  $dc_fields = _jarrow_data_model_get_dc_fields_form();

  return array(
    '#tree' => TRUE,
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => 'The name of this role',
      '#default_value' => '',
    ),
    'type' => array(
      '#type' => 'select',
      '#title' => t("Source"),
      '#description' => 'The type of input required to read in this data element.  Corresponds to a type of form interface element',
      '#default_value' => 'form',
      '#options' => $source_options,
      '#ajax' => array(
        'callback' => 'jarrow_type_config_ajax',
        'wrapper' => 'config_form_type_wrapper',
        'method' => 'replace'
      )
    ),
    'type_config' => $type_config_array,
    'dc_mapping' => $dc_fields,
  );
}

function jarrow_etd_role_sources() {
  return array(
    'form' => array(
      'name' => 'Form',
      'config' => 'jarrow_form_role_config',
      'association_function' => 'jarrow_role_form_association',
    ),
    'drupal' => array(
      'name' => 'Drupal Roles',
      'config' => 'jarrow_role_drupal_config',
      'association_function' => 'jarrow_role_drupal_association',
    ),
  );
}

function jarrow_role_form_association($submission, $role_id, $config_object) {
  $role_list = &drupal_static(__FUNCTION__);
  if (!isset($role_list[$submission->id]) || !isset($role_list[$submission->id][$role_id])) {
    $role_result = db_select('etd_role_associations')
        ->fields('etd_role_associations', array('role', 'user'))
        ->condition('submission', $submission->id)
        ->execute();
    foreach ($role_result as $role) {
      $role_list[$submission->id][$role->role][] = $role->user;
    }
  }
  return $role_list[$submission->id][$role_id];
}

function jarrow_form_role_config() {
  $result = db_select('role', 'ur')
      ->fields('ur', array('rid', 'name'))
      ->execute();
  $drupal_roles = array('0' => t('all'));
  foreach ($result as $role) {
    $drupal_roles[$role->rid] = $role->name;
  }
  return array(
    'role' => array(
      '#type' => 'select',
      '#title' => 'Associated Drupal Role',
      '#description' => 'The Drupal Role to choose members from',
      '#options' => $drupal_roles,
    ),
    'maximum' => array(
      '#type' => 'select',
      '#title' => 'Maximum',
      '#description' => 'The maximum number of members this role can have',
      '#options' => array(
        0 => t('No Maximum'),
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
      ),
    ),
  );
}

function jarrow_role_drupal_association($submission, $role_id, $config_object) {
  $user_list = db_select('users_roles', 'r')
      ->fields('r', array('uid'))
      ->condition('r.rid', $config_object->drupal_role)
      ->execute()
      ->fetchCol();
  return $user_list;
}

function jarrow_role_drupal_config() {
  $drupal_roles = user_roles();
  return array(
    'drupal_role' => array(
      '#type' => 'select',
      '#title' => 'Drupal Role',
      '#description' => 'Everyone having this Drupal role will automatically be assigned to this Jarrow role',
      '#options' => $drupal_roles,
    )
  );
}

function jarrow_role_add_list() {
  $sources = module_invoke_all('etd_role_sources');
  $elements = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(),
  );


  foreach ($sources as $source => $source_info) {
    $elements['#items'][] = array(
      'data' => '<div>' . $source_info['name'] . '</div>',
      'class' => array('jarrow_add_item'),
      'id' => 'itemadd-' . $source,
    );
  }

  return $elements;
}


