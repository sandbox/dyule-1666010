/**
 * @file
 *  Provides code used by item_edit.js for creating form elements.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

jQuery.extend(true, jarrow, {
    item_edit : {
        defaultElement : function(id) {
            return {
                id : id,
                form : Drupal.settings.jarrow.item.id,
                description : "",
                required : true,
                type : 'view',
                type_config : {
                    associated_element : 0
                },
                name : "",
                weight : 0
            };
        },
        initializeData : function() {
            var $container = jQuery('#element_container');
            $container.append(jQuery('<ul/>', {
                className : 'element_list'
            }));
        }
    }
}); 