<?php
/**
 * @file
 *  Handles the creation and manipulation of forms and form elements.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

$path = drupal_get_path('module', 'jarrow');
include_once "$path/providers/element_sources.inc";

/**
 * Loads a form's definition from the database.
 *
 * @param int $form_id
 *    The id of the form to load from the database
 * @param boolean $load_draft
 *    If true, and there is a draft version of the form in the database,
 *    then the function will load that instead
 *
 * @return null | stdObj
 *    An object representing the form, or null if the form could not be loaded.
 */
function jarrow_load_form($form_id, $load_draft) {
  $form = FALSE;
  if ($load_draft) {
    //Check if there is a draft version of this form
    $result = db_select('etd_form_drafts', 'fmd')
      ->fields('fmd', array('id', 'data'))
      ->condition('id', $form_id)
      ->execute();
    $draft = TRUE;
    if ($result) {
      $form = $result->fetch();
    }
  }
  //If the form was not able to be loaded from the drafts table, then try to load it from the saved table.
  if (!$form) {

    $query = db_select('etd_forms', 'fm');
    $query->innerJoin('etd_data_models', 'dm', 'fm.data_model=dm.id');

    $result = $query->fields('fm', array('id', 'name', 'data_model'))
      ->fields('dm', array('name'))
      ->condition('fm.id', $form_id)
      ->execute();
    $draft = FALSE;
    if ($result) {
      $form = $result->fetch();
    }
  }

  //Make sure we were successful in loading the form.
  if ($form) {

    //Generate a javascript object to embed in the page to hold the form we'll be working with.
    if ($draft) {
      //If we've loaded a draft page, then we must simply decode the saved data.
      $form = json_decode($form->data);
      $form->isDraft = TRUE;
    } else {
      //If it's not a draft, we load the associated form elements from the form elements table.
      $elements = jarrow_load_form_elements($form_id);
      //Decorate each of the form elements with the label for their type
      $types = module_invoke_all('etd_form_element_types');
      foreach ($elements as $element) {
        $element->type_label = $types[$element->type]['name'];
      }
      $form->elements = $elements;
    }
    //$metatag_links = jarrow_metatags_get_linked_tag_list();
    //$form->metatag_links = (count($metatag_links) > 0) ? $metatag_links : NULL;

    $data_element_links = jarrow_form_editor_get_possible_links_by_data_element_id($form->data_model);
    $form->data_element_links = $data_element_links;
    $form_element_links = jarrow_form_editor_get_possible_linked_form_elements_list($data_element_links);
    $form->form_element_links = $form_element_links;
    return $form;
  }
  return NULL;
}

/**
 * Load all of the data model elements associated with this data model.
 *
 * @param type $form_id The form to load elements for
 *
 * @return array An array of elements that have been loaded.
 */
function jarrow_load_form_elements($form_id) {
  $result = db_select('etd_form_elements', 'fe')
    ->fields('fe')
    ->condition('form', $form_id)
    ->orderBy('weight')
    ->execute();
  $elements = array();
  foreach ($result as $element) {
    //As the type config field is module provided, it is simply stored as a serialized
    //string, which must be de-serialized before being passed to the user.
    $element->type_config = unserialize($element->type_config);

    $elements[$element->id] = $element;
  }

  return $elements;
}

/**
 * Generate the form used to configure each form element.
 *
 * @param array $form
 *    The currently generated form should be empty, and is ignored.
 * @param array $form_state
 *    The current state of the form, in case this is being
 *    submitted with ajax.
 * @param array $args
 *
 * @return array The configuration form represented as an array.
 * @todo Consider making types more human readable.
 */
function jarrow_form_element_config_pane($form, &$form_state, $args) {

  $data_model = $args->data_model;
  //Load all of the types for the forms elements
  $type_array = module_invoke_all('etd_form_element_types');

  //Build an array that is suitable for use in a select
  $types = array();


  foreach ($type_array as $type_id => $type) {
    $types[$type_id] = $type['name'];
  }

  //If this form is being requested from ajax with values already loaded,
  //then load the appropriate type config panel.
  if (isset($form_state['values']['type'])) {
    $type_config_function = $type_array[$form_state['values']['type']]['config'];
    $type_config_array = call_user_func($type_config_function, $data_model);
    $type_config_array['#type'] = 'fieldset';
    $type_config_array['#collapsible'] = TRUE;
    $type_config_array['#title'] = t('Type Configuration');
    jarrow_form_editor_add_linked_form_elements_form($type_config_array, $form_state);
  } else {
    $type_config_array = array();
  }

  //Put the type config panel in a wrapper.  Since the config function might
  //have added its own prefix or suffix, we must include those if they exist.
  $type_config_array['#prefix'] = '<div id="config_form_type_wrapper">' . (isset($type_config_array['#prefix']) ? $type_config_array['#prefix'] : "");
  $type_config_array['#suffix'] = (isset($type_config_array['#suffix']) ? $type_config_array['#suffix'] : "") . '</div>';

  //Build the form to display.  This form is built as a tree to allow modules supplying types or sources to
  //easily separate their configuration data from the rest.
  //Each of the two config panels (type and source) are placed in a wrapper.  Each wrapper is updated if any
  //change is made to the select elements associated with them.  This is handled with Drupal's standard ajax
  //library.
  $form_array = array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => 'The name of this element',
      '#default_value' => '',
    ),
    'description' => array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#description' => t('The description for this element, to be displayed as a tip for the user'),
      '#default_value' => '',
    ),
    'required' => array(
      '#type' => 'checkbox',
      '#title' => t('Required'),
      '#description' => t('Checked if this element must be filled out before the form can be submitted'),
    ),
    'type' => array(
      '#type' => 'select',
      '#title' => t("Type"),
      '#description' => 'The type of input required to read in this data element.  Corresponds to a type of form interface element',
      '#default_value' => 'text',
      '#options' => $types,
      '#ajax' => array(
        'callback' => 'jarrow_form_type_config_ajax',
        'wrapper' => 'config_form_type_wrapper',
        'method' => 'replace'
      )
    ),
    'type_config' => $type_config_array,
    'data_model_id' => array(
      '#type' => 'hidden',
      '#value' => $data_model,
    ),
    '#tree' => TRUE,
  );
  return $form_array;
}

/**
 * Create the linked form elements form. This is to allow the user to select form elements
 * that depend on each other when it comes to supplying metadata information. For example
 * if a form element is linked to the dc.contributor tag then it needs to now which form
 * item it needs to look at to get the related dc.contributor.role information.
 *
 * @param array $type_config_array
 * @param array $form_state
 *
 * @return array
 */
function jarrow_form_editor_add_linked_form_elements_form(&$type_config_array, $form_state) {

  $linked_list = jarrow_form_editor_get_possible_links($form_state);
  if (count($linked_list) == 0) {
    $default = null;
  } else {
    $data_element_id = $form_state['values']['type_config']['associated_element'];
    $default = (isset($linked_list[$data_element_id])) ? $data_element_id: null;
  }

  $type_config_array['linked_form_elements'] = array(
    '#type' => 'select',
    '#title' => t('Select the form item that this element is related to'),
    '#options' => $linked_list,
    '#default' => $default,
    '#prefix' => '<div id="linked_form_elements">',
    '#suffix' => '</div>',
  );
}

/**
 * Return the list of linked form elements for an ajax call.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function jarrow_form_editor_linked_form_ajax($form, &$form_state) {

  // it is important that we reset the form cache to reflect that the form has changed.
  // Otherwise you will get the "illegal choice has been detected please contact sight administrator"
  // error.
  form_set_cache($form['#build_id'], $form, $form_state);
  return $form['type_config']['linked_form_elements'];
}

/**
 * Filter the form for an ajax call concerning the type config box.
 *
 * @param array $form The form as it has been generated.
 * @param array $form_state The current state of the form.  Ignored.
 *
 * @return array The part of the form generated for configuring types
 */
function jarrow_form_type_config_ajax($form, &$form_state) {
  return $form['type_config'];
}

/**
 * Save all the form elements associated with the specified form.
 *
 * Any element not included will be removed from the etd_form_elements table.
 *
 * This function iterates through every submitted form element and either updates
 * the elements that exist or creates new elements if they don't.  It then deletes
 * any elements which are in the database but not the form.
 *
 * @param std_class $form
 *    The form we will be saving.  In particular, the id and elements attributes
 *    of the form must be set.  The id is an integer specifying the identity of
 *    this form, and the elements is an array of data elements, whose members
 *    represent fields in the etd_form_elements table.
 *
 * @return null|boolean
 *    True if every element was saved, or NULL if any transaction failed.
 */
function jarrow_save_form_elements($form) {

  //Create a list of ids for elements that are in the form.
  $valid_elements = array();
  foreach ($form->elements as $element) {

    //Verify that the data model for this element matches the current
    //data model.
    if ($element->form != $form->id) {
      return NULL;
    }

    //Add the current id to the list of elements that we are attempting to save.
    $valid_elements[] = intval($element->id);

    //Save all of the elements.
    if (db_merge('etd_form_elements')
        ->key(array('id' => $element->id, 'form' => $element->form))
        ->fields(array(
          'name' => $element->name,
          'description' => $element->description,
          'type' => $element->type,
          'type_config' => serialize($element->type_config),
          'weight' => $element->weight,
          'required' => $element->required ? 1 : 0,
        ))->execute() === NULL
    ) {
      return NULL;
    }
  }

  //Delete any form elements previously associated with this form, which have not been included
  //in the current version.
  $query = db_delete('etd_form_elements')->condition('form', $form->id);
  //The database doesn't like an empty set of ids for the not-in condition, so we only add it if we aren't deleting
  //every possible form element (such as if there is only one left, and we are erasing it).
  if (count($valid_elements) > 0) {
    $query = $query->condition('id', $valid_elements, 'not in');
  }
  return $query->execute() !== NULL;
}

/**
 * @param $form
 * @param $form_state
 * @param $form_id
 *
 * @return mixed
 */
function jarrow_preview_form($form, &$form_state, $form_id) {
  $return_array = jarrow_get_form_array($form_id, TRUE);
  if ($return_array) {
    return $return_array;
  }
  drupal_set_message(t('Form could not be loaded'));
  drupal_goto('admin/config/jarrow/workflow/forms');
}

/**
 * This method creates the form array for display on screen.
 *
 * @param int $form_id
 * @param boolean $editable
 * @param int $form_user_id
 * @param object $submission
 *
 * @return mixed
 */
function jarrow_get_form_array($form_id, $editable, $form_user_id = NULL, $submission = NULL) {

  $form = jarrow_load_form($form_id, FALSE);
  if ($form) {
    $types = module_invoke_all('etd_form_element_types');
    $form_title = $form->name;
    if (!is_null($form_user_id)) {
      $user_names = _jarrow_get_names_for_users(array($form_user_id));
      $form_title .= ' <span class="form_user_name">(' . $user_names[$form_user_id] . ')</span>';
    }
    $form_array = array(
      '#type' => 'fieldset',
      '#title' => $form_title,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ($form->elements as $element) {
      if (isset($types[$element->type]) && (isset($types[$element->type]['edit']) || isset($types[$element->type]['display']))) {
        if ($editable && isset($types[$element->type]['edit'])) {
          $element_array = call_user_func($types[$element->type]['edit'], $element->type_config, $form, $form_user_id, $submission, $element->id);
          $element_array['#description'] = $element->description;
          $element_array['#required'] = $element->required;
        } else {
          $element_array = call_user_func($types[$element->type]['display'], $element->type_config, $form, $form_user_id, $submission, $element->id);
        }
        $element_array['#title'] = $element->name;

        $form_array[$element->id] = $element_array;
      }
    }
    return $form_array;
  }
  return FALSE;
}

/**
 * @return array
 */
function jarrow_form_editor_add_list() {
  $types = module_invoke_all('etd_form_element_types');
  $elements = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(),
  );

  foreach ($types as $type => $type_info) {
    $elements['#items'][] = array(
      'data' => '<div>' . $type_info['name'] . '</div>',
      'class' => array('jarrow_add_item'),
      'id' => 'itemadd-' . $type,
    );
  }

  return $elements;
}

/**
 * Get the related data elements to create the dropdown tree data structure
 * and create the Related Items panel located on the right side of the editor
 * screen.
 *
 * @param array $form
 *
 * @return array
 */
function jarrow_form_editor_get_related($form) {

  $path = drupal_get_path('module', 'jarrow');
  include_once $path . '/workflow/data_model/data_model.inc';
  libraries_load('jqtree');

  $data_model = jarrow_load_data_model($form->data_model, FALSE);
  $tree = jarrow_data_element_tree_display(NULL, $data_model->elements, TRUE);
  drupal_add_js(array('jarrow' => array('data_source_tree' => json_encode($tree))), 'setting');
  drupal_add_js($path . '/providers/support/js/form_editor_data_element_tree.js');

  $form_result = db_select('etd_forms')
    ->fields('etd_forms', array('id', 'name'))
    ->condition('data_model', $form->data_model)
    ->condition('id', $form->id, '<>')
    ->execute();
  $related_forms = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => t('Forms'),
    '#items' => array(),
  );
  foreach ($form_result as $related_form) {
    $related_forms['#items'] [] = array(
      'data' => l(check_plain($related_form->name), JARROW_FORMS_PATH . '/' . $related_form->id . '/edit'),
    );
  }

  // Create the Related Items Panel.
  $process_result = db_select('etd_processes', 'ps')
    ->fields('ps', array('name', 'id'))
    ->exists(db_select('etd_process_form_instances', 'fi')
        ->fields('fi')
        ->condition('form', $form->id)
        ->where('fi.process=ps.id')
    )
    ->condition('data_model', $form->data_model)
    ->execute();
  $related_processes = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => t('Processes'),
    '#items' => array(),
  );
  foreach ($process_result as $process) {
    $related_processes['#items'] [] = array(
      'data' => l($process->name, JARROW_PROCESS_PATH . '/' . $process->id . '/edit'),
    );
  }


  $new_form = array(
    'related_pane' => array(
      '#type' => 'fieldset',
      '#title' => t('Related Items'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible', 'collapsed'),
      ),
      'related_data_model' => array(
        '#theme' => 'item_list',
        '#title' => t('Data Model'),
        '#type' => 'ul',
        '#items' => array(array('data' => l($data_model->name, JARROW_DATAMODEL_PATH . '/' . $data_model->id . '/edit'))),
      ),
      'related_forms' => jarrow_check_item_list($related_forms),
      'related_processes' => jarrow_check_item_list($related_processes),
    ),
    'data_model_pane' => array(
      '#type' => 'fieldset',
      '#title' => $data_model->name,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible'),
      ),
      'tree' => array(
        '#type' => 'container',
        '#attributes' => array('class' => 'data_model_tree'),
      )
    )
  );
  return $new_form;
}

/**
 * Create the list of data elements that could be linked to another
 * data element based on linking metatags.
 *
 * @param $data_model_id
 *
 * @return array
 */
function jarrow_form_editor_get_possible_links_by_data_element_id($data_model_id) {

  $query = db_select('etd_data_elements', 'ede');
  $query->join('etd_dublin_core_fields', 'edcf', 'ede.dc_mapping=edcf.id');
  $result = $query->fields('ede', array('id', 'name'))
    ->fields('edcf', array('id', 'name', 'linked_to'))
    ->condition('ede.data_model', $data_model_id)
    ->isNotNull('edcf.linked_to')
    ->execute();

  $elements = array();
  foreach ($result as $data) {
    $linked_tags = json_decode($data->linked_to, TRUE);
    $elements[$data->id] = $linked_tags;
  }
  return $elements;
}

/**
 * Create the linked data elements list so that we can use it in the
 * editor screen.
 *
 * @param array $data_elements
 *    A list of data element ids and their possible linked metatag ids.
 *
 * @return array
 */
function jarrow_form_editor_get_possible_linked_form_elements_list($data_elements) {

  $linked_forms = array();
  $selected_data_elements = array();
  foreach ($data_elements as $data_element) {
    $linked_tags = array_keys($data_element);

    if (count($linked_tags) > 0) {
      $results = db_select('etd_data_elements', 'de')
        ->fields('de', array('id', 'dc_mapping'))
        ->condition('dc_mapping', $linked_tags, 'IN')
        ->execute();

      foreach ($results as $result) {
        $selected_data_elements[$result->id] = $result->dc_mapping;
      }
    }
  }
  $form_elements = db_select('etd_form_elements', 'fe')
    ->fields('fe', array('id', 'form', 'type_config'))
    ->condition('type', 'data')
    ->execute();

  $ids = array_keys($selected_data_elements);
  foreach ($form_elements as $form_element) {
    $type_config = unserialize($form_element->type_config);
    if (in_array($type_config->associated_element, $ids)) {
      $key = $selected_data_elements[$type_config->associated_element];
      $form = array('form_element_id' => $form_element->id, 'form_id' => $form_element->form);
      if (isset($linked_forms[$key]) && is_array($linked_forms[$key])) {
        $linked_forms[$key][] = $form;
      } else {
        $linked_forms[$key] = array($form);
      }
    }
  }
  return $linked_forms;
}

/**
 * Create a list of possible form elements that the currently selected form element's
 * metatag could be related to.
 *
 * @param array $form_state
 *
 * @return array
 */
function jarrow_form_editor_get_possible_links($form_state) {

  $form_links = array();
  if (isset($form_state['values']) && isset($form_state['values']['type_config'])) {

    // Pull out the information we need.
    $form_element = $form_state['build_info']['args'][0];
    $data_element_id = $form_state['values']['type_config']['associated_element'];
    $data_element_links = $form_element->data_element_links;
    $form_element_links = $form_element->form_element_links;
    // Create the possible links list.
    if (isset($data_element_links[$data_element_id])) {
      $metatag_id = array_shift(array_values(array_keys($data_element_links[$data_element_id])));
      if (isset($form_element_links[$metatag_id])) {
        $links = $form_element_links[$metatag_id];
        //$form_links[0] = 'none'; // not sure if this should be an option or not.
        foreach ($links as $link) {
          $form_element_id = $link['form_element_id'];
          $element_details = $form_element->elements[$form_element_id];
          $form_links[$link['form_id'].'_'.$form_element_id] = $element_details->name;
        }
      }
    }
  }
  return $form_links;
}