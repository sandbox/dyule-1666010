/**
 * @file
 *  Handles the user interface interactions on the process manager page.
 *
 *  This file supercedes item_edit.js because the needs of the process manager
 *  are fairly different from the data model and form editor pages.  This file
 *  also completely replaces jarrow.element.container.js
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

jQuery(window).ready(function ($) {
    var unsaved_state = false,
        message_timer = null,
        itm;

    /**
     * Define the order of stages by assigning weights to them.
     *
     * @param $container
     */
    function assignWeights($container) {
        var stageWeight = 0,
            formWeight = 0,
            process = itm;
        $container.find('h3.stage_name').each(function (key, value) {
            var id = parseInt($(value).attr('id').substring(6), 10);
            process.stages[id].weight = stageWeight;
            stageWeight++;
        });
        $container.find('div.form_instance').each(function (key, value) {
            var idStrings = $(value).attr('id').substring(3).split('_', 2),
                stageID = parseInt(idStrings[0], 10),
                formID = parseInt(idStrings[1], 10);
            process.stages[stageID].forms[formID].weight = formWeight;
            formWeight++;
        });
    }

    /**
     * Set the flag that the workflow needs to be saved.
     */
    function requestSave() {
        unsaved_state = true;
    }

    function loadFromProcess($container, process) {
        var stageIdx,
            weighted_stages = [];
        for (stageIdx in process.stages) {
            if (process.stages.hasOwnProperty(stageIdx) && process.stages[stageIdx]) {
                weighted_stages[process.stages[stageIdx].weight] = process.stages[stageIdx];

            }
        }

        for (stageIdx = 0; stageIdx < weighted_stages.length; stageIdx++) {
            $container.append(createStageElement(weighted_stages[stageIdx]));
        }
    }

    /**
     * Display a message in the message area of the screen.
     *
     * @param message
     */
    function setMessage(message) {
        clearTimeout(message_timer);
        jQuery('#console').hide();
        jQuery('#message_field').text(message);
        message_timer = setTimeout(function () {
            jQuery('#message_field').text('');
        }, 30000);
    }

    function displayCommitMessage() {
        jQuery('#commit_message').text(Drupal.t('You have uncommitted changes.  All changes are automatically saved, but they will not be reflected in a workflow until you commit them.'));
    }

    function hideCommitMessage() {
        jQuery('#commit_message').text(Drupal.t('You have no uncommitted changes.'));
    }

    function deselectForm() {
        $('.form_instance').removeClass('selected');
        $('#config_form').dialog('close');
    }

    /**
     * Create the stage element.
     *
     * @param stage
     * @param activate
     * @returns {*|jQuery|HTMLElement}
     */
    function createStageElement(stage, activate) {
        var droppableObject,
            $container,
            $content,
            $title,
            $title_link;

        $container = $('<div/>');
        $content = $('<div/>', {
            className: 'stage_content'
        });

        $remove_link = $('<span/>', {
            className: 'remove_stage_link',
            text: Drupal.t('delete stage')
        }).click(function (event) {
            removeStage(stage, $container);
        });
        $title = $('<h3/>', {
            className: 'stage_name',
            id: 'stage_' + stage.id
        });

        $title_link = $('<a/>', {
            href: '#'
        });
        $title_link.append($('<span/>', {
            text: stage.name
        }));
        $title.append($title_link);


        $content.append($('<div/>', {
            className: 'stage_control'
        }).append($remove_link))
        createFormInstanceElements($content, stage.forms);
        if (stage.hasOwnProperty('page')) {
            createPageElements($content, stage.page);
        }
        droppableObject = {
            hoverClass: 'ui-object-container-hover',
            tolerance: 'pointer',
            items: 'div.digital_object',
            accept: '.jarrow_add_object',
            createObject: function (from, droppable) {
                var id_array = $(from).attr('id').split(/-/, 3),
                    object_id = id_array[1],
                    object_weight = id_array[2],
                    $parent = $(droppable.parentNode),
                    object;
                if (!stage.digital_objects) {
                    stage.digital_objects = {};
                }
                if (!stage.digital_objects[object_id]) {
                    object = {
                        stage: stage.id,
                        process: stage.process,
                        id: object_id,
                        roles: $.extend(true, {}, Drupal.settings.jarrow.roles),
                        name: $(from).find('.obj_proto_name').text(),
                        weight: object_weight
                    };
                    stage.digital_objects[object_id] = object;
                }
                $(droppable).remove();
                createDigtialObjectElements($parent, stage.digital_objects, droppableObject);
                requestSave();
                $('#obj_' + stage.id + '_' + object_id).dblclick();
            },
            drop: function (event, ui) {
                $(this).droppable('option').createObject(ui.draggable, this);
            }
        };

        createDigtialObjectElements($content, stage.digital_objects, droppableObject);

        $container.append($title).append($content);
        $content.children('div.form_container').sortable({
            placeholder: 'ui-sortable-placeholder',
            tolerance: 'pointer',
            items: 'div.form_instance',
            stop: function (event, ui) {
                if ($(ui.item).hasClass('jarrow_add_item')) {
                    var form_id = $(ui.item).attr('id').split(/-/, 2)[1],
                        formIdx,
                        form,
                        maxID = 1;
                    for (formIdx in stage.forms) {
                        if (stage.forms.hasOwnProperty(formIdx)) {
                            if (stage.forms[formIdx] && stage.forms[formIdx].id >= maxID) {
                                maxID = parseInt(stage.forms[formIdx].id, 10) + 1;
                            }
                        }
                    }
                    if ($(ui.item).hasClass('jarrow_add_page')) {
                        if (maxID > 1) {
                            createDialog('Notice', 'Web pages and forms cannot not be used together in the same stage.');
                            ($(ui.item)).remove();
                            return;
                        }
                        form = {
                            stage: stage.id,
                            process: stage.process,
                            id: maxID,
                            node_id: form_id,
                            name: $(ui.item).text(),
                            weight: 0
                        };
                        if (!stage.page) {
                            stage.page = {};
                        }
                        stage.page[form_id] = form;
                        pageElement = createPageElement(form);
                        $(ui.item).replaceWith(pageElement);
                        toggleSortableZone(pageElement.parent(), true);
                    } else {

                        form = {
                            stage: stage.id,
                            process: stage.process,
                            id: maxID,
                            roles: $.extend(true, {}, Drupal.settings.jarrow.roles),
                            form: form_id,
                            name: $(ui.item).text(),
                            acceptance_stage: 0,
                            rejection_stage: 0,
                            weight: 0
                        };
                        stage.forms[maxID] = form;
                        $(ui.item).replaceWith(createFormInstanceElement(form).dblclick());
                        assignWeights($('#element_container'));
                    }
                }
                //assignWeights($('#element_container'));
                requestSave();
            }
        });

        //Ludicrous work-around for Firefox
        //Whenever an <input> element is within an <a> tag in firefox, pressing
        //enter causes a click event on the a tag, which in this case causes the
        //accordion to collapse.
        $title_link.click(function (event) {
            //Check that the firefox specific input source member is present
            if (event.originalEvent.mozInputSource !== undefined
                //And that the source is the keyboard (for a click event?!?)
                && event.originalEvent.mozInputSource === event.originalEvent.MOZ_SOURCE_KEYBOARD) {
                //If it is, then we make sure that the accordion doesn't get the click
                event.stopImmediatePropagation();
            }
        });
        $title_link.find('span').editInPlace({
            callback: function (original_element, html) {
                stage.name = html;
                requestSave();
                return html;
            }
        });

        return $container;
    }


    /**
     * Display the digital object config pane. This displays the role
     * permissions for a particular digital object.
     *
     * @param digital_object
     * @param $container
     */
    function showDigitalObjectConfigPane(digital_object, $container) {

        $('.object_role_container input').unbind('change').change(function () {
            var idString = $(this).attr('id').split('_', 3),
                operation = idString[1],
                role = idString[2];
            switch (operation) {
                case 'view':
                    digital_object.roles[role].can_view = $(this).attr('checked') !== false;
                    break;
                case 'crea':
                    digital_object.roles[role].can_create = $(this).attr('checked') !== false;
                    break;
                case 'edit':
                    digital_object.roles[role].can_edit = $(this).attr('checked') !== false;
                    break;
            }
            requestSave();
        }).each(function () {
            var idString = $(this).attr('id').split('_', 3),
                operation = idString[1],
                role = idString[2];
            if (digital_object.roles[role] !== undefined) {
                switch (operation) {
                    case 'view':
                        if (digital_object.roles[role].can_view) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                    case 'crea':
                        if (digital_object.roles[role].can_create) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                    case 'edit':
                        if (digital_object.roles[role].can_edit) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                }
            }
        });
        $('#object_config_form').dialog('open');
    }

    /**
     * Enable or disable the specified sortable/drop zone.
     *
     * @param container
     * @param enabled
     */
    function toggleSortableZone(container, isEnabled) {
        container.sortable("option", "disabled", isEnabled);
    }

    /**
     * Remove a page instance from a stage.
     *
     * @param stage
     * @param page
     * @param pageElement
     */
    function removePageInstance(stage, page, pageElement) {
        toggleSortableZone(pageElement.parent(), false);
        stage.page[page.id] = undefined;
        $(pageElement).remove();
        deselectForm();
        requestSave();
    }

    /**
     * Create a page element
     *
     * @param page_instance
     * @returns {*|jQuery|HTMLElement}
     */
    function createPageElement(page_instance) {
        var $container = $('<div/>', {
            id: 'pi_' + page_instance.stage + '_' + page_instance.id,
            className: 'page_instance'
        });
        $container.append($('<span/>', {
                className: 'remove_form_link',
                text: Drupal.t('remove')
            }).click(function (event) {
                removePageInstance(itm.stages[page_instance.stage], page_instance, $(this.parentNode));
                event.stopPropagation();
            })).append($('<span/>', {
                className: 'page_name',
                text: page_instance.name
            }));

        $container.find('*').add($container).mouseover(function (e) {
            $('.page_instance').removeClass('selected');
            $container.addClass('selected');
        }).mouseout(function () {
            $('.page_instance').removeClass('selected');
        });

        return $container;
    }

    /**
     * Create the web page elements that appear in a stage.
     *
     * This is called when the page is first loaded to ensure any previous
     * saved work (draft or otherwise) loaded and displayed correctly.
     *
     * @param stage_element
     * @param page_object
     */
    function createPageElements(stage_element, page_object) {

        var formIdx,
            page,
            weighted_forms = [],
            form_container = stage_element.find('.form_container');

        for (formIdx in page_object) {
            if (page_object.hasOwnProperty(formIdx)) {
                page = page_object[formIdx];
                if (page) {
                    weighted_forms[page.weight] = page;

                }

            }
        }
        for (formIdx = 0; formIdx < weighted_forms.length; formIdx++) {
            page = weighted_forms[formIdx];
            if (page) {
                form_container.append(createPageElement(page));
            }
        }
    }

    /**
     * Create the specific digital object element that appears in a stage.
     *
     * @param object
     * @param droppableDefinition
     * @returns {*|jQuery|HTMLElement}
     */
    function createDigitalObjectElement(object, droppableDefinition) {
        var $container = $('<div/>', {
            id: 'obj_' + object.stage + '_' + object.id,
            className: 'digital_object'
        });
        $container.append($('<span/>', {
                className: 'object_name',
                text: object.name
            })).append($('<span/>', {
                className: 'remove_object_link',
                text: Drupal.t('remove')
            }).click(function (event) {
                var $stageContainer = $(this).parents('.stage_content'), $objectContainer = $(this).parents('.object_container');
                itm.stages[object.stage].digital_objects[object.id] = undefined;
                $objectContainer.remove();
                createDigtialObjectElements($stageContainer, itm.stages[object.stage].digital_objects, droppableDefinition);
                requestSave();
                event.stopPropagation();
            }));

        $container.find('*').add($container).mouseover(function (e) {
            $('.digital_object').removeClass('selected');
            $container.addClass('selected');
        }).mouseout(function () {
            $('.digital_object').removeClass('selected');
        });
        $container.dblclick(function () {
            showDigitalObjectConfigPane(object, $container);
        });
        return $container;
    }

    /**
     * Create the list of digital object elements that appear in the stage
     * object located on the left hand side of the screen. Under the title
     * of 'Digital Objects'.
     *
     * This is called when the page is first loaded to ensure any previous
     * saved work (draft or otherwise) loaded and displayed correctly.
     *
     * @param $stage_element
     * @param digital_objects
     * @param droppableDefinition
     */
    function createDigtialObjectElements($stage_element, digital_objects, droppableDefinition) {
        var $object_container = $('<div/>', {
            'class': 'object_container'
        }).append($('<span/>', {
                'class': 'container-label',
                'text': Drupal.t('Digital Objects')
            }));
        var weighted_objects = [],
            objIdx,
            object;
        for (objIdx in digital_objects) {
            if (digital_objects.hasOwnProperty(objIdx)) {
                object = digital_objects[objIdx];
                if (object) {
                    weighted_objects[object.weight] = object;
                }
            }
        }
        for (objIdx = 0; objIdx < weighted_objects.length; objIdx++) {
            object = weighted_objects[objIdx];
            if (object) {
                $object_container.append(createDigitalObjectElement(object, droppableDefinition));
            }
        }
        $stage_element.append($object_container);
        $object_container.droppable(droppableDefinition);
    }

    /**
     * Display the config pane when a user double clicks a form element.
     *
     * @param form_instance
     * @param $container
     */
    function showFormConfigPane(form_instance, $container) {
        this.selectedForm = form_instance;
        function displayThrobber(onElement, message) {
            //Adapted from Drupal's ajax.js function beforeSerialize
            var progressElement = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
            if (message) {
                $('.throbber', progressElement).after('<div class="message">' + message + '</div>');
            }
            $(onElement).after(progressElement);
        }


        $('[name="form_instance_id"]').val(form_instance.id);
        $('[name="stage_id"]').val(form_instance.stage);
        $('#load_trigger_config').mousedown();

        $('#edit-form').val(form_instance.form).unbind('change').change(function () {
            form_instance.form = $(this).val();
            form_instance.name = $(this).find('option:selected').text();
            $container.find('.form_name').text(form_instance.name);
            requestSave();
        });

        $('body').undelegate('input,select', 'change');
        $('body').delegate('input,select', 'change', function () {
            var elementName = $(this).attr('name'),
                name_array,
                condition_id,
                action_id,
                form_array;
            //console.log('jarrow', jarrow);
            // console.log('form_instance', form_instance);
            // Set the trigger and action values
            if (elementName.substring(0, 24) === 'trigger_config[triggers]') {
                name_array = elementName.substring(25, elementName.length - 1).split('][');
                //console.log('name_array', name_array);
                condition_id = name_array[0];
                //console.log('condition id', condition_id);
                if (name_array[1] === 'type') {
                    //console.log('** this is a type **');
                    form_instance['conditions'][condition_id].type = $(this).val();
                } else if (name_array[1] === 'config') {
                    //console.log('** this is a config **');
                    form_array = jarrow.createArrayFromFormElements();
                    form_instance['conditions'][condition_id].config = form_array['trigger_config']['triggers'][condition_id].config;
                } else if (name_array[1] === 'actions') {
                    //console.log('** this is an action **');
                    action_id = name_array[2];
                    if (name_array[3] === 'type') {
                        form_instance['conditions'][condition_id]['actions'][action_id].type = $(this).val();
                    } else if (name_array[3] === 'config') {
                        form_array = jarrow.createArrayFromFormElements();
                        //console.log('form_array', form_array);
                        // Need to use this form of accessing the the inner properties of each object without errors.
                        form_instance['conditions'][condition_id]['actions'][action_id].config = form_array['trigger_config']['triggers'][condition_id]['actions'][action_id].config;
                    }
                }
                displayThrobber(this, Drupal.t('Updating...'));
                requestSave();
            }
        });

        $('#edit-trigger-config-add-trigger').unbind('click');

        $('#edit-trigger-config-add-trigger').click(function (event) {
            var maxID = 0,
                newID = 0,
                id;
            if (form_instance.conditions === undefined) {
                form_instance.conditions = {}
            }
            //console.log('form instance', form_instance);
            for (id in form_instance.conditions) {
                if (parseInt(id, 10) > maxID) {
                    maxID = parseInt(id, 10);
                }
            }
            newID = maxID + 1;
            form_instance.conditions[newID] = {
                type: 'submission',
                config: {},
                actions: {}
            };
            requestSave();
            displayThrobber(this, Drupal.t('Adding Trigger...'));
            event.stopPropagation();
            event.preventDefault();
        });

        $('body').undelegate('.add_action_button', 'click').delegate('.add_action_button', 'click',function (event) {
            var cond_id = $(this).attr('id').split('_')[1],
                maxID = 0,
                newID = 0,
                id;
            console.log('form instance', form_instance);
            for (id in form_instance.conditions[cond_id].actions) {
                if (parseInt(id, 10) > maxID) {
                    maxID = parseInt(id, 10);
                }
            }
            newID = maxID + 1;

            form_instance.conditions[cond_id].actions[newID] = {
                type: 'move_stage',
                config: {
                    stage: 0
                }
            };
            requestSave();
            displayThrobber(this, Drupal.t('Adding Action...'));
            event.stopPropagation();
            event.preventDefault();
        }).undelegate('.remove_action', 'click').delegate('.remove_action', 'click',function (event) {
            var id_array = $(event.target).attr('id').split('_'),
                cond_id = id_array[1],
                act_id = id_array[2];
            form_instance.conditions[cond_id].actions[act_id] = undefined;
            requestSave();
            displayThrobber(this, Drupal.t('Removing...'));
            event.stopPropagation();
            event.preventDefault();
        }).undelegate('.remove_condition', 'click').delegate('.remove_condition', 'click', function (event) {
            var cond_id = $(event.target).attr('id').split('_')[1];
            form_instance.conditions[cond_id] = undefined;
            requestSave();
            displayThrobber(this, Drupal.t('Removing...'));
            event.stopPropagation();
            event.preventDefault();
        });

        $('#edit-acceptance-stage').html(createStageSelect()).unbind('change').change(function () {
            form_instance.acceptance_stage = $(this).val();
            requestSave();
        }).val(form_instance.acceptance_stage);
        $('#edit-rejection-stage').html(createStageSelect()).unbind('change').change(function () {
            form_instance.rejection_stage = $(this).val();
            requestSave();
        }).val(form_instance.rejection_stage);

        $('.role_container input').unbind('change').change(function () {
            var idString = $(this).attr('id').split('_', 3),
                operation = idString[1],
                role = idString[2],
                key;

            switch (operation) {
                case 'view':
                    //form_instance.roles[role].can_view = $(this).attr('checked') !== false;
                    form_instance.roles[role].can_view = $(this).attr('checked');
                    break;
                case 'crea':
                    for (key in form_instance.roles) {
                        // only allow a single role to create a form
                        if (key !== role) {
                            //form_instance.roles[key].can_create = $(this).attr('checked') === false;
                            form_instance.roles[key].can_create = false;
                            $('#rc_crea_' + key).removeAttr('checked');
                        }
                    }
                    //form_instance.roles[role].can_create = $(this).attr('checked') !== false;
                    form_instance.roles[role].can_create = $(this).attr('checked');
                    break;
                case 'edit':
                    //form_instance.roles[role].can_edit = $(this).attr('checked') !== false;
                    form_instance.roles[role].can_edit = $(this).attr('checked');
                    break;
                case 'oapr':
                    if ($(this).attr('checked')) {
                        form_instance.roles[role].can_one_approve = true;
                        form_instance.roles[role].can_all_approve = false;
                        $('#rc_aapr_' + role).removeAttr('checked');
                    } else {
                        form_instance.roles[role].can_one_approve = false;
                    }
                    break;
                case 'aapr':
                    if ($(this).attr('checked')) {
                        form_instance.roles[role].can_all_approve = true;
                        form_instance.roles[role].can_one_approve = false;
                        $('#rc_oapr_' + role).removeAttr('checked');
                    } else {
                        form_instance.roles[role].can_all_approve = false;
                    }
                    break;
            }
            requestSave();
        }).each(function () {
            var idString = $(this).attr('id').split('_', 3),
                operation = idString[1],
                role = idString[2];
            if (form_instance.roles[role] !== undefined) {
                switch (operation) {
                    case 'view':
                        if (form_instance.roles[role].can_view) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                    case 'crea':
                        if (form_instance.roles[role].can_create) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                    case 'edit':
                        if (form_instance.roles[role].can_edit) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                    case 'oapr':
                        if (form_instance.roles[role].can_one_approve) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                    case 'aapr':
                        if (form_instance.roles[role].can_all_approve) {
                            $(this).attr('checked', 'checked');
                        } else {
                            $(this).removeAttr('checked');
                        }
                        break;
                }
            }
        });

        $('#config_form').dialog('open');
    }

    /**
     * Remove a form instance from a stage.
     *
     * @param stage
     * @param form
     * @param formElement
     */
    function removeFormInstance(stage, form, formElement) {
        stage.forms[form.id] = undefined;
        $(formElement).remove();
        deselectForm();
        assignWeights($('#element_container'));
        requestSave();
    }

    /**
     * Create the specific form instance that appears in a stage.
     *
     * @param form_instance
     * @returns {*|jQuery|HTMLElement}
     */
    function createFormInstanceElement(form_instance) {
        var $container = $('<div/>', {
            id: 'fi_' + form_instance.stage + '_' + form_instance.id,
            className: 'form_instance'
        });
        $container.append($('<span/>', {
                className: 'remove_form_link',
                text: Drupal.t('remove')
            }).click(function (event) {
                removeFormInstance(itm.stages[form_instance.stage], form_instance, this.parentNode);
                event.stopPropagation();
            })).append($('<span/>', {
                className: 'form_name',
                text: form_instance.name
            }));

        $container.find('*').add($container).mouseover(function (e) {
            $('.form_instance').removeClass('selected');
            $container.addClass('selected');
        }).mouseout(function () {
            $('.form_instance').removeClass('selected');
        });

        $container.dblclick(function () {
            showFormConfigPane(form_instance, $container);

        });
        return $container;
    }

    /**
     * This creates the list of form instances that appear in the stage
     * object located on the left hand side of the screen. Under the title
     * of 'Forms.'
     *
     * This is called when the page is first loaded to ensure any previous
     * saved work (draft or otherwise) loaded and displayed correctly.
     *
     * @param $stage_element
     * @param form_instances
     * @param digital_objects
     */
    function createFormInstanceElements($stage_element, form_instances, digital_objects) {
        var formIdx,
            form,
            weighted_forms = [],
            $form_container = $('<div/>', {
                'class': 'form_container'
            }).append($('<span/>', {
                    'class': 'container-label',
                    'text': Drupal.t('Forms')
                }));

        for (formIdx in form_instances) {
            if (form_instances.hasOwnProperty(formIdx)) {
                form = form_instances[formIdx];
                if (form) {
                    weighted_forms[form.weight] = form;

                }

            }
        }
        for (formIdx = 0; formIdx < weighted_forms.length; formIdx++) {
            form = weighted_forms[formIdx];
            if (form) {
                $form_container.append(createFormInstanceElement(form));
            }
        }
        $stage_element.append($form_container);

    }

    function createDialog(title, text) {
        return $("<div class='dialog' title='" + title + "'><p>" + text + "</p></div>")
            .dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "Okay": function () {
                        $(this).dialog("close");
                    }
                }
            });
    }

    /**
     * Create the accordion that contains the information about a
     * specific stage.
     *
     * @param $container
     * @param id
     */
    function createAccordion($container, id) {
        var active,
            itm = Drupal.settings.jarrow.item,
            lowestWeight = 100000,
            stageIdx,
            property,
            objectDblClick;
        if (id === undefined) {
            for (stageIdx in itm.stages) {
                if (itm.stages.hasOwnProperty(stageIdx)) {
                    property = itm.stages[stageIdx];
                    if ((property !== null) && (property.weight < lowestWeight)) {
                        id = stageIdx;
                        lowestWeight = itm.stages[stageIdx].weight;
                    }
                }
            }
        }
        active = '#stage_' + id;
        //Connect double clicking on one of the addable digital objects to the action
        //of dropping it on the active stage.
        objectDblClick = function (event, ui) {
            if (ui.newHeader !== undefined) {
                active = '#' + ui.newHeader.attr('id');
            }
            $('.jarrow_add_object').unbind('dblclick').bind('dblclick', function (e) {
                var activeDroppable = $(active).siblings('.stage_content').children('.object_container');
                activeDroppable.droppable('option').createObject(e.currentTarget, activeDroppable[0]);
            });
        };
        $container.accordion({
            header: "h3",
            active: active,
            collapsible: true,
            animated: 'bounceslide',
            event: 'click',
            autoHeight: false,
            changestart: function (e, ui) {
                deselectForm();
                objectDblClick(e, ui);
            },
            create: function (e, ui) {
                objectDblClick(e, ui);
            }
        });
    }

    /**
     * Remove the specified stage from the workflow.
     *
     * @param {Object} stage
     * @param {Object} stageElement
     */
    function removeStage(stage, stageElement) {
        var stageID = stage.id,
            stageIdx,
            formIdx,
            curStage,
            form,
            $container = $('#element_container');
        itm.stages[stageID] = undefined;
        for (stageIdx in itm.stages) {
            if (itm.stages.hasOwnProperty(stageIdx) && itm.stages[stageIdx]) {
                curStage = itm.stages[stageIdx];
                for (formIdx in curStage.forms) {
                    if (curStage.forms.hasOwnProperty(stageIdx) && curStage.forms[formIdx]) {
                        form = curStage.forms[formIdx];
                        if (form.acceptance_stage === stageID) {
                            form.acceptance_stage = 0;
                        }
                        if (form.rejection_stage === stageID) {
                            form.rejection_stage = 0;
                        }
                    }
                }
            }
        }

        stageElement.remove();
        deselectForm();
        assignWeights($container);
        requestSave();

        $container.accordion('destroy');
        createAccordion($container);
    }

    /**
     *
     * @returns {string}
     */
    function createStageSelect() {
        var stages = itm.stages,
            options = "<option value='0'>Default</option>",
            stageIdx,
            stage;
        for (stageIdx in stages) {
            if (stages.hasOwnProperty(stageIdx)) {
                stage = stages[stageIdx];
                if (stage) {
                    options += "<option value='" + stage.id + "'>" + stage.name + "</option>";
                }
            }
        }
        return options;
    }

    /**
     * Save the workflow back to Drupal.
     *
     * @param process
     * @param saveAsDraft
     */
    function saveProcess(process, saveAsDraft) {
        if (undefined === saveAsDraft) {
            saveAsDraft = false;
        }
        if (saveAsDraft) {
            setTimeout(function () {
                saveProcess(process, true);
            }, 2000);
            if (!unsaved_state) {
                return;
            }
        }
        setMessage("Saving...");
        unsaved_state = false;
        var serialized_collection = JSON.stringify(itm);
        $.post(Drupal.settings.jarrow.save_url, {
            draft: saveAsDraft,
            collection: serialized_collection

        }, function (data) {
            if (data.success) {
                if (saveAsDraft) {
                    setMessage("Draft Saved");
                    $('#discard_draft,#save_item').show();
                    $('#export_item').hide();
                    displayCommitMessage();
                } else {
                    setMessage("Save Complete");
                    $('#discard_draft,#save_item').hide();
                    $('#export_item').hide();
                    hideCommitMessage();
                }

                var $load_button = $('#load_trigger_config');
                $load_button.mousedown();
            } else {
                setMessage("Error while saving");
            }
        });
    }

    /**
     * Initialize the workflow screen.
     *
     */
    function initialize() {
        itm = Drupal.settings.jarrow.item;
        var delete_buttons = {},
            discard_buttons = {},
            currentRoleCol = null,
            self = this,
            $container,
            roles = Drupal.settings.jarrow.roles;

        if (itm.isDraft) {
            setMessage("Draft loaded");
            displayCommitMessage();
            $('#export_item').hide();
        } else {
            hideCommitMessage();
            $('#discard_draft,#save_item').hide();
            $('#export_item').show();
        }

        $container = $('#element_container');
        loadFromProcess($container, itm);
        createAccordion($container);
        $container.sortable({
            axis: "y",
            handle: "h3",
            stop: function () {
                assignWeights($container);
                requestSave();
            }
        });

        $('#element_create').button().click(function () {
            var stageIdx,
                maxID = 1,
                stage,
                $stageElement,
                $configForm,
                $objectConfigForm,
                roleIdx;
            for (stageIdx in itm.stages) {
                if (itm.stages.hasOwnProperty(stageIdx) && itm.stages[stageIdx]) {
                    if (itm.stages[stageIdx].id >= maxID) {
                        maxID = parseInt(itm.stages[stageIdx].id, 10) + 1;
                    }
                }
            }
            stage = {
                name: Drupal.t('New Stage'),
                process: itm.id,
                id: maxID,
                forms: {},
                digital_objects: {
                    main: {
                        id: 'main',
                        name: Drupal.t('Main Object'),
                        process: itm.id,
                        roles: {},
                        stage: maxID,
                        weight: 0
                    }
                },
                weight: 0
            };
            for (roleIdx in roles) {
                if (roles.hasOwnProperty(roleIdx)) {
                    stage.digital_objects['main'].roles[roleIdx] = {
                        can_create: false,
                        can_edit: false,
                        can_view: true
                    };
                }
            }
            $stageElement = createStageElement(stage);
            $container.append($stageElement);
            itm.stages[maxID] = stage;
            assignWeights($container);
            $container.accordion('destroy');
            createAccordion($container, maxID);
            deselectForm();
            requestSave();
            $stageElement.find('a span').click();

        });

        $configForm = $("#config_form");
        $objectConfigForm = $('#object_config_form');

        $('#save_item').button().click(function () {
            saveProcess(itm, false);
        });

        $('#delete_item').button().click(function () {
            $("#item_delete").dialog('open');
        });

        delete_buttons = {};
        delete_buttons[Drupal.t("Delete")] = function () {
            window.location.href = Drupal.settings.jarrow.delete_url;
        };
        delete_buttons[Drupal.t("Cancel")] = function () {
            $(this).dialog("close");
        };

        //Generate the "Delete Data Model" dialog box.
        $("#item_delete").dialog({
            autoOpen: false,
            modal: true,
            width: 400,
            buttons: delete_buttons
        });

        $('#discard_draft').button().click(function () {
            $("#item_discard").dialog('open');
        });

        $('#export_item').button().click(function () {
            window.location.href = Drupal.settings.jarrow.export_url;
        });
        discard_buttons = {};
        discard_buttons[Drupal.t("Discard")] = function () {
            window.location.href = Drupal.settings.jarrow.discard_url;
        };
        discard_buttons[Drupal.t("Cancel")] = function () {
            $(this).dialog("close");
        };

        $('#itm_name').editInPlace({
            bg_over: 'transparent',
            callback: function (original_element, html) {
                itm.name = html;
                requestSave();
                return html;
            }
        });

        //Generate the "Discard Draft" dialog box.
        $("#item_discard").dialog({
            autoOpen: false,
            modal: true,
            width: 400,
            buttons: discard_buttons
        });

        $('.role_row div, .role_header div').mouseover(function () {
            var $elem = $(this);

            if ($elem.hasClass('role_selected_column')) {
                return;
            }
            $('.role_container div').removeClass('role_selected_column');
            if ($elem.hasClass('role_create')) {
                currentRoleCol = 'role_create';
            } else if ($elem.hasClass('role_edit')) {
                currentRoleCol = 'role_edit';
            } else if ($elem.hasClass('role_view')) {
                currentRoleCol = 'role_view';
            } else if ($elem.hasClass('role_all_approve')) {
                currentRoleCol = 'role_all_approve';
            } else if ($elem.hasClass('role_one_approve')) {
                currentRoleCol = 'role_one_approve';
            } else if ($elem.hasClass('role_label')) {
                currentRoleCol = null;
            }

            $('.role_container .' + currentRoleCol).addClass('role_selected_column');
        }).mouseout(function () {
            $('.role_container div').removeClass('role_selected_column');
        });

        Drupal.behaviors.jarrow_process_config = {
            attach: function () {
                $('.ajax-progress').remove();
                function sortTriggers() {
                    var newTriggers = {},
                        newID = 0;
                    $('#etd_trigger_container').find('fieldset.condition').each(function (key, value) {
                        newID++;
                        //Since the remove condition link has the condition's id embedded in its
                        //id attribute, we use it to look up the id of this condition'
                        var removeConditionButton = $(value).find('.remove_condition'),
                            cond_id = removeConditionButton.attr('id').split('_')[1],
                            newActID = 0,
                            newActions = {};
                        removeConditionButton.attr('id', 'remove-condition_' + newID);
                        $(value).find('.add_action_button').attr('id', 'add-action_' + newID);
                        newTriggers[newID] = self.selectedForm.conditions[cond_id];

                        $(value).find('fieldset.action').each(function (key, value) {
                            newActID++;
                            var removeActionButton = $(value).find('.remove_action'),
                                act_id = removeActionButton.attr('id').split('_')[2];
                            removeActionButton.attr('id', 'remove-action_' + newID + '_' + newActID);
                            newActions[newActID] = self.selectedForm.conditions[cond_id].actions[act_id];
                        });
                        newTriggers[newID].actions = newActions;
                    });
                    self.selectedForm.conditions = newTriggers;
                    requestSave();
                }


                $('#etd_trigger_container').sortable({
                    items: 'fieldset.condition',
                    stop: function () {
                        sortTriggers();
                    }
                });

                $('fieldset.condition>.fieldset-wrapper').sortable({
                    items: 'fieldset.action',
                    stop: function () {
                        sortTriggers();
                    }
                });


            }
        };

        setTimeout(function () {
            saveProcess(itm, true);
        }, 2000);

        $configForm.dialog({
            autoOpen: false,
            modal: true,
            position: 'top',
            resizable: false,
            width: 600,
            buttons: [
                {
                    text: Drupal.t('Close'),
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        }).keyup(function (e) {
            if (e.keyCode === $.ui.keyCode.ENTER) {
                $(this).dialog('close');
            }

        });

        $objectConfigForm.dialog({
            autoOpen: false,
            modal: true,
            position: 'top',
            resizable: false,
            width: 600,
            buttons: [
                {
                    text: Drupal.t('Close'),
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        }).keyup(function (e) {
            if (e.keyCode === $.ui.keyCode.ENTER) {
                $(this).dialog('close');
            }

        });
        $('.jarrow_add_item').draggable({
            connectToSortable: '.stage_content .form_container',
            helper: 'clone'
        });

        $('.jarrow_add_object').draggable({
            cursorAt: {
                left: 0
            },
            helper: 'clone'
        });
    }

    initialize();
}); 