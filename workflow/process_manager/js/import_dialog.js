/**
 * @file
 *  A import dialog box for selecting which file to import
 *
 * @author
 *  Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

jQuery.extend(jarrow, {
    import_process: function () {
        return function ($) {
            var buttons;

            //Generate the "Import Process" dialog box.

            buttons = {}
            buttons[Drupal.t("Import")] = function () {
                $("#import_item").submit()
                $(this).dialog("close");
            },
            buttons[Drupal.t("Cancel")] = function () {
                $(this).dialog("close");
            }

            $("#import_process").dialog({
                autoOpen: false,
                width: 400,
                buttons: buttons,
                modal: true,
                open: function () {


                }
            });

            $('.import_link').button().click(function () {
                $("#import_process").dialog('open');
            });
        };
    }
});

