<?php

/**
 * @file
 *  Functions for handling creating and manipulation of processes and stages.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */
$path = drupal_get_path('module', 'jarrow');
include_once $path . '/workflow/triggers/triggers.inc';

/**
 * Loads a processes's definition from the database.
 *
 * @param int $process_id
 * The id of the process to load from the database
 *
 * @param boolean $load_draft
 * If true, and there is a draft version of the process in the database,
 * then the function will load that instead
 *
 * @return null| stdObj
 * An object representing the form, or null if the form could not be loaded.
 */
function jarrow_load_process($process_id, $load_draft) {

  $process = FALSE;
  if ($load_draft) {
    //Check if there is a draft version of this form
    $result = db_select('etd_process_drafts', 'psd')
      ->fields('psd', array('id', 'data'))
      ->condition('id', $process_id)
      ->execute();
    $draft = TRUE;
    if ($result) {
      $process = $result->fetch();
    }
  }
  //If the form was not able to be loaded from the drafts table, then try to load it from the saved table.
  if (!$process) {

    $query = db_select('etd_processes', 'ps');
    $query->innerJoin('etd_data_models', 'dm', 'ps.data_model=dm.id');

    $result = $query->fields('ps', array('id', 'name', 'data_model'))
      ->fields('dm', array('name'))
      ->condition('ps.id', $process_id)
      ->execute();
    $draft = FALSE;
    if ($result) {
      $process = $result->fetch();
    }
  }


  //Make sure we were successful in loading the form.
  if ($process) {


    //Generate a javascript object to embed in the page to hold the form we'll be working with.
    if ($draft) {
      //If we've loaded a draft page, then we must simply decode the saved data.
      $process = json_decode($process->data);
      $process->isDraft = TRUE;
//      sort_weighted_elements($process->stages);
    } else {
      //If it's not a draft, we load the associated form elements from the form elements table.
      $elements = jarrow_process_load_stages($process_id, $process->data_model);
      $process->stages = $elements;
    }
    return $process;
  }
  return NULL;
}

/**
 * Load the process stages for the specified process and data model
 *
 * @param int $process_id
 * @param int $data_model_id
 *
 * @return array
 */
function jarrow_process_load_stages($process_id, $data_model_id) {

  $result = db_select('etd_process_stages', 'pstg')
    ->fields('pstg')
    ->condition('process', $process_id)
    ->orderBy('weight')
    ->execute();
  $stages = array();
  foreach ($result as $stage) {
    $stage->forms = jarrow_process_load_stage_form_instances($process_id, $stage->id, $data_model_id);
    $stage->page = jarrow_process_load_stage_page_instances($process_id, $stage->id, $data_model_id);
    $stage->digital_objects = jarrow_load_stage_digital_objects($process_id, $stage->id, $data_model_id);
    $stages[$stage->id] = $stage;
  }

  return $stages;
}

/**
 * Load the digital objects associated with this particular stage of the process.
 *
 * @param int $process_id
 * @param int $stage_id
 * @param int $data_model_id
 *
 * @return array
 */
function jarrow_load_stage_digital_objects($process_id, $stage_id, $data_model_id) {

  //This function uses drupal_static not to store the computed result of the
  //entire function, as the results are different each time the function is called
  //Instead, it's used to reduce the number of database calls that must be made
  //to retrieve all of the permissions.  The rest of the loading functions in this
  //and other workflow files could likely be handled similarly.
  $permissions = & drupal_static(__FUNCTION__);

  if (!isset($permissions[$process_id])) {
    $permissions[$process_id] = array();
    $object_permissions_result = db_select('etd_digital_object_permissions')
      ->fields('etd_digital_object_permissions')
      ->condition('process', $process_id)
      ->execute();
    foreach ($object_permissions_result as $object_permission) {
      if (!isset($permissions[$process_id][$object_permission->stage])) {
        $permissions[$process_id][$object_permission->stage] = array();
      }
      if (!isset($permissions[$process_id][$object_permission->stage][$object_permission->digital_object])) {
        $permissions[$process_id][$object_permission->stage][$object_permission->digital_object] = array();
      }
      $permissions[$process_id][$object_permission->stage][$object_permission->digital_object][$object_permission->role] = $object_permission;
    }
  }

  include_once drupal_get_path('module', 'jarrow') . '/workflow/digital_objects/digital_objects.inc';
  $digital_objects = jarrow_digital_object_load_definitions($data_model_id);
  $digital_objects['main'] = new stdClass();
  $digital_objects['main']->id = 'main';
  $digital_objects['main']->name = t('Main Object');
  $digital_objects['main']->weight = '0';

  $roles = jarrow_get_roles($data_model_id);
  $stage_digital_objects = array();
  foreach ($digital_objects as $digital_object) {
    $object = array(
      'id' => $digital_object->id,
      'stage' => $stage_id,
      'process' => $process_id,
      'name' => $digital_object->name,
      'roles' => array(),
      'weight' => $digital_object->weight,
    );
    $proxy_id = $digital_object->id === 'main' ? 0 : $digital_object->id;
    foreach ($roles as $roleID => $role) {
      if (isset($permissions[$process_id][$stage_id][$proxy_id][$roleID])) {
        $object['roles'][$roleID] = new stdClass();
        $object['roles'][$roleID]->can_view = $permissions[$process_id][$stage_id][$proxy_id][$roleID]->can_view == 1;
        $object['roles'][$roleID]->can_edit = $permissions[$process_id][$stage_id][$proxy_id][$roleID]->can_edit == 1;
        $object['roles'][$roleID]->can_create = $permissions[$process_id][$stage_id][$proxy_id][$roleID]->can_create == 1;
      }
    }
    if (count($object['roles']) > 0) {
      $stage_digital_objects[$digital_object->id] = (object) $object;
    }
  }
  return $stage_digital_objects;
}

/**
 * Load the forms that are associated with this particular stage of the process.
 *
 * @param $process_id
 * @param $stage_id
 * @param $data_model_id
 *
 * @return array
 */
function jarrow_process_load_stage_form_instances($process_id, $stage_id, $data_model_id) {

  $form_query = db_select('etd_process_form_instances', 'pstg');
  $form_query->innerJoin('etd_forms', 'forms', 'forms.id=pstg.form');
  $result = $form_query
    ->fields('pstg')
    ->fields('forms', array('name'))
    ->condition('process', $process_id)
    ->condition('stage', $stage_id)
    ->orderBy('weight')
    ->execute();
  $instances = array();
  foreach ($result as $instance) {
    $instance->roles = jarrow_load_form_instances_permissions($process_id, $stage_id, $instance->id, $data_model_id);
    $instance->conditions = jarrow_load_triggers($process_id, $stage_id, $instance->id);
    $instances[$instance->id] = $instance;
  }

  return $instances;
}

function jarrow_process_load_stage_page_instances($process_id, $stage_id) {

  $form_query = db_select('etd_process_page_instance', 'ppi');
  $result = $form_query
    ->fields('ppi')
    ->condition('process', $process_id)
    ->condition('stage', $stage_id)
    ->orderBy('weight')
    ->execute();
  $instances = array();
  foreach ($result as $instance) {
    $node = node_load($instance->node_id);
    if (isset($node)) {
      $instance->name = $node->title;
    }
    $instances[$instance->id] = $instance;
  }

  return $instances;
}

/**
 * @param $process_id
 * @param $stage_id
 * @param $instance_id
 * @param $data_model_id
 *
 * @return array
 */
function jarrow_load_form_instances_permissions($process_id, $stage_id, $instance_id, $data_model_id) {

  $roles = jarrow_get_roles($data_model_id);
  $permissions = array();
  foreach ($roles as $role_id => $role) {
    $permission = new stdClass();
    $permission->role = $role;
    $result = db_select('etd_role_form_permissions', 'fmp')
      ->fields('fmp', array('can_view', 'can_edit', 'can_one_approve', 'can_all_approve', 'can_create'))
      ->condition('process', $process_id)
      ->condition('stage', $stage_id)
      ->condition('form_instance', $instance_id)
      ->condition('role', $role_id)
      ->execute()
      ->fetch();
    if ($result) {
      $permission->can_edit = $result->can_edit == 1;
      $permission->can_view = $result->can_view == 1;
      $permission->can_one_approve = $result->can_one_approve == 1;
      $permission->can_all_approve = $result->can_all_approve == 1;
      $permission->can_create = $result->can_create == 1;
    } else {
      $permission->can_edit = FALSE;
      $permission->can_view = FALSE;
      $permission->can_one_approve = FALSE;
      $permission->can_all_approve = FALSE;
      $permission->can_create = FALSE;
    }
    $permissions[$role_id] = $permission;
  }
  return $permissions;
}

/**
 * @param $form
 * @param $form_state
 * @param $process
 *
 * @return array
 */
function jarrow_digital_object_permissions_config_pane($form, &$form_state, $process) {

  $roles = jarrow_get_roles($process->data_model);

  // Create the markup for creating the top row of the permissions matrix
  $role_markup = '<div class="object_role_container">'
    . '<div class="role_header">'
    . '<div class="role_top_left_corner role_label"></div>'
    . '<div class="role_view"><span>' . t('View') . '</span></div>'
    . '<div class="role_edit"><span>' . t('Edit') . '</span></div>'
    . '<div class="role_create"><span>' . t('Create') . '</span></div>'
    . '</div>';

  // Create a row for each role in the permissions matrix
  foreach ($roles as $roleID => $role) {
    $role_markup .= '<div class="role_row">'
      . '<div class="role_label" id="label_' . $roleID . '">' . $role . '</div>'
      . '<div class="role_view"><input type="checkbox" id = "ro_view_' . $roleID . '" /></div>'
      . '<div class="role_edit"><input type="checkbox" id = "ro_edit_' . $roleID . '" /></div>'
      . '<div class="role_create"><input type="checkbox" id = "ro_crea_' . $roleID . '" /></div>'
      . '</div>';
  }

  $role_markup .= '<div style="clear:both"></div></div>';

  return array(
    'roles' => array(
      '#type' => 'item',
      '#title' => 'Roles',
      '#description' => 'Choose which positions have permissions to interact with this digital object in this stage.',
      '#markup' => $role_markup,
    ),
  );
}

/**
 * This creates the configuration dialog for a particular process.
 *
 * @param array $form
 * @param array $form_state
 * @param object $process
 *
 * @return array
 */
function jarrow_form_instance_config_pane($form, &$form_state, $process) {

  $result = db_select('etd_forms', 'fm')
    ->fields('fm', array('id', 'name'))
    ->condition('data_model', $process->data_model)
    ->execute();
  $forms = array();
  foreach ($result as $refernce_form) {
    $forms[$refernce_form->id] = $refernce_form->name;
  }

  // Get the roles that are associated with this process
  $roles = jarrow_get_roles($process->data_model);

  // Create the markup for creating the top row of the permissions matrix
  $role_markup = '<div class="role_container">'
    . '<div class="role_header">'
    . '<div class="role_top_left_corner role_label"></div>'
    . '<div class="role_view"><span>' . t('View') . '</span></div>'
    . '<div class="role_edit"><span>' . t('Edit') . '</span></div>'
    . '<div class="role_create"><span>' . t('Create') . '</span></div>'
    . '<div class="role_one_approve"><span>' . t('Single Approval') . '</span></div>'
    . '<div class="role_all_approve"><span>' . t('Group Approval') . '</span></div>'
    . '</div>';

  // Create a row for each role in the permissions matrix
  foreach ($roles as $roleID => $role) {
    $role_markup .= '<div class="role_row">'
      . '<div class="role_label" id="label_' . $roleID . '">' . $role . '</div>'
      . '<div class="role_view"><input type="checkbox" id = "rc_view_' . $roleID . '" /></div>'
      . '<div class="role_edit"><input type="checkbox" id = "rc_edit_' . $roleID . '" /></div>'
      . '<div class="role_create"><input type="checkbox" id = "rc_crea_' . $roleID . '" /></div>'
      . '<div class="role_one_approve"><input type="checkbox" id = "rc_oapr_' . $roleID . '" /></div>'
      . '<div class="role_all_approve"><input type="checkbox" id = "rc_aapr_' . $roleID . '" /></div>'
      . '</div>';
  }

  $role_markup .= '<div style="clear:both"></div></div>';

  // Create and return the form array.
  return array(
    'form' => array(
      '#type' => 'select',
      '#title' => 'Associated Form',
      '#options' => $forms,
    ),
    'roles' => array(
      '#type' => 'item',
      '#title' => 'Roles',
      '#description' => 'Choose which positions have permission for various actions on this form',
      '#markup' => $role_markup,
    ),
    'trigger_config' => jarrow_trigger_config($form, $form_state, $process),
  );
}

/**
 * Save all the stages associated with the specified process.
 *
 * Any stage not included will be removed from the etd_process_stages table.
 *
 * This function iterates through every submitted stage and either updates
 * the stages that exist or creates new stages if they don't.  It then deletes
 * any stages which are in the database but not the process.  This is repeated
 * for the form instances.
 *
 * @param std_class $process
 * The process we will be saving.  In particular, the id and elements attributes
 * of the process must be set.  The id is an integer specifying the identity of
 * this process, and elements is an array of stages,  whose members represent
 * fields in the etd_process_stages table.
 *
 * @return null|boolean
 * True if every element was saved, or NULL if any transaction failed.
 *
 * @todo Verify that the roles being passed in for each form instance are
 *       actually roles.
 */
function jarrow_process_save_stages($process) {

  //Create a list of ids for elements that are in the form.
  $valid_elements = array();

  //Now, because form elements have three primary keys, we can't use the same merge/delete pattern as for stages without
  //creating a rather ugly delete statement, which will probably not result in much saved time.  So instead, we simply
  //wipe out all form instances associated with this process and then re-create them.
  $result = TRUE;
  $result &= db_delete('etd_process_form_instances')->condition('process', $process->id)->execute();
  $result &= db_delete('etd_process_page_instance')->condition('process', $process->id)->execute();
  $result &= db_delete('etd_role_form_permissions')->condition('process', $process->id)->execute();
  $result &= db_delete('etd_digital_object_permissions')->condition('process', $process->id)->execute();

  if ($result === NULL) {
    return NULL;
  }

  foreach ($process->stages as $stage) {
    if (!$stage) {
      continue;
    }
    //Verify that the process for this element matches the current
    //data model.
    if ($stage->process != $process->id) {
      return NULL;
    }

    //Add the current id to the list of stages that we are attempting to save.
    $valid_elements[] = intval($stage->id);

    //Save all of the stages.
    if (db_merge('etd_process_stages')
        ->key(array('id' => $stage->id, 'process' => $stage->process))
        ->fields(array(
          'name' => $stage->name,
          'weight' => $stage->weight,
        ))->execute() === NULL
    ) {
      return NULL;
    }

    //Record all of the digital object permissions for this stage
    foreach ($stage->digital_objects as $digital_object) {
      foreach ($digital_object->roles as $roleID => $role) {
        if (db_insert('etd_digital_object_permissions')
            ->fields(array(
              'role' => $roleID,
              'digital_object' => ($digital_object->id !== 'main' ? $digital_object->id : 0),
              'stage' => $stage->id,
              'process' => $process->id,
              'can_create' => $role->can_create ? 1 : 0,
              'can_view' => $role->can_view ? 1 : 0,
              'can_edit' => $role->can_edit ? 1 : 0,
            ))
            ->execute() === NULL
        ) {
          return NULL;
        }
      }
    }
    debug($stage, true);
    // If there are any page instance associated with this stage then save them.
    if (isset($stage->page)) {

      foreach ($stage->page as $page_instance) {
        if (db_insert('etd_process_page_instance')
            ->fields(array(
              'id' => $page_instance->id,
              'stage' => $page_instance->stage,
              'process' => $page_instance->process,
              'node_id' => $page_instance->node_id,
              'weight' => $page_instance->weight,
            ))
            ->execute() === NULL
        ) {
          return NULL;
        }
      }
    }

    //Insert each form instance associated with the stage into the database.  Each will need to be inserted, because
    //they were entirely erased at the beginning of the function.
    foreach ($stage->forms as $form_instance) {
      if (!$form_instance) {
        continue;
      }

      if (db_insert('etd_process_form_instances')
          ->fields(array(
            'id' => $form_instance->id,
            'stage' => $form_instance->stage,
            'process' => $form_instance->process,
            'form' => $form_instance->form,
            'acceptance_stage' => $form_instance->acceptance_stage == '0' ? NULL : $form_instance->acceptance_stage,
            'rejection_stage' => $form_instance->rejection_stage == '0' ? NULL : $form_instance->rejection_stage,
            'weight' => $form_instance->weight,
          ))
          ->execute() === NULL
      ) {
        return NULL;
      }
      //Now we must insert the permissions for this form instance
      foreach ($form_instance->roles as $roleID => $role) {
        if (db_insert('etd_role_form_permissions')
            ->fields(array(
              'role' => $roleID,
              'form_instance' => $form_instance->id,
              'stage' => $form_instance->stage,
              'process' => $form_instance->process,
              'can_create' => $role->can_create ? 1 : 0,
              'can_view' => $role->can_view ? 1 : 0,
              'can_edit' => $role->can_edit ? 1 : 0,
              'can_one_approve' => $role->can_one_approve ? 1 : 0,
              'can_all_approve' => $role->can_all_approve ? 1 : 0,
            ))
            ->execute() === NULL
        ) {
          return NULL;
        }
      }

      //Finally, we save the triggers associated with this form instance
      jarrow_trigger_save($form_instance->conditions, $process->id, $form_instance->stage, $form_instance->id);
    }
  }

  //Delete any stages previously associated with this process, which have not been included
  //in the current version.
  $query = db_delete('etd_process_stages')->condition('process', $process->id);
  //The database doesn't like an empty set of ids for the not-in condition, so we only add it if we aren't deleting
  //every possible form element (such as if there is only one left, and we are erasing it).
  if (count($valid_elements) > 0) {
    $query = $query->condition('id', $valid_elements, 'not in');
  }
  return $query->execute() !== NULL;
}

/**
 * @param $process
 *
 * @return array
 */
function jarrow_process_add_list($process) {

  $form_result = db_select('etd_forms')
    ->fields('etd_forms', array('id', 'name'))
    ->condition('data_model', $process->data_model)
    ->execute();

  $elements = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(),
  );

  foreach ($form_result as $form) {
    $elements['#items'][] = array(
      'data' => '<div>' . check_plain($form->name) . '</div>',
      'class' => array('jarrow_add_item'),
      'id' => 'formadd-' . $form->id,
    );
  }

  return $elements;
}

/**
 * @param $process
 *
 * @return array
 */
function jarrow_process_manager_get_related($process) {


  $form_result = db_select('etd_forms')
    ->fields('etd_forms', array('id', 'name'))
    ->condition('data_model', $process->data_model)
    ->execute();
  $related_forms = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => t('Forms'),
    '#items' => array(),
  );
  foreach ($form_result as $related_form) {
    $related_forms['#items'] [] = array(
      'data' => l(check_plain($related_form->name), 'admin/config/jarrow/workflow/forms/' . $related_form->id . '/edit'),
    );
  }

  $process_result = db_select('etd_processes', 'ps')
    ->fields('ps', array('name', 'id'))
    ->condition('id', $process->id, '<>')
    ->condition('data_model', $process->data_model)
    ->execute();
  $related_processes = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => t('Processes'),
    '#items' => array(),
  );
  foreach ($process_result as $related_process) {
    $related_processes['#items'] [] = array(
      'data' => l(check_plain($related_process->name), JARROW_PROCESS_PATH . '/' . $related_process->id . '/edit'),
    );
  }

  $digital_objects = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(
      array(
        'data' => '<div><span class="obj_proto_name">' . t('Main Object') . '</span><span class="obj_edit_link">' .
          l(t('edit'), JARROW_DATAMODEL_PATH . '/' . $process->data_model . '/edit')
          . '</span></div>',
        'class' => array('jarrow_add_object'),
        'id' => 'objectadd-main-0',
      )
    ),
  );
  $object_result = db_select('etd_digital_objects')
    ->fields('etd_digital_objects', array('id', 'name', 'weight'))
    ->condition('data_model', $process->data_model)
    ->orderBy('weight')
    ->execute();

  foreach ($object_result as $digital_object) {
    $digital_objects['#items'][] = array(
      'data' => '<div><span class="obj_proto_name">' . check_plain($digital_object->name) . '</span><span class="obj_edit_link">' .
        l(t('edit'), JARROW_DATAMODEL_PATH . '/' . $process->data_model . '/digital_objects')
        . '</span></div>',
      'class' => array('jarrow_add_object'),
      'id' => 'objectadd-' . $digital_object->id . '-' . $digital_object->weight,
    );
  }

  $web_pages = module_invoke_all('etd_get_webpages', $process);

  return array(
    'related_pane' => array(
      '#type' => 'fieldset',
      '#title' => t('Related Items'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible', 'collapsed'),
      ),
      'related_data_model' => array(
        '#theme' => 'item_list',
        '#title' => t('Data Model'),
        '#type' => 'ul',
        '#items' => array(array('data' => l(check_plain($process->dm_name), JARROW_DATAMODEL_PATH . '/' . $process->data_model . '/edit'))),
      ),
      'related_forms' => jarrow_check_item_list($related_forms),
      'related_processes' => jarrow_check_item_list($related_processes),
    ),
    'digital_object_pane' => array(
      '#type' => 'fieldset',
      '#title' => t('Digital Objects'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible'),
      ),
      'digital_objects' => $digital_objects,
    ),
    'web_page_pane' => array(
      '#type' => 'fieldset',
      '#title' => t('Web Pages'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible'),
      ),
      'web_pages' => $web_pages,
    ),
  );
}

/**
 * Override the default implementation of jarrow_item_list by adding
 * an import button to the default screen.
 *
 * @param array $item_properties
 *
 * @return string
 */
function jarrow_process_create_item_list($item_properties) {

  // Create the standard form
  $output = jarrow_item_list($item_properties);

  //Add a link for importing a process.
  $import_button = array(
    '#type' => 'submit',
    '#value' => t("Import {$item_properties['item_name']}"),
    '#submit' => array('jarrow_process_display_import_dialog'),
    // '#attributes' => array('class' => array('import_link')),
  );
  $output .= drupal_render($import_button);
  return $output;
}

function jarrow_process_display_import_dialog($form, &$form_state) {
  //dsm('import button pressed');
  drupal_goto(JARROW_PROCESS_PATH . '/import');
}

