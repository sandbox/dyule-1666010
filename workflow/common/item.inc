<?php

/**
 * @file
 *  Provides functions common to all workflow items (Data Models, Forms and Processes)
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Displays secondary tabs for the "role" and edit" actions on a Data Model.
 *
 * This function is based on the logic of menu_local_tasks() in menu.inc in the
 * Drupal core.  It's not the world's best function, as it attempts to mimic a
 * drupal core function that can change arbitrarily.  Furthermore, it makes some
 * assumptions about the nature of the theme being used for rendering.  It does,
 *  however, work with Drupal's default administration theme.  It's structured
 * the way it is because the menu system does not appear to be flexible enough
 * to display the secondary navigation on its own without causing errors on
 * pages higher up the navigation hierarchy.
 *
 * @TODO Find a better way of accomplishing this.
 */
function _jarrow_display_secondary_tabs_for_item() {

  $router_item = menu_get_item();

  $result = db_select('menu_router', NULL, array('fetch' => PDO::FETCH_ASSOC))
    ->fields('menu_router')
    ->condition('tab_parent', $router_item['tab_parent'])
    ->condition('context', MENU_CONTEXT_INLINE, '<>')
    ->orderBy('weight')
    ->orderBy('title')
    ->execute();
  $map = $router_item['original_map'];

  $links = array();
  $path = $router_item['path'];
  foreach ($result as $item) {
    _menu_translate($item, $map, TRUE);
    if (count($map) <= 7) {
      if ($item['path'] == $path) {
        $links[] = array(
          '#theme' => 'menu_local_task',
          '#link' => $item,
          '#active' => TRUE,
        );
      } else {
        $links[] = array(
          '#theme' => 'menu_local_task',
          '#link' => $item,
        );
      }
    }
  }
  if (count($links) <= 1) {
    return '';
  }

  $nav_menu = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $links,
  );
  return '<div class="tabs-secondary clearfix">' . render($nav_menu) . '</div>';
}

/**
 * Display the add item dialog box for creating a data model, a form editor or a
 * new process manager.
 *
 * @param $item_properties
 *
 *      The properties of this item, used for accessing the database and creating the page.  Includes the following
 *      attributes:
 *    - item_name: The name of the item type as presented to the user
 *    - item_url: An identifier for the item type as used in a url
 *    - name_field: The field in the POST that will hold the name of the newly constructed item
 *    - table_name: The name of the database table that holds the items.  This table must have an id, name and
 *      optional data_model columns
 *    - is_data_model True if this item type refers to a data model
 *
 * @return string
 */
function jarrow_add_item_dialog($item_properties) {

  //$output = jarrow_item_list($item_properties);
  $form = drupal_get_form('jarrow_add_item_form', $item_properties);
  $output = drupal_render($form);
  return $output;
}

/**
 * Construct the form we'll need for creating new items. This form may be displayed
 * using a jQuery UI dialog thus, the prefix div is pretty important.  The form
 * initially has only a text box for the new name, but can also optionally allow
 * for the selection of an associated data model.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function jarrow_add_item_form($form, $form_state) {

  //Get the item properties from the build_info
  $item_properties = $form_state['build_info']['args'][0];

  // Add the necessary javascript to display this form in a modal dialog
  jarrow_item_add_dialog_script();

  $form = array(
    'name_field' => array(
      '#type' => 'textfield',
      '#title' => t('Enter the name of the new ' . $item_properties['item_name'] . '<span class="form-required" title="This field is required.">*</span>'),
      '#default_value' => '',
      '#maxlength' => 50,
      '#size' => 50,
    )
  );
  if (!$item_properties['is_data_model']) {
    $models = jarrow_item_get_item_names('etd_data_models');
    if (count($models) > 0) {
      $form['data_model'] = array(
        '#type' => 'select',
        '#title' => t('Select the data model this ' . $item_properties['item_name'] . ' is associated with:'),
        '#options' => $models,
        '#id' => 'data_model',
      );
    }
  }
  $form['create'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#validate' => array('jarrow_add_item_form_validate'),
    '#submit' => array('jarrow_add_item_submit'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'jarrow_cancel_button',
    '#submit' => array('jarrow_item_cancel_redirect'),
    '#attributes' => array('id' => 'jarrow_cancel_add_item'),
    '#limit_validation_errors' => array(),
  );
  $form['create_url'] = array(
    '#type' => 'hidden',
    '#value' => JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/create",
  );
  $form['cancel_url'] = array(
    '#type' => 'hidden',
    '#value' => JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}",
  );
  $form['#prefix'] = '<div id="item_create_dialog" title="Create new ' . $item_properties['item_name'] . '">';
  $form['#suffix'] = '</div>';
  $form['#attributes'] = array('id' => 'add_item');

  // We only want to display the cancel button when javascript is enabled so hide by default
  drupal_add_css('#jarrow_cancel_add_item {display: none;}', array(
      'type' => 'inline',
      'media' => 'screen',
      'preprocess' => FALSE,
    )
  );
  return $form;
}

/**
 * Return a list of item names for the specified property
 *
 * @param array $table_name
 *
 * @return array
 */
function jarrow_item_get_item_names($table_name) {

  $result = db_select($table_name, 'tbl')
    ->fields('tbl', array('id', 'name'))
    ->execute();

  $models = array();
  foreach ($result as $model) {
    $models[$model->id] = $model->name;
  }

  return $models;
}

/**
 * Ensure the the from is validated.
 *
 * @param array $form
 * @param array $form_state .
 * @param boolean $set_form_error
 */
function jarrow_add_item_form_validate($form, &$form_state, $set_form_error = TRUE) {

  if (strlen(trim($form_state['values']['name_field'])) == 0) {
    if ($set_form_error) {
      $title = explode('<span', $form['name_field']['#title']);
      form_set_error('name_field', $title[0] . ' is a required field.');
    }
  }
}

/**
 * Implements the submit handler for the create new item form.
 *
 * @param array $form
 * @param array $form_state
 */
function jarrow_add_item_submit($form, $form_state) {

  $item_properties = $form_state['build_info']['args'][0];
  if (isset($form_state['values']['name_field']) && (strlen(trim($form_state['values']['name_field'])) > 0)) {
    //Create a new database record for the name
    $name = $form_state['values']['name_field'];

    //If this item type is associated with a data model, then add the data model
    //to the information being saved to the database
    if ($item_properties['is_data_model']) {
      $fields = array('name' => $name);
    } else {
      $data_model = $form_state['values']['data_model'];
      $fields = array('name' => $name, 'data_model' => $data_model);
    }
    $id = db_insert($item_properties['table_name'])
      ->fields($fields)
      ->execute();
    if ($id !== NULL) {
      //Redirect the user to the editing page.
      drupal_set_message(t("Successfully created {$item_properties['item_name']}"));
      drupal_goto(JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/$id/edit");
    }
  }
  //There was some problem with creating the item, so inform the user.
  drupal_set_message(t("Could not create {$item_properties['item_name']}"), 'error');
  drupal_goto(JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}");
}

/**
 * If the cancel button was clicked, then redirect to the specified url as
 * contained in the cancel_url variable in form_state.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_item_cancel_redirect($form, $form_state) {

  $url = $form_state['input']['cancel_url'];
  drupal_goto($url);
}

/**
 * @param $item_id
 * @param $item_properties
 *
 * @return mixed
 */
function jarrow_item_clone($item_id, $item_properties) {
  dsm($item_id);
  dsm($item_properties);
  if ($item_properties['is_data_model']) {
    jarrow_item_clone_datamodel($item_id, $item_properties);
  }
  if ($item_properties['is_process']) {
    // jarrow_item_clone_process($item_id, $item_properties);
  }
  if ($item_properties['is_form']) {
    // jarrow_item_clone_form($item_id, $item_properties);
  }
}

function jarrow_item_clone_datamodel($item_id, $item_properties) {

  $original_datamodel = db_select('etd_data_models')
    ->fields('etd_data_models')
    ->condition('id', $item_id)
    ->execute()
    ->fetch();
  if ($original_datamodel) {
    $new_id = db_insert('etd_data_models')
      ->fields(array(
        'name' => $original_datamodel->name . ' - ' . t('copy'),
      ))
      ->execute();
    if ($new_id !== NULL) {
      $query = db_select('etd_data_elements');
      $query->addExpression('MAX(id)');
      $max = $query->execute()->fetchField();
      $id = ($max != NULL) ? $max + 1 : 1;
      $original_data_elements = db_select('etd_data_elements')
        ->fields('etd_data_elements')
        ->condition('data_model', $item_id)
        ->execute();
      //->fetch();
      if ($new_id) {
        $map = array();
        foreach ($original_data_elements as $element) {
          $parent = ($element->parent != NULL) ? $map[$element->parent] : NULL;
          $new_entry = db_insert('etd_data_elements')
            ->fields(array(
              'id' => $id,
              'data_model' => $new_id,
              'digital_object' => $element->digital_object,
              'name' => $element->name,
              'description' => $element->description,
              'parent' => $parent,
              'type' => $element->type,
              'type_config' => $element->type_config,
              'source' => $element->source,
              'source_config' => $element->source_config,
              'dc_mapping' => $element->dc_mapping,
              'weight' => $element->weight,
            ))
            ->execute();
          if ($element->parent == NULL) {
            $map[$element->id] = $id;
          }
          $id++;
        }
      }
      drupal_set_message(t('Data model cloned'));
      drupal_goto(JARROW_DATAMODEL_PATH . '/' . $new_id . '/edit');
    }
  }
  drupal_set_message(t('Could not clone the data model'), 'error');
  drupal_goto(JARROW_DATAMODEL_PATH);
}

/**
 * Create the form for item deletion.
 *
 * @param $item_id
 * @param $item_properties
 *
 * @return mixed
 */
function jarrow_item_delete($item_id, $item_properties) {
  $form = drupal_get_form('jarrow_item_confirm_delete', $item_properties, $item_id);
  $output = drupal_render($form);
  return $output;
}

/**
 * Add the necessary javascript to display this form in a modal dialog.
 *
 */
function jarrow_item_add_dialog_script() {

  drupal_add_library('system', 'ui.dialog');
  $path = drupal_get_path('module', 'jarrow');
  drupal_add_js($path . '/workflow/common/js/add_item_dialog.js');
}

/**
 * Create the form to confirm that the user wants to permanently delete this item.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function jarrow_item_confirm_delete($form, $form_state) {

  //Get the item properties from the build_info
  $item_properties = $form_state['build_info']['args'][0];

  // Add the necessary javascript to display this form in a modal dialog
  jarrow_item_add_dialog_script();

  $models = jarrow_item_get_item_names($item_properties['table_name']);

  $form = array(
    'text' => array(
      '#type' => 'markup',
      '#markup' => t("Are you sure you want to delete the {$item_properties['item_name']} with the name: {$models[$form_state['build_info']['args'][1]]}</p>"),
      '#attributes' => array('display' => 'block'),
    ),
    'create' => array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('jarrow_item_confirm_delete_submit'),
    ),
    'cancel' => array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#name' => 'jarrow_cancel_button',
      '#submit' => array('jarrow_item_cancel_redirect'),
      '#attributes' => array('id' => 'jarrow_cancel_add_item'),
      '#limit_validation_errors' => array(),
    ),
    'delete_url' => array(
      '#type' => 'hidden',
      '#value' => JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/delete",
    ),
    'cancel_url' => array(
      '#type' => 'hidden',
      '#value' => JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}",
    ),
    '#prefix' => '<div id="item_create_dialog" title="Delete ' . $item_properties['item_name'] . '">',
    '#suffix' => '</div>',
    '#attributes' => array('id' => 'delete_item'),
  );

  return $form;
}

/**
 * Since the user has confirmed deletion, then remove the item from the database.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_item_confirm_delete_submit($form, $form_state) {

  jarrow_item_remove_from_database($form_state['build_info']['args'][1], $form_state['build_info']['args'][0]);
}

/**
 * Deletes an item and redirects the user back to the item listing page,
 * regardless of whether or not it succeeded.
 *
 * @param int $item_id The id of the item to delete.
 * @param string $item_properties ['item_name'] The name of the item type as presented to the user
 */
function jarrow_item_remove_from_database($item_id, $item_properties) {

  //Start a transaction in case any of the deletes fail
  $txn = db_transaction();

  //Delete the draft table
  $success = db_delete($item_properties['draft_table_name'])->condition('id', $item_id)->execute();

  //The following is included because Drupal 7 does not respect foreign key constraints.
  //Iterate through each associated table (typically element tables) and remove any entries with references to the
  //item being deleted.
  foreach ($item_properties['associated_tables'] as $table) {
    $success &= db_delete($table)->condition($item_properties['item_label'], $item_id)->execute();
  }


  //Delete the table holding the data about this item
  $success &= db_delete($item_properties['table_name'])->condition('id', $item_id)->execute();

  if (is_null($success)) {
    $txn->rollback();
    drupal_set_message(t("Could not delete {$item_properties['item_name']}"), 'error');
  } else {
    drupal_set_message(t("Successfully deleted {$item_properties['item_name']}"));
  }

  //No matter what, go back to the item listing page.
  drupal_goto(JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}");
}

/**
 * Creates a page which lists all available items of the given type. These items can be filtered based
 * on the data model they are associated with, if they are associated with a data model.
 *
 * Also provides links for deleting and editing existing items, as well
 * as the ability to create new items.
 *
 * $item_properties contains the following values:
 *
 * ['item_name'] The name of the item type as presented to the user
 * ['item_url'] An identifier for the item type as used in a url
 * ['name_field'] The field in the POST that will hold the name of the newly constructed item
 * ['table_name'] The name of the database table that holds the items.  This table must have an id, name and optional data_model columns
 * ['draft_table_name'] The name of the database table that holds draft data for each item
 * ['is_data_model'] True if this item type refers to a data model
 *
 * @param array $item_properties
 *
 * @return string
 */

function jarrow_item_list($item_properties) {

  //Add the necessary javascript to the page.
  drupal_add_library('system', 'ui.dialog');
  $path = drupal_get_path('module', 'jarrow') . '';
//  drupal_add_js($path . '/workflow/common/js/item_list.js');
//  drupal_add_js("jQuery(window).ready(jarrow.item_list());", 'inline');
  drupal_add_css($path . '/workflow/common/css/item_list.css');


  //The table of items will have two columns, one for the items and one for the delete button.
  //If the data model filter is included, then the header will have two fields, whereas, without
  //the filter, the header will need to stretch across both columns.
  $table_header = array(
    array(
      'data' => t(ucwords($item_properties['item_name'])),
      'colspan' => $item_properties['is_data_model'] ? 3 : 1,
    )
  );


  //Load the all the items
  $query = db_select($item_properties['table_name'], 'itm');
  //Check if there is a draft version of any of the items
  $query->leftJoin($item_properties['draft_table_name'], 'itmd', 'itm.id=itmd.id');
  if (!$item_properties['is_data_model']) {

    //We'll need data models to put in a select element for filtering and for creating new items.
    $models = jarrow_item_get_item_names('etd_data_models');

    //Build the database filter element and add it to the header
    $filter_select = array(
      '#type' => 'select',
      '#options' => array('0' => t('All')) + $models,
      '#default_value' => '0',
      '#id' => 'data_model_filter',
    );
    $filter = '<form>' . theme('select', array('element' => $filter_select)) . '</form>';
    $table_header[] = array(
      'data' => $filter . '<span>' . t('Data Model Filter') . '</span>',
      'id' => 'data_model_header_cell'
    );


    //Add in the name of the associated data model to the item list.
    $query->innerJoin('etd_data_models', 'dm', 'itm.data_model=dm.id');
    $query = $query->fields('itm', array('id', 'name', 'data_model'))
      ->fields('dm', array('name'))
      ->orderBy('data_model');
  } else {
    //If there is no associated data model, don't load anything to do with it.
    $query = $query->fields('itm', array('id', 'name'));
  }

  //Look for draft data if present.
  $query = $query->fields('itmd', array('data'));

  $rows = jarrow_item_create_table_row_data($query, $item_properties);

  $output = theme('table', array(
    'header' => $table_header,
    'rows' => $rows,
    'attributes' => array('id' => 'items'),
    'sticky' => FALSE
  ));

  //Add a link for creating new elements.
  $link_options = array(
    'attributes' => array(
      'class' => 'add_link button',
    )
  );
  $add_button = array(
    '#type' => 'markup',
    '#markup' => l(t("Add new {$item_properties['item_name']}"), JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/add", $link_options),
  );
  $output .= drupal_render($add_button);


  return $output;
}

/**
 * Create the rows data that go into the table displayed on the Data Model, Form Editor and
 * Process Manager screens. This method requires that the query for selection this information
 * has already been name and is ready to execute as this is a utility function.
 *
 * @param queryObject $query
 * @param array $item_properties
 *
 * @return array
 */
function jarrow_item_create_table_row_data($query, $item_properties) {

  $items = $query->execute();
  //Create a row in the table for each form.
  $rows = array();
  // Track the last seen data model.  Since the results are sorted by data model,
  // every time the current data model is different from the last one, we're in a new grouping.
  $current_dm_id = 0;
  foreach ($items as $item) {
    if (!$item_properties['is_data_model']) {
      //Add in a subheader for every data model, so the user can see which data
      //models are associcated with which items
      if ($item->data_model != $current_dm_id) {
        $current_dm_id = $item->data_model;
        $rows[] = array(
          'data' => array(
            array('data' => l($item->dm_name, JARROW_DATAMODEL_PATH . "/{$item->data_model}/edit"), 'colspan' => 2),
          ),
          'class' => array('subheader', 'item_row'),
          'no_striping' => TRUE,
        );
      }
      $class_array = array('item_row', 'model_' . $item->data_model);
    } else {
      $class_array = array('item_row');
    }
    //If there is a saved draft, we'll be loading that instead of the actual version.
    //Thus, we add some text to indicate this to the user.
    if ($item->data !== NULL) {
      $draft_text = ' (' . t('draft') . ')';
    } else {
      $draft_text = '';
    }
    if ($item_properties['is_data_model']) {
      $rows [] = array(
        'data' => array(
          array('data' => l($item->name, JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/{$item->id}/edit") . $draft_text),
          array('data' => l(t('Clone'), JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/{$item->id}/clone", array('attributes' => array('class' => array('clone_link'))))),
          //This delete link is picked up by some javascript on the page and a confirmation
          // is added before the link is followed.  This depends on the class delete_link being added.
          array('data' => l(t('Delete'), JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/{$item->id}/delete", array('attributes' => array('class' => array('delete_link'))))),
        ),
        'class' => $class_array,
      );
    } else {
      $rows [] = array(
        'data' => array(
          array('data' => l($item->name, JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/{$item->id}/edit") . $draft_text),
          //This delete link is picked up by some javascript on the page and a confirmation
          // is added before the link is followed.  This depends on the class delete_link being added.
          array('data' => l(t('Delete'), JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/{$item->id}/delete", array('attributes' => array('class' => array('delete_link'))))),
        ),
        'class' => $class_array,
      );
    }
  }

  return $rows;
}

/**
 * Loads and displays a single item for editing.  This includes the title of the item as
 * well as all of its associated item elements.  If a draft has been saved, this function
 * will display the draft instead.  All of the data displayed will be able to be edited.
 *
 * This function does two separate things.  First, it returns the structure of the editing page, with
 * no data attached.  Secondly, it returns the item itself encoded to be parsed in javascript.
 * The attached javascript will read this data and populate the page to reflect the current data.
 *
 * @param int $item_id
 *   The id of the item to display
 * @param array $item_properties
 *   An array of properties to use for determining the correct method for displaying
 *   this item.  Includes the following properties:
 *   - item_url: The url path component used for accessing this item type
 *   - item_name: A human readable name for this item type
 *   - is_data_model: True if this item is a data model, false otherwise
 *   - include_file: A file to include before attempting to invoke any functions
 *   - load_function: the function to use for loading this item from the database
 *   - config_form: A function to generate a form for configuration
 *   - script: A javascript file to include along with this page.
 * @param int $filter_id
 *   The id to use for filtering these elements
 *
 * @return string
 *   A representation of the page structure in HTML.
 * @todo Change the returned text to use some sort of template page
 */
function jarrow_item_edit($item_id, $item_properties, $filter_id = NULL) {

  $item = call_user_func($item_properties['load_function'], $item_id, TRUE);

  // If we were successful in loading the item then create the edit page.
  if ($item) {
    // Add the javascript necessary to this page for allow for clientside editing.
    $button_html = jarrow_item_edit_add_javascript($item, $item_properties, $filter_id);

    // Create the buttons for interacting with the item.
    $control_array = array(
      '#type' => 'fieldset',
      '#title' => t('Status'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#attached' => array(
        'js' => array(
          'misc/form.js',
          'misc/collapse.js',
        ),
      ),
      '#attributes' => array(
        'class' => array('collapsible', 'control_box'),
      ),
      'commit_message' => array(
        '#type' => 'container',
        '#attributes' => array(
          'id' => 'commit_message',
        ),
      ),
      //The discard draft button will be hidden using css and javascript unless
      //a draft is currently being edited.
      'discard_draft' => array(
        '#type' => 'button',
        '#value' => t("Discard Changes"),
        '#id' => 'discard_draft',
      ),
      'save_item' => array(
        '#type' => 'button',
        '#value' => t("Commit Changes"),
        '#id' => 'save_item',
      ),
    );
    if ($item_properties['is_process']) {
      $control_array['export_item'] = array(
        '#type' => 'button',
        '#value' => 'Export',
        '#id' => 'export_item',
      );
    }
    $control_array['message_field'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'message_field',
      ),
    );
    $control_html = drupal_render($control_array);

    //Load the form used for editing each data element.  This is handled by a different function
    //both for modularity and also so that drupal's ajax facility will be able to easily generate
    //replacement html for components of this form.

    $config_form_array = drupal_get_form($item_properties['config_form'], $item);
    $config_form = drupal_render($config_form_array);

    if (isset($item_properties['add_item_pane'])) {
      drupal_add_library('system', 'ui.draggable');
      $add_pane_array = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#attached' => array(
          'js' => array(
            'misc/form.js',
            'misc/collapse.js',
          ),
        ),
        '#attributes' => array(
          'class' => array('collapsible'),
        ),
        '#title' => t($item_properties['new_item_label']),
        '#weight' => 0,
        'items' => call_user_func($item_properties['add_item_pane'], $item),
      );
    } else {
      $add_pane_array = array();
    }

    $add_pane = drupal_render($add_pane_array);


    if (isset($item_properties['related_pane'])) {
      $related_pane_array = call_user_func($item_properties['related_pane'], $item);
    } else {
      $related_pane_array = array();
    }

    $related_pane = drupal_render($related_pane_array);

    $output = _jarrow_display_secondary_tabs_for_item();

    // This is only used for debugging so that we can see any printed messages.
    // If we don't have this then we can't see any messages on this page.
    //jarrow_item_print_debug_messages($output);

    //Add the structure of the page to the output
    $output .= "<div id='item_title_bar'><span id='itm_meta_data'><h2 id='itm_name'>{$item->name}</h2>";
    $output .= "</span></div><div id='collection_form'><div id='element_container'></div>$button_html</div><div id='option_panel'>$control_html$related_pane$add_pane</div><div id='config_form'>$config_form</div>";

    if ($item_properties['is_process']) {
      $object_config_form_array = drupal_get_form('jarrow_digital_object_permissions_config_pane', $item);
      $output .= '<div id="object_config_form">' . drupal_render($object_config_form_array) . '</div>';
    }

    //Add two confirmation dialogs (one for deleting and one for discarding) to the page.
    $delete_confirm_text = t("Are you sure you want to delete this {$item_properties['item_name']}?");
    $output .= '<div id="item_delete"><p>' . $delete_confirm_text . '</p></div>';
    $discard_confirm_text = t('Are you sure you want to discard your changes?');
    $output .= '<div id="item_discard"><p>' . $discard_confirm_text . '</p></div>';

    return $output;
  }
  //If the data model couldn't be loaded, display an error message and go back to the list of data models.
  drupal_set_message(t("Could not load {$item_properties['item_name']}"), 'error');
  drupal_goto(JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}");
}

/**
 * Add all the necessary javascript and css styles to the item edit page.
 *
 * @param object $item
 * @param array $item_properties
 *   An array of properties to use for determining the correct method for displaying
 *   this item.  Includes the following properties:
 *   - item_url: The url path component used for accessing this item type
 *   - item_name: A human readable name for this item type
 *   - is_data_model: True if this item is a data model, false otherwise
 *   - include_file: A file to include before attempting to invoke any functions
 *   - load_function: the function to use for loading this item from the database
 *   - config_form: A function to generate a form for configuration
 *   - script: A javascript file to include along with this page.
 * @param int $filter_id
 *   The id to use for filtering these elements
 *
 * @return string
 *   An html string for rendering a button.
 */
function jarrow_item_edit_add_javascript($item, $item_properties, $filter_id) {

  global $base_url;
  $path = drupal_get_path('module', 'jarrow');

  libraries_load('jquery_editinplace');
  libraries_load('json_js');
  drupal_add_js($path . $item_properties['script']);
  drupal_add_css($path . '/workflow/common/css/item_edit.css');

  if ($item_properties['is_process']) {
    // If the item is a process then add process specific javascript files and css
    drupal_add_library('system', 'ui.accordion');
    drupal_add_css($path . '/workflow/process_manager/css/process_edit.css');
    drupal_add_library('system', 'effects');
    $button_array = array(
      '#type' => 'button',
      '#value' => t('Add Stage'),
      '#id' => 'element_create',
    );
    $button_html = drupal_render($button_array);
  } else {
    drupal_add_js($path . '/workflow/common/js/item_edit.js');
    drupal_add_js($path . '/lib/js/jarrow.element.container.js');
    drupal_add_css($path . '/lib/css/jarrow.element.container.css');
    drupal_add_js($path . '/validation/js/dynamic_textfield_type_validator.js');
    $button_html = '';
  }

  // Add the common JavaScript files that we need.
  drupal_add_library('jarrow', 'ui.editable-list');
  drupal_add_library('system', 'ui.sortable');
  drupal_add_library('system', 'ui.button');
  drupal_add_library('system', 'ui.dialog');
  drupal_add_library('system', 'ui.droppable');
  libraries_load('nestedsortable');
  drupal_add_js($path . '/lib/js/jquery.tree.dropdown.js', 'file');

  // Add the variables that we would like to access from JavaScript
  drupal_add_js(array(
    'jarrow' => array(
      'save_url' => "$base_url/" . JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/save",
      'edit_url' => "$base_url/" . JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/$item->id/edit",
      'discard_url' => "$base_url/" . JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/$item->id/discard",
      'delete_url' => "$base_url/" . JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/$item->id/delete",
      'export_url' => "$base_url/" . JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/$item->id/export",
      'tree_url' => "$base_url/" . JARROW_DATAMODEL_PATH . "/$item->id/tree",
      'element_index' => $item_properties['element_index'],
      'item' => $item,
    )
  ), 'setting');

  if ($item_properties['is_data_model']) {
    // If the item is a data model then add the filter for digital objects
    if (is_null($filter_id)) {
      drupal_add_js(array(
        'jarrow' => array(
          'filter_id' => 0,
          'filter_field' => 'digital_object',
        )
      ), 'setting');
    } else {
      drupal_add_js(array(
        'jarrow' => array(
          'filter_id' => $filter_id,
          'filter_field' => 'digital_object',
        )
      ), 'setting');
    }
  } else {
    // If it isn't a data model then add the roles information to the JavaScript accessible parameters
    $roles = jarrow_get_roles($item->data_model);

    $role_array = array();
    foreach ($roles as $roleID => $role) {
      $role_array[$roleID] = new stdClass();
      $role_array[$roleID]->can_one_approve = FALSE;
      $role_array[$roleID]->can_all_approve = FALSE;
      $role_array[$roleID]->can_view = FALSE;
      $role_array[$roleID]->can_edit = FALSE;
      $role_array[$roleID]->can_create = FALSE;
    }
    drupal_add_js(array(
      'jarrow' => array(
        'tree_url' => "$base_url/" . JARROW_DATAMODEL_PATH . "/$item->data_model/tree",
        'roles' => $role_array,
      )
    ), 'setting');
  }
  // Check if the item is a form item
  /*
    if ($item_properties['is_form']) {
      drupal_add_js(array(
        'jarrow' => array(
          'linked_elements' => jarrow_form_editor_get_linked_elements_list($item->data_model),
        )
      ), 'setting');
    }
  */
  return $button_html;
}

/**
 * Save the item which has been submitted through post.  This should only ever be
 * called when an item has been serialized and posted to the web server.  It is an
 * ajax callback method.
 *
 * This function depends on two submitted fields.  The first is the 'draft' field which
 * indicates if the client wishes to save the submitted item as a draft or as
 * full save, which will discard any previous drafts.
 *
 * The second field is the 'collection' field which includes a JSON encoded representation
 * of the item object.  This item object needs to be deserialized and stored.
 *
 * If the item is being stored as a draft, it will replace the current draft (or add a
 * new one if it doesn't exist) of the item by serializing the item and storing it in the
 * 'data' field of the etd_{item}_drafts table
 *
 * If, on the other hand, the item is not being stored as a draft, the current draft version
 * is thrown away and the item is stored in the standard database tables.
 *
 * @todo Should I include more detailed error messages in case of failure?
 */
function jarrow_item_save($item_properties) {

  //If a draft save is requested, then just save the serialized item
  if (isset($_POST['draft']) && $_POST['draft'] == 'true') {
    if (isset($_POST['collection'])) {
      //Load in the posted item
      $item_text = $_POST['collection'];
      $item = json_decode($item_text);

      //Save the item by its id to the drafts table.
      $result = db_merge($item_properties['draft_table_name'])
        ->key(array('id' => $item->id))
        ->fields(array('data' => $item_text))
        ->execute();

      //Inform the client of the result of the operation
      drupal_json_output(array('success' => $result !== NULL));
      drupal_exit();
    }
  } else {
    cache_clear_all('jarrow', 'cache', TRUE);

    //If a non draft save is requested, then save a proper version of the item that
    //can be referenced by other components.
    if (isset($_POST['collection'])) {

      //Unserialize the data sent from the client.
      $item_text = $_POST['collection'];
      $item = json_decode($item_text);
      //debug(print_r($item, TRUE));
      //debug(print_r($item_properties, TRUE));
      //Since this involves a whole wack of changes to complete, we want to make sure we bundle them all in
      //a transaction.
      $txn = db_transaction();

      //Save the name of the item in the item's main database table.
      $result = db_update($item_properties['table_name'])
        ->fields(array('name' => $item->name))
        ->condition('id', $item->id)
        ->execute();
      //So long as this worked, save the item elements
      if ($result !== NULL) {
        if ($item_properties['element_save_function']($item)) {
          //If everything so far has succeeded, then delete the draft associated with this update.
          db_delete($item_properties['draft_table_name'])->condition('id', $item->id)->execute();
          //Then inform the user of success and exit.
          drupal_json_output(array('success' => TRUE));
          drupal_exit();
        }
      }
      //If any one of the database saves failed, then roll back the transaction and bail out.
      $txn->rollback();
    }
  }
  //Inform the client of the failure to save.
  drupal_json_output(array('success' => FALSE));
  drupal_exit();
}

/**
 * Discards the current draft of the specified item and redirects to the viewing page.
 *
 * @param string $item_id
 *    The ID of the item to discard the draft for.
 * @param $item_properties
 */
function jarrow_item_discard_draft($item_id, $item_properties) {

  $result = db_delete($item_properties['draft_table_name'])->condition('id', $item_id)->execute();
  if ($result !== NULL) {
    drupal_set_message(t('Draft discarded'));
  } else {
    drupal_set_message(t('Draft could not be discarded'), 'error');
  }
  drupal_goto(JARROW_WORKFLOW_PATH . "/{$item_properties['item_url']}/$item_id/edit");
}

