/**
 * @file
 *  Handles the code used on the item listing page
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */


jQuery.extend(jarrow, {
        item_list: function () {
        return function ($) {
            var buttons;


            //Generate the "Add Item" dialog box.
            buttons = {};
            buttons[Drupal.t("Create")] = function () {
                $("#add_item").submit();
                $(this).dialog("close");
            };
            buttons[Drupal.t("Cancel")] = function () {
                $(this).dialog("close");
            };

            $("#item_create").dialog({
                autoOpen: false,
                width: 400,
                buttons: buttons,
                modal: true,
                open: function () {
                    //Set the item to be the one currently being filtered on
                    //by default.  If "All" is selected, this will default to the
                    //first data model, which is expected behavior.  This will
                    //do nothing if there is no data model filter.
                    var id = $('select#data_model_filter').val();
                    $('select#data_model').val(id);

                }
            });

            $('.add_link').button().click(function () {
                $("#item_create").dialog('open');
            });
            //Generate the "Delete Item" dialog box.
            $("#item_delete").dialog({
                autoOpen: false,
                modal: true,
                width: 400
            });
            //Based on code taken from http://stackoverflow.com/questions/887029/how-to-implement-confirmation-dialog-in-jquery-ui-dialog
            $(".delete_link").click(function (e) {
                e.preventDefault();
                var theHREF = $(this).attr("href");
                buttons = {};
                buttons[Drupal.t("Delete")] = function () {
                    window.location.href = theHREF;
                };
                    buttons[Drupal.t("Cancel")] = function () {
                        $(this).dialog("close");
                    }
                $("#item_delete").dialog("option", "buttons", buttons);
                $("#item_delete").dialog('open');
            });

            $('select#data_model_filter').change(function () {
                var id = $('select#data_model_filter').val()
                if (id === '0') {
                    $('.item_row').show();
                } else {
                    $('.item_row').hide();
                    $('.model_' + id).show();
                }
            });
        };
    }
});

