/**
 * @file
 *  Code for managing the general case of item editing.
 *
 *  Requires supporting files for each item type to handle the specifics. Not
 *  used for the process manager.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */


jQuery.extend(true, jarrow, {
        item_edit: (function () {
            var message_timer = null;

            function setMessage(message) {
                clearTimeout(message_timer);
                jQuery('#console').hide();
                jQuery('#message_field').text(message);
                message_timer = setTimeout(function () {
                    jQuery('#message_field').text('');
                }, 30000);
            }

            function displayCommitMessage() {
                jQuery('#commit_message').text(Drupal.t('You have uncommitted changes.  All changes are automatically saved, but they will not be reflected in a workflow until you commit them.'));
            }

            function hideCommitMessage() {
                jQuery('#commit_message').text(Drupal.t('You have no uncommitted changes.'));
            }

            return {
                initialize: function ($) {
                    jarrow.item_edit.initializeData();
                    $('.item-list li.jarrow_add_item').draggable({
                        helper: 'clone',
                        connectToSortable: "#element_container>ul"
                    });
                    var itm = Drupal.settings.jarrow.item,
                        delete_buttons = {},
                        discard_buttons = {},
                        element_index = Drupal.settings.jarrow.element_index,
                        data_source = {
                            defaultElement: jarrow.item_edit.defaultElement,
                            getElement: function (id) {
                                return itm[element_index][id];
                            },
                            getElements: function () {
                                return itm[element_index];
                            },
                            deleteElement: function (id) {
                                delete itm[element_index][id];
                            },

                            isGroup: function (element) {
                                return element.type === 'group';
                            },

                            isDraft: function () {
                                return itm.isDraft;
                            },

                            serialize: function () {
                                return JSON.stringify(itm);
                            },

                            createElement: function (type, source, replacementElement) {
                                var maxID = 0,
                                    element,
                                    id;
                                for (element in itm[element_index]) {
                                    id = parseInt(element, 10);
                                    if (id > maxID) {
                                        maxID = id;
                                    }
                                }
                                id = maxID + 1;
                                element = jarrow.item_edit.defaultElement(id);
                                element.type = type;
                                element.source = source;
                                element.type_label = replacementElement.text();
                                if (id === 1) {
                                    itm[element_index] = {};
                                }
                                itm[element_index][id] = element;

                                $container.elementContainer('addElement', element, replacementElement);
                                replacementElement.remove();
                            },

                            /*
                             * Create a new data form element
                             */
                            createDataFormElement: function ($sourceElement, associated_element_id) {
                                var maxID = 0,
                                    element,
                                    id;
                                for (element in itm.elements) {
                                    id = parseInt(element, 10);
                                    if (id > maxID) {
                                        maxID = id;
                                    }
                                }
                                id = maxID + 1;
                                if ($sourceElement.hasClass('folder')) {
                                    $sourceElement.find('>ul>li').each(function (key, value) {
                                        associated_element_id = $(value).attr('id').split(/-/, 2)[1];
                                        element = jarrow.item_edit.defaultElement(id);
                                        element.name = $(value).text();
                                        element.type = 'data';
                                        element.type_config = {associated_element: associated_element_id};
                                        element.type_label = Drupal.t('Data Source');
                                        if (id === 1) {
                                            itm.elements = {};
                                        }
                                        itm.elements[id] = element;
                                        $container.elementContainer('addElement', element, $sourceElement, true);
                                        id++;
                                    });

                                } else {
                                    element = jarrow.item_edit.defaultElement(id);
                                    element.name = $sourceElement.text();
                                    element.type = 'data';
                                    element.type_config = {associated_element: associated_element_id};
                                    element.type_label = Drupal.t('Data Source');
                                    if (id === 1) {
                                        itm.elements = {};
                                    }
                                    itm.elements[id] = element;
                                    $container.elementContainer('addElement', element, $sourceElement);
                                }
                                $sourceElement.remove();
                            }


                        },
                        $container = $('.element_list').elementContainer({
                            dataSource: data_source,
                            configForm: $('#config_form'),
                            saveURL: Drupal.settings.jarrow.save_url
                        });

                    if (itm.isDraft) {
                        setMessage("Draft loaded");
                        displayCommitMessage();
                    } else {
                        hideCommitMessage();
                        $('#discard_draft,#save_item').hide();
                    }


                    $("#config_form").submit(function (e) {
                        //Under some circumstances, this form is submitted on enter, which
                        //we do not want, as we are saving the form in a different way.
                        e.preventDefault();
                    });


                    $container.bind('savebegin', function () {
                        setMessage('Saving...');
                    });
                    $container.bind('savecomplete', function (event, success, draft) {
                        if (success) {
                            if (draft) {
                                setMessage("Saved a draft!");
                                $('#discard_draft,#save_item').show();
                                displayCommitMessage();
                            } else {
                                setMessage("Saved!");
                                $('#discard_draft,#save_item').hide();
                                hideCommitMessage();
                            }
                        } else {
                            setMessage("Error Saving!");
                        }

                    });

                    $('#element_create').button().click(function () {
                        var maxID = 0,
                            element,
                            id;
                        for (element in itm[element_index]) {
                            id = parseInt(element, 10);
                            if (id > maxID) {
                                maxID = id;
                            }
                        }
                        id = maxID + 1;
                        element = data_source.defaultElement(id);
                        if (id === 1) {
                            itm[element_index] = {};
                        }
                        itm[element_index][id] = element;
                        $container.elementContainer('addElement', element);

                    });

                    $('#group_create').button().click(function () {
                        var maxID = 0,
                            element,
                            id;
                        for (element in itm[element_index]) {
                            id = parseInt(element, 10);
                            if (id > maxID) {
                                maxID = id;
                            }
                        }
                        id = maxID + 1;
                        element = data_source.defaultElement(id);
                        element.type = 'group';
                        if (id === 1) {
                            itm[element_index] = {}
                        }
                        itm[element_index][id] = element;

                        $container.elementContainer('addElement', element);
                    });

                    $('#save_item').button().click(function () {
                        $container.elementContainer('saveCollection');
                    });

                    $('#delete_item').button().click(function () {
                        $("#item_delete").dialog('open');
                    });

                    delete_buttons = {};
                    delete_buttons[Drupal.t("Delete")] = function () {
                        window.location.href = Drupal.settings.jarrow.delete_url;
                    };
                    delete_buttons[Drupal.t("Cancel")] = function () {
                        $(this).dialog("close");
                    };

                    //Generate the "Delete Data Model" dialog box.
                    $("#item_delete").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 400,
                        buttons: delete_buttons
                    });

                    $('#discard_draft').button().click(function () {
                        $("#item_discard").dialog('open');
                    });

                    discard_buttons = {};
                    discard_buttons[Drupal.t("Discard")] = function () {
                        window.location.href = Drupal.settings.jarrow.discard_url;
                    };
                    discard_buttons[Drupal.t("Cancel")] = function () {
                        $(this).dialog("close");
                    };

                    $('#itm_name').editInPlace({
                        bg_over: 'transparent',
                        callback: function (original_element, html) {
                            itm.name = html;
                            $container.elementContainer('requestSave');
                            return(html);
                        }
                    });

                    //Generate the "Discard Draft" dialog box.
                    $("#item_discard").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 400,
                        buttons: discard_buttons
                    });

                    //        initialConfigTop = $configForm.offset().top;
                    //        $(window).scroll(function(event) {
                    //          var configTop = $configForm.offset().top;
                    //          var scrollJump = Math.max($(event.target).scrollTop() - initialConfigTop + 80, 0);
                    //          if(scrollJump < (configTop - initialConfigTop) || configTop + $configForm.outerHeight() < $collectionForm.offset().top + $collectionForm.outerHeight()) {
                    //            $configForm.css('top', scrollJump +'px');
                    //          }
                    //        });
                    //        $configForm.hide();
                    //        $configForm.css('visibility', 'visible');
                }

            };
        }())
    }
);

jQuery(document).ready(function ($) {

    jarrow.item_edit.initialize($);


});