(function ($) {
    $(document).ready(function () {
        $("#item_create_dialog").dialog({
            autoOpen: true,
            modal: true,
            width: 400,
            open: function() {
                $('#jarrow_cancel_add_item').css({display: 'inline', float: 'right'});
                var id = $('select#data_model_filter').val();
                $('select#data_model').val(id);
                $('.messages').prependTo('#item_create_dialog');
            }
        }).bind('dialogclose', function(event, ui) {
                var newURL = $('input[name="cancel_url"]').val();
                window.location = newURL;
        });
    });
}(jQuery));
