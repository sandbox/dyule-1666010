<?php

// Define the constants
define('JARROW_METATAGS_PATH', 'admin/jarrow/settings/metatags');


/**
 * Create the metatags settings administration page.
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function jarrow_metatags_settings($form, &$form_state) {

  $form = array();
  $form['meta_tags'] = array(
    '#type' => 'markup',
    '#markup' => '<h3>' . t('Meta Tag Fields') . '</h3><p>' . t('Manage the metatag fields used by the ETD system. Typically these consist of Dublin Core fields, or MODS fields or even your own tagging system. You can mix and match as you see fit.') . '</p>',
  );

  $form['add_tag'] = array(
    '#type' => 'markup',
    '#markup' => l('+ Add Metatag', JARROW_METATAGS_PATH . '/add'),
  );

  $header = array(
    'meta_tag' => array('data' => t('Metatag')),
    'linked_tag' => array('data' => t('Link with tag')),
    'operation' => array('data' => t('Operation')),
  );

  $header['operation']['colspan'] = 2;

  $form['metatag_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => jarrow_metatags_get_tag_table_data(),
    '#empty' => t('No metatags have been added.'),
  );

  $form['actions'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Return a list of all metatags
 *
 * @return array
 */
function jarrow_metatags_get_tag_table_data() {

  $metatags = array();
  $result = jarrow_metatags_get_tags();
  foreach ($result as $field) {
    $linked_ids = jarrow_metatags_get_linked_tags($field);
    $metatags[] = array(
      check_plain($field->name),
      implode(', ', array_values($linked_ids)),
      l(t('edit'), JARROW_METATAGS_PATH . '/' . $field->id . '/edit'),
      l(t('delete'), JARROW_METATAGS_PATH . '/' . $field->id . '/delete'),
    );
  }

  return $metatags;
}

/**
 * Return an array of only the metatags that have links along with what tags they are linked to.
 *
 * @return array
 */
function jarrow_metatags_get_linked_tag_list() {

  $metatags = array();
  $result = jarrow_metatags_get_tags();
  foreach ($result as $field) {
    $linked_ids = jarrow_metatags_get_linked_tags($field);
    if ($linked_ids) {
      $metatags[$field->id] = array('name' => $field->name, 'linked_tags' => $linked_ids);
    }
  }
  return $metatags;
}

/**
 * Return a list of associated metatags.
 *
 * @param object $metatag
 *
 * @return array|mixed
 */
function jarrow_metatags_get_linked_tags($metatag) {

  $empty_array = array();
  if ($metatag == NULL) {
    return $empty_array;
  }
  return ($metatag->linked_to == NULL) ? $empty_array : json_decode($metatag->linked_to, TRUE);
}

/**
 * Return a list of all metatags
 *
 * @return array
 */
function jarrow_metatags_get_tags() {

  $result = db_select('etd_dublin_core_fields', 'dc')
    ->fields('dc')
    ->execute();
  return $result;
}

/**
 * Return the list of metatags for us in a select box
 * @return array
 */
function jarrow_metatags_get_tag_options() {

  $result = jarrow_metatags_get_tags();
  foreach ($result as $tag) {
    $metatags[$tag->id] = check_plain($tag->name);
  }
  return $metatags;
}

/**
 * Create the form to allow the user to add an external recipient.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function jarrow_metatags_add($form, &$form_state) {

  // Add the necessary javascript to display this form in a modal dialog
  jarrow_item_add_dialog_script();

  $metatag_options = jarrow_metatags_get_tag_options();
  $form = array(
    'name_field' => array(
      '#type' => 'textfield',
      '#title' => t('Enter the tag name<span class="form-required" title="This field is required.">*</span>'),
      '#default_value' => '',
      '#maxlength' => 100,
      '#size' => 50,
    ),
    'link_field' => array(
      '#type' => 'select',
      '#title' => t('Use this tag in conjunction with:'),
      '#multiple' => TRUE,
      '#options' => $metatag_options,
    ),
  );

  $form['create'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#validate' => array('jarrow_metatags_form_validate'),
    '#submit' => array('jarrow_metatags_add_submit'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'jarrow_cancel_button',
    '#submit' => array('jarrow_item_cancel_redirect'),
    '#attributes' => array('id' => 'jarrow_cancel_add_item'),
    '#limit_validation_errors' => array(),
  );
  $form['create_url'] = array(
    '#type' => 'hidden',
    '#value' => JARROW_METATAGS_PATH,
  );
  $form['cancel_url'] = array(
    '#type' => 'hidden',
    '#value' => JARROW_METATAGS_PATH,
  );
  $form['allow_duplicates'] = array(
    '#type' => 'hidden',
    '#value' => FALSE,
  );
  $form['#prefix'] = '<div id="item_create_dialog" title="Add Metatag">';
  $form['#suffix'] = '</div>';
  $form['#attributes'] = array('id' => 'add_item');

  return $form;
}

/**
 * Validate the metatag submission. Ensure all required fields are valid.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_metatags_form_validate($form, &$form_state) {

  if (strlen(trim($form_state['values']['name_field'])) == 0) {
    form_set_error('name_field', t("The name of the metatag cannot be blank."));
  } else {
    if ((!$form_state['values']['allow_duplicates']) && (jarrow_metatags_exists($form_state['values']['name_field']))) {
      form_set_error('name_field', t("A metatag already exists with the name {$form_state['values']['name_field']}."));
    }
  }
}

/**
 * Check to see if an metatag with the same name already exists.
 *
 * @param string $name
 *
 * @return boolean
 */

function jarrow_metatags_exists($name) {

  $recipient = jarrow_metatags_get_tag_with_name($name);
  return isset($recipient->name);
}

/**
 * Return the metatag details as identified by metatag name.
 *
 * @param string $name
 *
 * @return mixed
 */
function jarrow_metatags_get_tag_with_name($name) {

  $result = db_select('etd_dublin_core_fields', 'dc')
    ->fields('dc')
    ->condition('name', $name)
    ->execute()
    ->fetchObject();
  return $result;
}

/**
 * Return the metatag details as identified by metatag id.
 *
 * @param $metatag_id
 *
 * @return mixed
 */
function jarrow_metatags_get_tag_with_id($metatag_id) {

  $result = db_select('etd_dublin_core_fields', 'dc')
    ->fields('dc')
    ->condition('id', $metatag_id)
    ->execute()
    ->fetchObject();
  return $result;
}

/**
 * The user has submitted the form to add a new metatag, so perform the
 * appropriate actions.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_metatags_add_submit($form, &$form_state) {

  $links = jarrow_metatags_process_selected_links($form_state);
  jarrow_metatags_save_tag($form_state['values']['name_field'], $links);
  drupal_goto(JARROW_METATAGS_PATH);
}

/**
 * Add the new metatag to the database table.
 *
 * @param string $name
 * @param array $links
 * @param int $metatag_id
 */
function  jarrow_metatags_save_tag($name, $links, $metatag_id = NULL) {

  if (count($links) == 0) {
    $links == NULL;
  }
  if ($metatag_id == NULL) {
    $result = db_merge('etd_dublin_core_fields')
      ->key(array('name' => $name))
      ->fields(array(
        'linked_to' => json_encode($links),
      ))
      ->execute();
  } else {
    $result = db_merge('etd_dublin_core_fields')
      ->key(array('id' => $metatag_id))
      ->fields(array(
        'name' => $name,
        'linked_to' => json_encode($links),
      ))
      ->execute();
  }
  if ($result === NULL) {
    //There was some problem with creating the item, so inform the user.
    drupal_set_message(t("Could not create metatag entry for {$name}"), 'error');
  }

}

/**
 * If the cancel button was clicked, then redirect to the specified url as
 * contained in the cancel_url variable in form_state.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_metatags_cancel_redirect($form, $form_state) {

  $url = $form_state['input']['cancel_url'];
  drupal_goto($url);
}

/**
 * Allow the user to edit a metatag
 *
 * @param arary $form
 * @param array $form_state
 * @param int $metatag_id
 *
 * @return mixed
 */
function jarrow_metatags_edit($form, &$form_state, $metatag_id) {

  $metatag = jarrow_metatags_get_tag_with_id($metatag_id);
  $metatag_options = jarrow_metatags_get_tag_options();

  if (empty($metatag)) {
    drupal_set_message(t('Could not load metatag'), 'error');
    drupal_goto(JARROW_METATAGS_PATH);
  } else {
    // Add the necessary javascript to display this form in a modal dialog
    jarrow_item_add_dialog_script();
    $form['name_field'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The name of the recipient'),
      '#maxlength' => 100,
      '#size' => 50,
      '#default_value' => $metatag->name,
      '#required' => TRUE,
    );

    $options = json_decode($metatag->linked_to, TRUE);
    $list = ($options != NULL) ?array_keys($options): NULL;

    $form['link_field'] = array(
      '#type' => 'select',
      '#title' => t('Use this tag in conjunction with:'),
      '#multiple' => TRUE,
      '#options' => $metatag_options,
      '#default_value' => $list,
    );

    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#validate' => array('jarrow_metatags_form_validate'),
    );

    $form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#name' => 'jarrow_cancel_button',
      '#submit' => array('jarrow_item_cancel_redirect'),
      '#attributes' => array('id' => 'jarrow_cancel_add_item'),
      '#limit_validation_errors' => array(),
    );

    $form['cancel_url'] = array(
      '#type' => 'hidden',
      '#value' => JARROW_METATAGS_PATH,
    );

    $form['metatag_id'] = array(
      '#type' => 'hidden',
      '#value' => $metatag_id,
    );

    $form['allow_duplicates'] = array(
      '#type' => 'hidden',
      '#value' => TRUE,
    );

    $form['#prefix'] = '<div id="item_create_dialog" title="Edit Metatag">';
    $form['#suffix'] = '</div>';
    $form['#attributes'] = array('id' => 'edit_email');

    return $form;
  }
}

/**
 * Update the submitted email address.
 *
 * @param array $form
 * @param array $form_state
 */
function jarrow_metatags_edit_submit($form, &$form_state) {

  $links = jarrow_metatags_process_selected_links($form_state);
  jarrow_metatags_save_tag($form_state['values']['name_field'], $links, $form_state['values']['metatag_id']);
  drupal_goto(JARROW_METATAGS_PATH);
}

/**
 * Process the selected links into a readable array.
 *
 * @param $form_state
 *
 * @return array
 */
function jarrow_metatags_process_selected_links($form_state) {

  $links = array();
  if (count($form_state['values']['link_field']) > 0) {
    foreach ($form_state['values']['link_field'] as $link) {
      $links[$link] = $form_state['complete form']['link_field']['#options'][$link];
    }
  }
  return $links;
}

/**
 * Confirm that the user wants to delete the selected metatag.
 *
 * @param $form
 * @param $form_state
 * @param $metatag_id
 *
 * @return array
 */
function jarrow_metatags_delete($form, &$form_state, $metatag_id) {

  $metatag = jarrow_metatags_get_tag_with_id($metatag_id);

  // Add the necessary javascript to display this form in a modal dialog
  jarrow_item_add_dialog_script();

  $form = array(
    'text' => array(
      '#type' => 'markup',
      '#markup' => t("Are you sure you want to delete the metatag:<strong> {$metatag->name} </strong></p>"),
      '#attributes' => array('display' => 'block'),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('jarrow_metatags_delete_tag_confirmed'),
    ),
    'cancel' => array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#name' => 'jarrow_metatags_cancel_button',
      '#submit' => array('jarrow_item_cancel_redirect'),
      '#attributes' => array('id' => 'jarrow_cancel_add_item'),
      '#limit_validation_errors' => array(),
    ),
    'delete_metatag_id' => array(
      '#type' => 'hidden',
      '#value' => $metatag_id,
    ),
    'cancel_url' => array(
      '#type' => 'hidden',
      '#value' => JARROW_METATAGS_PATH,
    ),
    '#prefix' => '<div id="item_create_dialog" title="Delete Metatag">',
    '#suffix' => '</div>',
    '#attributes' => array('id' => 'delete_metatag'),
  );

  return $form;
}

/**
 * The user confirmed that they would like to delete the metatag.
 *
 * @param array $form
 * @param array $form_state
 */
function jarrow_metatags_delete_tag_confirmed($form, &$form_state) {

  $metatag_ids = array($form_state['values']['delete_metatag_id']);
  jarrow_metatags_delete_tags($metatag_ids);
}

/**
 * Delete the list of metatag_ids from the database. Ensure that if there
 * are any linked tags to the scheduled deleted tag that their link list
 * is updated.
 *
 * @param array $metatag_ids
 */
function jarrow_metatags_delete_tags($metatag_ids) {

  //Start a transaction in case any of the deletes fail
  $txn = db_transaction();

  $success = db_delete('etd_dublin_core_fields')
    ->condition('id', $metatag_ids, 'IN')
    ->execute();

  if (is_null($success)) {
    $txn->rollback();
    drupal_set_message(t("Could not delete metatag"), 'error');
  } else {
    jarrow_metatags_remove_links($metatag_ids);
    drupal_set_message(t("Successfully deleted metatag"));
  }

  //No matter what, go back to the email settings page.
  drupal_goto(JARROW_METATAGS_PATH);
}

/**
 * Delete from the linked tags any tag that in the list of
 * metatag ids
 *
 * @param $metatag_ids
 */
function jarrow_metatags_remove_links($metatag_ids) {

  $metatags = db_select('etd_dublin_core_fields', 'dc')
    ->fields('dc')
    ->isNotNull('linked_to')
    ->execute();

  foreach ($metatags as $tag) {
    $update = FALSE;
    $linked_ids = json_decode($tag->linked_to, TRUE);
    if (!empty($linked_ids)) {
      foreach ($metatag_ids as $id) {
        if (isset($linked_ids[$id])) {
          unset($linked_ids[$id]);
          $update = TRUE;
        }
      }
      if ($update) {
        jarrow_metatags_save_tag($tag->name, $linked_ids, $tag->id);
      }
    }
  }
}