<?php
/**
 * @file
 *  Handles the logic of handling the digital objects.
 *
 * @author
 *  Daniel Yule <dyule@unbc.ca> Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

// Define the needed constants
define('JARROW_JHOVE_PDF_CMD', $_SERVER['HTTP_JHOVE_HOME'].'/jhove -m PDF-hul ');
/*
define('JARROW_JHOVE_GIF_CMD', $_SERVER['HTTP_JHOVE_HOME'].'/jhove -m gif-hul ');
define('JARROW_JHOVE_JPG_CMD', $_SERVER['HTTP_JHOVE_HOME'].'/jhove -m jpeg-hul ');
define('JARROW_JHOVE_JPG2000_CMD', $_SERVER['HTTP_JHOVE_HOME'].'/jhove -m jpeg2000-hul ');
define('JARROW_JHOVE_TIFF_CMD', $_SERVER['HTTP_JHOVE_HOME'].'/jhove -m tiff-hul ');
*/

/**
 * Load the digital object definitions for the specified data model (by its id)
 *
 * @param int $data_model_id
 * @return mixed
 */
function jarrow_digital_object_load_definitions($data_model_id) {
  $digital_objects = &drupal_static(__FUNCTION__);
  if (!isset($digital_objects[$data_model_id])) {

    $digital_objects[$data_model_id] = array();

    $digital_object_types = module_invoke_all('etd_digital_object_types');
    $result = db_select('etd_digital_objects')
        ->fields('etd_digital_objects')
        ->condition('data_model', $data_model_id)
        ->orderBy('weight')
        ->execute();
    foreach ($result as $digital_object) {
      $digital_objects[$data_model_id][$digital_object->id] = $digital_object;
      $digital_objects[$data_model_id][$digital_object->id]->type_label = $digital_object_types[$digital_object->type];
    }
  }
  return $digital_objects[$data_model_id];
}

/**
 * Load the digital object instance as identified by its id.
 *
 * @param int $digital_object_instance_id
 * @return object
 */
function jarrow_digital_object_load_instance($digital_object_instance_id) {

  $query = db_select('etd_submission_digital_object_instances', 'doi');
  $query->leftJoin('etd_digital_objects', 'do', 'doi.digital_object=do.id');
  $query->fields('doi')
    ->fields('do', array('type'))
    ->condition('doi.id', $digital_object_instance_id);

  $digital_object_instance = $query->execute()->fetch();
  if (!$digital_object_instance) {
    drupal_set_message(t('Could not load digital object as it does not exist'), 'error');
  }
  return $digital_object_instance;
}

/**
 * Implements hook_etd_digital_object_types()
 *
 * Return an array of digital object types. This is a hook
 * that other modules can use to incorportate other digital
 * documents
 *
 * @return array
 */
function jarrow_etd_digital_object_types() {
  return array(
    'pdfa' => t('Archival Quality PDF'),
    'image' => t('Image'),
    'document' => t('Document'),
    'video' => t('Video'),
    'audio' => t('Audio'),
    'other' => t('Other'),
  );
}

/**
 * Create the configuration form for digital objects.
 *
 * @return array
 */

function jarrow_digital_object_configure() {
  $digital_object_types = module_invoke_all('etd_digital_object_types');

  return array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The name of the data object'),
    ),
    'description' => array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#description' => t('A description of this data object to be displayed to the user'),
    ),
    'type' => array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#description' => t('The type of file accepted by this digital object'),
      '#options' => $digital_object_types,
    ),
    'minimum' => array(
      '#type' => 'select',
      '#title' => t('Minimum'),
      '#description' => t('The minimum number of instances of this digital object that must be associated with this submission'),
      '#options' => array(
        0 => t('No Minimum'),
        1 => t('1'),
        2 => t('2'),
        3 => t('3'),
        4 => t('4'),
        5 => t('5'),
      ),
    ),
    'maximum' => array(
      '#type' => 'select',
      '#title' => t('Maximum'),
      '#description' => t('The maximum number of instances of this file that can be associated with this submission'),
      '#options' => array(
        0 => t('No Maximum'),
        1 => t('1'),
        2 => t('2'),
        3 => t('3'),
        4 => t('4'),
        5 => t('5'),
      ),
    ),
    'access' => array(
      '#type' => 'select',
      '#title' => t('Access'),
      '#description' => t('Select single for one copy of the file for all users or multiple for an individual copy for each user'),
      '#options' => array(
        0 => t('Single'),
        1 => t('Multiple'),
      ),
    ),
      //'config',
  );
}

/**
 * Return a form list of the available digital object types.
 *
 * return array
 */
function jarrow_digital_object_add_list() {
  $types = module_invoke_all('etd_digital_object_types');
  $elements = array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(),
  );

  foreach ($types as $type => $type_name) {
    $elements['#items'][] = array(
      'data' => '<div>' . $type_name . '</div>',
      'class' => array('jarrow_add_item'),
      'id' => 'itemadd-' . $type,
    );
  }

  return $elements;
}

/**
 * Create the element array for displaying the form.
 *
 * @param array $elements
 * @param int $parent_id
 * @param object $submission
 * @param object $digital_object_instance
 * @param boolean $readonly
 * @return array
 */
function _jarrow_get_data_elements_render_array($elements, $parent_id, $submission, $digital_object_instance, $readonly) {
  global $user;

  $element_array = array();
  foreach ($elements as $element) {
    if ($element->parent == $parent_id && $element->digital_object == $digital_object_instance->digital_object) {
      //@see get_form_object() in form_element_types.inc.
      if ($element->type == 'group') {
        $ret_array = _jarrow_get_data_elements_render_array($elements, $element->id, $submission, $digital_object_instance, $readonly);
        $ret_array['#type'] = 'fieldset';
      } else {
        $ret_array = array();
        $types = module_invoke_all('etd_data_element_types', $element->source);
        if (isset($types[$element->type])) {
          $sources = module_invoke_all('etd_data_element_sources');
          if (isset($sources[$element->source])) {
            $value = call_user_func($sources[$element->source]['get'], $element->source_config, $element->id, $user->uid, $submission, $digital_object_instance);
            if ($readonly) {
              $display_function = $types[$element->type]['view'];
            } else {
              $display_function = $types[$element->type]['edit'];
            }
            $ret_array = call_user_func($display_function, $value, $element->type_config);
          }
        }
      }
      $ret_array['#title'] = $element->name;
      $ret_array['#description'] = $element->description;
      if (!$readonly) {
        $ret_array['#title'] .= '<span class="form-required" title="This field is required.">*</span>';
       // $ret_array['#required'] = TRUE;
      }
      $element_array['obj_' . $element->id] = $ret_array;
    }
  }
  return $element_array;
}

/**
 * Load the permissions associated with the digital object.
 *
 * @param object $submission
 * @param array $roles
 * @param int $digital_object_id
 * @return object
 */
function jarrow_digital_object_load_permissions($submission, $roles, $digital_object_id) {

  global $user;
  // if the user can access all submissions then grant them all the
  // priviledges.
  if (jarrow_submission_can_access_all_submissions()) {
    $permissions = array(
      'can_view' => TRUE, //Give permission to view attached files
      'can_edit' => TRUE, //Give permission to delete attached files
      'can_create' => FALSE, //Don't provide permission to attached files as that is the submitter's responsibility
    );
    return (object) $permissions;
  }

  // determine the stage the user is viewing
  $current_stage = jarrow_submission_get_currently_viewed_stage($user, $submission->id);
  if ($current_stage == NULL) {
    $current_stage = $submission->current_stage;
  }

  // Get the permissions for the file
  $perm_query = db_select('etd_digital_object_permissions', 'perm');
  $perm_query->addExpression('max(can_view)', 'can_view');
  $perm_query->addExpression('max(can_edit)', 'can_edit');
  $perm_query->addExpression('max(can_create)', 'can_create');
  $permissions = $perm_query->condition('role', $roles)
      ->condition('process', $submission->process)
      ->condition('stage',  $current_stage)
      ->condition('digital_object', $digital_object_id)
      ->execute()
      ->fetchObject();
  return $permissions;
}

/**
 * Return the ajax results
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function jarrow_digital_object_ajax_callback($form, &$form_state) {
  return array();
}



/**
 * Based on the type of file being accepted, define what file extensions
 * are permissable.
 *
 * @param string $file_type
 * @return array
 */
function jarrow_digital_object_define_file_types($file_type) {

  $valid_file_types = array();
  switch ($file_type) {
    case 'pdfa':
      $valid_file_types['file_validate_extensions'] = array("pdf");
      break;
    case 'image':
      $valid_file_types['file_validate_extensions'] = array("png gif jpg jpeg");
      break;
    case 'document':
      $valid_file_types['file_validate_extensions'] = array("doc csv xls xlsx ppt pps docx odt");
      break;
    case 'audio':
      $valid_file_types['file_validate_extensions'] = array("wma wav aif ogg mp3");
      break;
    case 'video':
      $valid_file_types['file_validate_extensions'] = array("mov mpg swf wmv avi flv");
      break;
    case 'other':
      $valid_file_types['file_validate_extensions'] = array("zip gzip");
      break;
  }
  return $valid_file_types;
}


/**
 * Create the file upload form.
 *
 * @param array $form
 * @param array $form_state
 * @param int $submission_id
 * @param int $digital_object_instance_id
 * @return array
 */
function jarrow_digital_object_instance_edit($form, &$form_state, $submission_id, $digital_object_instance_id) {

  $path = drupal_get_path('module', 'jarrow');
  include_once $path . '/submission/submission.inc';

  $submission = _jarrow_load_submission($submission_id);
  $roles = jarrow_get_user_roles_for_submission($submission);

  $digital_object_instance = jarrow_digital_object_load_instance($digital_object_instance_id);

  if (!$digital_object_instance) {
    drupal_goto('jarrow/submission/' . $submission_id . '/view');
  }

  // Load the permissions for the digital object
  $permissions = jarrow_digital_object_load_permissions($submission, $roles, $digital_object_instance->digital_object);
  $editable = $permissions->can_edit || $permissions->can_create;
  if (!($editable || $permissions->can_view)) {
    drupal_set_message(t('You do not have permission to view this digital object'), 'error');
    return array();
  }

  drupal_add_js($path . '/workflow/digital_objects/digital_objects_view.js');
  drupal_add_css($path . '/submission/submission_view.css');
  drupal_add_library('system', 'ui.dialog');


  $form = array();
  $digitial_objects_metadata = _jarrow_get_data_elements_render_array($submission->data_model->elements, NULL, $submission, $digital_object_instance, !$editable);
  $digitial_objects_metadata['#type'] = 'fieldset';
  $digitial_objects_metadata['#title'] = t('Metadata');
  $digitial_objects_metadata['#collapsible'] = TRUE;
  $form['message_box_wrapper'] = array(
    '#type' => 'container',
  );

  $file_object = array(
    '#type' => 'managed_file',
    '#default_value' => isset($digital_object_instance->file_id) ? $digital_object_instance->file_id : 0,
    '#upload_location' => 'private://etd_temp',
    //'#required' => TRUE,
    '#title' => t('File').'<span class="form-required" title="This field is required.">*</span>',
    '#description' => t('Please select the supplemental file associated with this submission.'),
  );
  $valid_file_types = jarrow_digital_object_define_file_types($digital_object_instance->type);
  if (count($valid_file_types) > 0) {
     $file_object['#upload_validators'] = $valid_file_types;
  }

  $form['file_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upload File'),
    '#collapsible' => TRUE,
    '#weight' => -1,
    'file_object' => $file_object,
  );
  $form['digital_object_metadata'] = $digitial_objects_metadata;
  $form['save_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#validate' => array('jarrow_digital_object_form_validate'),
    '#submit' => array('jarrow_digital_object_instance_edit_submit'),
  );

  $form['cancel_button'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#name' => 'cancel',
    '#submit' => array('jarrow_digital_object_form_cancel'),
    '#limit_validation_errors' => array(),
  );

  if (isset($digital_object_instance->file_id)) {
    $form['delete_link'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('jarrow_digital_object_delete'),
   /* '#type' => 'markup',
    '#markup' => l('Delete Object', 'jarrow/submission/' . $submission->id . '/object/' . $digital_object_instance->id . '/delete'),*/
    );
  }
  $form_state['submission'] = $submission;
  $form_state['digital_object_instance'] = $digital_object_instance;

  return $form;
  //Check permissions!
}

/**
 * Ensure the the from is validated.
 * @param array $form
 * @param array $form_state.
 * @param boolean $set_form_error
 */
function jarrow_digital_object_form_validate($form, &$form_state, $set_form_error = TRUE) {

  foreach($form_state['values'] as $id => $value) {
    $prefix = substr($id, 0, 4);
    if ($prefix === 'obj_') {
      if (strlen(trim($value)) == 0) {
        if ($set_form_error) {
          $title = explode('<span', $form['digital_object_metadata'][$id]['#title']);
          form_set_error($id, $title[0].' is a required field.');
        }
      }
    } else if ($id === 'file_object') {
      if ($value === 0) {
        if ($set_form_error) {
          $title = explode('<span', $form['file_fieldset'][$id]['#title']);
          form_set_error($id, $title[0].' is a required field.');
        }
      }
    }
  }
}

/**
 * Cancel the creation of this digital object.
 * @param array $form
 * @param array $form_state.
 */
function jarrow_digital_object_form_cancel($form, &$form_state) {

  // check if the form is empty
  $digital_object_instance = $form_state['digital_object_instance'];
  $digital_object = db_select('etd_submission_digital_object_instances', 'doi')
    ->fields('doi', array('file_id'))
    ->condition('id', $digital_object_instance->id)
    ->condition('submission', $digital_object_instance->submission)
    ->condition('digital_object', $digital_object_instance->digital_object)
    ->execute()
    ->fetch();
  if ((is_object($digital_object)) && ($digital_object->file_id === NULL)) {
    jarrow_digital_object_remove_instance($digital_object_instance);
  }
  $submission = $form_state['submission'];
  drupal_goto('jarrow/submission/'.$submission->id.'/view');
}

/**
 * The user has submitted the form to create a new digital object.
 * Process the form to ensure that everything is as it should be.
 *
 * @param array $form
 * @param array $form_state.
 */
function jarrow_digital_object_instance_edit_submit($form, &$form_state) {

  global $user;

  $file_accepted = TRUE;
  $submission = $form_state['submission'];
  $digital_object_instance = $form_state['digital_object_instance'];
  $sources = module_invoke_all('etd_data_element_sources');

  foreach ($form_state['values'] as $name => $value) {
    $name_array = explode('_', $name, 2);
    if (count($name_array) > 1) {
      if ($name_array[0] === 'obj') {
        $element_id = $name_array[1];
        $data_element = $submission->data_model->elements[$element_id];
        if (isset($sources[$data_element->source]) && isset($sources[$data_element->source]['set'])) {
          $set_function = $sources[$data_element->source]['set'];
          call_user_func($set_function, $data_element->source_config, $element_id, $submission->id, $user->uid, $value, $digital_object_instance->id);
        }
      } else if ($name_array[0] === 'file' && $name_array[1] == 'object') {
        $result = jarrow_digital_object_process_file_submission($submission, $value, 'digital_object');

        if ($result['update'] !== NULL) {
          db_update('etd_submission_digital_object_instances')
              ->condition('id', $digital_object_instance->id)
              ->fields(array('file_id' => $result['update']))
              ->execute();
        }
        $file_accepted = $result['file_accepted'];
      }
    }
  }
  if ($file_accepted) {
    $current_stage = jarrow_submission_get_currently_viewed_stage($user, $submission->id);
    $url_path = ($current_stage == NULL) ? '/view' : '/view/'.$current_stage;
    drupal_goto('jarrow/submission/'.$submission->id.$url_path);
  }
}

/**
 * Either save or delete the associated file based on the users submission.
 *
 * @param object $submission
 * @param int $file_id
 * @param string $type the name of the object that uses this file. In this case it is either
 * 'submission' or 'digital_object'
 * @return string
 */
function jarrow_digital_object_process_file_submission($submission, $file_id, $type) {

  $update = NULL;
  $file_accepted = TRUE;
  if (($file_id == 0) && isset($submission->file)) {
    // If file_id = 0 and a file has already been saved to the submission,
    // then delete the file.
      $file = file_load($submission->file);
      file_delete($file);
      $update = array('file' => NULL);
  } else if ($file_id > 0) {
    // Ensure the submitted file is acceptable
    if (jarrow_digital_object_is_file_acceptable($file_id)) {
      // If the file_id does not equal the existing file id already associated
      // with the submission then delete that file and then add the new file.
      if (isset($submission->file) && ($submission->file != $file_id)) {
        $old_file = file_load($submission->file);
        file_usage_delete($old_file, 'jarrow', $type, $submission->id, 0);
        file_delete($old_file);
      }
      $file = file_load($file_id);
      if ($file->status != FILE_STATUS_PERMANENT) {
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        file_usage_add($file, 'jarrow', $type, $submission->id);
        $update = array('file' => $file_id);
      }
    } else {
      $file_accepted = FALSE;
    }
  }
  return array('file_accepted' => $file_accepted, 'update' => $update);
}

/**
 * Determine if the uploaded file is well formed and of the desired type.
 *
 * @param $file_id
 * @return boolean
 */
function jarrow_digital_object_is_file_acceptable($file_id) {

  $acceptable = TRUE;

  $file_info = db_select('file_managed', 'fm')
  ->fields('fm', array('uri','filemime'))
  ->condition('fid', $file_id)
  ->execute()
  ->fetch();

  if (strpos(strtolower($file_info->filemime), 'pdf') !== FALSE) {
    if ($filepath = jarrow_digital_object_realpath($file_info->uri)) {
      $output = array();
      $return_result = 0;
      exec(JARROW_JHOVE_PDF_CMD.$filepath, $output, $return_result);
      if ($return_result == 0) {
        $acceptable = ((strpos(strtolower($output[12]), 'pdf/a') !== FALSE) && (strpos(strtolower($output[8]), 'status: well-formed') !== FALSE));
      } else {
        $acceptable = FALSE;
      }
    } else {
      $acceptable = FALSE;
    }
    if (! $acceptable) {
      drupal_set_message(t('The file you uploaded is not in PDF/A format. Please upload your submission again in PDF/A format.'), 'error');
    }
  }
  return $acceptable;
}

/**
 * Copied from Drupal core as drupal_realpath is deprecated. This function is
 * still useful for our purposes.
 *
 * @param string $uri
 * @return mixed
 */
function jarrow_digital_object_realpath($uri) {

  // If this URI is a stream, pass it off to the appropriate stream wrapper.
  // Otherwise, attempt PHP's realpath. This allows use of drupal_realpath even
  // for unmanaged files outside of the stream wrapper interface.
  if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri)) {
    return $wrapper->realpath();
  }
  // Check that the URI has a value. There is a bug in PHP 5.2 on *BSD systems
  // that makes realpath not return FALSE as expected when passing an empty
  // variable.
  elseif (!empty($uri)) {
    return realpath($uri);
  }
  return FALSE;
}

/**
 * Create a new entry in the etd_submission_digital_object_instances table for the newly
 * created object.
 *
 * @param array $form
 * @param array $form_state
 * @param int $submission_id
 * @param int $digital_object_id
 */
function jarrow_digital_object_instance_add($form, &$form_state, $submission_id, $digital_object_id) {
  $new_id = db_insert('etd_submission_digital_object_instances')
      ->fields(array(
        'submission' => $submission_id,
        'digital_object' => $digital_object_id))
      ->execute();

  if ($new_id !== NULL) {
    drupal_set_message(t('Digital object created'));
    drupal_goto('jarrow/submission/' . $submission_id . '/object/' . $new_id);
  } else {
    drupal_set_message(t('Could not create digital object'), 'error');
    drupal_goto('jarrow/submission/' . $submission_id);
  }
}

/**
 * Create the digital object block that appears in the sidebar.
 *
 * @param int $submission_id
 * @return array
 */
function jarrow_digital_object_block($submission_id) {

  $submission = _jarrow_load_submission($submission_id);
  if (!$submission) {
    return array();
  }
  $roles = jarrow_get_user_roles_for_submission($submission);
  $permission_query = db_select('etd_digital_object_permissions', 'perm');
  $permission_query->addExpression('max(can_view)', 'can_view');
  $permission_query->addExpression('max(can_edit)', 'can_edit');
  $permission_query->addExpression('max(can_create)', 'can_create');
  $main_permissions = $permission_query
      ->condition('role', $roles)
      ->condition('process', $submission->process)
      ->condition('stage', $submission->current_stage)
      ->condition('digital_object', 0)
      ->execute()
      ->fetchObject();
  if ($main_permissions->can_view || $main_permissions->can_edit || $main_permissions->can_create) {
    if ($submission->file) {
      $file_obj = file_load($submission->file);
      if($file_obj) {
          $file_obj->uri = 'jarrow/submission/' . $submission_id . '/view';
          $file_array = array(
            '#theme' => 'file_link',
            '#file' => $file_obj,
          );
          $submission_file = array('data' =>
            drupal_render($file_array),
          );
      }
    }
    else {
      $submission_file = array(
        'data' => l(t('<No File Yet>'), 'jarrow/submission/' . $submission_id . '/view'),
      );
    }
  }
  else {
    $submission_file = array(
      'data' => l(t('<Not Shown>'), 'jarrow/submission/' . $submission_id . '/view'),
    );
  }

  $digital_objects = array();
  $object_query = db_select('etd_digital_objects', 'obj');
  $object_query->innerJoin('etd_digital_object_permissions', 'perm', 'obj.id=perm.digital_object');
  $object_result = $object_query->fields('obj')
      ->condition('data_model', $submission->data_model->id)
      ->condition('role', $roles)
      ->condition('process', $submission->process)
      ->condition('stage', $submission->current_stage)
      ->condition(db_or()->condition('can_view', 1)->condition('can_edit', 1)->condition('can_create', 1))
      ->orderBy('weight')
      ->execute();

  foreach ($object_result as $digital_object) {
    $digital_object->instances = array();
    $digital_objects[$digital_object->id] = $digital_object;
  }


  if ($digital_objects) {
    $object_instance_result = db_select('etd_submission_digital_object_instances', 'inst')
        ->fields('inst')
        ->condition('submission', $submission_id)
        ->condition('digital_object', array_keys($digital_objects))
        ->execute();

    foreach ($object_instance_result as $object_instance) {
      $digital_objects[$object_instance->digital_object]->instances[] = $object_instance;
    }

    $supplementary_objects = array();
    foreach ($digital_objects as $digital_object) {
      $this_object = array(
        'data' => $digital_object->name,
        'children' => array(),
      );
      foreach ($digital_object->instances as $instance) {
        if (isset($instance->file_id) && $instance->file_id) {
          $this_file = file_load($instance->file_id);
          $this_file->uri = 'jarrow/submission/' . $submission_id . '/object/' . $instance->id;
          $file_array = array(
            '#theme' => 'file_link',
            '#file' => $this_file,
          );
          $this_object['children'][] = array('data' =>
            drupal_render($file_array),
          );
        }
        else {
          $this_object['children'][] = array(
            'data' => l(t('<No File Yet>'), 'jarrow/submission/' . $submission_id . '/object/' . $instance->id),
          );
        }
      }
      if ($digital_object->maximum == 0 || count($digital_object->instances) < $digital_object->maximum) {
        $this_object['children'][] = array(
          'data' => l(t('Add New'), 'jarrow/submission/' . $submission_id . '/object/add/' . $digital_object->id),
          'class' => array('new_digital_object', 'ui-button', 'button'),
        );
      }
      $supplementary_objects[$digital_object->id] = $this_object;
    }
  }
  else {
    $supplementary_objects = array();
  }

  return array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#items' => array(
      'submission_object' => array(
        'data' => t('Submission File'),
        'children' => array(
          $submission_file,
        ),
      ),
      'supplementary_files' => array(
        'data' => t('Supplementary Files'),
        'children' => $supplementary_objects,
      )
    ),
  );
}

/**
 * The user has clicked the delete button so remove the digital object.
 *
 * @param array $form
 * @param array $form_state
 */
function jarrow_digital_object_delete($form, &$form_state) {

  $digital_object_instance = $form_state['digital_object_instance'];
  if (jarrow_digital_object_remove_from_database($digital_object_instance)) {
    drupal_goto('jarrow/submission/'.$digital_object_instance->submission.'/view');
  }
}

/**
 * Remove all references of the sepecified digital object from the database.
 *
 * @param $digital_object_instance
 * @return boolean
 */
function jarrow_digital_object_remove_from_database($digital_object_instance) {

  $transaction = db_transaction();
  try{
    db_delete('etd_submission_digital_object_instances')
        ->condition('id', $digital_object_instance->id)
        ->execute();
    db_delete('etd_submission_field')
        ->condition('digital_object_instance', $digital_object_instance->id)
        ->condition('submission', $digital_object_instance->submission)
        ->execute();
    $file = file_load($digital_object_instance->file_id);
    file_delete($file, TRUE);
    return TRUE;
  } catch (Exception $error) {
    $transaction->rollback();
    $filename = db_select('file_managed', 'fm')
        ->fields('fm', array('filename'))
        ->condition('fid', $digital_object_instance->file_id)
        ->execute()
        ->fetch();
    watchdog_exception('Supplementary file deletion error. Could not remove file: '.$filename, $error);
    drupal_set_message('Failed to remove the file: '.$filename, 'error');
    return FALSE;
  }
}

/**
 * Remove the digital object instance from the etd_submission_field amd
 * etd_submission_digital_object_instances table.
 *
 * @param object $digital_object_instance
 * @return boolean
 */
function jarrow_digital_object_remove_instance($digital_object_instance) {

  $transaction = db_transaction();
  try {
    db_delete('etd_submission_digital_object_instances')
      ->condition('id', $digital_object_instance->id)
      ->execute();
    db_delete('etd_submission_field')
      ->condition('digital_object_instance', $digital_object_instance->id)
      ->condition('submission', $digital_object_instance->submission)
      ->execute();
    return TRUE;
  } catch (Exception $error) {
    $transaction->rollback();
    watchdog_exception('Failed to delete digital object instance with id: '.$digital_object_instance->id.' for submission id: '. $digital_object_instance->submission, $error);
    drupal_set_message('Failed to remove digital object.', 'error');
    return FALSE;
  }
}


/**
 * Given a file id, determine the digital objects instance id and
 * return it. Assign -1 to the $digital_object_instance variable if
 * item is not found.
 *
 * @param int $file_id
 * @return mixed
 */
function jarrow_digital_object_get_instance_id($file_id) {

  $file_info = db_select('file_usage', 'fu')
    ->fields('fu', array('fid', 'module', 'type','id'))
    ->condition('fid', $file_id)
    ->execute()
    ->fetch();

  if (($file_info->module === 'jarrow') && ($file_info->type === 'submission')) {
    // This is the main submission so its id is 0
    $digital_object_instance = 0;
  } else if ($file_info->type === 'digital_object') {
    // This is a supplemental file so we need to load the required information
    $result = db_select('etd_submission_digital_object_instances', 'doi')
        ->fields('doi', array('digital_object'))
        ->condition('file_id', $file_info->fid)
        ->execute()
        ->fetch();
    if ($result->digital_object) {
      $digital_object_instance = $result->digital_object;
    } else {
      drupal_set_message(t('Could not load digital object.'), 'error');
      $digital_object_instance = -1;
    }
  } else {
    drupal_set_message(t('Unknown digital object type: ').$file_info->type, 'error');
    $digital_object_instance = -1;
  }
  return array('id' => $digital_object_instance, 'file_info' => $file_info);
}

/**
 * Return the file info associated with the given file id.
 *
 * @param int $file_id
 *    assumens the file id  is for a managed file.
 *
 * @return mixed
 */
function jarrow_digital_object_get_file_info($file_id) {

  $file_info = db_select('file_managed', 'fm')
    ->fields('fm')
    ->condition('fid', $file_id)
    ->execute()
    ->fetch();
  return $file_info;
}
