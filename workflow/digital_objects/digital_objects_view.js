jQuery(document).ready(function($) {
  $('input,select,textarea').change(function() {
    $('#edit-save-button').mousedown();
  });
  Drupal.behaviors.jarrowObjectSave = {
    attach: function (context) {
      $('#edit-save-button').mousedown();
    }
  };
});