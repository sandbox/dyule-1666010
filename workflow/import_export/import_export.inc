<?php

/**
 * @file
 *  Provides functions necessary to import and export a process.
 *
 * @author
 *  Peter Hvezda <peter.hvezda@unbc.ca>
 *
 * @copyright
 *
 *  Copyright 2012 Geoffrey R Weller Library, University of Northern British Columbia
 *
 *  This file is part of Jarrow ETD Module.
 *
 *  Jarrow is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jarrow is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jarrow.  It can be found in the file COPYRIGHT.txt in
 *  the root directory.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Create the import form so that the user can supply a name for the new
 * process along with the configuration file to use.
 *
 * @param $form
 * @param $form_state
 * @param array $item_properties
 *
 * @return array
 */
function jarrow_create_import_form($form, &$form_state, $item_properties) {

  $import_form = array(
    'name_field' => array(
      '#type' => 'textfield',
      '#title' => 'Name',
      '#description' => t('Enter a new name for the imported ' . $item_properties['item_name'] . ':'),
      '#default_value' => '',
      '#maxlength' => 50,
      '#size' => 50,
    ),
    'file_selector' => array(
      '#type' => 'managed_file',
      '#default_value' => '',
      '#upload_location' => 'temporary://',
      '#title' => t('File') . '<span class="form-required" title="This field is required.">*</span>',
      '#description' => t('Please select the configuration file.'),
      '#upload_validators' => array('file_validate_extensions' => array('xml')),
      '#size' => 40,
    ),
    'import_button' => array(
      '#type' => 'submit',
      '#value' => t('Import'),
      '#validate' => array('jarrow_process_import_form_validate'),
      '#submit' => array('jarrow_process_import_form_submit'),
    ),
    'cancel_button' => array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#name' => 'cancel',
      '#submit' => array('jarrow_process_import_form_cancel'),
      '#limit_validation_errors' => array(),
    ),
  );

  $form_state['process_properties'] = $item_properties;
  return $import_form;
}

/**
 * Validate that the user has submitted the required values.
 *
 * @param $form
 * @param $form_state
 * @param bool $set_form_error
 */
function jarrow_process_import_form_validate($form, &$form_state, $set_form_error = TRUE) {

  if (strlen($form_state['values']['name_field']) == 0) {
    if ($set_form_error) {
      $title = $form['name_field']['#title'];
      form_set_error('name_field', $title . ' is a required field.');
    }
  }
  if ($form_state['values']['file_selector'] === 0) {
    if ($set_form_error) {
      $title = explode('<span', $form['file_selector']['#title']);
      form_set_error('file_selector', $title[0] . ' is a required field.');
    }
  }
}

/**
 * If the user has pressed the cancel button then redirect them back to the
 * process manager screen.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_process_import_form_cancel($form, &$form_state) {

  drupal_goto(JARROW_WORKFLOW_PATH);
}

/**
 * Import the process.
 *
 * @param $form
 * @param $form_state
 */
function jarrow_process_import_form_submit($form, &$form_state) {

  module_load_include('inc', 'jarrow', 'workflow/digital_objects/digital_objects');

  $process_name = $form_state['values']['name_field'];
  $file_id = $form_state['values']['file_selector'];
  $file_info = jarrow_digital_object_get_file_info($file_id);
  $filepath = jarrow_digital_object_realpath($file_info->uri);
  $result = jarrow_import_process($process_name, $filepath);
  if ($result) {
    drupal_set_message(t('Process successfully imported'), 'status');
  } else {
    drupal_set_message(t('Process failed to be imported'), 'error');
  }
}


/**
 * Exports a process, associated data model and forms as xml
 *
 * @param int
 *  $process_id The process to export
 */
function jarrow_export_process($process_id) {

  $path = drupal_get_path('module', 'jarrow');
  include_once $path . '/workflow/process_manager/process_manager.inc';
  include_once $path . '/workflow/data_model/data_model.inc';
  include_once $path . '/workflow/form_editor/form_editor.inc';
  //Create the XML Document we will be adding things to.
  $xmlDoc = DOMImplementation::createDocument('http://code.library.unbc.ca/jarrow/workflow', 'workflow');

  $xmlDoc->documentElement->setAttributeNS('http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', 'http://code.library.unbc.ca/jarrow/workflow etdworkflow.xsd');

  $xmlDoc->formatOutput = TRUE;
  //Load the necessary components for saving.
  $process = jarrow_load_process($process_id, FALSE);
  $data_model = jarrow_load_data_model($process->data_model, FALSE);
  $form_ids = db_select('etd_process_form_instances', 'forms')
    ->fields('forms', array('form'))
    ->distinct()
    ->condition('process', $process->id)
    ->execute()->fetchCol('form');
  $forms = array();
  foreach ($form_ids as $form_id) {
    $forms[$form_id] = jarrow_load_form($form_id, FALSE);
  }

  //Load the auxillary data we'll need to fill this out.
  $dublin_core_result = db_select('etd_dublin_core_fields', 'dub')
    ->fields('dub')
    ->execute();

  $dublin_core_fields = array(0 => 'None');
  foreach ($dublin_core_result as $dc_field) {
    $dublin_core_fields[$dc_field->id] = $dc_field->name;
  }

  //Generate the data model xml
  $data_model_element = $xmlDoc->createElement('datamodel');
  $data_model_element->setAttribute('name', $data_model->name);
  //Create a DOMElement to store all of the data elements in
  $data_model_elements_element = $xmlDoc->createElement('elements');
  $data_model_element->appendChild($data_model_elements_element);

  //Iterate through all the data elements and serialize them as xml
  foreach ($data_model->elements as $data_element) {
    //Create the DOMElement corresponding to this data element.
    $element_element = $xmlDoc->createElement('dataelement');

    //Set its attributes
    $element_element->setAttribute('name', $data_element->name);
    $element_element->setAttribute('identifier', $data_element->id);
    $element_element->setAttribute(('dc_mapping'), $dublin_core_fields[$data_element->dc_mapping]);
    $element_element->setAttribute('weight', $data_element->weight);

    //Description is serialized as an element instead of an attribute
    $description_element = $xmlDoc->createElement('description', $data_element->description);
    $element_element->appendChild($description_element);

    //Serialize the type and type configuration of the data element
    $type_element = $xmlDoc->createElement('type');
    $type_element->setAttribute('type', $data_element->type);
    if (isset($data_element->type_config)) {
      jarrow_object_to_xml($data_element->type_config, $type_element, $xmlDoc);
    }
    $element_element->appendChild($type_element);

    //Serialize the source and source configuration of the data element
    $source_element = $xmlDoc->createElement('source');
    $source_element->setAttribute('source', $data_element->source);
    if (isset($data_element->source_config)) {
      jarrow_object_to_xml($data_element->source_config, $source_element, $xmlDoc);
    }
    $element_element->appendChild($source_element);

    $data_model_elements_element->appendChild($element_element);
  }

  $xmlDoc->documentElement->appendChild($data_model_element);

  //Create a DOMElement to store all of the forms in
  $forms_element = $xmlDoc->createElement('forms');
  foreach ($forms as $form) {
    //Generate the form xml
    $form_dom_element = $xmlDoc->createElement('form');
    $form_dom_element->setAttribute('name', $form->name);
    //Create a DOMElement to store all of the data elements in
    $form_elements_element = $xmlDoc->createElement('elements');
    $form_dom_element->appendChild($form_elements_element);

    //Iterate through all the data elements and serialize them as xml
    foreach ($form->elements as $form_element) {
      //Create the DOMElement corresponding to this data element.
      $element_element = $xmlDoc->createElement('formelement');

      //Set its attributes
      $element_element->setAttribute('name', $form_element->name);
      $element_element->setAttribute('identifier', $form_element->id);
      $element_element->setAttribute('weight', $form_element->weight);

      //Description is serialized as an element instead of an attribute
      $description_element = $xmlDoc->createElement('description', $form_element->description);
      $element_element->appendChild($description_element);

      //Serialize the type and type configuration of the data element
      $type_element = $xmlDoc->createElement('type');
      $type_element->setAttribute('type', $form_element->type);
      if (isset($form_element->type_config)) {
        jarrow_object_to_xml($form_element->type_config, $type_element, $xmlDoc);
      }
      $element_element->appendChild($type_element);


      $form_elements_element->appendChild($element_element);
    }

    $forms_element->appendChild($form_dom_element);
  }

  $xmlDoc->documentElement->appendChild($forms_element);

  //Create the stages element
  $roles = jarrow_get_roles($data_model->id);
  $stages_element = $xmlDoc->createElement('stages');
  foreach ($process->stages as $stage) {
    $stage_element = $xmlDoc->createElement('stage');
    $stage_element->setAttribute('identifier', $stage->id);
    $stage_element->setAttribute('name', $stage->name);
    foreach ($stage->forms as $form_instance) {
      $form_instance_element = $xmlDoc->createElement('formInstance');
      $form_instance_element->setAttribute('identifier', $form_instance->id);
      $form_instance_element->setAttribute('associatedForm', $form_instance->form);
      $form_instance_element->setAttribute('acceptanceStage', isset($form_instance->acceptance_stage) ? $form_instance->acceptance_stage : 0);
      $form_instance_element->setAttribute('rejectionStage', isset($form_instance->rejection_stage) ? $form_instance->rejection_stage : 0);

      $roles_element = $xmlDoc->createElement('roles');
      foreach ($form_instance->roles as $role => $permissions) {
        $role_element = $xmlDoc->createElement('role');
        $role_text = $roles[$role];
        $role_element->setAttribute('name', $role_text);
        $role_element->setAttribute('canView', jarrow_output_as_boolean($permissions->can_view));
        $role_element->setAttribute('canCreate', jarrow_output_as_boolean($permissions->can_create));
        $role_element->setAttribute('canEdit', jarrow_output_as_boolean($permissions->can_edit));
        $role_element->setAttribute('canOneApprove', jarrow_output_as_boolean($permissions->can_one_approve));
        $role_element->setAttribute('canAllApprove', jarrow_output_as_boolean($permissions->can_all_approve));

        $roles_element->appendChild($role_element);
      }
      $form_instance_element->appendChild($roles_element);
      $stage_element->appendChild($form_instance_element);
    }
    $stages_element->appendChild($stage_element);
  }

  $xmlDoc->documentElement->appendChild($stages_element);


  //Indicate to the browser that this is a downloadable xml file.
  drupal_add_http_header('Content-Type', 'text/xml');
  $filename = 'jarrow_'
    . str_replace(array('-', ' ', ';'), '_', $process->name)
    . '_export.xml';
  drupal_add_http_header('Content-Disposition', "attachment; filename=$filename");
  echo($xmlDoc->saveXML());
  drupal_exit();
}

/**
 * Converts the object or array passed in to an xml element suitable for
 * serialization
 *
 * @param object|array $object
 *  The object to serialize
 * @param DOMElement $element
 *  The element in which to serialze the object, passed by reference
 * @param DOMDocument $xmlDoc
 *  The document to use for creating elements.
 */
function jarrow_object_to_xml($object, &$element, $xmlDoc) {

  if (is_array($object) || is_object($object)) {
    foreach ((array) $object as $index => $value) {
      if (is_array($value) || is_object($value)) {
        $sub_element = $xmlDoc->createElement($index);
        jarrow_object_to_xml($value, $sub_element, $xmlDoc);
      } else {
        $sub_element = $xmlDoc->createElement($index, $value);
      }
      $element->appendChild($sub_element);
    }
  }
}

function jarrow_output_as_boolean($intval) {

  if ($intval) {
    return 'true';
  } else {
    return 'false';
  }
}

function jarrow_check_item_list($item_list) {

  if (count($item_list['#items']) > 0) {
    return $item_list;
  }
  return array();
}

/**
 * Import the process configuration xml file
 *
 * @param string $process_name
 * @param string $filepath
 */
function jarrow_import_process($process_name, $filepath) {

  //$xmlDoc = new DOMDocument();
  //$xmlDoc->load($filepath);
  $xml = simplexml_load_file($filepath);

  // Import the the data model
  jarrow_import_data_model($xml);

  // Import the the forms model
  jarrow_import_forms_model($xml);

  // Import the the process stages
  jarrow_import_process_stages($xml);

}

/**
 * Parse the data model components out of the xml file.
 *
 * @param $xml
 */
function jarrow_import_data_model($xml) {

  // Need to check if the data model name already exists.
  $data_model = array('name' => (string) $xml->datamodel['name']);

  $data_elements = array();

  foreach ($xml->datamodel->elements->children() as $datamodel_element) {
    $data_element = array(
      'name' => (string) $datamodel_element['name'],
      'dc_mapping' => (string) $datamodel_element['dc_mapping'],
      'weight' => (string) $datamodel_element['weight'],
    );

    foreach ($datamodel_element->children() as $child) {
      $data_element = jarrow_import_xml_values($child, $data_element);
    }
    $data_elements[] = $data_element;
  }
  dsm($data_elements);
  $data_model['elements'] = $data_elements;
  //jarrow_save_data_elements((object) $data_model);
}

/**
 * Parse the form model components out of the xml file.
 *
 * @param $xml
 */
function jarrow_import_forms_model($xml) {

  // Need to check if the forms model name already exists.
  $forms_model = array('name' => (string) $xml->forms['name']);

  $forms = array();

  foreach ($xml->forms->children() as $forms_model_element) {
    $name = (string) $forms_model_element['name'];
    $form = array();
    foreach ($forms_model_element->elements->children() as $child) {
      $temp = array();
      $result = jarrow_import_xml_values($child, $temp);
      $key = (string) $child['name'];
      $form[$key] = $result;
    }
    $forms[$name] = $form;
  }
  dsm($forms);
  //$forms_model['elements'] = $forms;
  //jarrow_save_form_elements((object) $forms);
}

/**
 * Parse the stages model components out of the xml file.
 *
 * @param $xml
 */
function jarrow_import_process_stages($xml) {

  // Need to check if the forms model name already exists.
  $stages_model = array('name' => (string) $xml->forms['name']);

  $stages = array();

  foreach ($xml->stages->children() as $child) {
    $temp = array();
    $result = jarrow_import_xml_values($child, $temp);
    $key = (string) $child['name'];
    $stages[$key] = $result;
  }
  dsm($stages);
  $stages_model['elements'] = $stages;
  //jarrow_save_form_elements((object) $form);
}


function jarrow_import_xml_values($xml, &$property) {

  $children = (array) $xml->children();
  $attributes = (array) $xml->attributes();
  if (count($children) > 0) {
    $data_element = array();
    if (count($attributes > 0)) {
      foreach ($xml->attributes() as $name => $value) {
        $data_element[$name] = (string) $value;
      }
    }
    foreach ($xml->children() as $child) {
      $data_element = jarrow_import_xml_values($child, $data_element);
    }
    $property[$xml->getName()] = $data_element;
  } else {
    if (count($attributes) > 0) {
      $data_element = array();
      foreach ($xml->attributes() as $name => $value) {
        $data_element[$name] = (string) $value;
      }
      //$property[$xml->getName()] = $data_element;
      if (isset($data_element['name'])) {
        $property[$data_element['name']] = $data_element;
      } else {
        $property[] = $data_element;
      }
    } else {
      $property[$xml->getName()] = (string) $xml;
    }
  }
  return $property;
}
